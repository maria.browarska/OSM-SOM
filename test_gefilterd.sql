INSERT INTO "untitled_name" ("Id", "Name", "Descript", "LONG", "LAT", "aerodrome_", "ele", "iata", "icao", "name_ru", "x", "y", "id", "ident", "type", "name", "latitude_deg", "longitude_deg", "elevation_ft", "continent", "iso_country", "iso_region", "municipality", "scheduled_service", "gps_code", "iata_code", "local_code", "home_link", "wikipedia_link", "keywords") VALUES
('0', 'Aéroport International Félix Houphouët-Boigny', 'aerodrome=international
aerodrome:type=military/public
aerow', '3° 55'' 31.809"', '5° 15'' 55.221"', 'military/public', '6', 'ABJ', 'DIAP', ' ', '-3.9255026', '5.2653392', '2096', 'DIAP', 'medium_airport', 'Félix-Houphouët-Boigny International Airport', '5.26139', '-3.92629', '21', 'AF', 'CI', 'CI-01', 'Abidjan', 'yes', 'DIAP', 'ABJ', '', '', 'https://en.wikipedia.org/wiki/F%C3%A9lix-Houphou%C3%ABt-Boigny_International_Airport', 'Abidjan, Port Bouët'),
('0', 'Albury Airport', 'aeroway=aerodrome
ele=164 m
iata=ABX
icao=YMAY
name=Albury A', '146° 57'' 37.34', '36° 4'' 8.944"', ' ', '0', 'ABX', 'YMAY', ' ', '146.9603738', '-36.0691512', '27044', 'YMAY', 'medium_airport', 'Albury Airport', '-36.067798614502', '146.957992553711', '539', 'OC', 'AU', 'AU-NSW', 'Albury', 'yes', 'YMAY', 'ABX', '', '', 'https://en.wikipedia.org/wiki/Albury_Airport', ''),
('0', 'Aeropuerto Internacional de Acapulco "General Juan N. Álvar', 'aerodrome:type=public
aeroway=aerodrome
ele=4
iata=ACA
icao=', '99° 45'' 16.556', '16° 45'' 35.788', 'public', '4', 'ACA', 'MMAA', ' ', '-99.7545988', '16.7599412', '4688', 'MMAA', 'large_airport', 'General Juan N Alvarez International Airport', '16.7570991516113', '-99.7539978027344', '16', 'NA', 'MX', 'MX-GRO', 'Acapulco', 'yes', 'MMAA', 'ACA', '', '', 'https://en.wikipedia.org/wiki/General_Juan_N._%C3%81lvarez_International_Airport', ''),
('0', 'Kotoka International Airport', 'aerodrome=international
aerodrome:type=military/public
aerow', '0° 10'' 3.795"', '5° 36'' 13.870"', 'military/public', '62', 'ACC', 'DGAA', ' ', '-0.1677208', '5.6038527', '2090', 'DGAA', 'large_airport', 'Kotoka International Airport', '5.60518980026245', '-0.166786000132561', '205', 'AF', 'GH', 'GH-AA', 'Accra', 'yes', 'DGAA', 'ACC', '', '', 'https://en.wikipedia.org/wiki/Kotoka_International_Airport', ''),
('0', 'California Redwood Coast - Humboldt County Airport', 'ALAND=2871993
AWATER=739
Tiger:MTFCC=K2457
addr:state=CA
aer', '124° 6'' 30.441', '40° 58'' 34.864', ' ', '65', 'ACV', 'KACV', ' ', '-124.1084557', '40.9763512', '3362', 'KACV', 'large_airport', 'California Redwood Coast-Humboldt County International Airport', '40.978101', '-124.109', '221', 'NA', 'US', 'US-CA', 'Arcata/Eureka', 'yes', 'KACV', 'ACV', 'ACV', '', 'https://en.wikipedia.org/wiki/Arcata-Eureka_Airport', 'Arcata Airport'),
('0', 'Adelaide Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '138° 31'' 51.22', '34° 56'' 50.856', 'public', '6', 'ADL', 'YPAD', ' ', '138.5308948', '-34.94746', '27096', 'YPAD', 'large_airport', 'Adelaide International Airport', '-34.945', '138.531006', '20', 'OC', 'AU', 'AU-SA', 'Adelaide', 'yes', 'YPAD', 'ADL', '', '', 'https://en.wikipedia.org/wiki/Adelaide_International_Airport', ''),
('0', 'Aeropuerto Internacional Gustavo Rojas Pinilla', 'aerodrome:type=civil
aeroway=aerodrome
closest_town=San Andr', '81° 42'' 40.460', '12° 35'' 1.091"', 'civil', '27', 'ADZ', 'SKSP', ' ', '-81.711239', '12.5836365', '6163', 'SKSP', 'medium_airport', 'Gustavo Rojas Pinilla International Airport', '12.5836', '-81.7112', '19', 'SA', 'CO', 'CO-SAP', 'San Andrés', 'yes', 'SKSP', 'ADZ', 'ADZ', '', 'https://en.wikipedia.org/wiki/Gustavo_Rojas_Pinilla_International_Airport', ''),
('0', '百色右江机场', 'aeroway=aerodrome
alt_name:en=Bose Youjiang Airport
iata=AEB', '106° 57'' 38.79', '23° 43'' 10.327', ' ', '0', 'AEB', 'ZGBS', ' ', '106.9607755', '23.7195354', '44160', 'CN-0082', 'medium_airport', 'Baise Youjiang Airport', '23.7206', '106.959999', '490', 'AS', 'CN', 'CN-45', 'Baise (Tianyang)', 'yes', 'ZGBS', 'AEB', '', '', 'https://en.wikipedia.org/wiki/Baise_Youjiang_Airport', 'Tian Yang Air Base, Bose airport, 百色右江机场'),
('0', 'Aeroparque Jorge Newbery', 'aerodrome=public
aerodrome:type=military/public
aeroway=aero', '58° 25'' 2.888"', '34° 33'' 32.867', 'military/public', '5', 'AEP', 'SABE', 'Аэропорт Хорхе Ньюбери', '-58.4174689', '-34.5591297', '5771', 'SABE', 'medium_airport', 'Jorge Newbery Airpark', '-34.5592', '-58.4156', '18', 'SA', 'AR', 'AR-C', 'Buenos Aires', 'yes', 'SABE', 'AEP', 'AER', '', 'https://en.wikipedia.org/wiki/Aeroparque_Jorge_Newbery', ''),
('0', 'Aeropuerto Internacional Guaraní', 'aeroway=aerodrome
barrier=fence
ele=258
iata=AGT
icao=SGES
i', '54° 50'' 33.513', '25° 27'' 20.365', ' ', '258', 'AGT', 'SGES', ' ', '-54.8426426', '-25.455657', '6091', 'SGES', 'medium_airport', 'Guarani International Airport', '-25.454516', '-54.842682', '846', 'SA', 'PY', 'PY-10', 'Ciudad del Este', 'yes', 'SGES', 'AGT', '', '', 'https://en.wikipedia.org/wiki/Guarani_International_Airport', ''),
('0', 'Aeropuerto Intl. Lic. Jesús Terán Peredo', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Aguasca', '102° 18'' 58.68', '21° 42'' 2.751"', 'public', '1863', 'AGU', 'MMAS', ' ', '-102.3163019', '21.7007642', '4691', 'MMAS', 'medium_airport', 'Jesús Terán Paredo International Airport', '21.705601', '-102.318001', '6112', 'NA', 'MX', 'MX-AGU', 'Aguascalientes', 'yes', 'MMAS', 'AGU', '', 'https://www.aeropuertosgap.com.mx/es/aguascalientes.html', 'https://en.wikipedia.org/wiki/Lic._Jes%C3%BAs_Ter%C3%A1n_Peredo_International_Airport', 'Aguascalientes international'),
('0', '旭川空港', 'KSJ2:AAC=01453
KSJ2:AAC_label=北海道上川郡東神楽町', '142° 26'' 49.56', '43° 40'' 7.208"', 'public', '0', 'AKJ', 'RJEC', 'Аэропорт Асахикава', '142.4471025', '43.6686688', '5558', 'RJEC', 'medium_airport', 'Asahikawa Airport', '43.670799', '142.447006', '721', 'AS', 'JP', 'JP-01', 'Asahikawa', 'yes', 'RJEC', 'AKJ', '', '', 'https://en.wikipedia.org/wiki/Asahikawa_Airport', ''),
('0', 'Auckland International Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '174° 47'' 12.93', '37° 0'' 31.745"', 'international', '7', 'AKL', 'NZAA', ' ', '174.7869275', '-37.0088181', '5023', 'NZAA', 'large_airport', 'Auckland International Airport', '-37.008099', '174.792007', '23', 'OC', 'NZ', 'NZ-AUK', 'Auckland', 'yes', 'NZAA', 'AKL', '', 'http://www.auckland-airport.co.nz/', 'https://en.wikipedia.org/wiki/Auckland_International_Airport', ''),
('0', 'Международный Аэропорт Алматы', 'aerodrome:type=public
aeroway=aerodrome
ele=681
iata=ALA
ica', '77° 2'' 37.433"', '43° 21'' 20.393', 'public', '681', 'ALA', 'UAAA', 'Аэропорт Алма-Ата', '77.0437314', '43.3556648', '6421', 'UAAA', 'large_airport', 'Almaty Airport', '43.3521003723145', '77.0404968261719', '2234', 'AS', 'KZ', 'KZ-ALM', 'Almaty', 'yes', 'UAAA', 'ALA', '', '', 'https://en.wikipedia.org/wiki/Almaty_International_Airport', 'Alma Ata'),
('0', 'Albany Regional Airport', 'aerodrome=regional
aerodrome:type=public
aeroway=aerodrome
a', '117° 48'' 32.83', '34° 56'' 43.641', 'public', '71', 'ALH', 'YABA', ' ', '117.8091199', '-34.9454559', '26888', 'YABA', 'medium_airport', 'Albany Airport', '-34.9432983398437', '117.80899810791', '233', 'OC', 'AU', 'AU-WA', 'Albany', 'yes', 'YABA', 'ALH', '', '', 'https://en.wikipedia.org/wiki/Albany_Airport_(Australia)', 'ABA, YPAL'),
('0', 'Ahmedabad Airport (AMD) - Sardar Vallabhai Patel Internation', 'addr:city=अहमदाबाद, गुजरात
addr:', '72° 37'' 52.345', '23° 4'' 33.143"', ' ', '0', 'AMD', 'VAAH', ' ', '72.6312069', '23.0758731', '26431', 'VAAH', 'medium_airport', 'Sardar Vallabhbhai Patel International Airport', '23.0771999359', '72.6346969604', '189', 'AS', 'IN', 'IN-GJ', 'Ahmedabad', 'yes', 'VAAH', 'AMD', '', '', 'https://en.wikipedia.org/wiki/Sardar_Vallabhbhai_Patel_International_Airport', 'Gandhinagar Air Force Station'),
('0', 'V. C. Bird International Airport', 'aeroway=aerodrome
ele=18.9
iata=ANU
icao=TAPA
name=V. C. Bir', '61° 47'' 25.805', '17° 8'' 8.889"', ' ', '19', 'ANU', 'TAPA', ' ', '-61.7905015', '17.1358025', '6359', 'TAPA', 'medium_airport', 'V.C. Bird International Airport', '17.1367', '-61.792702', '62', 'NA', 'AG', 'AG-03', 'St. John''s', 'yes', 'TAPA', 'ANU', '', '', 'https://en.wikipedia.org/wiki/VC_Bird_International_Airport', ''),
('0', 'Faleolo International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '172° 0'' 21.015', '13° 49'' 51.975', 'public', '18', 'APW', 'NSFA', ' ', '-172.0058376', '-13.8311043', '4976', 'NSFA', 'medium_airport', 'Faleolo International Airport', '-13.8299999237061', '-172.007995605469', '58', 'OC', 'WS', 'WS-AA', 'Apia', 'yes', 'NSFA', 'APW', '', '', 'https://en.wikipedia.org/wiki/Faleolo_International_Airport', ''),
('0', 'Aeroporto Estadual Bartholomeu de Gusmão de Araraquara', 'aeroway=aerodrome
barrier=fence
iata=AQA
icao=SBAQ
loc_name=', '48° 8'' 2.221"', '21° 48'' 44.113', ' ', '0', 'AQA', 'SBAQ', ' ', '-48.1339504', '-21.8122535', '5861', 'SBAQ', 'medium_airport', 'Araraquara Airport', '-21.812', '-48.132999', '2334', 'SA', 'BR', 'BR-SP', 'Araraquara', 'yes', 'SBAQ', 'AQA', 'SP0012', 'http://www.daesp.sp.gov.br/aeroporto-estadual-de-araraquara-bartolomeu-de-gusmao/', 'https://en.wikipedia.org/wiki/Araraquara_Airport', ''),
('0', '安庆天柱山机场', 'aeroway=aerodrome
iata=AQG
icao=ZSAQ
name=安庆天柱山机', '117° 3'' 1.465"', '30° 34'' 56.253', ' ', '0', 'AQG', 'ZSAQ', ' ', '117.0504069', '30.5822926', '30650', 'ZSAQ', 'medium_airport', 'Anqing Tianzhushan Airport', '30.582199', '117.050003', NULL, 'AS', 'CN', 'CN-34', 'Anqing', 'yes', 'ZSAQ', 'AQG', '', '', 'https://en.wikipedia.org/wiki/Anqing_Tianzhushan_Airport', 'Anqing Air Base'),
('0', 'Rodriguez Ballon International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Arequip', '71° 34'' 17.392', '16° 20'' 32.289', 'public', '2562', 'AQP', 'SPQU', ' ', '-71.5714977', '-16.3423025', '6233', 'SPQU', 'medium_airport', 'Rodríguez Ballón International Airport', '-16.3411006927', '-71.5830993652', '8405', 'SA', 'PE', 'PE-ARE', 'Arequipa', 'yes', 'SPQU', 'AQP', '', '', 'https://en.wikipedia.org/wiki/Rodriguez_Ballon_International_Airport', ''),
('0', 'Armidale Regional Airport', 'addr:city=Armidale
addr:country=AU
addr:postcode=2350
addr:s', '151° 36'' 58.84', '30° 31'' 43.697', 'public', '1084', 'ARM', 'YARM', ' ', '151.6163461', '-30.5288046', '26893', 'YARM', 'medium_airport', 'Armidale Airport', '-30.5280990601', '151.617004395', '3556', 'OC', 'AU', 'AU-NSW', 'Armidale', 'yes', 'YARM', 'ARM', '', '', 'https://en.wikipedia.org/wiki/Armidale_Airport', ''),
('0', 'Aeroporto Estadual de Araçatuba Dario Guarita', 'aerodrome:type=public
aeroway=aerodrome
ele=415
iata=ARU
ica', '50° 25'' 30.031', '21° 8'' 26.036"', 'public', '415', 'ARU', 'SBAU', ' ', '-50.4250085', '-21.1405655', '5865', 'SBAU', 'medium_airport', 'Araçatuba Airport', '-21.141479', '-50.424575', '1361', 'SA', 'BR', 'BR-SP', 'Araçatuba', 'yes', 'SBAU', 'ARU', 'SP0009', 'http://www.daesp.sp.gov.br/aeroporto-estadual-de-aracatuba-dario-guarita/', 'https://en.wikipedia.org/wiki/Ara%C3%A7atuba_Airport', 'Dario Guarita State Airport,'),
('0', 'Alice Springs Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '133° 54'' 13.12', '23° 48'' 23.614', 'public', '545', 'ASP', 'YBAS', ' ', '133.9036444', '-23.8065594', '26900', 'YBAS', 'medium_airport', 'Alice Springs Airport', '-23.8066997528076', '133.901992797852', '1789', 'OC', 'AU', 'AU-NT', 'Alice Springs', 'yes', 'YBAS', 'ASP', '', 'http://www.alicespringsairport.com.au/', 'https://en.wikipedia.org/wiki/Alice_Springs_Airport', ''),
('0', 'Aeropuerto Internacional Silvio Pettirossi', 'OACI=SGAS
aeroway=aerodrome
area=yes
iata=ASU
icao=SGAS
is_i', '57° 31'' 1.308"', '25° 14'' 10.558', ' ', '0', 'ASU', 'SGAS', ' ', '-57.5170301', '-25.2362662', '6088', 'SGAS', 'medium_airport', 'Silvio Pettirossi International Airport', '-25.2399997711182', '-57.5200004577637', '292', 'SA', 'PY', 'PY-11', 'Asunción', 'yes', 'SGAS', 'ASU', '', '', 'https://en.wikipedia.org/wiki/Silvio_Pettirossi_International_Airport', ''),
('0', 'Aeropuerto Internacional Reina Beatrix', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '70° 0'' 56.330"', '12° 30'' 7.459"', 'public', '18', 'AUA', 'TNCA', ' ', '-70.0156471', '12.5020719', '6402', 'TNCA', 'large_airport', 'Queen Beatrix International Airport', '12.5014', '-70.015198', '60', 'NA', 'AW', 'AW-U-A', 'Oranjestad', 'yes', 'TNCA', 'AUA', '', '', 'https://en.wikipedia.org/wiki/Queen_Beatrix_International_Airport', 'Playa'),
('0', 'Santiago Pérez', 'aeroway=aerodrome
closest_town=Arauca
ele=122
iata=AUC
icao=', '70° 44'' 13.194', '7° 4'' 4.858" N', ' ', '122', 'AUC', 'SKUC', ' ', '-70.7369982', '7.068016', '6171', 'SKUC', 'medium_airport', 'Santiago Perez Airport', '7.06888', '-70.7369', '420', 'SA', 'CO', 'CO-ARA', 'Arauca', 'yes', 'SKUC', 'AUC', 'AUC', '', 'https://en.wikipedia.org/wiki/Santiago_P%C3%A9rez_Airport', ''),
('0', 'Hiva Oa - Atuona Airport', 'aeroway=aerodrome
iata=AUQ
icao=NTMN
name=Hiva Oa - Atuona A', '139° 0'' 36.586', '9° 45'' 59.973"', ' ', '0', 'AUQ', 'NTMN', ' ', '-139.0101627', '-9.7666592', '5002', 'NTMN', 'medium_airport', 'Hiva Oa-Atuona Airport', '-9.76879024506', '-139.011001587', '1481', 'OC', 'PF', 'PF-U-A', 'Hiva Oa Island', 'yes', 'NTMN', 'AUQ', '', '', 'https://en.wikipedia.org/wiki/Atuona_Airport', 'HIX'),
('0', 'Aeroporto de Araguaína', 'aeroway=aerodrome
iata=AUX
icao=SWGN
name=Aeroporto de Aragu', '48° 14'' 28.245', '7° 13'' 40.549"', ' ', '0', 'AUX', 'SWGN', ' ', '-48.2411792', '-7.2279303', '538', 'SWGN', 'medium_airport', 'Araguaína Airport', '-7.22787', '-48.240501', '771', 'SA', 'BR', 'BR-TO', 'Araguaína', 'yes', 'SWGN', 'AUX', 'SWGN', '', 'https://en.wikipedia.org/wiki/Aragua%C3%ADna_Airport', ''),
('0', 'Арвайхээр', 'aerodrome:type=military/public
aeroway=aerodrome
iata=AVK
ic', '102° 48'' 13.73', '46° 14'' 57.075', 'military/public', '0', 'AVK', 'ZMAH', 'Арвайхээр', '102.8038143', '46.2491874', '29670', 'ZMAH', 'medium_airport', 'Arvaikheer Airport', '46.250301361084', '102.802001953125', '5932', 'AS', 'MN', 'MN-055', 'Arvaikheer', 'yes', 'ZMAH', 'AVK', '', '', 'https://en.wikipedia.org/wiki/Arvaikheer_Airport', ''),
('0', 'Avalon Airport', 'aeroway=aerodrome
ele=11 m
iata=AVV
icao=YMAV
internet_acces', '144° 28'' 4.741', '38° 2'' 20.400"', ' ', '0', 'AVV', 'YMAV', ' ', '144.4679836', '-38.039', '27043', 'YMAV', 'medium_airport', 'Avalon Airport', '-38.039398', '144.468994', '35', 'OC', 'AU', 'AU-VIC', 'Melbourne', 'yes', 'YMAV', 'AVV', '', 'https://www.avalonairport.com.au/', 'https://en.wikipedia.org/wiki/Avalon_Airport', 'Lara, Geelong'),
('0', 'Clayton J. Lloyd International Airport', 'aeroway=aerodrome
ele=38.7
iata=AXA
icao=TQPF
is_in=The Vall', '63° 3'' 14.165"', '18° 12'' 20.658', ' ', '39', 'AXA', 'TQPF', ' ', '-63.0539346', '18.2057382', '6407', 'TQPF', 'medium_airport', 'Clayton J Lloyd International Airport', '18.204773', '-63.05383', '127', 'NA', 'AI', 'AI-U-A', 'The Valley', 'yes', 'TQPF', 'AXA', '', 'http://www.news.ai/ref/airport.html', 'https://en.wikipedia.org/wiki/Anguilla_Wallblake_Airport', 'Wallblake Airport'),
('0', 'Spring Point Airport', 'aeroway=aerodrome
ele=3
iata=AXP
icao=MYAP
is_in=Acklins Isl', '73° 58'' 13.184', '22° 26'' 30.524', ' ', '3', 'AXP', 'MYAP', ' ', '-73.9703289', '22.4418121', '4929', 'MYAP', 'medium_airport', 'Spring Point Airport', '22.4417991638', '-73.9709014893', '11', 'NA', 'BS', 'BS-AK', 'Spring Point', 'yes', 'MYAP', 'AXP', '', '', '', ''),
('0', '秋田空港', 'KSJ2:AAC=05201
KSJ2:AAC_label=秋田県秋田市
KSJ2:AD2=5
', '140° 13'' 4.283', '39° 36'' 52.646', 'public', '95', 'AXT', 'RJSK', 'Аэропорт Акита', '140.2178563', '39.614624', '5611', 'RJSK', 'medium_airport', 'Akita Airport', '39.6156005859375', '140.218994140625', '313', 'AS', 'JP', 'JP-05', 'Akita', 'yes', 'RJSK', 'AXT', '', '', 'https://en.wikipedia.org/wiki/Akita_Airport', ''),
('0', 'Coronel FAP Alfredo Mendivil Duarte Airport', 'aeroway=aerodrome
closest_town=Ayacucho
ele=2718
iata=AYP
ic', '74° 12'' 17.315', '13° 9'' 17.277"', ' ', '2718', 'AYP', 'SPHO', ' ', '-74.2048096', '-13.1547993', '6214', 'SPHO', 'medium_airport', 'Coronel FAP Alfredo Mendivil Duarte Airport', '-13.1548004150391', '-74.2043991088867', '8917', 'SA', 'PE', 'PE-AYA', 'Ayacucho', 'yes', 'SPHO', 'AYP', '', '', 'https://en.wikipedia.org/wiki/Coronel_FAP_Alfredo_Mendivil_Duarte_Airport', ''),
('0', 'Connellan Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Ayers Rock', '130° 58'' 37.71', '25° 10'' 57.733', 'public', '496', 'AYQ', 'YAYE', ' ', '130.9771426', '-25.1827037', '26895', 'YAYE', 'medium_airport', 'Ayers Rock Connellan Airport', '-25.1861', '130.975998', '1626', 'OC', 'AU', 'AU-NT', 'Ayers Rock', 'yes', 'YAYE', 'AYQ', '', '', 'https://en.wikipedia.org/wiki/Ayers_Rock_Airport', 'Ayers Rock,Uluru'),
('0', 'Aéroport d''Adrar Cheikh Sidi Mohamed Belkebir', 'aerodrome=international
aeroway=aerodrome
alt_name:ar=مطا', '0° 11'' 25.069"', '27° 50'' 6.627"', ' ', '0', 'AZR', 'DAUA', ' ', '-0.190297', '27.8351743', '2075', 'DAUA', 'medium_airport', 'Touat Cheikh Sidi Mohamed Belkebir Airport', '27.837601', '-0.186414', '919', 'AF', 'DZ', 'DZ-01', 'Adrar', 'yes', 'DAUA', 'AZR', '', '', 'https://en.wikipedia.org/wiki/Touat_Cheikh_Sidi_Mohamed_Belkebir_Airport', ''),
('0', 'Loakan Airport', 'addr:city=Baguio
addr:postcode=2600
aerodrome:type=public
ae', '120° 37'' 9.269', '16° 22'' 31.221', 'public', '0', 'BAG', 'RPUB', ' ', '120.6192415', '16.3753392', '5715', 'RPUB', 'medium_airport', 'Loakan Airport', '16.3750991821289', '120.620002746582', '4251', 'AS', 'PH', 'PH-BEN', 'Baguio City', 'yes', 'RPUB', 'BAG', '', '', 'https://en.wikipedia.org/wiki/Loakan_Airport', 'Baguio Airport'),
('0', 'Aeropuerto Internacional Ernesto Cortissoz', 'aeroway=aerodrome
iata=BAQ
icao=SKBQ
name=Aeropuerto Interna', '74° 46'' 54.360', '10° 53'' 16.660', ' ', '0', 'BAQ', 'SKBQ', ' ', '-74.7817667', '10.8879612', '6105', 'SKBQ', 'medium_airport', 'Ernesto Cortissoz International Airport', '10.8896', '-74.7808', '98', 'SA', 'CO', 'CO-ATL', 'Barranquilla', 'yes', 'SKBQ', 'BAQ', 'BAQ', '', 'https://en.wikipedia.org/wiki/Ernesto_Cortissoz_International_Airport', ''),
('0', '包头二里半机场', 'aeroway=aerodrome
iata=BAV
icao=ZBOW
name=包头二里半机', '109° 59'' 58.14', '40° 33'' 35.268', ' ', '0', 'BAV', 'ZBOW', ' ', '109.9994858', '40.5597968', '30679', 'ZBOW', 'medium_airport', 'Baotou Airport', '40.560001373291', '109.997001647949', '3321', 'AS', 'CN', 'CN-15', 'Baotou', 'yes', 'ZBOW', 'BAV', '', '', 'https://en.wikipedia.org/wiki/Baotou_Airport', ''),
('0', 'Biju Patnaik Airport', 'aeroway=aerodrome
alt_name:en=Bhubaneswar Airport
ele=42
iat', '85° 48'' 46.869', '20° 15'' 8.688"', ' ', '42', 'BBI', 'VEBS', ' ', '85.8130192', '20.2524133', '26494', 'VEBS', 'medium_airport', 'Biju Patnaik Airport', '20.2444000244', '85.8178024292', '138', 'AS', 'IN', 'IN-OR', 'Bhubaneswar', 'yes', 'VEBS', 'BBI', '', 'http://aai.aero/allAirports/bhubaneshwar_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Biju_Patnaik_Airport', ''),
('0', 'Kasane Airport', 'aerodrome:type=public
aeroway=aerodrome
city_served=Kasane
e', '25° 9'' 59.711"', '17° 49'' 52.388', 'public', '1003', 'BBK', 'FBKE', ' ', '25.1665865', '-17.8312188', '2862', 'FBKE', 'medium_airport', 'Kasane Airport', '-17.8328990936279', '25.1623992919922', '3289', 'AF', 'BW', 'BW-NW', 'Kasane', 'yes', 'FBKE', 'BBK', '', '', 'https://en.wikipedia.org/wiki/Kasane_Airport', ''),
('0', 'Bacolod-Silay Domestic Airport', 'addr:city=Silay City
addr:postcode=6116
addr:street=Airport', '123° 1'' 9.840"', '10° 46'' 35.365', 'public', '8', 'BCD', 'RPVB', ' ', '123.0194', '10.7764903', '35178', 'RPVB', 'medium_airport', 'Bacolod-Silay Airport', '10.7764', '123.014999', '82', 'AS', 'PH', 'PH-NEC', 'Bacolod City', 'yes', 'RPVB', 'BCD', '', 'http://www.bacolod-silayairport.com/', 'https://en.wikipedia.org/wiki/Bacolod%E2%80%93Silay_Airport', ''),
('0', 'Barcaldine Aerodrome', 'aerodrome:type=public
aeroway=aerodrome
ele=268.2
iata=BCI
i', '145° 18'' 19.97', '23° 33'' 56.069', 'public', '268', 'BCI', 'YBAR', ' ', '145.3055478', '-23.5655746', '26899', 'YBAR', 'medium_airport', 'Barcaldine Airport', '-23.5652999878', '145.307006836', '878', 'OC', 'AU', 'AU-QLD', 'Barcaldine', 'yes', 'YBAR', 'BCI', '', '', 'https://en.wikipedia.org/wiki/Barcaldine_Airport', ''),
('0', 'Bundaberg Airport', 'aeroway=aerodrome
ele=33
iata=BDB
icao=YBUD
name=Bundaberg A', '152° 19'' 15.74', '24° 54'' 13.595', ' ', '33', 'BDB', 'YBUD', ' ', '152.3210393', '-24.9037763', '26936', 'YBUD', 'medium_airport', 'Bundaberg Airport', '-24.9039001465', '152.319000244', '107', 'OC', 'AU', 'AU-QLD', 'Bundaberg', 'yes', 'YBUD', 'BDB', '', 'http://bundaberg.qld.gov.au/services/airport', 'https://en.wikipedia.org/wiki/Bundaberg_Airport', ''),
('0', 'Vadodara Green Airport Harni', 'aeroway=aerodrome
closest_town=Vadodara
ele=39
iata=BDQ
icao', '73° 13'' 36.093', '22° 20'' 11.513', ' ', '39', 'BDQ', 'VABO', ' ', '73.2266924', '22.3365315', '26438', 'VABO', 'medium_airport', 'Vadodara Airport', '22.336201', '73.226303', '129', 'AS', 'IN', 'IN-GJ', 'Vadodara', 'yes', 'VABO', 'BDQ', '', 'http://aai.aero/allAirports/vadodara_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Vadodara_Airport', 'Civil Airport Harni'),
('0', 'Aeropuerto de Bluefields', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Bluefie', '83° 46'' 26.796', '11° 59'' 26.601', 'public', '12', 'BEF', 'MNBL', ' ', '-83.7741099', '11.9907224', '4770', 'MNBL', 'medium_airport', 'Bluefields Airport', '11.9910001754761', '-83.7741012573242', '20', 'NA', 'NI', 'NI-AS', 'Bluefileds', 'yes', 'MNBL', 'BEF', '', '', 'https://en.wikipedia.org/wiki/Bluefields_Airport', ''),
('0', 'Aeroporto Internacional de Belém Val-de-Cans', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeroporto I', '48° 28'' 33.729', '1° 22'' 49.902"', 'public', '0', 'BEL', 'SBBE', ' ', '-48.4760359', '-1.3805283', '5867', 'SBBE', 'large_airport', 'Val de Cans/Júlio Cezar Ribeiro International Airport', '-1.37925004959', '-48.4762992859', '54', 'SA', 'BR', 'BR-PA', 'Belém', 'yes', 'SBBE', 'BEL', '', 'http://www.infraero.gov.br/index.php/br/aeroportos/para/aeroporto-internacional-de-belem.html', 'https://en.wikipedia.org/wiki/Val_de_Cans_International_Airport', ''),
('0', 'Meadows Field Airport', 'addr:state=CA
aerodrome:type=public
aeroway=aerodrome
alt_na', '119° 3'' 24.800', '35° 26'' 12.768', 'public', '150', 'BFL', 'KBFL', ' ', '-119.056889', '35.4368799', '3402', 'KBFL', 'medium_airport', 'Meadows Field', '35.433601', '-119.056999', '510', 'NA', 'US', 'US-CA', 'Bakersfield', 'yes', 'KBFL', 'BFL', 'BFL', '', 'https://en.wikipedia.org/wiki/Meadows_Field_Airport', ''),
('0', 'Bram Fischer Airport', 'aeroway=aerodrome
iata=BFN
icao=FABL
ifr=yes
licensed:sacaa=', '26° 17'' 48.224', '29° 5'' 32.340"', ' ', '0', 'BFN', 'FABL', ' ', '26.2967289', '-29.0923167', '2772', 'FABL', 'medium_airport', 'Bram Fischer International Airport', '-29.092699', '26.302401', '4457', 'AF', 'ZA', 'ZA-FS', 'Bloemfontain', 'yes', 'FABL', 'BFN', '', '', 'https://en.wikipedia.org/wiki/Bloemfontein_Airport', ''),
('0', 'Palonegro', 'aeroway=aerodrome
iata=BGA
icao=SKBG
name=Palonegro
name:en=', '73° 11'' 3.882"', '7° 7'' 35.282"', ' ', '0', 'BGA', 'SKBG', ' ', '-73.1844117', '7.1264673', '6102', 'SKBG', 'medium_airport', 'Palonegro Airport', '7.1265', '-73.1848', '3897', 'SA', 'CO', 'CO-SAN', 'Bucaramanga', 'yes', 'SKBG', 'BGA', 'BGA', '', 'https://en.wikipedia.org/wiki/Palo_Negro_International_Airport', ''),
('0', 'Grantley Adams International Airport', 'aeroway=aerodrome
closest_town=Bridgetown
ele=52
iata=BGI
ic', '59° 29'' 29.193', '13° 4'' 38.294"', ' ', '52', 'BGI', 'TBPB', ' ', '-59.4914426', '13.0773038', '6360', 'TBPB', 'medium_airport', 'Grantley Adams International Airport', '13.0746', '-59.4925', '169', 'NA', 'BB', 'BB-01', 'Bridgetown', 'yes', 'TBPB', 'BGI', '', 'http://www.gaia.bb/content/about-gaia-inc', 'https://en.wikipedia.org/wiki/Grantley_Adams_International_Airport', 'Seawell'),
('0', 'Woodbourne Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '173° 52'' 9.015', '41° 31'' 2.391"', ' ', '0', 'BHE', 'NZWB', ' ', '173.8691709', '-41.5173309', '5058', 'NZWB', 'medium_airport', 'Woodbourne Airport', '-41.5182991027832', '173.869995117188', '109', 'OC', 'NZ', 'NZ-MBH', 'Blenheim', 'yes', 'NZWB', 'BHE', '', '', 'https://en.wikipedia.org/wiki/Woodbourne_Airport', ''),
('0', 'Bhuj Rudra Mata Airport', 'aeroway=aerodrome
barrier=wall
closest_town=Bhuj
ele=82
iata', '69° 39'' 48.511', '23° 16'' 32.341', ' ', '82', 'BHJ', 'VABJ', ' ', '69.6634753', '23.2756502', '26436', 'VABJ', 'medium_airport', 'Bhuj Airport', '23.2877998352', '69.6701965332', '268', 'AS', 'IN', 'IN-GJ', 'Bhuj', 'yes', 'VABJ', 'BHJ', '', 'http://aai.aero/allAirports/bhuj_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Bhuj_Airport', 'Bhuj Rudra Mata Air Force Station, Shyamji Krishna Verma, Bhuj Airport'),
('0', 'Buxoro Xalqaro aeroporti', 'aeroway=aerodrome
barrier=wall
ele=229
iata=BHK
icao=UTSB
na', '64° 28'' 50.691', '39° 46'' 29.857', ' ', '229', 'BHK', 'UTSB', 'Международный Аэропорт Бухар', '64.4807475', '39.7749602', '26388', 'UTSB', 'medium_airport', 'Bukhara Airport', '39.7750015258789', '64.4832992553711', '751', 'AS', 'UZ', 'UZ-BU', 'Bukhara', 'yes', 'UTSB', 'BHK', '', '', 'https://en.wikipedia.org/wiki/Bukhara_Airport', 'Buhara Airport'),
('0', 'Bhopal Airport', 'aeroway=aerodrome
barrier=wall
closest_town=Bhopal
ele=524
i', '77° 19'' 43.274', '23° 17'' 24.726', ' ', '524', 'BHO', 'VABP', ' ', '77.3286873', '23.2902017', '26439', 'VABP', 'medium_airport', 'Raja Bhoj International Airport', '23.2875003815', '77.3374023438', '1711', 'AS', 'IN', 'IN-MP', 'Bhopal', 'yes', 'VABP', 'BHO', '', 'http://aai.aero/allAirports/bhopal_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Bhopal_Airport', ''),
('0', 'Broken Hill Airport', 'aeroway=aerodrome
iata=BHQ
icao=YBHI
name=Broken Hill Airpor', '141° 28'' 19.36', '32° 0'' 22.973"', ' ', '0', 'BHQ', 'YBHI', ' ', '141.4720467', '-32.0063815', '26908', 'YBHI', 'medium_airport', 'Broken Hill Airport', '-32.0013999939', '141.472000122', '958', 'OC', 'AU', 'AU-NSW', 'Broken Hill', 'yes', 'YBHI', 'BHQ', '', '', 'https://en.wikipedia.org/wiki/Broken_Hill_Airport', ''),
('0', 'Bhavnagar Airport', 'aeroway=aerodrome
closest_town=Bhavnagar
ele=6
iata=BHU
icao', '72° 11'' 9.300"', '21° 45'' 11.594', ' ', '6', 'BHU', 'VABV', ' ', '72.1859168', '21.7532206', '26440', 'VABV', 'medium_airport', 'Bhavnagar Airport', '21.752199173', '72.1852035522', '44', 'AS', 'IN', 'IN-GJ', 'Bhavnagar', 'yes', 'VABV', 'BHU', '', 'http://aai.aero/allAirports/bhavnagar.jsp', 'https://en.wikipedia.org/wiki/Bhavnagar_Airport', ''),
('0', 'Bahawalpur', 'aeroway=aerodrome
iata=BHV
icao=OPBW
name=Bahawalpur
name:ja', '71° 42'' 59.701', '29° 20'' 53.985', ' ', '0', 'BHV', 'OPBW', ' ', '71.7165836', '29.3483291', '5247', 'OPBW', 'medium_airport', 'Bahawalpur Airport', '29.3481006622314', '71.7180023193359', '392', 'AS', 'PK', 'PK-PB', 'Bahawalpur', 'yes', 'OPBW', 'BHV', '', '', 'https://en.wikipedia.org/wiki/Bahawalpur_Airport', ''),
('0', 'Bandar Udara Frans Kaisiepo', 'aeroway=aerodrome
iata=BIK
icao=WABB
name=Bandar Udara Frans', '136° 6'' 18.419', '1° 11'' 18.660"', ' ', '0', 'BIK', 'WABB', ' ', '136.1051165', '-1.1885167', '26746', 'WABB', 'medium_airport', 'Frans Kaisiepo Airport', '-1.19001996517181', '136.108001708984', '46', 'AS', 'ID', 'ID-PA', 'Biak-Supiori Island', 'yes', 'WABB', 'BIK', '', '', 'https://en.wikipedia.org/wiki/Frans_Kaisiepo_Airport', ''),
('0', 'South Bimini Airport', 'aeroway=aerodrome
ele=3
iata=BIM
icao=MYBS
is_in=South Bimin', '79° 15'' 53.441', '25° 42'' 2.089"', ' ', '3', 'BIM', 'MYBS', ' ', '-79.2648448', '25.7005804', '4935', 'MYBS', 'medium_airport', 'South Bimini Airport', '25.6998996735', '-79.2647018433', '10', 'NA', 'BS', 'BS-BI', 'South Bimini', 'yes', 'MYBS', 'BIM', '', '', 'https://en.wikipedia.org/wiki/South_Bimini_Airport', ''),
('0', 'Biratnagar Airport', 'aeroway=aerodrome
ele=72
iata=BIR
icao=VNVT
is_in:country=Ne', '87° 15'' 48.764', '26° 28'' 58.314', ' ', '72', 'BIR', 'VNVT', ' ', '87.2635456', '26.4828649', '26600', 'VNVT', 'medium_airport', 'Biratnagar Airport', '26.4815006256104', '87.2639999389648', '236', 'AS', 'NP', 'NP-KO', 'Biratnagar', 'yes', 'VNVT', 'BIR', '', '', 'https://en.wikipedia.org/wiki/Biratnagar_Airport', ''),
('0', 'Banjul International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '16° 39'' 1.745"', '13° 20'' 9.941"', 'public', '29', 'BJL', 'GBYD', ' ', '-16.6504846', '13.3360948', '3076', 'GBYD', 'medium_airport', 'Banjul International Airport', '13.3380002975464', '-16.6522006988525', '95', 'AF', 'GM', 'GM-W', 'Banjul', 'yes', 'GBYD', 'BJL', '', '', 'https://en.wikipedia.org/wiki/Banjul_International_Airport', 'Yundum international Airport'),
('0', 'Del Bajío International Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=1815
iata=BJX
ic', '101° 28'' 54.85', '20° 59'' 33.230', 'public', '1815', 'BJX', 'MMLO', ' ', '-101.4819036', '20.9925638', '4722', 'MMLO', 'medium_airport', 'Del Bajío International Airport', '20.9935', '-101.481003', '5956', 'NA', 'MX', 'MX-GUA', 'Silao', 'yes', 'MMLO', 'BJX', 'BJ1', '', 'https://en.wikipedia.org/wiki/Del_Baj%C3%ADo_International_Airport', ''),
('0', 'Kota Kinabalu International Airport', 'aeroway=aerodrome
iata=BKI
icao=WBKK
name=Kota Kinabalu Inte', '116° 3'' 0.113"', '5° 55'' 59.664"', ' ', '0', 'BKI', 'WBKK', ' ', '116.0500313', '5.93324', '26814', 'WBKK', 'medium_airport', 'Kota Kinabalu International Airport', '5.93721008300781', '116.051002502441', '10', 'AS', 'MY', 'MY-12', 'Kota Kinabalu', 'yes', 'WBKK', 'BKI', '', '', 'https://en.wikipedia.org/wiki/Kota_Kinabalu_International_Airport', ''),
('0', 'ท่าอากาศยานสุวรรณภูม', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '100° 45'' 8.351', '13° 40'' 54.028', 'public', '2', 'BKK', 'VTBS', 'Суварнабхуми', '100.7523198', '13.6816744', '28118', 'VTBS', 'large_airport', 'Suvarnabhumi Airport', '13.6810998916626', '100.747001647949', '5', 'AS', 'TH', 'TH-10', 'Bangkok', 'yes', 'VTBS', 'BKK', '', '', 'https://en.wikipedia.org/wiki/Suvarnabhumi_Airport', ''),
('0', 'Aéroport international Modibo Keïta', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '7° 56'' 54.642"', '12° 32'' 6.924"', 'public', '380', 'BKO', 'GABS', ' ', '-7.9485117', '12.5352567', '3066', 'GABS', 'large_airport', 'Modibo Keita International Airport', '12.5335', '-7.94994', '1247', 'AF', 'ML', 'ML-2', 'Bamako', 'yes', 'GABS', 'BKO', '', '', 'https://en.wikipedia.org/wiki/Bamako%E2%80%93S%C3%A9nou_International_Airport', 'Senou Airport'),
('0', 'Blackall Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=283
iata=BKQ
ica', '145° 25'' 42.04', '24° 25'' 40.907', 'public', '283', 'BKQ', 'YBCK', ' ', '145.4283459', '-24.4280297', '26903', 'YBCK', 'medium_airport', 'Blackall Airport', '-24.4277992249', '145.429000854', '928', 'OC', 'AU', 'AU-QLD', 'Blackall', 'yes', 'YBCK', 'BKQ', '', '', 'https://en.wikipedia.org/wiki/Blackall_Airport', ''),
('0', 'Aeropuerto General José António Anzoátegui', 'aeroway=aerodrome
iata=BLA
icao=SVBC
name=Aeropuerto General', '64° 41'' 34.630', '10° 6'' 27.031"', ' ', '0', 'BLA', 'SVBC', ' ', '-64.6929528', '10.1075085', '6255', 'SVBC', 'large_airport', 'General José Antonio Anzoategui International Airport', '10.111111', '-64.692222', '30', 'SA', 'VE', 'VE-B', 'Barcelona', 'yes', 'SVBC', 'BLA', '', 'http://aigdjaa.baer.gob.ve', 'https://en.wikipedia.org/wiki/General_Jos%C3%A9_Antonio_Anzo%C3%A1tegui_International_Airport', 'Barcelona, Puero La Cruz,Lecheria'),
('0', 'Broome International Airport', 'aerodrome=continental
aerodrome:type=public
aeroway=aerodrom', '122° 13'' 37.35', '17° 56'' 57.665', 'public', '0', 'BME', 'YBRM', ' ', '122.227042', '-17.9493515', '26927', 'YBRM', 'medium_airport', 'Broome International Airport', '-17.9447002410889', '122.232002258301', '56', 'OC', 'AU', 'AU-WA', 'Broome', 'yes', 'YBRM', 'BME', '', '', 'https://en.wikipedia.org/wiki/Broome_International_Airport', ''),
('0', 'Sân bay Buôn Ma Thuột', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Buon Ma', '108° 7'' 8.377"', '12° 40'' 0.762"', 'public', '527', 'BMV', 'VVBM', ' ', '108.1189935', '12.6668784', '26690', 'VVBM', 'medium_airport', 'Buon Ma Thuot Airport', '12.668299675', '108.120002747', '1729', 'AS', 'VN', 'VN-33', 'Buon Ma Thuot', 'yes', 'VVBM', 'BMV', '', '', 'https://en.wikipedia.org/wiki/Buon_Ma_Thuot_Airport', ''),
('0', 'Brisbane Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '153° 6'' 59.368', '27° 23'' 15.238', 'public', '4', 'BNE', 'YBBN', ' ', '153.116491', '-27.3875661', '26901', 'YBBN', 'large_airport', 'Brisbane International Airport', '-27.3841991424561', '153.117004394531', '13', 'OC', 'AU', 'AU-QLD', 'Brisbane', 'yes', 'YBBN', 'BNE', '', '', 'https://en.wikipedia.org/wiki/Brisbane_Airport', ''),
('0', 'Ballina Airport', 'aeroway=aerodrome
iata=BNK
icao=YBNA
name=Ballina Airport', '153° 33'' 40.62', '28° 50'' 4.465"', ' ', '0', 'BNK', 'YBNA', ' ', '153.5612858', '-28.8345736', '26920', 'YBNA', 'medium_airport', 'Ballina Byron Gateway Airport', '-28.8339004517', '153.56199646', '7', 'OC', 'AU', 'AU-NSW', 'Ballina', 'yes', 'YBNA', 'BNK', '', '', 'https://en.wikipedia.org/wiki/Ballina_Airport', ''),
('0', 'Aeropuerto Luisa Cáceres de Arismendi', 'aeroway=aerodrome
iata=BNS
icao=SVBI
name=Aeropuerto Luisa C', '70° 12'' 56.676', '8° 36'' 48.274"', ' ', '0', 'BNS', 'SVBI', ' ', '-70.2157433', '8.6134094', '6256', 'SVBI', 'medium_airport', 'Barinas Airport', '8.615', '-70.21416667', '615', 'SA', 'VE', 'VE-E', 'Barinas', 'yes', 'SVBI', 'BNS', '', '', '', ''),
('0', 'Aeropuerto Internacional de Bocas del Toro Isla Colón', 'aeroway=aerodrome
iata=BOC
icao=MPBO
name=Aeropuerto Interna', '82° 15'' 2.734"', '9° 20'' 25.658"', ' ', '0', 'BOC', 'MPBO', ' ', '-82.2507595', '9.3404605', '4786', 'MPBO', 'medium_airport', 'Bocas Del Toro International Airport', '9.34084987640381', '-82.2508010864258', '10', 'NA', 'PA', 'PA-1', 'Isla Colón', 'yes', 'MPBO', 'BOC', '', '', '', ''),
('0', 'El Dorado', 'aerodrome=international
aerodrome:type=military/public
aerow', '74° 8'' 47.934"', '4° 42'' 5.764"', 'military/public', '2628', 'BOG', 'SKBO', ' ', '-74.1466483', '4.7016012', '6104', 'SKBO', 'large_airport', 'El Dorado International Airport', '4.70159', '-74.1469', '8361', 'SA', 'CO', 'CO-CUN', 'Bogota', 'yes', 'SKBO', 'BOG', 'BOG', '', 'https://en.wikipedia.org/wiki/El_Dorado_International_Airport', ''),
('0', 'Chhatrapati Shivaji International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '72° 51'' 46.198', '19° 5'' 24.740"', 'public', '11', 'BOM', 'VABB', ' ', '72.8628327', '19.0902056', '26434', 'VABB', 'large_airport', 'Chhatrapati Shivaji International Airport', '19.0886993408', '72.8678970337', '39', 'AS', 'IN', 'IN-MM', 'Mumbai', 'yes', 'VABB', 'BOM', '', 'http://www.csia.in/', 'https://en.wikipedia.org/wiki/Chhatrapati_Shivaji_International_Airport', 'Bombay, Sahar International Airport'),
('0', 'Bobo-Dioulasso', 'IFR=yes
aerodrome=international
aerodrome:type=civil
aeroway', '4° 20'' 8.040"', '11° 9'' 23.653"', 'civil', '0', 'BOY', 'DFOO', ' ', '-4.3355667', '11.1565704', '2089', 'DFOO', 'medium_airport', 'Bobo Dioulasso Airport', '11.1600999832153', '-4.33096981048584', '1511', 'AF', 'BF', 'BF-HOU', 'Bobo Dioulasso', 'yes', 'DFOO', 'BOY', '', '', 'https://en.wikipedia.org/wiki/Bobo_Dioulasso_Airport', ''),
('0', '昌都邦达机场', 'aeroway=aerodrome
alt_name=Bangda Jichang;帮达机场
barri', '97° 6'' 38.748"', '30° 33'' 8.492"', ' ', '4334', 'BPX', 'ZUBD', ' ', '97.1107633', '30.5523589', '30788', 'ZUBD', 'medium_airport', 'Qamdo Bangda Airport', '30.5536003112793', '97.1082992553711', '14219', 'AS', 'CN', 'CN-54', 'Bangda', 'yes', 'ZUBD', 'BPX', '', '', 'https://en.wikipedia.org/wiki/Bangda_Airport', ''),
('0', 'Boulia Aerodrome', 'aeroway=aerodrome
iata=BQL
icao=YBOU
name=Boulia Aerodrome
s', '139° 54'' 9.273', '22° 54'' 44.641', ' ', '0', 'BQL', 'YBOU', ' ', '139.9025757', '-22.9124003', '26923', 'YBOU', 'medium_airport', 'Boulia Airport', '-22.9132995605469', '139.899993896484', '542', 'OC', 'AU', 'AU-QLD', '', 'yes', 'YBOU', 'BQL', '', '', 'https://en.wikipedia.org/wiki/Boulia_Airport', ''),
('0', 'Aeropuerto Internacional Rafael Hernández', 'addr:state=PR
aerodrome:type=public
aeroway=aerodrome
alt_na', '67° 7'' 38.228"', '18° 29'' 34.627', 'public', '64', 'BQN', 'TJBQ', ' ', '-67.1272855', '18.492952', '6379', 'TJBQ', 'medium_airport', 'Rafael Hernandez Airport', '18.4948997497559', '-67.1294021606445', '237', 'NA', 'PR', 'PR-U-A', 'Aguadilla', 'yes', 'TJBQ', 'BQN', 'BQN', '', 'https://en.wikipedia.org/wiki/Rafael_Hern%C3%A1ndez_Airport', 'Ramey, TJFF'),
('0', 'Bourke', 'aeroway=aerodrome
iata=BRK
icao=YBKE
name=Bourke
source=bing', '145° 56'' 45.35', '30° 2'' 17.924"', ' ', '0', 'BRK', 'YBKE', ' ', '145.9459328', '-30.0383122', '26912', 'YBKE', 'medium_airport', 'Bourke Airport', '-30.0391998291016', '145.951995849609', '352', 'OC', 'AU', 'AU-NSW', '', 'yes', 'YBKE', 'BRK', '', '', 'https://en.wikipedia.org/wiki/Bourke_Airport', ''),
('0', 'Brownsville/South Padre Island International Airport', 'aeroway=aerodrome
faa=BRO
gnis:feature_id=1386303
iata=BRO
i', '97° 25'' 28.494', '25° 54'' 21.354', ' ', '0', 'BRO', 'KBRO', ' ', '-97.4245818', '25.9059317', '3426', 'KBRO', 'medium_airport', 'Brownsville South Padre Island International Airport', '25.9067993164062', '-97.4259033203125', '22', 'NA', 'US', 'US-TX', 'Brownsville', 'yes', 'KBRO', 'BRO', 'BRO', '', 'https://en.wikipedia.org/wiki/Brownsville/South_Padre_Island_International_Airport', ''),
('0', 'Aeropuerto Internacional María Montez', 'aeroway=aerodrome
ele=3
iata=BRX
icao=MDBH
name=Aeropuerto I', '71° 7'' 16.593"', '18° 15'' 6.807"', ' ', '3', 'BRX', 'MDBH', ' ', '-71.1212759', '18.2518907', '4632', 'MDBH', 'medium_airport', 'Maria Montez International Airport', '18.2514991760254', '-71.1203994750977', '10', 'NA', 'DO', 'DO-04', 'Barahona', 'yes', 'MDBH', 'BRX', '', '', 'https://en.wikipedia.org/wiki/Mar%C3%ADa_Montez_International_Airport', ''),
('0', 'Bata Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Bata, E', '9° 47'' 59.439"', '1° 54'' 6.408"', 'public', '11', 'BSG', 'FGBT', ' ', '9.7998441', '1.9017801', '2875', 'FGBT', 'medium_airport', 'Bata Airport', '1.90547', '9.80568', '13', 'AF', 'GQ', 'GQ-LI', 'Bata', 'yes', 'FGBT', 'BSG', '', '', 'https://en.wikipedia.org/wiki/Bata_Airport', ''),
('0', 'Bandara Hang Nadim', 'aeroway=aerodrome
iata=BTH
icao=WIDD
name=Bandara Hang Nadim', '104° 7'' 7.090"', '1° 7'' 17.408"', ' ', '0', 'BTH', 'WIDD', ' ', '104.1186361', '1.1215021', '26828', 'WIDD', 'medium_airport', 'Hang Nadim International Airport', '1.12102997303', '104.119003296', '126', 'AS', 'ID', 'ID-RI', 'Batam Island', 'yes', 'WIDD', 'BTH', '', '', 'https://en.wikipedia.org/wiki/Hang_Nadim_Airport', 'WIKB'),
('0', 'Gerardo Tobar López', 'aeroway=aerodrome
iata=BUN
icao=SKBU
name=Gerardo Tobar Lóp', '76° 59'' 23.649', '3° 49'' 11.669"', ' ', '0', 'BUN', 'SKBU', ' ', '-76.9899024', '3.819908', '6107', 'SKBU', 'medium_airport', 'Gerardo Tobar López Airport', '3.81963', '-76.9898', '48', 'SA', 'CO', 'CO-VAC', 'Buenaventura', 'yes', 'SKBU', 'BUN', 'BUN', '', 'https://en.wikipedia.org/wiki/Gerardo_Tobar_L%C3%B3pez_Airport', ''),
('0', 'Joshua Mqabuko Nkomo International Airport', 'aerodrome=international
aeroway=aerodrome
iata=BUQ
icao=FVBU', '28° 37'' 21.890', '20° 0'' 43.546"', ' ', '0', 'BUQ', 'FVBU', ' ', '28.6227473', '-20.0120961', '3000', 'FVBU', 'medium_airport', 'Joshua Mqabuko Nkomo International Airport', '-20.017401', '28.617901', '4359', 'AF', 'ZW', 'ZW-BU', 'Bulawayo', 'yes', 'FVJN', 'BUQ', '', '', 'https://en.wikipedia.org/wiki/Joshua_Mqabuko_Nkomo_International_Airport', ''),
('0', 'Aeroporto Internacional Aristides Pereira', 'aeroway=aerodrome
iata=BVC
icao=GVBA
name=Aeroporto Internac', '22° 53'' 19.269', '16° 8'' 14.984"', ' ', '0', 'BVC', 'GVBA', ' ', '-22.8886858', '16.1374955', '3151', 'GVBA', 'medium_airport', 'Rabil Airport', '16.1364994049072', '-22.8889007568359', '69', 'AF', 'CV', 'CV-B', 'Rabil', 'yes', 'GVBA', 'BVC', '', '', 'https://en.wikipedia.org/wiki/Rabil_Airport', 'Boa Vista Island'),
('0', 'Aeroporto Brigadeiro Camarão', 'aeroway=aerodrome
iata=BVH
icao=SBVH
name=Aeroporto Brigadei', '60° 6'' 1.936"', '12° 41'' 23.242', ' ', '0', 'BVH', 'SBVH', ' ', '-60.1005378', '-12.6897894', '5997', 'SBVH', 'medium_airport', 'Brigadeiro Camarão Airport', '-12.694399833679', '-60.098300933838', '2018', 'SA', 'BR', 'BR-RO', 'Vilhena', 'yes', 'SBVH', 'BVH', '', '', 'https://en.wikipedia.org/wiki/Vilhena_Airport', 'Vilhena Airport'),
('0', 'Birdsville Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=48.5
iata=BVI
ic', '139° 20'' 44.35', '25° 53'' 40.937', 'public', '48', 'BVI', 'YBDV', ' ', '139.345655', '-25.8947046', '26907', 'YBDV', 'medium_airport', 'Birdsville Airport', '-25.8974990844727', '139.348007202148', '159', 'OC', 'AU', 'AU-QLD', '', 'yes', 'YBDV', 'BVI', '', '', 'https://en.wikipedia.org/wiki/Birdsville_Airport', ''),
('0', 'Gautam Buddha Airport (Bhairahawa Airport)', 'aeroway=aerodrome
alt_name=Gautam Buddha airfield IVO Lumbin', '83° 25'' 0.666"', '27° 30'' 21.955', ' ', '109', 'BWA', 'VNBW', ' ', '83.4168516', '27.5060985', '26593', 'VNBW', 'medium_airport', 'Gautam Buddha International Airport', '27.505328', '83.412594', '358', 'AS', 'NP', 'NP-LU', 'Siddharthanagar (Bhairahawa)', 'yes', 'VNBW', 'BWA', '', '', 'https://en.wikipedia.org/wiki/Gautam_Buddha_Airport', 'Bhairahawa Airport'),
('0', 'Burnie Wynyard Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Burnie Airp', '145° 44'' 5.418', '40° 59'' 55.574', 'public', '19', 'BWT', 'YWYY', ' ', '145.7348382', '-40.9987706', '27184', 'YWYY', 'medium_airport', 'Wynyard Airport', '-40.9989013671875', '145.731002807617', '62', 'OC', 'AU', 'AU-TAS', 'Burnie', 'yes', 'YWYY', 'BWT', '', '', 'https://en.wikipedia.org/wiki/Burnie_Airport', ''),
('0', 'Aéroport de Bouaké', 'addr:city=Bouaké
addr:street=Route de l''aéroport
aeroway=a', '5° 4'' 6.926" W', '7° 44'' 24.840"', ' ', '375', 'BYK', 'DIBK', ' ', '-5.0685905', '7.7402334', '2097', 'DIBK', 'medium_airport', 'Bouaké Airport', '7.7388', '-5.07367', '1230', 'AF', 'CI', 'CI-04', 'Bouaké', 'yes', 'DIBK', 'BYK', '', '', 'https://en.wikipedia.org/wiki/Bouak%C3%A9_Airport', ''),
('0', 'Aeropuerto Carlos Manuel de Céspedes', 'aerodrome:type=civil
aeroway=aerodrome
iata=BYM
icao=MUBY
na', '76° 37'' 15.311', '20° 23'' 45.570', 'civil', '0', 'BYM', 'MUBY', ' ', '-76.6209197', '20.3959918', '4828', 'MUBY', 'medium_airport', 'Carlos Manuel de Cespedes Airport', '20.3964004516602', '-76.6213989257813', '203', 'NA', 'CU', 'CU-12', 'Bayamo', 'yes', 'MUBY', 'BYM', '', '', 'https://en.wikipedia.org/wiki/Carlos_Manuel_C%C3%A9spedes_Airport', ''),
('0', 'Philip S. W. Goldson International Airport', 'aeroway=aerodrome
ele=5
iata=BZE
icao=MZBZ
is_in=Belize City', '88° 18'' 29.366', '17° 32'' 10.510', ' ', '5', 'BZE', 'MZBZ', ' ', '-88.3081571', '17.5362527', '4956', 'MZBZ', 'large_airport', 'Philip S. W. Goldson International Airport', '17.5391006469727', '-88.3081970214844', '15', 'NA', 'BZ', 'BZ-BZ', 'Belize City', 'yes', 'MZBZ', 'BZE', '', 'http://www.pgiabelize.com/', 'https://en.wikipedia.org/wiki/Philip_S._W._Goldson_International_Airport', 'Belize International, Ladyville'),
('0', 'Aeroporto Municipal de Cascavel - Coronel Adalberto Mendes d', 'aeroway=aerodrome
iata=CAC
icao=SBCA
name=Aeroporto Municipa', '53° 30'' 4.202"', '25° 0'' 3.247"', ' ', '0', 'CAC', 'SBCA', ' ', '-53.5011673', '-25.0009019', '5878', 'SBCA', 'medium_airport', 'Cascavel Airport', '-25.0002994537', '-53.5008010864', '2473', 'SA', 'BR', 'BR-PR', 'Cascavel', 'yes', 'SBCA', 'CAC', '', '', 'https://en.wikipedia.org/wiki/Cascavel_Airport', 'Adalberto Mendes da Silva Airport'),
('0', 'Cap-Haitien International Airport', 'addr:country=HT
aeroway=aerodrome
city_served=Cap-Haïtien
e', '72° 12'' 1.198"', '19° 43'' 36.277', ' ', '3', 'CAP', 'MTCH', ' ', '-72.2003327', '19.7267435', '4823', 'MTCH', 'medium_airport', 'Cap Haitien International Airport', '19.726734', '-72.199576', '10', 'NA', 'HT', 'HT-ND', 'Cap Haitien', 'yes', 'MTCH', 'CAP', '', '', 'https://en.wikipedia.org/wiki/Cap-Ha%C3%AFtien_International_Airport', ''),
('0', 'Aéroport de Cayenne - Félix Éboué', 'CLC:id=FR-9731378
CLC:year=2012
aeroway=aerodrome
closest_to', '52° 21'' 43.209', '4° 49'' 10.829"', ' ', '8', 'CAY', 'SOCA', ' ', '-52.3620026', '4.8196746', '6198', 'SOCA', 'medium_airport', 'Cayenne – Félix Eboué Airport', '4.81981', '-52.360401', '26', 'SA', 'GF', 'GF-CY', 'Matoury', 'yes', 'SOCA', 'CAY', '', '', 'https://en.wikipedia.org/wiki/Cayenne_%E2%80%93_F%C3%A9lix_Ebou%C3%A9_Airport', 'Cayenne-Rochambeau, Matoury'),
('0', 'Cobar Airport', 'aerodrome:category=certified
aerodrome:type=public
aeroway=a', '145° 47'' 38.18', '31° 32'' 21.662', 'public', '0', 'CAZ', 'YCBA', ' ', '145.7939412', '-31.5393505', '26943', 'YCBA', 'medium_airport', 'Cobar Airport', '-31.5382995605469', '145.794006347656', '724', 'OC', 'AU', 'AU-NSW', '', 'yes', 'YCBA', 'CAZ', '', '', 'https://en.wikipedia.org/wiki/Cobar_Airport', ''),
('0', 'Aeropuerto Internacional Jorge Wilstermann', 'addr:street=Avenida Guillermo Killman
aerodrome=internationa', '66° 10'' 39.112', '17° 25'' 14.400', 'military/public', '2548', 'CBB', 'SLCB', ' ', '-66.1775312', '-17.4206667', '6180', 'SLCB', 'medium_airport', 'Jorge Wilsterman International Airport', '-17.4211006164551', '-66.1771011352539', '8360', 'SA', 'BO', 'BO-C', 'Cochabamba', 'yes', 'SLCB', 'CBB', '', '', 'https://en.wikipedia.org/wiki/Jorge_Wilstermann_International_Airport', ''),
('0', 'Canberra Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '149° 11'' 32.54', '35° 18'' 11.468', 'public', '575', 'CBR', 'YSCB', ' ', '149.1923724', '-35.3031855', '27131', 'YSCB', 'large_airport', 'Canberra International Airport', '-35.3069000244141', '149.195007324219', '1886', 'OC', 'AU', 'AU-ACT', 'Canberra', 'yes', 'YSCB', 'CBR', '', '', 'https://en.wikipedia.org/wiki/Canberra_International_Airport', 'RAAF Base Fairbairn'),
('0', 'Catumbela Airport', 'aeroway=aerodrome
barrier=fence
iata=CBT
icao=FNCT
name=Catu', '13° 29'' 36.093', '12° 28'' 42.823', ' ', '0', 'CBT', 'FNCT', ' ', '13.4933592', '-12.4785619', '30813', 'FNCT', 'medium_airport', 'Catumbela Airport', '-12.4792', '13.4869', '23', 'AF', 'AO', 'AO-BGU', 'Catumbela', 'yes', 'FNCT', 'CBT', '', '', 'https://en.wikipedia.org/wiki/Catumbela_Airport', ''),
('0', 'Aeropuerto Internacional Jardines del Rey', 'aeroway=aerodrome
ele=4
iata=CCC
icao=MUCC
internet_access=y', '78° 19'' 45.065', '22° 27'' 41.089', ' ', '4', 'CCC', 'MUCC', ' ', '-78.3291846', '22.4614135', '4831', 'MUCC', 'medium_airport', 'Jardines Del Rey Airport', '22.4610004425', '-78.3283996582', '13', 'NA', 'CU', 'CU-08', 'Cayo Coco', 'yes', 'MUCC', 'CCC', '', '', 'https://en.wikipedia.org/wiki/Jardines_del_Rey_Airport', 'Cayo Coco'),
('0', 'Aeroporto Diomício Freitas', 'aerodrome=public
aeroway=aerodrome
alt_name=Aeroporto de Cri', '49° 25'' 29.572', '28° 43'' 30.202', ' ', '0', 'CCM', 'SBCM', ' ', '-49.424881', '-28.725056', '5887', 'SBCM', 'medium_airport', 'Diomício Freitas Airport', '-28.7244434357', '-49.4213905334', '93', 'SA', 'BR', 'BR-SC', 'Criciúma', 'yes', 'SBCM', 'CCM', '', '', 'https://en.wikipedia.org/wiki/Diom%C3%ADcio_Freitas_Airport', 'Forquilhinha - Criciúma Airport'),
('0', 'Aeropuerto Internacional de Maiquetía Simón Bolívar', 'aeroway=aerodrome
closest_town=Caracas
ele=72
iata=CCS
icao=', '66° 59'' 38.772', '10° 36'' 4.291"', ' ', '72', 'CCS', 'SVMI', ' ', '-66.9941032', '10.601192', '6300', 'SVMI', 'large_airport', 'Simón Bolívar International Airport', '10.601194', '-66.991222', '234', 'SA', 'VE', 'VE-X', 'Caracas', 'yes', 'SVMI', 'CCS', '', 'http://www.aeropuerto-maiquetia.com.ve/web/', 'https://en.wikipedia.org/wiki/Sim%C3%B3n_Bol%C3%ADvar_International_Airport', 'Maiquetía "Simón Bolívar" International Airport'),
('0', 'Netaji Subhash Chandra Bose International Airport', 'aeroway=aerodrome
iata=CCU
icao=VECC
name=Netaji Subhash Cha', '88° 26'' 43.514', '22° 39'' 22.294', ' ', '0', 'CCU', 'VECC', ' ', '88.4454205', '22.6561928', '26496', 'VECC', 'large_airport', 'Netaji Subhash Chandra Bose International Airport', '22.6546993255615', '88.4467010498047', '16', 'AS', 'IN', 'IN-WB', 'Kolkata', 'yes', 'VECC', 'CCU', '', '', 'https://en.wikipedia.org/wiki/Netaji_Subhash_Chandra_Bose_International_Airport', 'Calcutta, Kolkatta, Dum Dum Air Force Station'),
('0', 'Mactan–Cebu International Airport', 'addr:city=Lapu-Lapu City
addr:postcode=6016
aerodrome:type=p', '123° 58'' 47.12', '10° 18'' 35.848', 'public', '0', 'CEB', 'RPVM', ' ', '123.9797555', '10.3099578', '5742', 'RPVM', 'large_airport', 'Mactan Cebu International Airport', '10.307499885559', '123.97899627686', '31', 'AS', 'PH', 'PH-CEB', 'Lapu-Lapu City', 'yes', 'RPVM', 'CEB', '', 'http://www.mactan-cebuairport.com.ph/', 'https://en.wikipedia.org/wiki/Mactan-Cebu_International_Airport', ''),
('0', 'Del Norte County Regional Airport, Jack McNamara Field', 'aeroway=aerodrome
faa=CEC
iata=CEC
icao=KCEC
name=Del Norte', '124° 14'' 11.73', '41° 46'' 53.229', ' ', '0', 'CEC', 'KCEC', ' ', '-124.2365932', '41.7814525', '3443', 'KCEC', 'medium_airport', 'Jack Mc Namara Field Airport', '41.78020096', '-124.2369995', '61', 'NA', 'US', 'US-CA', 'Crescent City', 'yes', 'KCEC', 'CEC', 'CEC', '', 'https://en.wikipedia.org/wiki/Del_Norte_County_Airport', 'Jack McNamara Field'),
('0', 'Ceduna Airport', 'aeroway=aerodrome
ele=23 m
iata=CED
icao=YCDU
name=Ceduna Ai', '133° 42'' 23.50', '32° 7'' 53.763"', ' ', '0', 'CED', 'YCDU', ' ', '133.706528', '-32.1316007', '26950', 'YCDU', 'medium_airport', 'Ceduna Airport', '-32.1305999755859', '133.710006713867', '77', 'OC', 'AU', 'AU-SA', '', 'yes', 'YCDU', 'CED', '', '', 'https://en.wikipedia.org/wiki/Ceduna_Airport', ''),
('0', 'Aeropuerto Intl. de Ciudad Obregón', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Ciudad', '109° 50'' 6.896', '27° 23'' 45.477', 'public', '62', 'CEN', 'MMCN', ' ', '-109.835249', '27.3959658', '4699', 'MMCN', 'medium_airport', 'Ciudad Obregón International Airport', '27.392599105835', '-109.833000183105', '243', 'NA', 'MX', 'MX-SON', 'Ciudad Obregón', 'yes', 'MMCN', 'CEN', '', 'http://obregon.asa.gob.mx/wb/webasa/obregon_aeropuertos', 'https://en.wikipedia.org/wiki/Ciudad_Obreg%C3%B3n_International_Airport', ''),
('0', 'Coffs Harbour Regional Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Coffs H', '153° 6'' 54.643', '30° 19'' 32.046', 'public', '6', 'CFS', 'YCFS', ' ', '153.1151785', '-30.3255682', '27132', 'YCFS', 'medium_airport', 'Coffs Harbour Airport', '-30.320601', '153.115997', '18', 'OC', 'AU', 'AU-NSW', 'Coffs Harbour', 'yes', 'YCFS', 'CFS', '', '', 'https://en.wikipedia.org/wiki/Coffs_Harbour_Airport', 'RAAF Base Coffs Harbour'),
('0', 'Aeroporto Internacional de Cuiabá - Marechal Rondon', 'aeroway=aerodrome
iata=CGB
icao=SBCY
name=Aeroporto Internac', '56° 6'' 32.126"', '15° 39'' 33.998', ' ', '0', 'CGB', 'SBCY', ' ', '-56.1089239', '-15.6594439', '5894', 'SBCY', 'medium_airport', 'Marechal Rondon Airport', '-15.6528997421', '-56.1166992188', '617', 'SA', 'BR', 'BR-MT', 'Cuiabá', 'yes', 'SBCY', 'CGB', '', 'http://www.infraero.gov.br/index.php/aeroportos/mato-grosso/aeroporto-internacional-marechal-rondon', 'https://en.wikipedia.org/wiki/Marechal_Rondon_International_Airport', ''),
('0', '常德桃花源机场', 'aeroway=aerodrome
iata=CGD
icao=ZGCD
name=常德桃花源机', '111° 38'' 15.26', '28° 55'' 7.611"', ' ', '0', 'CGD', 'ZGCD', ' ', '111.6375721', '28.9187808', '30825', 'ZGCD', 'medium_airport', 'Changde Airport', '28.9188995361', '111.63999939', '128', 'AS', 'CN', 'CN-43', 'Changde', 'yes', 'ZGCD', 'CGD', '', '', 'https://en.wikipedia.org/wiki/Changde_Taohuayuan_Airport', ''),
('0', 'Aeroporto Deputado Freitas Nobre', 'aerodrome=regional
aerodrome:type=public
aeroway=aerodrome
a', '46° 39'' 26.915', '23° 37'' 38.858', 'public', '802', 'CGH', 'SBSP', ' ', '-46.6574764', '-23.6274605', '5974', 'SBSP', 'large_airport', 'Congonhas Airport', '-23.6261100769043', '-46.6563873291016', '2631', 'SA', 'BR', 'BR-SP', 'São Paulo', 'yes', 'SBSP', 'CGH', '', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=219', 'https://en.wikipedia.org/wiki/Congonhas-S%C3%A3o_Paulo_International_Airport', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=219'),
('0', 'Bandar Udara Internasional Soekarno-Hatta', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '106° 39'' 33.35', '6° 7'' 30.965"', 'public', '10', 'CGK', 'WIII', ' ', '106.6592663', '-6.125268', '26835', 'WIII', 'large_airport', 'Soekarno-Hatta International Airport', '-6.1255698204', '106.65599823', '34', 'AS', 'ID', 'ID-BT', 'Jakarta', 'yes', 'WIII', 'CGK', '', 'http://www.jakartasoekarnohattaairport.com/', 'https://en.wikipedia.org/wiki/Soekarno-Hatta_International_Airport', 'JKT, Cengkareng, Java'),
('0', '郑州新郑国际机场', 'aerodrome:type=international
aeroway=aerodrome
alt_name=郑', '113° 51'' 11.29', '34° 31'' 37.583', 'international', '151', 'CGO', 'ZHCC', ' ', '113.8531368', '34.5271063', '27199', 'ZHCC', 'large_airport', 'Zhengzhou Xinzheng International Airport', '34.5196990967', '113.841003418', '495', 'AS', 'CN', 'CN-41', 'Zhengzhou', 'yes', 'ZHCC', 'CGO', '', '', 'https://en.wikipedia.org/wiki/Zhengzhou_Xinzheng_International_Airport', 'Xinzheng Airport'),
('0', '長春龍嘉國際機場', 'aerodrome:type=international
aeroway=aerodrome
iata=CGQ
icao', '125° 41'' 8.141', '43° 59'' 47.315', 'international', '0', 'CGQ', 'ZYCC', 'Аэропорт Лунцзя', '125.6855947', '43.9964765', '27237', 'ZYCC', 'medium_airport', 'Longjia Airport', '43.9962005615', '125.684997559', '706', 'AS', 'CN', 'CN-22', 'Changchun', 'yes', 'ZYCC', 'CGQ', '', '', 'https://en.wikipedia.org/wiki/Changchun_Longjia_International_Airport', ''),
('0', 'Aeroporto Internacional de Campo Grande', 'aerodrome:type=military/public
aeroway=aerodrome
ele=559
iat', '54° 40'' 54.689', '20° 28'' 15.584', 'military/public', '559', 'CGR', 'SBCG', 'Международный аэропорт Кампу', '-54.6818581', '-20.4709955', '5883', 'SBCG', 'medium_airport', 'Campo Grande Airport', '-20.4687004089', '-54.6725006104', '1833', 'SA', 'BR', 'BR-MS', 'Campo Grande', 'yes', 'SBCG', 'CGR', '', '', 'https://en.wikipedia.org/wiki/Campo_Grande_International_Airport', ''),
('0', 'Christchurch International Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '172° 32'' 10.79', '43° 29'' 5.874"', 'international', '0', 'CHC', 'NZCH', ' ', '172.5363326', '-43.484965', '5026', 'NZCH', 'large_airport', 'Christchurch International Airport', '-43.4893989562988', '172.531997680664', '123', 'OC', 'NZ', 'NZ-CAN', 'Christchurch', 'yes', 'NZCH', 'CHC', '', 'http://www.christchurchairport.co.nz/', 'https://en.wikipedia.org/wiki/Christchurch_International_Airport', ''),
('0', 'Aeropuerto Tnte. FAP Jaime Montreuil Morales', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Chimbote Ai', '78° 31'' 27.571', '9° 9'' 4.722" S', 'public', '21', 'CHM', 'SPEO', ' ', '-78.5243253', '-9.1513117', '6208', 'SPEO', 'medium_airport', 'Teniente FAP Jaime A De Montreuil Morales Airport', '-9.14960956573486', '-78.5238037109375', '69', 'SA', 'PE', 'PE-ANC', 'Chimbote', 'yes', 'SPEO', 'CHM', '', '', 'https://en.wikipedia.org/wiki/Tnte._FAP_Jaime_Montreuil_Morales_Airport', ''),
('0', 'Inia William Tuuta Memorial Airport', 'LINZ:source_version=V16
aerodrome:type=private
aeroway=aerod', '176° 27'' 53.69', '43° 48'' 49.760', 'private', '13', 'CHT', 'NZCI', ' ', '-176.4649154', '-43.8138222', '5027', 'NZCI', 'medium_airport', 'Chatham Islands-Tuuta Airport', '-43.810001373291', '-176.457000732422', '43', 'OC', 'NZ', 'NZ-WGN', 'Waitangi', 'yes', 'NZCI', 'CHT', '', '', 'https://en.wikipedia.org/wiki/Chatham_Islands_/_Tuuta_Airport', 'Karewa'),
('0', 'Aeropuerto Internacional Aníbal Arab Fadul', 'aerodrome=international
aeroway=aerodrome
iata=CIJ
icao=SLCO', '68° 46'' 55.660', '11° 2'' 14.611"', ' ', '0', 'CIJ', 'SLCO', ' ', '-68.7821278', '-11.0373919', '6182', 'SLCO', 'medium_airport', 'Capitán Aníbal Arab Airport', '-11.0403995514', '-68.7829971313', '889', 'SA', 'BO', 'BO-N', 'Cobija', 'yes', 'SLCO', 'CIJ', '', '', 'https://en.wikipedia.org/wiki/Cap._Anibal_Arab_Airport', ''),
('0', 'Аэропорт Шымкент', 'aerodrome:type=civil
aeroway=aerodrome
closest_town=Shymkent', '69° 28'' 31.381', '42° 21'' 54.225', 'civil', '422', 'CIT', 'UAII', 'Аэропорт Шымкент', '69.4753835', '42.3650624', '6429', 'UAII', 'medium_airport', 'Shymkent Airport', '42.364200592041', '69.4788970947266', '1385', 'AS', 'KZ', 'KZ-YUZ', 'Shymkent', 'yes', 'UAII', 'CIT', '', '', 'https://en.wikipedia.org/wiki/Shymkent_Airport', 'Chimkent Airport, Аэропорт Шымкент, Аэропорт Чимкент'),
('0', 'Aeropuerto Internacional Capitán FAP José A. Quiñones', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Chiclay', '79° 49'' 37.168', '6° 47'' 22.322"', 'public', '30', 'CIX', 'SPHI', ' ', '-79.8269911', '-6.7895339', '6213', 'SPHI', 'medium_airport', 'Capitan FAP Jose A Quinones Gonzales International Airport', '-6.78747987747192', '-79.8281021118164', '97', 'SA', 'PE', 'PE-LAM', 'Chiclayo', 'yes', 'SPHI', 'CIX', '', '', 'https://en.wikipedia.org/wiki/Cap._FAP_Jos%C3%A9_A._Qui%C3%B1ones_Gonzales_International_Airport', ''),
('0', 'My. Gral. FAP. Armando Revoredo Iglesias Airport', 'aeroway=aerodrome
closest_town=Cajamarca
ele=2676
iata=CJA
i', '78° 29'' 21.667', '7° 8'' 33.449"', ' ', '2676', 'CJA', 'SPJR', ' ', '-78.489352', '-7.1426247', '6223', 'SPJR', 'medium_airport', 'Mayor General FAP Armando Revoredo Iglesias Airport', '-7.13918018341064', '-78.4894027709961', '8781', 'SA', 'PE', 'PE-CAJ', 'Cajamarca', 'yes', 'SPJR', 'CJA', '', '', 'https://en.wikipedia.org/wiki/My._Gral._FAP._Armando_Revoredo_Iglesias_Airport', ''),
('0', 'Coimbatore International Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Coimbatore', '77° 2'' 38.827"', '11° 1'' 52.964"', 'public', '396', 'CJB', 'VOCB', ' ', '77.0441185', '11.0313789', '26607', 'VOCB', 'medium_airport', 'Coimbatore International Airport', '11.029999733', '77.0434036255', '1324', 'AS', 'IN', 'IN-TN', 'Coimbatore', 'yes', 'VOCB', 'CJB', '', 'http://www.airportsindia.org.in/allAirports/coimbatore_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Coimbatore_Airport', ''),
('0', 'Aeropuerto Internacional El Loa', 'aeroway=aerodrome
ele=2299
iata=CJC
icao=SCCF
name=Aeropuert', '68° 53'' 53.289', '22° 29'' 55.152', ' ', '2299', 'CJC', 'SCCF', ' ', '-68.8981357', '-22.4986534', '6007', 'SCCF', 'medium_airport', 'El Loa Airport', '-22.4981994628906', '-68.9036026000977', '7543', 'SA', 'CL', 'CL-AN', 'Calama', 'yes', 'SCCF', 'CJC', '', '', '', ''),
('0', 'چترال ہوائی اڈا', 'aeroway=aerodrome
iata=CJL
icao=OPCH
name=چترال ہوا', '71° 47'' 59.495', '35° 53'' 10.806', ' ', '0', 'CJL', 'OPCH', 'Аэродром Читрал', '71.7998598', '35.8863351', '5248', 'OPCH', 'medium_airport', 'Chitral Airport', '35.886213', '71.799922', '4920', 'AS', 'PK', 'PK-KP', 'Chitral', 'yes', 'OPCH', 'CJL', '', '', 'https://en.wikipedia.org/wiki/Chitral_Airport', ''),
('0', '重庆江北国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=CKG
icao', '106° 39'' 13.69', '29° 43'' 33.932', 'international', '0', 'CKG', 'ZUCK', ' ', '106.6538053', '29.7260922', '27228', 'ZUCK', 'large_airport', 'Chongqing Jiangbei International Airport', '29.7192001342773', '106.641998291016', '1365', 'AS', 'CN', 'CN-50', 'Chongqing', 'yes', 'ZUCK', 'CKG', '', '', 'https://en.wikipedia.org/wiki/Chongqing_Jiangbei_International_Airport', ''),
('0', 'Aeroporto de Carajás', 'aerodrome:type=public
aeroway=aerodrome
anac=3204/2013
anac:', '50° 0'' 6.189"', '6° 7'' 2.833" S', 'public', '0', 'CKS', 'SBCJ', ' ', '-50.0017192', '-6.1174536', '5886', 'SBCJ', 'medium_airport', 'Carajás Airport', '-6.11527776718', '-50.0013885498', '2064', 'SA', 'BR', 'BR-PA', 'Parauapebas', 'yes', 'SBCJ', 'CKS', '', 'http://www.infraero.gov.br/index.php/us/airports/para/carajas-airport.html', 'https://en.wikipedia.org/wiki/Caraj%C3%A1s_Airport', ''),
('0', 'Aéroport de Conakry-Gbessia', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '13° 36'' 45.546', '9° 34'' 35.195"', 'public', '22', 'CKY', 'GUCY', ' ', '-13.6126517', '9.576443', '3146', 'GUCY', 'medium_airport', 'Conakry International Airport', '9.57689', '-13.612', '72', 'AF', 'GN', 'GN-C', 'Conakry', 'yes', 'GUCY', 'CKY', '', 'http://www.aeroport-conakry.com/aeroport_international_de_conakry.php', 'https://en.wikipedia.org/wiki/Conakry_International_Airport', 'Gbessia International Airport'),
('0', 'Alfonso Bonilla Aragón', 'aerodrome=international
aeroway=aerodrome
alt_name=Palmaseca', '76° 23'' 3.690"', '3° 32'' 34.691"', ' ', '0', 'CLO', 'SKCL', ' ', '-76.3843584', '3.5429698', '6110', 'SKCL', 'medium_airport', 'Alfonso Bonilla Aragon International Airport', '3.54322', '-76.3816', '3162', 'SA', 'CO', 'CO-VAC', 'Cali', 'yes', 'SKCL', 'CLO', 'CLO', 'https://www.aerocali.com.co/', 'https://en.wikipedia.org/wiki/Alfonso_Bonilla_Arag%C3%B3n_International_Airport', 'Palmaseca International, 02-20'),
('0', 'Aeropuerto Nacional Licenciado Miguel de la Madrid', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '103° 34'' 39.66', '19° 16'' 39.094', 'public', '752', 'CLQ', 'MMIA', ' ', '-103.577685', '19.2775261', '4714', 'MMIA', 'medium_airport', 'Licenciado Miguel de la Madrid Airport', '19.277', '-103.577002', '2467', 'NA', 'MX', 'MX-COL', 'Colima', 'yes', 'MMIA', 'CLQ', 'COL', '', 'https://en.wikipedia.org/wiki/Lic._Miguel_de_la_Madrid_Airport', 'Lic. Miguel de la Madrid Airport, Colima Airport'),
('0', 'Cunnamulla Aerodrome', 'aeroway=aerodrome
iata=CMA
icao=YCMU
name=Cunnamulla Aerodro', '145° 37'' 29.08', '28° 2'' 1.537"', ' ', '0', 'CMA', 'YCMU', ' ', '145.6247444', '-28.0337603', '26957', 'YCMU', 'medium_airport', 'Cunnamulla Airport', '-28.0300006866455', '145.621994018555', '630', 'OC', 'AU', 'AU-QLD', '', 'yes', 'YCMU', 'CMA', '', '', 'https://en.wikipedia.org/wiki/Cunnamulla_Airport', ''),
('0', 'Bandaranaike International Airport', 'aerodrome=international
aeroway=aerodrome
ele=8
iata=CMB
ica', '79° 53'' 5.031"', '7° 10'' 48.074"', ' ', '8', 'CMB', 'VCBI', 'Международный аэропорт Банда', '79.8847309', '7.1800206', '26463', 'VCBI', 'large_airport', 'Bandaranaike International Colombo Airport', '7.1807599067688', '79.8841018676758', '30', 'AS', 'LK', 'LK-1', 'Colombo', 'yes', 'VCBI', 'CMB', '', 'http://www.airport.lk/', 'https://en.wikipedia.org/wiki/Bandaranaike_International_Airport', 'RAF Negombo, Katunayake International Airport'),
('0', 'Ciudad del Carmen International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Ciudad', '91° 47'' 59.663', '18° 39'' 6.349"', 'public', '3', 'CME', 'MMCE', ' ', '-91.7999065', '18.6517635', '4694', 'MMCE', 'medium_airport', 'Ciudad del Carmen International Airport', '18.6536998748779', '-91.7990036010742', '10', 'NA', 'MX', 'MX-CAM', 'Ciudad del Carmen', 'yes', 'MMCE', 'CME', '', '', 'https://en.wikipedia.org/wiki/Ciudad_del_Carmen_International_Airport', ''),
('0', 'Aeroporto Internacional de Corumbá', 'aeroway=aerodrome
iata=CMG
icao=SBCR
name=Aeroporto Internac', '57° 40'' 23.333', '19° 0'' 45.284"', ' ', '0', 'CMG', 'SBCR', ' ', '-57.673148', '-19.0125788', '5890', 'SBCR', 'medium_airport', 'Corumbá International Airport', '-19.0119438171', '-57.6713905334', '461', 'SA', 'BR', 'BR-MS', 'Corumbá', 'yes', 'SBCR', 'CMG', '', '', 'https://en.wikipedia.org/wiki/Corumb%C3%A1_International_Airport', ''),
('0', 'Chimbu Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=1516
iata=CMU
ic', '144° 58'' 16.58', '6° 1'' 23.833"', 'public', '1516', 'CMU', 'AYCH', ' ', '144.9712721', '-6.023287', '54', 'AYCH', 'medium_airport', 'Chimbu Airport', '-6.02429008483887', '144.970993041992', '4974', 'OC', 'PG', 'PG-CPK', 'Kundiawa', 'yes', 'AYCH', 'CMU', '', '', 'https://en.wikipedia.org/wiki/Chimbu_Airport', ''),
('0', 'Aeroporto Internacional Tancredo Neves', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeroporto d', '43° 58'' 6.276"', '19° 38'' 0.808"', 'public', '828', 'CNF', 'SBCF', ' ', '-43.9684099', '-19.6335579', '5882', 'SBCF', 'large_airport', 'Tancredo Neves International Airport', '-19.6244430541992', '-43.9719429016113', '2715', 'SA', 'BR', 'BR-MG', 'Belo Horizonte', 'yes', 'SBCF', 'CNF', '', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=207', 'https://en.wikipedia.org/wiki/Tancredo_Neves_International_Airport', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=207'),
('0', 'Cloncurry Aerodrome', 'aeroway=aerodrome
iata=CNJ
icao=YCCY
name=Cloncurry Aerodrom', '140° 30'' 31.51', '20° 40'' 7.118"', ' ', '0', 'CNJ', 'YCCY', ' ', '140.5087544', '-20.668644', '26948', 'YCCY', 'medium_airport', 'Cloncurry Airport', '-20.6686000824', '140.503997803', '616', 'OC', 'AU', 'AU-QLD', 'Cloncurry', 'yes', 'YCCY', 'CNJ', '', '', 'https://en.wikipedia.org/wiki/Cloncurry_Airport', ''),
('0', 'Cairns Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=3
iata=CNS
icao=', '145° 45'' 3.746', '16° 52'' 40.770', 'public', '3', 'CNS', 'YBCS', ' ', '145.7510405', '-16.8779918', '26904', 'YBCS', 'medium_airport', 'Cairns International Airport', '-16.885799408', '145.755004883', '10', 'OC', 'AU', 'AU-QLD', 'Cairns', 'yes', 'YBCS', 'CNS', '', '', 'https://en.wikipedia.org/wiki/Cairns_International_Airport', ''),
('0', 'ท่าอากาศยานนานาชาติเ', 'aerodrome=international
aeroway=aerodrome
iata=CNX
icao=VTCC', '98° 57'' 53.507', '18° 46'' 3.694"', ' ', '0', 'CNX', 'VTCC', ' ', '98.9648631', '18.7676929', '26647', 'VTCC', 'large_airport', 'Chiang Mai International Airport', '18.7667999268', '98.962600708', '1036', 'AS', 'TH', 'TH-50', 'Chiang Mai', 'yes', 'VTCC', 'CNX', '', '', 'https://en.wikipedia.org/wiki/Chiang_Mai_International_Airport', ''),
('0', 'Aeropuerto Internacional Ingeniero Ambrosio Taravella', 'aeroway=aerodrome
alt_name=Aeropuerto Internacional de Córd', '64° 12'' 22.532', '31° 18'' 12.591', ' ', '489', 'COR', 'SACO', ' ', '-64.2062589', '-31.3034975', '5773', 'SACO', 'medium_airport', 'Ingeniero Ambrosio Taravella Airport', '-31.323601', '-64.208', '1604', 'SA', 'AR', 'AR-X', 'Cordoba', 'yes', 'SACO', 'COR', 'CBA', '', 'https://en.wikipedia.org/wiki/Ingeniero_Ambrosio_L.V._Taravella_International_Airport', ''),
('0', 'Ing. Alberto Acuña Ongay International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Campech', '90° 30'' 3.122"', '19° 48'' 54.350', 'public', '10', 'CPE', 'MMCP', ' ', '-90.5008671', '19.8150971', '4700', 'MMCP', 'medium_airport', 'Ingeniero Alberto Acuña Ongay International Airport', '19.8167991638', '-90.5002975464', '34', 'NA', 'MX', 'MX-CAM', 'Campeche', 'yes', 'MMCP', 'CPE', '', '', 'https://en.wikipedia.org/wiki/Ing._Alberto_Acu%C3%B1a_Ongay_International_Airport', ''),
('0', 'Aeropuerto Desierto de Atacama', 'aerodrome:type=public
aeroway=aerodrome
ele=204
iata=CPO
ica', '70° 46'' 24.559', '27° 15'' 42.778', 'public', '204', 'CPO', 'SCAT', ' ', '-70.7734885', '-27.2618829', '6002', 'SCAT', 'medium_airport', 'Desierto de Atacama Airport', '-27.2611999512', '-70.7791976929', '670', 'SA', 'CL', 'CL-AT', 'Copiapo', 'yes', 'SCAT', 'CPO', '', '', 'https://en.wikipedia.org/wiki/Desierto_de_Atacama_Airport', ''),
('0', 'Cape Town International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '18° 36'' 14.855', '33° 58'' 18.107', 'public', '46', 'CPT', 'FACT', 'Международный аэропорт Кейпт', '18.6041265', '-33.9716964', '2775', 'FACT', 'large_airport', 'Cape Town International Airport', '-33.9648017883', '18.6016998291', '151', 'AF', 'ZA', 'ZA-WC', 'Cape Town', 'yes', 'FACT', 'CPT', '', 'http://www.airports.co.za/home.asp?pid=229', 'https://en.wikipedia.org/wiki/Cape_Town_International_Airport', 'Kaapstad Internasionale Lughawe, CTIA, DF Malan Airport'),
('0', 'Aeropuerto Internacional General Mosconi', 'aeroway=aerodrome
iata=CRD
icao=SAVC
name=Aeropuerto Interna', '67° 27'' 54.285', '45° 46'' 47.202', ' ', '0', 'CRD', 'SAVC', ' ', '-67.4650793', '-45.7797784', '5822', 'SAVC', 'medium_airport', 'General E. Mosconi Airport', '-45.7853', '-67.4655', '189', 'SA', 'AR', 'AR-U', 'Comodoro Rivadavia', 'yes', 'SAVC', 'CRD', 'CRV', '', '', ''),
('0', 'Colonel Hill Airport', 'aeroway=aerodrome
area=yes
ele=2
iata=CRI
icao=MYCI
is_in=Co', '74° 10'' 54.488', '22° 44'' 46.884', ' ', '2', 'CRI', 'MYCI', ' ', '-74.1818021', '22.7463568', '4938', 'MYCI', 'medium_airport', 'Colonel Hill Airport', '22.7456', '-74.182404', '5', 'NA', 'BS', 'BS-CK', 'Colonel Hill', 'yes', 'MYCI', 'CRI', '', '', 'https://en.wikipedia.org/wiki/Colonial_Hill_Airport', 'Crooked Island Airport'),
('0', 'Corpus Christi International Airport', 'addr:city=Corpus Christi
addr:housenumber=1000
addr:postcode', '97° 30'' 37.883', '27° 46'' 14.895', ' ', '46', 'CRP', 'KCRP', ' ', '-97.5105231', '27.7708041', '3466', 'KCRP', 'large_airport', 'Corpus Christi International Airport', '27.7703990936279', '-97.5011978149414', '44', 'NA', 'US', 'US-TX', 'Corpus Christi', 'yes', 'KCRP', 'CRP', 'CRP', '', 'https://en.wikipedia.org/wiki/Corpus_Christi_International_Airport', ''),
('0', 'Türkmenabat Airport', 'aeroway=aerodrome
iata=CRZ
icao=UTAV
int_name=Turkmenabat Ai', '63° 36'' 34.621', '39° 5'' 5.873"', ' ', '0', 'CRZ', 'UTAV', 'Аэропорт Туркменабад', '63.609617', '39.0849647', '26384', 'UTAV', 'medium_airport', 'Türkmenabat International Airport', '38.930662', '63.563982', '649', 'AS', 'TM', 'TM-L', 'Türkmenabat', 'yes', 'UTAV', 'CRZ', '', '', 'https://en.wikipedia.org/wiki/Turkmenabat_International_Airport', 'Turkmenabad Airport, Chardzhou Airport, Аэропорт Туркменабат, Аэропорт Туркменабад , Аэропорт Чарджоу, چهارجوی'),
('0', 'Aéroport de Cap Skirring', 'aerodrome:type=public
aeroway=aerodrome
ele=16
iata=CSK
icao', '16° 44'' 53.913', '12° 23'' 40.986', 'public', '16', 'CSK', 'GOGS', ' ', '-16.7483091', '12.3947184', '3123', 'GOGS', 'medium_airport', 'Cap Skirring Airport', '12.39533', '-16.748', '52', 'AF', 'SN', 'SN-ZG', 'Cap Skirring', 'yes', 'GOGS', 'CSK', '', '', 'https://en.wikipedia.org/wiki/Cap_Skirring_Airport', ''),
('0', 'Aeropuerto Coronel Felipe Varela', 'addr:city=Los Puestos, Valle Viejo
addr:housenumber=Km. 22
a', '65° 45'' 4.606"', '28° 35'' 34.065', ' ', '0', 'CTC', 'SANC', ' ', '-65.7512794', '-28.5927958', '5789', 'SANC', 'medium_airport', 'Catamarca Airport', '-28.5956001282', '-65.751701355', '1522', 'SA', 'AR', 'AR-K', 'Catamarca', 'yes', 'SANC', 'CTC', 'CAT', '', '', ''),
('0', 'Rafael Núñez', 'aeroway=aerodrome
closest_town=Cartagena
ele=1
iata=CTG
icao', '75° 30'' 48.363', '10° 26'' 32.595', ' ', '1', 'CTG', 'SKCG', ' ', '-75.5134342', '10.4423875', '6109', 'SKCG', 'medium_airport', 'Rafael Nuñez International Airport', '10.4424', '-75.513', '4', 'SA', 'CO', 'CO-BOL', 'Cartagena', 'yes', 'SKCG', 'CTG', 'CTG', '', 'https://en.wikipedia.org/wiki/Rafael_N%C3%BA%C3%B1ez_International_Airport', ''),
('0', 'Aeropuerto Internacional de Chetumal', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Chetuma', '88° 19'' 30.669', '18° 30'' 17.873', 'public', '12', 'CTM', 'MMCM', ' ', '-88.3251859', '18.5049647', '4698', 'MMCM', 'medium_airport', 'Chetumal International Airport', '18.5046997070312', '-88.3267974853516', '39', 'NA', 'MX', 'MX-ROO', 'Chetumal', 'yes', 'MMCM', 'CTM', '', 'http://chetumal.asa.gob.mx/wb/webasa/chetumal_aeropuertos', 'https://en.wikipedia.org/wiki/Chetumal_International_Airport', ''),
('0', '新千歳空港', 'KSJ2:AAC=01224
KSJ2:AAC_label=北海道千歳市
KSJ2:AD2=1
', '141° 40'' 59.50', '42° 47'' 2.440"', ' ', '0', 'CTS', 'RJCC', 'Аэропорт Новый Читос', '141.6831949', '42.7840111', '5544', 'RJCC', 'large_airport', 'New Chitose Airport', '42.7752', '141.692001', '82', 'AS', 'JP', 'JP-01', 'Sapporo', 'yes', 'RJCC', 'CTS', '', 'http://www.new-chitose-airport.jp/language/english/', 'https://en.wikipedia.org/wiki/New_Chitose_Airport', ''),
('0', 'Camilo Daza', 'aeroway=aerodrome
barrier=fence
closest_town=Cúcuta
ele=334', '72° 30'' 47.808', '7° 55'' 43.253"', ' ', '334', 'CUC', 'SKCC', ' ', '-72.5132799', '7.9286813', '6108', 'SKCC', 'medium_airport', 'Camilo Daza International Airport', '7.92757', '-72.5115', '1096', 'SA', 'CO', 'CO-NSA', 'Cúcuta', 'yes', 'SKCC', 'CUC', 'CUC', '', 'https://en.wikipedia.org/wiki/Camilo_Daza_International_Airport', ''),
('0', 'Aeropuerto Mariscal Lamar', 'aeroway=aerodrome
iata=CUE
icao=SECU
name=Aeropuerto Marisca', '78° 59'' 4.832"', '2° 53'' 21.884"', ' ', '0', 'CUE', 'SECU', ' ', '-78.9846755', '-2.8894123', '6054', 'SECU', 'medium_airport', 'Mariscal Lamar Airport', '-2.88947', '-78.984398', '8306', 'SA', 'EC', 'EC-A', 'Cuenca', 'yes', 'SECU', 'CUE', '', '', 'https://en.wikipedia.org/wiki/Mariscal_Lamar_International_Airport', ''),
('0', 'Aeropuerto internacional de Cancun', 'aeroway=aerodrome
iata=CUN
icao=MMUN
internet_access=wlan
is', '86° 52'' 33.407', '21° 2'' 26.638"', ' ', '0', 'CUN', 'MMUN', ' ', '-86.8759463', '21.0407327', '4762', 'MMUN', 'large_airport', 'Cancún International Airport', '21.0365009308', '-86.8770980835', '22', 'NA', 'MX', 'MX-ROO', 'Cancún', 'yes', 'MMUN', 'CUN', '', 'http://www.asur.com.mx/asur/ingles/aeropuertos/cancun/cancun.asp', 'https://en.wikipedia.org/wiki/Canc%C3%BAn_International_Airport', ''),
('0', 'Coen Airport', 'aeroway=aerodrome
iata=CUQ
icao=YCOE
name=Coen Airport
sourc', '143° 6'' 58.948', '13° 45'' 45.431', ' ', '0', 'CUQ', 'YCOE', ' ', '143.1163745', '-13.7626197', '26961', 'YCOE', 'medium_airport', 'Coen Airport', '-13.761133', '143.113311', '532', 'OC', 'AU', 'AU-QLD', 'Coen', 'yes', 'YCOE', 'CUQ', '', '', 'https://en.wikipedia.org/wiki/Coen_Airport', ''),
('0', 'Curaçao International Airport', 'aeroway=aerodrome
alt_name=Hato International Airport
iata=C', '68° 57'' 31.319', '12° 11'' 20.581', ' ', '0', 'CUR', 'TNCC', ' ', '-68.9586996', '12.1890504', '6404', 'TNCC', 'large_airport', 'Hato International Airport', '12.1889', '-68.959801', '29', 'NA', 'CW', 'CW-U-A', 'Willemstad', 'yes', 'TNCC', 'CUR', '', '', 'https://en.wikipedia.org/wiki/Hato_International_Airport', 'Curaçao'),
('0', 'Aeropuerto General Mariano Matamoros', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Cuernav', '99° 15'' 38.447', '18° 49'' 58.700', 'public', '1304', 'CVJ', 'MMCB', ' ', '-99.2606796', '18.8329722', '4693', 'MMCB', 'medium_airport', 'General Mariano Matamoros (Cuernavaca) Airport', '18.834801', '-99.261299', '4277', 'NA', 'MX', 'MX-MOR', 'Temixco', 'yes', 'MMCB', 'CVJ', '', '', 'https://en.wikipedia.org/wiki/General_Mariano_Matamoros_Airport', ''),
('0', 'General Pedro J. Méndez International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Ciudad', '98° 57'' 24.840', '23° 42'' 20.160', 'public', '232', 'CVM', 'MMCV', ' ', '-98.9569', '23.7056', '4704', 'MMCV', 'medium_airport', 'General Pedro Jose Mendez International Airport', '23.7033004761', '-98.9564971924', '761', 'NA', 'MX', 'MX-TAM', 'Ciudad Victoria', 'yes', 'MMCV', 'CVM', '', '', 'https://en.wikipedia.org/wiki/General_Pedro_J._M%C3%A9ndez_International_Airport', ''),
('0', 'Carnarvon Airport', 'aerodrome=regional
aerodrome:type=public
aeroway=aerodrome
i', '113° 40'' 18.58', '24° 52'' 56.719', 'public', '0', 'CVQ', 'YCAR', ' ', '113.67183', '-24.882422', '26941', 'YCAR', 'medium_airport', 'Carnarvon Airport', '-24.880211', '113.67174', '13', 'OC', 'AU', 'AU-WA', 'Carnarvon', 'yes', 'YCAR', 'CVQ', '', '', 'https://en.wikipedia.org/wiki/Carnarvon_Airport_(Western_Australia)', ''),
('0', 'Aeroporto Internacional de Curitiba', 'aeroway=aerodrome
alt_name=Afonso Pena
ele=911
iata=CWB
icao', '49° 10'' 23.611', '25° 31'' 58.145', ' ', '911', 'CWB', 'SBCT', 'Аэропорт Афонсу Пена', '-49.1732252', '-25.5328181', '5891', 'SBCT', 'large_airport', 'Afonso Pena Airport', '-25.5284996033', '-49.1758003235', '2988', 'SA', 'BR', 'BR-PR', 'Curitiba', 'yes', 'SBCT', 'CWB', '', '', 'https://en.wikipedia.org/wiki/Afonso_Pena_International_Airport', ''),
('0', 'Coxs Bazar Airport', 'aeroway=aerodrome
iata=CXB
icao=VGCB
name=Coxs Bazar Airport', '91° 57'' 52.408', '21° 27'' 10.265', ' ', '0', 'CXB', 'VGCB', ' ', '91.9645579', '21.4528514', '26526', 'VGCB', 'medium_airport', 'Cox''s Bazar Airport', '21.4521999359131', '91.9638977050781', '12', 'AS', 'BD', 'BD-2', 'Cox''s Bazar', 'yes', 'VGCB', 'CXB', '', '', 'https://en.wikipedia.org/wiki/Cox''s_Bazar_Airport', ''),
('0', 'Aeroporto Regional de Caxias do Sul - Hugo Cantergiani', 'aerodrome=domestic
aerodrome:type=public
aeroway=aerodrome
a', '51° 11'' 17.529', '29° 11'' 49.568', 'public', '754', 'CXJ', 'SBCX', ' ', '-51.1882025', '-29.1971021', '5893', 'SBCX', 'medium_airport', 'Hugo Cantergiani Regional Airport', '-29.1970996857', '-51.1875', '2472', 'SA', 'BR', 'BR-RS', 'Caxias Do Sul', 'yes', 'SBCX', 'CXJ', '', '', 'https://en.wikipedia.org/wiki/Caxias_do_Sul_Airport', 'Campo dos Bugres Airport, Caxias do Sul Airport'),
('0', 'Sân bay quốc tế Cam Ranh', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=C', '109° 13'' 11.14', '11° 59'' 54.533', 'military/public', '12', 'CXR', 'VVCR', 'Международный аэропорт Камра', '109.2197633', '11.9984815', '26693', 'VVCR', 'medium_airport', 'Cam Ranh International Airport / Cam Ranh Air Base', '11.9982', '109.219002', '40', 'AS', 'VN', 'VN-34', 'Cam Ranh', 'yes', 'VVCR', 'CXR', '', '', 'https://en.wikipedia.org/wiki/Cam_Ranh_International_Airport', ''),
('0', 'Aéroport Antoine Simon des Cayes', 'aeroway=aerodrome
alt_name:en=Les Cayes Airport
alt_name:fr=', '73° 47'' 16.966', '18° 16'' 15.803', ' ', '2', 'CYA', 'MTCA', ' ', '-73.788046', '18.2710563', '4822', 'MTCA', 'medium_airport', 'Les Cayes Airport', '18.2710990905762', '-73.7882995605469', '203', 'NA', 'HT', 'HT-SD', 'Les Cayes', 'yes', 'MTCA', 'CYA', '', '', 'https://en.wikipedia.org/wiki/Les_Cayes_Airport', ''),
('0', 'Charles Kirkconnell International Airport', 'addr:city=West End
addr:country=KY
addr:postcode=KY-2-2400
a', '79° 52'' 45.808', '19° 41'' 23.590', 'civil', '2', 'CYB', 'MWCB', ' ', '-79.879391', '19.689886', '4861', 'MWCB', 'medium_airport', 'Gerrard Smith International Airport', '19.6870002746582', '-79.8827972412109', '8', 'NA', 'KY', 'KY-U-A', 'Cayman Brac', 'yes', 'MWCB', 'CYB', '', '', 'https://en.wikipedia.org/wiki/Gerrard_Smith_International_Airport', ''),
('0', '嘉義水上機場', 'aeroway=aerodrome
alt_name:vi=Sân bay Thủy Thượng;Sân', '120° 23'' 33.06', '23° 27'' 33.274', ' ', '0', 'CYI', 'RCKU', ' ', '120.3925172', '23.4592427', '5517', 'RCKU', 'medium_airport', 'Chiayi Airport', '23.461799621582', '120.392997741699', '85', 'AS', 'TW', 'TW-CYQ', 'Chiayi City', 'yes', 'RCKU', 'CYI', '', 'http://www.cya.gov.tw/web/english/index.asp', 'https://en.wikipedia.org/wiki/Chiayi_Airport', 'Shueishang Airport, 嘉義航空站, 水上機場'),
('0', 'Aeropuerto Vitalio Acuña', 'aeroway=aerodrome
closest_town=Cayo Largo del Sur
ele=3
iata', '81° 32'' 41.974', '21° 36'' 57.099', ' ', '3', 'CYO', 'MUCL', ' ', '-81.5449928', '21.6158608', '4833', 'MUCL', 'medium_airport', 'Vilo Acuña International Airport', '21.6165008545', '-81.5459976196', '10', 'NA', 'CU', 'CU-99', 'Cayo Largo del Sur', 'yes', 'MUCL', 'CYO', '', '', 'https://en.wikipedia.org/wiki/Vilo_Acu%C3%B1a_Airport', ''),
('0', 'Aeropuerto Nacional Capitán Rogelio Castillo', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '100° 53'' 12.37', '20° 32'' 44.614', 'public', '1740', 'CYW', 'MMCY', ' ', '-100.8867695', '20.5457262', '4705', 'MMCY', 'medium_airport', 'Captain Rogelio Castillo National Airport', '20.546', '-100.887001', '5709', 'NA', 'MX', 'MX-GUA', 'Celaya', 'yes', 'MMCY', 'CYW', 'CYA', '', 'https://en.wikipedia.org/wiki/Captain_Rogelio_Castillo_National_Airport', ''),
('0', 'Cauayan Airport', 'aeroway=aerodrome
iata=CYZ
icao=RPUY
name=Cauayan Airport
op', '121° 45'' 7.464', '16° 55'' 49.540', ' ', '0', 'CYZ', 'RPUY', ' ', '121.7520734', '16.9304278', '5730', 'RPUY', 'medium_airport', 'Cauayan Airport', '16.9298992157', '121.752998352', '200', 'AS', 'PH', 'PH-ISA', 'Cauayan City', 'yes', 'RPUY', 'CYZ', '', '', 'https://en.wikipedia.org/wiki/Cauayan_Airport', ''),
('0', 'Aeropuerto José Leonardo Chirino', 'aeroway=aerodrome
iata=CZE
icao=SVCR
name=Aeropuerto José L', '69° 41'' 6.535"', '11° 24'' 57.947', ' ', '0', 'CZE', 'SVCR', ' ', '-69.6851486', '11.4160964', '6271', 'SVCR', 'medium_airport', 'José Leonardo Chirinos Airport', '11.4149436950684', '-69.6809005737305', '52', 'SA', 'VE', 'VE-I', 'Coro', 'yes', 'SVCR', 'CZE', '', '', '', ''),
('0', 'Aeropuerto Internacional de Cozumel', 'aeroway=aerodrome
iata=CZM
icao=MMCZ
name=Aeropuerto Interna', '86° 55'' 37.294', '20° 31'' 22.287', ' ', '0', 'CZM', 'MMCZ', ' ', '-86.927026', '20.5228576', '4706', 'MMCZ', 'medium_airport', 'Cozumel International Airport', '20.5223999023437', '-86.9255981445312', '15', 'NA', 'MX', 'MX-ROO', 'Cozumel', 'yes', 'MMCZ', 'CZM', '', 'http://www.asur.com.mx/', 'https://en.wikipedia.org/wiki/Cozumel_International_Airport', ''),
('0', 'Aeroporto Internacional de Cruzeiro do Sul', 'aeroway=aerodrome
iata=CZS
icao=SBCZ
name=Aeroporto Internac', '72° 46'' 14.199', '7° 35'' 55.337"', ' ', '0', 'CZS', 'SBCZ', ' ', '-72.7706107', '-7.5987047', '5895', 'SBCZ', 'medium_airport', 'Cruzeiro do Sul Airport', '-7.59991', '-72.769501', '637', 'SA', 'BR', 'BR-AC', 'Cruzeiro Do Sul', 'yes', 'SBCZ', 'CZS', 'AC0002', 'http://www.infraero.gov.br/index.php/aeroportos/acre/aeroporto-cruzeiro-do-sul.html', 'https://en.wikipedia.org/wiki/Cruzeiro_do_Sul_International_Airport', ''),
('0', 'Cảng hàng không quốc tế Đà Nẵng', 'addr:city=Da Nang
addr:country=VN
addr:district=Hai Chau
add', '108° 11'' 54.24', '16° 2'' 34.398"', ' ', '0', 'DAD', 'VVDN', 'Дананг (аэропорт)', '108.1984008', '16.0428882', '26697', 'VVDN', 'large_airport', 'Da Nang International Airport', '16.0438995361328', '108.198997497559', '33', 'AS', 'VN', 'VN-60', 'Da Nang', 'yes', 'VVDN', 'DAD', '', '', 'https://en.wikipedia.org/wiki/Da_Nang_International_Airport', 'Danang, Cảng Hàng không Quốc tế Đà Nẵng'),
('0', 'Daru Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=6
iata=DAU
icao=', '143° 12'' 33.56', '9° 5'' 11.922"', 'public', '6', 'DAU', 'AYDU', ' ', '143.2093236', '-9.0866449', '55', 'AYDU', 'medium_airport', 'Daru Airport', '-9.08675956726', '143.207992554', '20', 'OC', 'PG', 'PG-WPD', 'Daru', 'yes', 'AYDU', 'DAU', '', '', 'https://en.wikipedia.org/wiki/Daru_Airport', ''),
('0', 'Dubbo City Regional Airport', 'addr:city=Dubbo
addr:postcode=2830
addr:state=NSW
addr:stree', '148° 34'' 43.57', '32° 12'' 49.836', ' ', '285', 'DBO', 'YSDU', ' ', '148.5787695', '-32.2138433', '27135', 'YSDU', 'medium_airport', 'Dubbo City Regional Airport', '-32.2167015076', '148.574996948', '935', 'OC', 'AU', 'AU-NSW', 'Dubbo', 'yes', 'YSDU', 'DBO', '', 'http://www.dubbo.nsw.gov.au/BusinessandIndustry/DubboCityRegionalAirport.html', 'https://en.wikipedia.org/wiki/Dubbo_Airport', ''),
('0', 'Canefield Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=4
iata=DCF
icao=', '61° 23'' 30.729', '15° 20'' 23.152', 'public', '4', 'DCF', 'TDCF', ' ', '-61.3918693', '15.3397645', '6361', 'TDCF', 'medium_airport', 'Canefield Airport', '15.336693', '-61.392108', '13', 'NA', 'DM', 'DM-10', 'Canefield', 'yes', 'TDCF', 'DCF', '', '', 'https://en.wikipedia.org/wiki/Canefield_Airport', ''),
('0', 'Dehradun Jolly Grant Airport', 'aeroway=aerodrome
closest_town=Dehradun
ele=558
iata=DED
ica', '78° 10'' 53.895', '30° 11'' 32.861', ' ', '558', 'DED', 'VIDN', ' ', '78.1816374', '30.1924614', '26554', 'VIDN', 'medium_airport', 'Dehradun Airport', '30.189699', '78.180298', '1831', 'AS', 'IN', 'IN-UL', 'Dehradun', 'yes', 'VIDN', 'DED', '', 'http://aai.aero/allAirports/dehradun_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Jolly_Grant_Airport', 'Jolly Grant Airport'),
('0', 'Indira Gandhi International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '77° 5'' 49.822"', '28° 33'' 18.611', 'public', '237', 'DEL', 'VIDP', ' ', '77.0971727', '28.5551696', '26555', 'VIDP', 'large_airport', 'Indira Gandhi International Airport', '28.5665', '77.103104', '777', 'AS', 'IN', 'IN-DL', 'New Delhi', 'yes', 'VIDP', 'DEL', '', 'http://www.newdelhiairport.in/', 'https://en.wikipedia.org/wiki/Indira_Gandhi_International_Airport', 'Palam Air Force Station'),
('0', 'Aeropuerto Intl. General Guadalupe Victoria', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Durango', '104° 32'' 2.114', '24° 7'' 34.685"', 'public', '1860', 'DGO', 'MMDO', ' ', '-104.5339205', '24.1263013', '4707', 'MMDO', 'medium_airport', 'General Guadalupe Victoria International Airport', '24.1242008209', '-104.527999878', '6104', 'NA', 'MX', 'MX-DUR', 'Durango', 'yes', 'MMDO', 'DGO', '', '', 'https://en.wikipedia.org/wiki/General_Guadalupe_Victoria_International_Airport', ''),
('0', 'Gaggal Airport', 'aeroway=aerodrome
ele=770
iata=DHM
icao=VIGG
name=Gaggal Air', '76° 15'' 47.325', '32° 9'' 53.381"', ' ', '770', 'DHM', 'VIGG', ' ', '76.2631458', '32.1648281', '26557', 'VIGG', 'medium_airport', 'Kangra Airport', '32.1651', '76.263397', '2525', 'AS', 'IN', 'IN-HP', 'Gaggal', 'yes', 'VIGG', 'DHM', '', '', '', ''),
('0', '迪庆香格里拉机场', 'aeroway=aerodrome
iata=DIG
icao=ZPDQ
int_name=Diqing Airport', '99° 40'' 41.649', '27° 47'' 31.198', ' ', '0', 'DIG', 'ZPDQ', ' ', '99.6782358', '27.7919995', '30916', 'ZPDQ', 'medium_airport', 'Diqing Airport', '27.7936', '99.6772', '10761', 'AS', 'CN', 'CN-53', 'Shangri-La', 'yes', 'ZPDQ', 'DIG', '', '', 'https://en.wikipedia.org/wiki/Diqing_Airport', ''),
('0', 'Aeroporto Internacional Presidente Nicolau Lobato', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Dili, E', '125° 31'' 29.48', '8° 32'' 49.805"', 'public', '8', 'DIL', 'WPDL', ' ', '125.5248564', '-8.5471681', '26881', 'WPDL', 'medium_airport', 'Presidente Nicolau Lobato International Airport', '-8.54640007019', '125.526000977', '154', 'AS', 'TL', 'TL-DI', 'Dili', 'yes', 'WPDL', 'DIL', '', '', 'https://en.wikipedia.org/wiki/Presidente_Nicolau_Lobato_International_Airport', 'Komoro Airport'),
('0', 'Aéroport International de Douala', 'aerodrome=international
aerodrome:type=military/public
aerow', '9° 43'' 22.196"', '4° 0'' 51.355"', 'military/public', '10', 'DLA', 'FKKD', ' ', '9.7228321', '4.0142652', '2882', 'FKKD', 'medium_airport', 'Douala International Airport', '4.0060801506', '9.71947956085', '33', 'AF', 'CM', 'CM-LT', 'Douala', 'yes', 'FKKD', 'DLA', '', '', 'https://en.wikipedia.org/wiki/Douala_International_Airport', ''),
('0', '大连周水子国际机场', 'aerodrome:type=international
aeroway=aerodrome
closest_town=', '121° 32'' 16.79', '38° 57'' 50.750', 'international', '33', 'DLC', 'ZYTL', 'Аэропорт Дальний', '121.5379981', '38.9640972', '27242', 'ZYTL', 'large_airport', 'Dalian Zhoushuizi International Airport', '38.965698', '121.539001', '107', 'AS', 'CN', 'CN-21', 'Ganjingzi, Dalian', 'yes', 'ZYTL', 'DLC', '', '', 'https://en.wikipedia.org/wiki/Dalian_Zhoushuizi_International_Airport', 'Dalian Air Base'),
('0', '大理荒草坝机场', 'aeroway=aerodrome
iata=DLU
icao=ZPDL
name=大理荒草坝机', '100° 19'' 7.900', '25° 38'' 59.649', ' ', '0', 'DLU', 'ZPDL', ' ', '100.3188611', '25.6499026', '30925', 'ZPDL', 'medium_airport', 'Dali Airport', '25.649401', '100.319', '7050', 'AS', 'CN', 'CN-53', 'Xiaguan', 'yes', 'ZPDL', 'DLU', '', '', 'https://en.wikipedia.org/wiki/Dali_Airport', 'Dali Air Base'),
('0', 'Аэропорт Аулие-Ата', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Аэроп', '71° 18'' 14.558', '42° 51'' 15.570', 'public', '664', 'DMB', 'UADD', ' ', '71.304044', '42.854325', '6424', 'UADD', 'medium_airport', 'Taraz Airport', '42.8535995483398', '71.303596496582', '2184', 'AS', 'KZ', 'KZ-ZHA', 'Taraz', 'yes', 'UADD', 'DMB', '', '', 'https://en.wikipedia.org/wiki/Taraz_Airport', 'Aulie Ata'),
('0', 'ท่าอากาศยานดอนเมือง', 'aerodrome=international
aerodrome:type=military/public
aerow', '100° 36'' 14.61', '13° 54'' 45.499', 'military/public', '3', 'DMK', 'VTBD', ' ', '100.6040594', '13.9126385', '26638', 'VTBD', 'large_airport', 'Don Mueang International Airport', '13.9125995636', '100.607002258', '9', 'AS', 'TH', 'TH-10', 'Bangkok', 'yes', 'VTBD', 'DMK', '', 'http://www2.airportthai.co.th/airportnew/bangkok/index.asp?lang=en', 'https://en.wikipedia.org/wiki/Don_Mueang_International_Airport', 'Old Bangkok International Airport, Don Muang Royal Thai Air Force Base'),
('0', 'Dimapur Airport', 'aeroway=aerodrome
iata=DMU
icao=VEMR
is_in:country=India
mil', '93° 46'' 16.456', '25° 53'' 1.698"', ' ', '0', 'DMU', 'VEMR', ' ', '93.7712379', '25.8838051', '26513', 'VEMR', 'medium_airport', 'Dimapur Airport', '25.8838996887', '93.7711029053', '487', 'AS', 'IN', 'IN-NL', 'Dimapur', 'yes', 'VEMR', 'DMU', '', '', 'https://en.wikipedia.org/wiki/Dimapur_Airport', ''),
('0', '敦煌机场', 'aeroway=aerodrome
barrier=fence
iata=DNH
icao=ZLDH
name=敦', '94° 48'' 30.310', '40° 9'' 48.707"', ' ', '0', 'DNH', 'ZLDH', ' ', '94.8084194', '40.1635296', '30929', 'ZLDH', 'medium_airport', 'Dunhuang Airport', '40.1610984802246', '94.809196472168', NULL, 'AS', 'CN', 'CN-62', 'Dunhuang', 'yes', 'ZLDH', 'DNH', '', '', 'https://en.wikipedia.org/wiki/Dunhuang_Airport', ''),
('0', 'Douglas-Charles Airport', 'aeroway=aerodrome
arp=yes
closest_town=Dominica
ele=22.25
ia', '61° 18'' 6.067"', '15° 32'' 47.664', ' ', '22', 'DOM', 'TDPD', ' ', '-61.3016853', '15.5465734', '6362', 'TDPD', 'medium_airport', 'Douglas-Charles Airport', '15.547', '-61.299999', '73', 'NA', 'DM', 'DM-02', 'Marigot', 'yes', 'TDPD', 'DOM', '', '', 'https://en.wikipedia.org/wiki/Melville_Hall_Airport', ''),
('0', 'Devonport Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Devonpo', '146° 25'' 45.55', '41° 10'' 10.325', 'public', '10', 'DPO', 'YDPO', ' ', '146.4293212', '-41.1695347', '26975', 'YDPO', 'medium_airport', 'Devonport Airport', '-41.1697006226', '146.429992676', '33', 'OC', 'AU', 'AU-TAS', 'Devonport', 'yes', 'YDPO', 'DPO', '', 'http://www.tasports.com.au/infrastructure/devonport_airport.html', 'https://en.wikipedia.org/wiki/Devonport_Airport', ''),
('0', 'Bandar Udara Internasional Ngurah Rai', 'addr:city=Denpasar
aeroway=aerodrome
iata=DPS
icao=WADD
is_i', '115° 10'' 2.557', '8° 44'' 45.930"', ' ', '0', 'DPS', 'WADD', ' ', '115.167377', '-8.7460916', '26751', 'WADD', 'large_airport', 'Ngurah Rai (Bali) International Airport', '-8.7481698989868', '115.16699981689', '14', 'AS', 'ID', 'ID-BA', 'Denpasar-Bali Island', 'yes', 'WADD', 'DPS', '', 'http://www.angkasapura1.co.id/eng/location/bali.htm', 'https://en.wikipedia.org/wiki/Ngurah_Rai_Airport', 'WRRR'),
('0', 'Darwin International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '130° 52'' 58.83', '12° 24'' 42.556', 'public', '31', 'DRW', 'YPDN', ' ', '130.8830087', '-12.4118212', '27101', 'YPDN', 'medium_airport', 'Darwin International Airport', '-12.4146995544434', '130.876998901367', '103', 'OC', 'AU', 'AU-NT', 'Darwin', 'yes', 'YPDN', 'DRW', '', 'http://www.darwin-airport.com.au/', 'https://en.wikipedia.org/wiki/Darwin_International_Airport', 'RAAF Base Darwin'),
('0', '鄂尔多斯伊金霍洛机场', 'aeroway=aerodrome
iata=DSN
icao=ZBDS
name=鄂尔多斯伊金', '109° 51'' 34.00', '39° 29'' 36.410', ' ', '0', 'DSN', 'ZBDS', ' ', '109.8594453', '39.4934473', '300513', 'ZBDS', 'medium_airport', 'Ordos Ejin Horo Airport', '39.49', '109.861388889', '4557', 'AS', 'CN', 'CN-15', 'Ordos', 'yes', 'ZBDS', 'DSN', '', '', 'https://en.wikipedia.org/wiki/Ordos_Ejin_Horo_Airport', ''),
('0', 'Dunedin International Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '170° 11'' 48.27', '45° 55'' 40.544', 'public', '1', 'DUD', 'NZDN', ' ', '170.1967433', '-45.9279289', '5028', 'NZDN', 'medium_airport', 'Dunedin Airport', '-45.9281005859375', '170.197998046875', '4', 'OC', 'NZ', 'NZ-OTA', 'Dunedin', 'yes', 'NZDN', 'DUD', '', 'http://www.dnairport.co.nz/', 'https://en.wikipedia.org/wiki/Dunedin_International_Airport', ''),
('0', 'King Shaka International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '31° 7'' 8.134"', '29° 36'' 43.404', 'public', '97', 'DUR', 'FALE', ' ', '31.1189261', '-29.6120567', '300795', 'FALE', 'large_airport', 'King Shaka International Airport', '-29.6144444444', '31.1197222222', '295', 'AF', 'ZA', 'ZA-NL', 'Durban', 'yes', 'FALE', 'DUR', '', '', 'https://en.wikipedia.org/wiki/King_Shaka_International_Airport', 'La Mercy Airport'),
('0', 'Фурудгохи байналмилалии Душанбе', 'aeroway=aerodrome
iata=DYU
icao=UTDD
name=Фурудгохи', '68° 49'' 39.035', '38° 32'' 43.830', ' ', '0', 'DYU', 'UTDD', 'Международный аэропорт Душан', '68.8275098', '38.5455084', '26385', 'UTDD', 'medium_airport', 'Dushanbe Airport', '38.5433006287', '68.8249969482', '2575', 'AS', 'TJ', 'TJ-RR', 'Dushanbe', 'yes', 'UTDD', 'DYU', '', '', 'https://en.wikipedia.org/wiki/Dushanbe_Airport', ''),
('0', 'Terrance B. Lettsome International Airport', 'aeroway=aerodrome
closest_town=British Virgin Islands
ele=4.', '64° 32'' 28.692', '18° 26'' 44.813', ' ', '5', 'EIS', 'TUPJ', ' ', '-64.5413032', '18.4457813', '6411', 'TUPJ', 'medium_airport', 'Terrance B. Lettsome International Airport', '18.445492', '-64.541707', '15', 'NA', 'VG', 'VG-U-A', 'Road Town', 'yes', 'TUPJ', 'EIS', '', 'http://www.bareboatsbvi.com/beef_island.html', 'https://en.wikipedia.org/wiki/Terrance_B._Lettsome_International_Airport', 'BVI, Beef Island Airport'),
('0', 'East London Airport', 'aerodrome:type=public
aeroway=aerodrome
barrier=fence
city_s', '27° 49'' 21.872', '33° 2'' 10.801"', 'public', '133', 'ELS', 'FAEL', ' ', '27.8227421', '-33.0363335', '2779', 'FAEL', 'medium_airport', 'Ben Schoeman Airport', '-33.0355987549', '27.8258991241', '435', 'AF', 'ZA', 'ZA-EC', 'East London', 'yes', 'FAEL', 'ELS', '', 'http://www.airports.co.za/home.asp?pid=415', 'https://en.wikipedia.org/wiki/East_London_Airport', ''),
('0', 'Emerald Airport', 'aeroway=aerodrome
iata=EMD
icao=YEML
name=Emerald Airport
op', '148° 10'' 46.93', '23° 34'' 7.352"', ' ', '0', 'EMD', 'YEML', ' ', '148.1797052', '-23.568709', '26981', 'YEML', 'medium_airport', 'Emerald Airport', '-23.5674991608', '148.179000854', '624', 'OC', 'AU', 'AU-QLD', 'Emerald', 'yes', 'YEML', 'EMD', '', 'http://www.emerald.qld.gov.au/Council/Departments/Engineering_Operations/Engineering_Operations.htm', 'https://en.wikipedia.org/wiki/Emerald_Airport', ''),
('0', 'Olaya Herrera', 'aerodrome=public
aerodrome:type=public
aeroway=aerodrome
cit', '75° 35'' 22.370', '6° 13'' 12.183"', 'public', '1505', 'EOH', 'SKMD', ' ', '-75.5895473', '6.2200508', '6132', 'SKMD', 'medium_airport', 'Enrique Olaya Herrera Airport', '6.220549', '-75.590582', '4949', 'SA', 'CO', 'CO-ANT', 'Medellín', 'yes', 'SKMD', 'EOH', 'EOH', '', 'https://en.wikipedia.org/wiki/Enrique_Olaya_Herrera_International_Airport', ''),
('0', 'Esperance Airport', 'aerodrome=regional
aerodrome:type=public
aeroway=aerodrome
e', '121° 49'' 11.52', '33° 41'' 1.936"', 'public', '143', 'EPR', 'YESP', ' ', '121.8198687', '-33.6838711', '26982', 'YESP', 'medium_airport', 'Esperance Airport', '-33.684399', '121.822998', '470', 'OC', 'AU', 'AU-WA', 'Esperance', 'yes', 'YESP', 'EPR', '', '', 'https://en.wikipedia.org/wiki/Esperance_Airport', ''),
('0', 'Aeropuerto Internacional Antonio Parodi', 'aerodrome:type=public
aeroway=aerodrome
ele=799
iata=EQS
ica', '71° 8'' 10.431"', '42° 54'' 11.285', 'public', '799', 'EQS', 'SAVE', ' ', '-71.1362308', '-42.9031346', '5823', 'SAVE', 'medium_airport', 'Brigadier Antonio Parodi Airport', '-42.908000946', '-71.139503479', '2621', 'SA', 'AR', 'AR-U', 'Esquel', 'yes', 'SAVE', 'EQS', 'ESQ', '', 'http://es.wikipedia.org/wiki/Aeropuerto_Brigadier_General_Antonio_Parodi', ''),
('0', '二连浩特赛乌苏国际机场', 'aeroway=aerodrome
barrier=fence
iata=ERL
icao=ZBER
name=二', '112° 5'' 35.973', '43° 25'' 24.982', ' ', '0', 'ERL', 'ZBER', ' ', '112.0933259', '43.423606', '300671', 'ZBER', 'medium_airport', 'Erenhot Saiwusu International Airport', '43.4225', '112.096667', '3301', 'AS', 'CN', 'CN-15', 'Erenhot', 'yes', 'ZBER', 'ERL', '', '', 'https://en.wikipedia.org/wiki/Erenhot_Saiwusu_International_Airport', 'Saiwusu Airport'),
('0', 'Eros Airport', 'addr:street=Aviation Road, Windhoek, Namibia
aerodrome:type=', '17° 4'' 45.076"', '22° 36'' 18.552', 'public', '0', 'ERS', 'FYWE', ' ', '17.0791877', '-22.6051533', '3038', 'FYWE', 'medium_airport', 'Eros Airport', '-22.6121997833252', '17.0804004669189', '5575', 'AF', 'NA', 'NA-KH', 'Windhoek', 'yes', 'FYWE', 'ERS', '', '', 'https://en.wikipedia.org/wiki/Eros_Airport', ''),
('0', 'Eugene Airport', 'addr:state=OR
aeroway=aerodrome
alt_name=Mahlon Sweet Field
', '123° 12'' 49.63', '44° 7'' 21.468"', ' ', '113', 'EUG', 'KEUG', ' ', '-123.2137876', '44.12263', '3517', 'KEUG', 'medium_airport', 'Mahlon Sweet Field', '44.1245994567871', '-123.21199798584', '374', 'NA', 'US', 'US-OR', 'Eugene', 'yes', 'KEUG', 'EUG', 'EUG', '', 'https://en.wikipedia.org/wiki/Eugene_Airport', ''),
('0', 'Hassan I Airport', 'aeroway=aerodrome
alt_name=Laâyoune Airport
ele=63
iata=EUN', '13° 13'' 29.573', '27° 8'' 26.408"', ' ', '63', 'EUN', 'GMML/G', ' ', '-13.2248815', '27.1406688', '3112', 'GMML', 'medium_airport', 'Hassan I Airport', '27.151699', '-13.2192', '207', 'AF', 'EH', 'EH-U-A', 'El Aaiún', 'yes', 'GMML', 'EUN', '', '', 'https://en.wikipedia.org/wiki/Hassan_Airport', 'GSAI, Laâyoune'),
('0', 'F. D. Roosevelt Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=39.3
iata=EUX
ic', '62° 58'' 48.129', '17° 29'' 45.455', 'public', '39', 'EUX', 'TNCE', ' ', '-62.9800359', '17.4959596', '6405', 'TNCE', 'medium_airport', 'F. D. Roosevelt Airport', '17.4965000152588', '-62.9794006347656', '129', 'NA', 'BQ', 'BQ-U-A', 'Sint Eustatius', 'yes', 'TNCE', 'EUX', '', '', 'https://en.wikipedia.org/wiki/F.D._Roosevelt_Airport', ''),
('0', 'El Alcaraván', 'aeroway=aerodrome
ele=313
iata=EYP
icao=SKYP
is_in:city=El Y', '72° 23'' 1.459"', '5° 19'' 11.644"', ' ', '313', 'EYP', 'SKYP', ' ', '-72.3837386', '5.319901', '6178', 'SKYP', 'medium_airport', 'El Yopal Airport', '5.31911', '-72.384', '1028', 'SA', 'CO', 'CO-CAS', 'El Yopal', 'yes', 'SKYP', 'EYP', 'EYP', '', 'https://en.wikipedia.org/wiki/El_Alcarav%C3%A1n_Airport', ''),
('0', 'Aeropuerto Internacional Ministro Pistarini', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Ezeiza
ele=', '58° 32'' 8.973"', '34° 49'' 19.798', 'public', '20', 'EZE', 'SAEZ', ' ', '-58.5358257', '-34.8221662', '5781', 'SAEZ', 'large_airport', 'Ministro Pistarini International Airport', '-34.8222', '-58.5358', '67', 'SA', 'AR', 'AR-B', 'Buenos Aires (Ezeiza)', 'yes', 'SAEZ', 'EZE', 'EZE', 'http://www.aa2000.com.ar/index.php', 'https://en.wikipedia.org/wiki/Ministro_Pistarini_International_Airport', 'BUE, Ezeiza'),
('0', 'Fresno Yosemite International Airport', 'addr:state=CA
aeroway=aerodrome
ele=101
gnis:county_name=Fre', '119° 43'' 5.992', '36° 46'' 33.487', ' ', '101', 'FAT', 'KFAT', ' ', '-119.7183311', '36.7759685', '3524', 'KFAT', 'large_airport', 'Fresno Yosemite International Airport', '36.776199', '-119.718002', '336', 'NA', 'US', 'US-CA', 'Fresno', 'yes', 'KFAT', 'FAT', 'FAT', 'https://flyfresno.com/', 'https://en.wikipedia.org/wiki/Fresno_Yosemite_International_Airport', ''),
('0', 'Lubumbashi International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '27° 31'' 54.062', '11° 35'' 27.680', 'public', '0', 'FBM', 'FZQA', ' ', '27.5316839', '-11.5910223', '3059', 'FZQA', 'medium_airport', 'Lubumbashi International Airport', '-11.5913000107', '27.5308990479', '4295', 'AF', 'CD', 'CD-KA', 'Lubumbashi', 'yes', 'FZQA', 'FBM', '', '', 'https://en.wikipedia.org/wiki/Lubumbashi_International_Airport', ''),
('0', 'Fort Lauderdale-Hollywood International Airport', 'addr:city=Fort Lauderdale
addr:housenumber=100
addr:postcode', '80° 8'' 59.263"', '26° 4'' 20.330"', ' ', '2', 'FLL', 'KFLL', ' ', '-80.1497953', '26.0723139', '3531', 'KFLL', 'large_airport', 'Fort Lauderdale Hollywood International Airport', '26.072599', '-80.152702', '9', 'NA', 'US', 'US-FL', 'Fort Lauderdale', 'yes', 'KFLL', 'FLL', 'FLL', 'http://www.broward.org/airport', 'https://en.wikipedia.org/wiki/Fort_Lauderdale–Hollywood_International_Airport', 'MFW, South Florida'),
('0', 'Aeroporto Internacional Hercílio Luz', 'aerodrome=public
aeroway=aerodrome
contact:website=https://f', '48° 32'' 30.579', '27° 40'' 12.005', ' ', '0', 'FLN', 'SBFL', ' ', '-48.5418276', '-27.6700015', '5901', 'SBFL', 'large_airport', 'Hercílio Luz International Airport', '-27.6702785491943', '-48.5525016784668', '16', 'SA', 'BR', 'BR-SC', 'Florianópolis', 'yes', 'SBFL', 'FLN', '', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=228', 'https://en.wikipedia.org/wiki/Herc%C3%ADlio_Luz_International_Airport', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=228'),
('0', 'Aeropuerto Internacional de Formosa', 'aeroway=aerodrome
alt_name=El Pucú Airport
ele=59
iata=FMA
', '58° 13'' 42.847', '26° 12'' 49.118', ' ', '59', 'FMA', 'SARF', ' ', '-58.2285685', '-26.2136439', '5805', 'SARF', 'medium_airport', 'Formosa Airport', '-26.2127', '-58.2281', '193', 'SA', 'AR', 'AR-P', 'Formosa', 'yes', 'SARF', 'FMA', 'FSA', '', 'https://en.wikipedia.org/wiki/Formosa_International_Airport', ''),
('0', 'Lungi International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '13° 11'' 43.237', '8° 37'' 0.201"', 'public', '26', 'FNA', 'GFLL', ' ', '-13.1953437', '8.6167224', '3095', 'GFLL', 'large_airport', 'Lungi International Airport', '8.61644', '-13.1955', '84', 'AF', 'SL', 'SL-N', 'Freetown (Lungi-Town)', 'yes', 'GFLL', 'FNA', '', 'http://www.freetownairport.com/', 'https://en.wikipedia.org/wiki/Lungi_International_Airport', ''),
('0', '福州长乐国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=FOC
icao', '119° 39'' 55.47', '25° 56'' 10.211', 'international', '0', 'FOC', 'ZSFZ', ' ', '119.6654106', '25.9361697', '27217', 'ZSFZ', 'large_airport', 'Fuzhou Changle International Airport', '25.9351005554199', '119.66300201416', '46', 'AS', 'CN', 'CN-35', 'Fuzhou', 'yes', 'ZSFZ', 'FOC', '', 'http://www.xiafz.com.cn/en/index.asp', 'https://en.wikipedia.org/wiki/Fuzhou_Changle_International_Airport', ''),
('0', 'Aeroporto Internacional de Fortaleza - Pinto Martins', 'addr:city=Fortaleza
addr:street=Avenida Senador Carlos Jerei', '38° 32'' 8.713"', '3° 46'' 34.768"', ' ', '3', 'FOR', 'SBFZ', ' ', '-38.5357537', '-3.7763244', '5905', 'SBFZ', 'medium_airport', 'Pinto Martins International Airport', '-3.77628', '-38.5326', '82', 'SA', 'BR', 'BR-CE', 'Fortaleza', 'yes', 'SBFZ', 'FOR', 'CE0001', '', 'https://en.wikipedia.org/wiki/Pinto_Martins_International_Airport', ''),
('0', 'Grand Bahama International Airport', 'aeroway=aerodrome
closest_town=Freeport
ele=2
iata=FPO
icao=', '78° 41'' 49.955', '26° 33'' 26.116', ' ', '2', 'FPO', 'MYGF', ' ', '-78.6972096', '26.5572544', '4946', 'MYGF', 'medium_airport', 'Grand Bahama International Airport', '26.5587005615', '-78.695602417', '7', 'NA', 'BS', 'BS-FP', 'Freeport', 'yes', 'MYGF', 'FPO', '', '', 'https://en.wikipedia.org/wiki/Grand_Bahama_International_Airport', ''),
('0', 'Аэропорт Манас', 'aeroway=aerodrome
barrier=wall
ele=627
iata=FRU
icao=UCFM
is', '74° 28'' 41.759', '43° 3'' 29.249"', ' ', '627', 'FRU', 'UCFM', 'Аэропорт Манас', '74.4782664', '43.0581247', '6426', 'UAFM', 'large_airport', 'Manas International Airport', '43.0612983704', '74.4776000977', '2058', 'AS', 'KG', 'KG-C', 'Bishkek', 'yes', 'UCFM', 'FRU', '', '', 'https://en.wikipedia.org/wiki/Manas_International_Airport', 'UAFM, Manas Air Force Base'),
('0', 'Francistown Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Francis', '27° 28'' 10.796', '21° 9'' 36.355"', 'public', '1001', 'FRW', 'FBFT', ' ', '27.4696655', '-21.1600987', '2860', 'FBFT', 'medium_airport', 'Francistown Airport', '-21.1595993041992', '27.4745006561279', '3283', 'AF', 'BW', 'BW-NE', 'Francistown', 'yes', 'FBFT', 'FRW', '', '', 'https://en.wikipedia.org/wiki/Francistown_Airport', ''),
('0', '静岡空港', 'KSJ2:AAC=22226
KSJ2:AAC_label=静岡県, *
KSJ2:AD2=5
KSJ2:A', '138° 11'' 20.61', '34° 47'' 46.916', 'public', '132', 'FSZ', 'RJNS', ' ', '138.1890593', '34.7963656', '299238', 'rjns', 'large_airport', 'Mount Fuji Shizuoka Airport', '34.796043', '138.187752', '433', 'AS', 'JP', 'JP-22', 'Makinohara / Shimada', 'yes', 'RJNS', 'FSZ', '', 'http://www.mtfuji-shizuokaairport.jp/english/index.html', 'https://en.wikipedia.org/wiki/Shizuoka_Airport', ''),
('0', 'Aeropuerto Internacional Comandante Armando Tola', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Ae', '72° 3'' 10.651"', '50° 16'' 51.808', 'military/public', '204', 'FTE', 'SAWC', ' ', '-72.0529586', '-50.2810579', '5831', 'SAWC', 'medium_airport', 'El Calafate Airport', '-50.2803', '-72.053101', '669', 'SA', 'AR', 'AR-Z', 'El Calafate', 'yes', 'SAWC', 'FTE', 'ECA', 'http://www.aeropuertoelcalafate.com/en/', 'https://en.wikipedia.org/wiki/Comandante_Armando_Tola_International_Airport', ''),
('0', 'Fitiuta Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=34
faa_lid=FAQ
g', '169° 25'' 24.94', '14° 12'' 57.900', 'public', '34', 'FTI', 'NSFQ', ' ', '-169.4235968', '-14.2160834', '4977', 'NSFQ', 'medium_airport', 'Fitiuta Airport', '-14.2172', '-169.425003', '110', 'OC', 'AS', 'AS-U-A', 'Fitiuta Village', 'yes', 'NSFQ', 'FTI', 'FAQ', '', 'https://en.wikipedia.org/wiki/Fitiuta_Airport', ''),
('0', '阜阳西关机场', 'aeroway=aerodrome
iata=FUG
icao=ZSFY
name=阜阳西关机场', '115° 44'' 5.152', '32° 52'' 57.876', ' ', '0', 'FUG', 'ZSFY', ' ', '115.7347645', '32.8827434', '308240', 'ZSFY', 'medium_airport', 'Fuyang Xiguan Airport', '32.882157', '115.734364', '104', 'AS', 'CN', 'CN-34', 'Yingzhou, Fuyang', 'yes', 'ZSFY', 'FUG', '', '', 'https://en.wikipedia.org/wiki/Fuyang_Xiguan_Airport', ''),
('0', 'Funafuti International', 'aerodrome=public
aerodrome:type=public
aeroway=aerodrome
ele', '179° 11'' 48.33', '8° 31'' 26.212"', 'public', '3', 'FUN', 'NGFU', ' ', '179.1967609', '-8.5239479', '44571', 'NGFU', 'medium_airport', 'Funafuti International Airport', '-8.525', '179.195999', '9', 'OC', 'TV', 'TV-FUN', 'Funafuti', 'yes', 'NGFU', 'FUN', '', '', 'https://en.wikipedia.org/wiki/Funafuti_International_Airport', ''),
('0', 'Aéroport de Pointe Vele', 'aerodrome:type=public
aeroway=aerodrome
ele=6
iata=FUT
icao=', '178° 3'' 59.246', '14° 18'' 41.908', 'public', '6', 'FUT', 'NLWF', ' ', '-178.0664572', '-14.3116412', '31215', 'NLWF', 'medium_airport', 'Pointe Vele Airport', '-14.3114004135', '-178.065994263', '20', 'OC', 'WF', 'WF-U-A', 'Futuna Island', 'yes', 'NLWF', 'FUT', '', '', 'https://en.wikipedia.org/wiki/Pointe_Vele_Airport', ''),
('0', 'Gan International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Gan (Se', '73° 9'' 20.283"', '0° 41'' 34.713"', 'public', '2', 'GAN', 'VRMG', ' ', '73.1556342', '-0.6929759', '26633', 'VRMG', 'medium_airport', 'Gan International Airport', '-0.693342', '73.155602', '6', 'AS', 'MV', 'MV-01', 'Gan', 'yes', 'VRMG', 'GAN', 'MV', 'http://www.airports.com.mv/domestic/gan.htm', 'https://en.wikipedia.org/wiki/Gan_International_Airport', 'Gan Airport, Addu City, Maldives'),
('0', 'Aeropuerto Mariana Grajales', 'aeroway=aerodrome
area=yes
barrier=fence
iata=GAO
icao=MUGT
', '75° 9'' 30.225"', '20° 5'' 9.293"', ' ', '0', 'GAO', 'MUGT', ' ', '-75.1583959', '20.0859148', '4838', 'MUGT', 'medium_airport', 'Mariana Grajales Airport', '20.0853004455566', '-75.1583023071289', '56', 'NA', 'CU', 'CU-14', 'Guantánamo', 'yes', 'MUGT', 'GAO', '', '', 'https://en.wikipedia.org/wiki/Mariana_Grajales_Airport', 'Aerodromo los Caños'),
('0', 'Lokpriya Gopinath Bordoloi International Airport', 'aerodrome=international
aeroway=aerodrome
alt_name=Guwahati', '91° 35'' 12.811', '26° 6'' 40.696"', ' ', '0', 'GAU', 'VEGT', ' ', '91.5868919', '26.1113044', '26501', 'VEGT', 'medium_airport', 'Lokpriya Gopinath Bordoloi International Airport', '26.1061000823975', '91.5858993530273', '162', 'AS', 'IN', 'IN-AS', 'Guwahati', 'yes', 'VEGT', 'GAU', '', 'http://aai.aero/guwahati/index.jsp', 'https://en.wikipedia.org/wiki/Lokpriya_Gopinath_Bordoloi_International_Airport', 'Borjhar Airport, Mountain Shadow Air Force Station'),
('0', 'Gaya Airport', 'aeroway=aerodrome
iata=GAY
icao=VEGY
is_in:country=India
nam', '84° 56'' 59.136', '24° 44'' 36.068', ' ', '0', 'GAY', 'VEGY', ' ', '84.94976', '24.7433522', '26502', 'VEGY', 'medium_airport', 'Gaya Airport', '24.7443008422852', '84.9512023925781', '380', 'AS', 'IN', 'IN-BR', '', 'yes', 'VEGY', 'GAY', '', '', 'https://en.wikipedia.org/wiki/Gaya_Airport', ''),
('0', 'Sir Seretse Khama International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '25° 55'' 12.067', '24° 33'' 11.398', 'public', '1006', 'GBE', 'FBSK', ' ', '25.9200186', '-24.5531661', '2864', 'FBSK', 'large_airport', 'Sir Seretse Khama International Airport', '-24.555201', '25.9182', '3299', 'AF', 'BW', 'BW-SE', 'Gaborone', 'yes', 'FBSK', 'GBE', '', '', 'https://en.wikipedia.org/wiki/Sir_Seretse_Khama_International_Airport', ''),
('0', 'Owen Roberts International Airport', 'addr:city=George Town
addr:country=KY
addr:street=Owen Rober', '81° 21'' 36.197', '19° 17'' 36.947', 'public', '2', 'GCM', 'MWCR', ' ', '-81.3600546', '19.2935964', '4862', 'MWCR', 'large_airport', 'Owen Roberts International Airport', '19.2928009033', '-81.3576965332', '8', 'NA', 'KY', 'KY-U-A', 'Georgetown', 'yes', 'MWCR', 'GCM', '', '', 'https://en.wikipedia.org/wiki/Owen_Roberts_International_Airport', 'Grand Cayman, George Town'),
('0', 'JAGS McCartney International Airport', 'aeroway=aerodrome
closest_town=Grand Turk Island
ele=4
iata=', '71° 8'' 32.182"', '21° 26'' 40.666', ' ', '4', 'GDT', 'MBGT', ' ', '-71.1422728', '21.4446294', '4626', 'MBGT', 'medium_airport', 'JAGS McCartney International Airport', '21.4444999694824', '-71.1423034667969', '13', 'NA', 'TC', 'TC-GT', 'Cockburn Town', 'yes', 'MBGT', 'GDT', '', '', 'https://en.wikipedia.org/wiki/JAGS_McCartney_International_Airport', ''),
('0', 'Aérodrome de Magenta', 'aerodrome:type=public
aeroway=aerodrome
ele=3
iata=GEA
icao=', '166° 28'' 18.81', '22° 15'' 37.694', 'public', '3', 'GEA', 'NWWM', ' ', '166.4718935', '-22.2604706', '5017', 'NWWM', 'large_airport', 'Nouméa Magenta Airport', '-22.258301', '166.473007', '10', 'OC', 'NC', 'NC-U-A', 'Nouméa', 'yes', 'NWWM', 'GEA', '', '', 'https://en.wikipedia.org/wiki/Noum%C3%A9a_Magenta_Airport', 'l''Aéroport de Nouméa Magenta'),
('0', 'Aeroporto Regional Sepé Tiaraju', 'aerodrome=regional
aeroway=aerodrome
alt_name=Aeroporto de S', '54° 10'' 9.722"', '28° 16'' 55.925', ' ', '322', 'GEL', 'SBNM', ' ', '-54.1693673', '-28.2822013', '5948', 'SBNM', 'medium_airport', 'Santo Ângelo Airport', '-28.2817', '-54.169102', '1056', 'SA', 'BR', 'BR-RS', 'Santo Ângelo', 'yes', 'SBNM', 'GEL', '', '', 'https://en.wikipedia.org/wiki/Santo_%C3%82ngelo_Airport', ''),
('0', 'Cheddi Jagan International Airport', 'aerodrome:type=public
aeroway=aerodrome
barrier=fence
closes', '58° 15'' 15.720', '6° 29'' 57.400"', 'public', '29', 'GEO', 'SYCJ', ' ', '-58.2543667', '6.4992777', '6356', 'SYCJ', 'medium_airport', 'Cheddi Jagan International Airport', '6.4985499382019', '-58.2541007995606', '95', 'SA', 'GY', 'GY-DE', 'Georgetown', 'yes', 'SYCJ', 'GEO', 'SYGT', 'http://www.cjairport-gy.com/', 'https://en.wikipedia.org/wiki/Cheddi_Jagan_International_Airport', ''),
('0', 'Aeropuerto Rafael Cabrera', 'aeroway=aerodrome
iata=GER
icao=MUNG
name=Aeropuerto Rafael', '82° 46'' 53.745', '21° 50'' 19.529', ' ', '0', 'GER', 'MUNG', ' ', '-82.7815958', '21.838758', '4849', 'MUNG', 'medium_airport', 'Rafael Cabrera Airport', '21.8346996307373', '-82.7837982177734', '79', 'NA', 'CU', 'CU-99', 'Nueva Gerona', 'yes', 'MUNG', 'GER', '', '', 'https://en.wikipedia.org/wiki/Rafael_P%C3%A9rez_Airport', ''),
('0', 'Geraldton Airport', 'aerodrome=regional
aerodrome:type=public
aeroway=aerodrome
e', '114° 42'' 22.05', '28° 47'' 51.286', 'public', '97', 'GET', 'YGEL', ' ', '114.7061276', '-28.7975795', '26991', 'YGEL', 'medium_airport', 'Geraldton Airport', '-28.796101', '114.707001', '121', 'OC', 'AU', 'AU-WA', 'Geraldton', 'yes', 'YGEL', 'GET', '', '', 'https://en.wikipedia.org/wiki/Geraldton_Airport', ''),
('0', 'Griffith Airport', 'aerodrome:category=certified
aerodrome:type=public
aeroway=a', '146° 3'' 30.716', '34° 15'' 9.880"', 'public', '0', 'GFF', 'YGTH', ' ', '146.0585322', '-34.2527445', '26999', 'YGTH', 'medium_airport', 'Griffith Airport', '-34.2508010864', '146.067001343', '439', 'OC', 'AU', 'AU-NSW', 'Griffith', 'yes', 'YGTH', 'GFF', '', '', 'https://en.wikipedia.org/wiki/Griffith_Airport', ''),
('0', 'RIOgaleão - Aeroporto Internacional Tom Jobim', 'aerodrome=international
aerodrome:type=military/public
aerow', '43° 14'' 19.068', '22° 48'' 40.348', 'military/public', '9', 'GIG', 'SBGL', 'Международный аэропорт Рио-д', '-43.23863', '-22.8112077', '5906', 'SBGL', 'large_airport', 'Rio Galeão – Tom Jobim International Airport', '-22.8099994659', '-43.2505569458', '28', 'SA', 'BR', 'BR-RJ', 'Rio De Janeiro', 'yes', 'SBGL', 'GIG', '', '', 'https://en.wikipedia.org/wiki/Rio_de_Janeiro-Gale%C3%A3o_International_Airport', 'Galeão - Antônio Carlos Jobim International Airport'),
('0', 'Gisborne Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '177° 58'' 40.37', '38° 39'' 50.318', 'public', '5', 'GIS', 'NZGS', ' ', '177.9778817', '-38.6639771', '5030', 'NZGS', 'medium_airport', 'Gisborne Airport', '-38.6632995605469', '177.977996826172', '15', 'OC', 'NZ', 'NZ-GIS', 'Gisborne', 'yes', 'NZGS', 'GIS', '', '', 'https://en.wikipedia.org/wiki/Gisborne_Airport', ''),
('0', 'Goroka Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=1610
iata=GKA
ic', '145° 23'' 29.57', '6° 4'' 59.673"', 'public', '1610', 'GKA', 'AYGA', ' ', '145.3915492', '-6.0832425', '56', 'AYGA', 'medium_airport', 'Goroka Airport', '-6.08168983459', '145.391998291', '5282', 'OC', 'PG', 'PG-EHG', 'Goronka', 'yes', 'AYGA', 'GKA', '', '', 'https://en.wikipedia.org/wiki/Goroka_Airport', ''),
('0', 'Gladstone Airport', 'aeroway=aerodrome
closest_town=Gladstone, Queensland
ele=20
', '151° 13'' 32.65', '23° 52'' 9.951"', ' ', '20', 'GLT', 'YGLA', ' ', '151.2257361', '-23.8694307', '26994', 'YGLA', 'medium_airport', 'Gladstone Airport', '-23.869699', '151.223007', '64', 'OC', 'AU', 'AU-QLD', 'Gladstone', 'yes', 'YGLA', 'GLT', '4680', '', 'https://en.wikipedia.org/wiki/Gladstone_Airport_(Australia)', ''),
('0', 'Gombe Lawanti International Airport', 'aeroway=aerodrome
iata=GMO
icao=DNGO
name=Gombe Lawanti Inte', '10° 53'' 50.073', '10° 17'' 50.265', ' ', '0', 'GMO', 'DNGO', ' ', '10.8972425', '10.2972957', '44578', 'NG-0003', 'medium_airport', 'Gombe Lawanti International Airport', '10.298889', '10.9', '1590', 'AF', 'NG', 'NG-GO', 'Gombe', 'yes', 'DNGO', 'GMO', '', '', 'https://en.wikipedia.org/wiki/Gombe_Lawanti_International_Airport', ''),
('0', '김포국제공항', 'aeroway=aerodrome
ele=18
iata=GMP
icao=RKSS
international_fl', '126° 47'' 35.72', '37° 33'' 32.446', ' ', '18', 'GMP', 'RKSS', 'Аэропорт Гимпо', '126.7932556', '37.5590128', '5656', 'RKSS', 'large_airport', 'Gimpo International Airport', '37.5583', '126.791', '59', 'AS', 'KR', 'KR-11', 'Seoul', 'yes', 'RKSS', 'GMP', '', 'http://gimpo.airport.co.kr/eng/index.jsp', 'https://en.wikipedia.org/wiki/Gimpo_International_Airport', ''),
('0', 'Maurice Bishop International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=St. Geo', '61° 47'' 11.631', '12° 0'' 18.020"', 'public', '12', 'GND', 'TGPY', ' ', '-61.7865642', '12.0050056', '6367', 'TGPY', 'medium_airport', 'Point Salines International Airport', '12.0041999816895', '-61.7862014770508', '41', 'NA', 'GD', 'GD-GE', 'Saint George''s', 'yes', 'TGPY', 'GND', '', 'http://www.psiagrenada.com/', 'https://en.wikipedia.org/wiki/Point_Salines_International_Airport', ''),
('0', 'Goa International Airport', 'addr:city=Dabolim
addr:street=Airport Road
aeroway=aerodrome', '73° 49'' 57.520', '15° 22'' 49.899', ' ', '0', 'GOI', 'VOGO', ' ', '73.8326445', '15.3805275', '26444', 'VAGO', 'large_airport', 'Dabolim Airport', '15.3808002472', '73.8313980103', '150', 'AS', 'IN', 'IN-GA', 'Vasco da Gama', 'yes', 'VOGO', 'GOI', '', '', 'https://en.wikipedia.org/wiki/Dabolim_Airport', 'Goa Airport, Dabolim Navy Airbase, दाबोळी विमानतळ'),
('0', '格尔木机场', 'aeroway=aerodrome
iata=GOQ
icao=ZLGM
name=格尔木机场
na', '94° 47'' 9.915"', '36° 24'' 8.600"', ' ', '0', 'GOQ', 'ZLGM', ' ', '94.7860876', '36.4023889', '35308', 'ZLGM', 'medium_airport', 'Golmud Airport', '36.4006', '94.786102', '9334', 'AS', 'CN', 'CN-63', 'Golmud', 'yes', 'ZLGM', 'GOQ', '', '', 'https://en.wikipedia.org/wiki/Golmud_Airport', ''),
('0', 'George Airport', 'aerodrome:type=public
aeroway=aerodrome
city_served=George,', '22° 22'' 31.289', '34° 0'' 18.074"', 'public', '195', 'GRJ', 'FAGG', ' ', '22.375358', '-34.0050206', '2783', 'FAGG', 'large_airport', 'George Airport', '-34.0056', '22.378902', '648', 'AF', 'ZA', 'ZA-WC', 'George', 'yes', 'FAGG', 'GRJ', '', '', 'https://en.wikipedia.org/wiki/George_Airport', 'PW Botha Airport'),
('0', 'Aeroporto Internacional Governador André Franco Montoro', 'aerodrome=international
aerodrome:type=military/public
aerow', '46° 27'' 59.890', '23° 26'' 16.544', 'military/public', '750', 'GRU', 'SBGR', ' ', '-46.466636', '-23.437929', '5910', 'SBGR', 'large_airport', 'Guarulhos - Governador André Franco Montoro International Airport', '-23.4355564117432', '-46.4730567932129', '2459', 'SA', 'BR', 'BR-SP', 'São Paulo', 'yes', 'SBGR', 'GRU', '', '', 'https://en.wikipedia.org/wiki/S%C3%A3o_Paulo-Guarulhos_International_Airport', ''),
('0', 'Guam International Airport', 'addr:state=GU
aeroway=aerodrome
ele=82
gnis:county_name=Guam', '144° 48'' 0.521', '13° 29'' 11.761', ' ', '82', 'GUM', 'PGUM', ' ', '144.8001446', '13.4866002', '5433', 'PGUM', 'large_airport', 'Antonio B. Won Pat International Airport', '13.4834', '144.796005', '298', 'OC', 'GU', 'GU-U-A', 'Hagåtña, Guam International Airport', 'yes', 'PGUM', 'GUM', 'GUM', 'http://www.guamairport.com/', 'https://en.wikipedia.org/wiki/Antonio_B._Won_Pat_International_Airport', 'Agana'),
('0', 'Gurney Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=27
iata=GUR
icao', '150° 20'' 18.85', '10° 18'' 34.576', 'public', '27', 'GUR', 'AYGN', ' ', '150.3385718', '-10.3096044', '57', 'AYGN', 'medium_airport', 'Gurney Airport', '-10.3114995956', '150.333999634', '88', 'OC', 'PG', 'PG-MBA', 'Gurney', 'yes', 'AYGN', 'GUR', '', '', 'https://en.wikipedia.org/wiki/Gurney_Airport', ''),
('0', 'Gwadar International Airport', 'aeroway=aerodrome
iata=GWD
icao=OPGD
name=Gwadar Internation', '62° 19'' 38.591', '25° 13'' 56.303', ' ', '0', 'GWD', 'OPGD', ' ', '62.3273863', '25.2323063', '5252', 'OPGD', 'medium_airport', 'Gwadar International Airport', '25.232391', '62.327671', '36', 'AS', 'PK', 'PK-BA', 'Gwadar', 'yes', 'OPGD', 'GWD', '', '', 'https://en.wikipedia.org/wiki/Gwadar_International_Airport', ''),
('0', 'Gwalior Airport', 'aeroway=aerodrome
alt_name=Gwalior Maharajpur Airport
iata=G', '78° 13'' 38.092', '26° 17'' 52.499', ' ', '0', 'GWL', 'VIGR', ' ', '78.2272478', '26.2979165', '26558', 'VIGR', 'medium_airport', 'Gwalior Airport', '26.2933006286621', '78.2277984619141', '617', 'AS', 'IN', 'IN-MP', 'Gwalior', 'yes', 'VIGR', 'GWL', '', '', 'https://en.wikipedia.org/wiki/Gwalior_Airport', 'Maharajpur Air Force Station'),
('0', 'Aeropuerto Internacional José Joaquín de Olmedo', 'aerodrome:type=public
aeroway=aerodrome
ele=6
gns_classifica', '79° 53'' 3.463"', '2° 9'' 26.741"', 'public', '6', 'GYE', 'SEGU', ' ', '-79.8842952', '-2.1574281', '6056', 'SEGU', 'medium_airport', 'José Joaquín de Olmedo International Airport', '-2.15741991997', '-79.8835983276', '19', 'SA', 'EC', 'EC-G', 'Guayaquil', 'yes', 'SEGU', 'GYE', '', '', 'https://en.wikipedia.org/wiki/Jos%C3%A9_Joaqu%C3%ADn_de_Olmedo_International_Airport', 'Simon Bolivar International Airport'),
('0', 'Aeropuerto Internacional General José María Yáñez', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Guaymas', '110° 55'' 25.87', '27° 58'' 8.604"', 'public', '18', 'GYM', 'MMGM', ' ', '-110.9238547', '27.9690568', '4711', 'MMGM', 'medium_airport', 'General José María Yáñez International Airport', '27.9689998626709', '-110.925003051758', '59', 'NA', 'MX', 'MX-SON', 'Guaymas', 'yes', 'MMGM', 'GYM', '', 'http://guaymas.asa.gob.mx/wb/webasa/guaymas_aeropuertos', 'https://en.wikipedia.org/wiki/General_Jos%C3%A9_Mar%C3%ADa_Y%C3%A1%C3%B1ez_International_Airport', ''),
('0', 'Aeroporto Internacional Santa Genoveva', 'aeroway=aerodrome
alt_name=Aeroporto de Goiânia
height=745', '49° 13'' 9.128"', '16° 37'' 59.543', ' ', '0', 'GYN', 'SBGO', ' ', '-49.2192023', '-16.6332064', '5908', 'SBGO', 'medium_airport', 'Santa Genoveva Airport', '-16.6319999694824', '-49.2206993103027', '2450', 'SA', 'BR', 'BR-GO', 'Goiânia', 'yes', 'SBGO', 'GYN', '', '', 'https://en.wikipedia.org/wiki/Santa_Genoveva_Airport', ''),
('0', '广元盘龙机场', 'aeroway=aerodrome
iata=GYS
icao=ZUGU
int_name=Guang Yuan Air', '105° 41'' 40.31', '32° 23'' 26.009', ' ', '0', 'GYS', 'ZUGU', ' ', '105.6945314', '32.390558', '31562', 'ZUGU', 'medium_airport', 'Guangyuan Airport', '32.3911018371582', '105.702003479004', NULL, 'AS', 'CN', 'CN-51', 'Guangyuan', 'yes', 'ZUGU', 'GYS', '', '', 'https://en.wikipedia.org/wiki/Guangyuan_Airport', ''),
('0', '八丈島空港', 'KSJ2:AAC=13401
KSJ2:AAC_label=東京都八丈島八丈町
KS', '139° 47'' 9.054', '33° 6'' 53.553"', ' ', '0', 'HAC', 'RJTH', 'Аэропорт Хатидзёдзима', '139.7858483', '33.1148757', '5620', 'RJTH', 'medium_airport', 'Hachijojima Airport', '33.1150016785', '139.785995483', '303', 'AS', 'JP', 'JP-13', 'Hachijojima', 'yes', 'RJTH', 'HAC', '', '', 'https://en.wikipedia.org/wiki/Hachijojima_Airport', ''),
('0', '海口美兰国际机场', 'aerodrome:type=international
aeroway=aerodrome
alt_name:vi=S', '110° 27'' 32.04', '19° 56'' 17.196', 'international', '0', 'HAK', 'ZJHK', 'Аэропорт Мэйлань', '110.4589012', '19.9381099', '27201', 'ZJHK', 'large_airport', 'Haikou Meilan International Airport', '19.9349', '110.459', '75', 'AS', 'CN', 'CN-46', 'Haikou (Meilan)', 'yes', 'ZJHK', 'HAK', '', '', 'https://en.wikipedia.org/wiki/Haikou_Meilan_International_Airport', 'ZGHK'),
('0', 'Sân bay quốc tế Nội Bài', 'aeroway=aerodrome
barrier=fence
iata=HAN
icao=VVNB
internati', '105° 48'' 18.67', '21° 13'' 8.238"', ' ', '0', 'HAN', 'VVNB', ' ', '105.8051868', '21.2189549', '26700', 'VVNB', 'large_airport', 'Noi Bai International Airport', '21.221201', '105.806999', '39', 'AS', 'VN', 'VN-64', 'Soc Son, Hanoi', 'yes', 'VVNB', 'HAN', '', '', 'https://en.wikipedia.org/wiki/Noi_Bai_International_Airport', 'Noibai Airport, Sân bay Quốc tế Nội Bài'),
('0', 'Aeropuerto Internacional José Martí', 'addr:street=Avenida Van Troy y Final
aeroway=aerodrome
alt_n', '82° 24'' 43.321', '22° 59'' 16.003', ' ', '64', 'HAV', 'MUHA', ' ', '-82.4120335', '22.9877787', '4839', 'MUHA', 'large_airport', 'José Martí International Airport', '22.989200592041', '-82.4091033935547', '210', 'NA', 'CU', 'CU-03', 'Havana', 'yes', 'MUHA', 'HAV', '', '', 'https://en.wikipedia.org/wiki/Jos%C3%A9_Mart%C3%AD_International_Airport', 'Habana'),
('0', 'Hobart International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '147° 30'' 45.01', '42° 50'' 11.622', 'public', '0', 'HBA', 'YMHB', ' ', '147.5125041', '-42.8365617', '27058', 'YMHB', 'medium_airport', 'Hobart International Airport', '-42.836101532', '147.509994507', '13', 'OC', 'AU', 'AU-TAS', 'Hobart', 'yes', 'YMHB', 'HBA', '', '', 'https://en.wikipedia.org/wiki/Hobart_International_Airport', ''),
('0', 'Hubli Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Hubli-D', '75° 4'' 58.160"', '15° 21'' 35.454', 'public', '662', 'HBX', 'VAHB', ' ', '75.0828221', '15.3598482', '26445', 'VAHB', 'medium_airport', 'Hubli Airport', '15.361700058', '75.0848999023', '2171', 'AS', 'IN', 'IN-KA', 'Hubli', 'yes', 'VOHB', 'HBX', '', 'http://www.airportsindia.org.in/allAirports/hubli_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Hubli_Airport', 'VAHB'),
('0', '恆春機場', 'aeroway=aerodrome
alt_name=龍勤營區
barrier=wall
iata=HC', '120° 43'' 49.89', '22° 2'' 20.371"', ' ', '0', 'HCN', 'RCKW', ' ', '120.7305265', '22.038992', '5518', 'RCKW', 'medium_airport', 'Hengchun Airport', '22.0410995483398', '120.730003356934', '46', 'AS', 'TW', 'TW-PIF', 'Hengchung', 'yes', 'RCKW', 'HCN', '', '', 'https://en.wikipedia.org/wiki/Hengchun_Airport', ''),
('0', '邯郸机场', 'aeroway=aerodrome
iata=HDG
icao=ZBHD
name=邯郸机场
name:', '114° 25'' 28.54', '36° 31'' 30.815', ' ', '0', 'HDG', 'ZBHD', ' ', '114.424596', '36.5252264', '300860', 'ZBHD', 'medium_airport', 'Handan Airport', '36.5258333333', '114.425555556', '229', 'AS', 'CN', 'CN-42', 'Handan', 'yes', 'ZBHD', 'HDG', '', '', 'https://en.wikipedia.org/wiki/Handan_Airport', ''),
('0', 'ဟဲဟိုး လေဆိပ်', 'aeroway=aerodrome
amenity=airport
iata=HEH
icao=VYHH
is_in=T', '96° 47'' 33.233', '20° 44'' 51.216', ' ', '0', 'HEH', 'VYHH', ' ', '96.7925647', '20.7475601', '26718', 'VYHH', 'medium_airport', 'Heho Airport', '20.7469997406006', '96.7919998168945', '3858', 'AS', 'MM', 'MM-17', 'Heho', 'yes', 'VYHH', 'HEH', '', '', 'https://en.wikipedia.org/wiki/Heho_Airport', ''),
('0', '呼和浩特白塔国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=HET
icao', '111° 49'' 25.39', '40° 51'' 1.222"', 'international', '0', 'HET', 'ZBHH', ' ', '111.8237196', '40.8503395', '27189', 'ZBHH', 'large_airport', 'Baita International Airport', '40.851398', '111.823997', '3556', 'AS', 'CN', 'CN-15', 'Hohhot', 'yes', 'ZBHH', 'HET', '', 'http://www.hhhtbtjc.com/', 'https://en.wikipedia.org/wiki/Hohhot_Baita_International_Airport', ''),
('0', '合肥新桥国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=HFE
icao', '116° 58'' 25.14', '31° 59'' 18.960', 'international', '0', 'HFE', 'ZSOF', ' ', '116.9736503', '31.9885999', '27222', 'ZSOF', 'medium_airport', 'Hefei Luogang International Airport', '31.7800006866455', '117.297996520996', '108', 'AS', 'CN', 'CN-34', 'Hefei', 'yes', 'ZSOF', 'HFE', '', '', 'https://en.wikipedia.org/wiki/Hefei_Luogang_International_Airport', ''),
('0', 'Mount Hagen Airport', 'aeroway=aerodrome
city_served=Mount Hagen
ele=1642
iata=HGU
', '144° 17'' 53.73', '5° 49'' 39.698"', ' ', '1642', 'HGU', 'AYMH', ' ', '144.2982584', '-5.827694', '63', 'AYMH', 'medium_airport', 'Mount Hagen Kagamuga Airport', '-5.82678985595703', '144.296005249023', '5388', 'OC', 'PG', 'PG-WHM', 'Mount Hagen', 'yes', 'AYMH', 'HGU', '', '', 'https://en.wikipedia.org/wiki/Mount_Hagen_Airport', ''),
('0', '広島空港', 'KSJ2:AAC=34204
KSJ2:AAC_label=広島県三原市
KSJ2:AD2=1
', '132° 55'' 14.75', '34° 26'' 14.900', 'public', '332', 'HIJ', 'RJOA', ' ', '132.9207664', '34.4374722', '5590', 'RJOA', 'medium_airport', 'Hiroshima Airport', '34.4361', '132.919006', '1088', 'AS', 'JP', 'JP-34', 'Hiroshima', 'yes', 'RJOA', 'HIJ', '', '', 'https://en.wikipedia.org/wiki/Hiroshima_Airport', ''),
('0', 'Honiara International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Guadalc', '160° 3'' 3.961"', '9° 25'' 45.012"', 'public', '9', 'HIR', 'AGGH', ' ', '160.0511004', '-9.4291701', '3', 'AGGH', 'large_airport', 'Honiara International Airport', '-9.428', '160.054993', '28', 'OC', 'SB', 'SB-CT', 'Honiara', 'yes', 'AGGH', 'HIR', '', '', 'https://en.wikipedia.org/wiki/Honiara_International_Airport', 'Henderson Field'),
('0', '函館空港', 'KSJ2:AAC=01202
KSJ2:AAC_label=北海道函館市
KSJ2:AD2=1
', '140° 49'' 25.81', '41° 46'' 12.958', 'public', '34', 'HKD', 'RJCH', 'Аэропорт Хакодате', '140.8238376', '41.7702662', '5545', 'RJCH', 'medium_airport', 'Hakodate Airport', '41.7700004578', '140.822006226', '151', 'AS', 'JP', 'JP-01', 'Hakodate', 'yes', 'RJCH', 'HKD', '', '', 'https://en.wikipedia.org/wiki/Hakodate_Airport', ''),
('0', '香港國際機場 Hong Kong International Airport', 'aerodrome:type=international
aeroway=aerodrome
alt_name=赤', '113° 54'' 54.31', '22° 18'' 32.357', 'international', '9', 'HKG', 'VHHH', ' ', '113.9150883', '22.3089881', '26535', 'VHHH', 'large_airport', 'Hong Kong International Airport', '22.308901', '113.915001', '28', 'AS', 'HK', 'HK-U-A', 'Hong Kong', 'yes', 'VHHH', 'HKG', '', 'http://www.hongkongairport.com/', 'https://en.wikipedia.org/wiki/Hong_Kong_International_Airport', 'Chek Lap Kok Airport, 赤鱲角機場'),
('0', 'Hokitika Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '170° 59'' 9.631', '42° 42'' 48.197', 'public', '45', 'HKK', 'NZHK', ' ', '170.9860087', '-42.713388', '5032', 'NZHK', 'medium_airport', 'Hokitika Airfield', '-42.7136001586914', '170.985000610352', '146', 'OC', 'NZ', 'NZ-WTC', '', 'yes', 'NZHK', 'HKK', '', '', 'https://en.wikipedia.org/wiki/Hokitika_Airport', ''),
('0', 'Hoskins Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=20
iata=HKN
icao', '150° 24'' 19.99', '5° 27'' 40.758"', 'public', '20', 'HKN', 'AYHK', ' ', '150.4055527', '-5.4613217', '59', 'AYHK', 'medium_airport', 'Kimbe Airport', '-5.46217012405396', '150.404998779297', '66', 'OC', 'PG', 'PG-WBK', 'Hoskins', 'yes', 'AYHK', 'HKN', '', '', 'https://en.wikipedia.org/wiki/Hoskins_Airport', ''),
('0', 'Lanseria Airport', 'aeroway=aerodrome
iata=HLA
icao=FALA
ifr=yes
licensed:sacaa=', '27° 55'' 30.530', '25° 56'' 21.392', ' ', '0', 'HLA', 'FALA', ' ', '27.9251473', '-25.9392755', '2800', 'FALA', 'medium_airport', 'Lanseria Airport', '-25.9384994507', '27.9260997772', '4517', 'AF', 'ZA', 'ZA-GT', 'Johannesburg', 'yes', 'FALA', 'HLA', '', '', 'https://en.wikipedia.org/wiki/Lanseria_Airport', ''),
('0', 'Bandar Udara Halim Perdanakusuma', 'addr:city=DKI Jakarta
addr:postcode=13610
addr:street=Jl. Pr', '106° 53'' 27.97', '6° 15'' 46.835"', ' ', '0', 'HLP', 'WIIH', ' ', '106.8911037', '-6.2630097', '26832', 'WIHH', 'medium_airport', 'Halim Perdanakusuma International Airport', '-6.26661014556885', '106.890998840332', '84', 'AS', 'ID', 'ID-JK', 'Jakarta', 'yes', 'WIHH', 'HLP', '', 'http://www.angkasapura2.co.id/cabang/hlp/content.php?menu=8&page_id=1', 'https://en.wikipedia.org/wiki/Halim_Perdanakusuma_International_Airport', 'WIIH,WIIX,WIID'),
('0', 'Hamilton International Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '175° 19'' 53.00', '37° 51'' 53.295', 'public', '52', 'HLZ', 'NZHN', ' ', '175.3313902', '-37.8648042', '5033', 'NZHN', 'medium_airport', 'Hamilton International Airport', '-37.8666992188', '175.332000732', '172', 'OC', 'NZ', 'NZ-WKO', 'Hamilton', 'yes', 'NZHN', 'HLZ', '', 'http://www.hamiltonairport.co.nz/', 'https://en.wikipedia.org/wiki/Hamilton_International_Airport', ''),
('0', '哈密机场', 'aerodrome:type=military/public
aeroway=aerodrome
ele=823.9
i', '93° 39'' 56.356', '42° 50'' 28.990', 'military/public', '824', 'HMI', 'ZWHM', ' ', '93.6656545', '42.841386', '31609', 'ZWHM', 'medium_airport', 'Hami Airport', '42.8414', '93.669197', '2703', 'AS', 'CN', 'CN-65', 'Hami', 'yes', 'ZWHM', 'HMI', '', '', 'https://en.wikipedia.org/wiki/Hami_Airport', 'Kumul, Qumul'),
('0', '花巻空港', 'KSJ2:AAC=03205
KSJ2:AAC_label=岩手県花巻市
KSJ2:AD2=5
', '141° 8'' 7.410"', '39° 25'' 42.174', ' ', '0', 'HNA', 'RJSI', ' ', '141.1353917', '39.4283818', '5610', 'RJSI', 'medium_airport', 'Morioka Hanamaki Airport', '39.4286', '141.134995', '297', 'AS', 'JP', 'JP-03', 'Hanamaki', 'yes', 'RJSI', 'HNA', '', '', 'https://en.wikipedia.org/wiki/Hanamaki_Airport', ''),
('0', 'Daniel K. Inouye International Airport', 'addr:state=HI
aerodrome=international
aerodrome:type=militar', '157° 55'' 34.25', '21° 19'' 16.310', 'military/public', '4', 'HNL', 'PHNL', 'Международный аэропорт им. Дэ', '-157.9261811', '21.3211971', '5453', 'PHNL', 'large_airport', 'Daniel K Inouye International Airport', '21.32062', '-157.924228', '13', 'NA', 'US', 'US-HI', 'Honolulu', 'yes', 'PHNL', 'HNL', 'HNL', 'http://airports.hawaii.gov/hnl/', 'https://en.wikipedia.org/wiki/Daniel_K._Inouye_International_Airport', 'Hickam Air Force Base, HIK, PHIK, KHNL, Honolulu International'),
('0', 'Hāna Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
ele=21', '156° 0'' 53.264', '20° 47'' 42.326', 'public', '21', 'HNM', 'PHHN', ' ', '-156.0147955', '20.7950906', '5444', 'PHHN', 'medium_airport', 'Hana Airport', '20.7956008911133', '-156.014007568359', '78', 'NA', 'US', 'US-HI', 'Hana', 'yes', 'PHHN', 'HNM', 'HNM', '', 'https://en.wikipedia.org/wiki/Hana_Airport', ''),
('0', 'Aeropuerto Internacional Frank País', 'aeroway=aerodrome
closest_town=Holguin
ele=110
iata=HOG
icao', '76° 18'' 47.691', '20° 47'' 16.221', ' ', '110', 'HOG', 'MUHG', ' ', '-76.3132476', '20.7878391', '4840', 'MUHG', 'medium_airport', 'Frank Pais International Airport', '20.7856006622314', '-76.3151016235352', '361', 'NA', 'CU', 'CU-11', 'Holguin', 'yes', 'MUHG', 'HOG', '', '', 'https://en.wikipedia.org/wiki/Frank_Pa%C3%ADs_Airport', ''),
('0', '哈尔滨太平国际机场', 'aerodrome:type=international
aeroway=aerodrome
closest_town=', '126° 14'' 50.27', '45° 37'' 21.833', 'international', '139', 'HRB', 'ZYHB', 'Аэропорт Тайпин', '126.2472991', '45.6227313', '27238', 'ZYHB', 'large_airport', 'Taiping Airport', '45.6234016418457', '126.25', '457', 'AS', 'CN', 'CN-23', 'Harbin', 'yes', 'ZYHB', 'HRB', '', '', 'https://en.wikipedia.org/wiki/Harbin_Taiping_International_Airport', ''),
('0', 'Robert Gabriel Mugabe International Airport', 'aerodrome=international
aerodrome:type=military/public
aerow', '31° 5'' 30.487"', '17° 56'' 0.607"', 'military/public', '1490', 'HRE', 'FVHA', ' ', '31.0918019', '-17.933502', '3005', 'FVHA', 'large_airport', 'Robert Gabriel Mugabe International Airport', '-17.931801', '31.0928', '4887', 'AF', 'ZW', 'ZW-HA', 'Harare', 'yes', 'FVRG', 'HRE', '', '', 'https://en.wikipedia.org/wiki/Harare_International_Airport', 'FVHA,Harare'),
('0', 'Mattala Rajapaksa International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '81° 7'' 29.994"', '6° 17'' 15.722"', 'public', '48', 'HRI', 'VCRI', ' ', '81.1249982', '6.2877006', '309579', 'VCRI', 'large_airport', 'Mattala Rajapaksa International Airport', '6.284467', '81.124128', '157', 'AS', 'LK', 'LK-3', '', 'yes', 'VCRI', 'HRI', 'VCRI', 'http://www.mria.lk/', 'https://en.wikipedia.org/wiki/Mattala_Rajapaksa_International_Airport', 'Hambantota International Airport'),
('0', 'Valley International Airport', 'addr:state=TX
aeroway=aerodrome
alt_name=Rio Grande Valley I', '97° 39'' 12.699', '26° 13'' 51.862', ' ', '10', 'HRL', 'KHRL', ' ', '-97.6535276', '26.2310729', '3591', 'KHRL', 'medium_airport', 'Valley International Airport', '26.2285003662109', '-97.6544036865234', '36', 'NA', 'US', 'US-TX', 'Harlingen', 'yes', 'KHRL', 'HRL', 'HRL', '', 'https://en.wikipedia.org/wiki/Valley_International_Airport', ''),
('0', '佐賀空港（九州佐賀国際空港）', 'KSJ2:AAC=41302
KSJ2:AAC_label=佐賀県佐賀郡川副町
KS', '130° 18'' 16.19', '33° 8'' 57.899"', ' ', '0', 'HSG', 'RJFS', ' ', '130.3044984', '33.1494164', '5571', 'RJFS', 'medium_airport', 'Saga Airport', '33.1497001648', '130.302001953', '6', 'AS', 'JP', 'JP-41', 'Saga', 'yes', 'RJFS', 'HSG', '', '', 'https://en.wikipedia.org/wiki/Saga_Airport', ''),
('0', 'Hamilton Island Airport', 'aeroway=aerodrome
area=yes
iata=HTI
icao=YBHM
name=Hamilton', '148° 57'' 5.600', '20° 21'' 26.492', ' ', '0', 'HTI', 'YBHM', ' ', '148.9515556', '-20.357359', '26909', 'YBHM', 'medium_airport', 'Hamilton Island Airport', '-20.3581008911', '148.95199585', '15', 'OC', 'AU', 'AU-QLD', 'Hamilton Island', 'yes', 'YBHM', 'HTI', '', 'http://www.hamiltonisland.com.au/airport/', 'https://en.wikipedia.org/wiki/Hamilton_Island_Airport', ''),
('0', '和田机场', 'aeroway=aerodrome
iata=HTN
icao=ZWTN
is_in=Xinjiang, China
n', '79° 51'' 57.795', '37° 2'' 17.422"', ' ', '0', 'HTN', 'ZWTN', ' ', '79.8660541', '37.0381728', '27235', 'ZWTN', 'medium_airport', 'Hotan Airport', '37.038501739502', '79.8648986816406', '4672', 'AS', 'CN', 'CN-65', 'Hotan', 'yes', 'ZWTN', 'HTN', '', '', 'https://en.wikipedia.org/wiki/Hotan_Airport', 'Khotan, Heitan Air Base'),
('0', 'Sân bay quốc tế Phú Bài', 'aerodrome:type=public
aeroway=aerodrome
ele=15
iata=HUI
icao', '107° 42'' 16.60', '16° 24'' 1.222"', 'public', '15', 'HUI', 'VVBP', 'Хюэ (аэропорт)', '107.7046124', '16.4003394', '26702', 'VVPB', 'medium_airport', 'Phu Bai International Airport', '16.401501', '107.703003', '48', 'AS', 'VN', 'VN-56', 'Huế', 'yes', 'VVPB', 'HUI', '', '', 'https://en.wikipedia.org/wiki/Phu_Bai_International_Airport', 'Phubai'),
('0', '花蓮機場', 'addr:full=花蓮縣新城鄉嘉新村花師街37號
aerodrom', '121° 37'' 5.734', '24° 1'' 25.274"', 'public', '16', 'HUN', 'RCYU', ' ', '121.6182595', '24.0236872', '5530', 'RCYU', 'medium_airport', 'Hualien Airport', '24.023099899292', '121.61799621582', '52', 'AS', 'TW', 'TW-HUA', 'Hualien City', 'yes', 'RCYU', 'HUN', '', 'http://www.hulairport.gov.tw/', 'https://en.wikipedia.org/wiki/Hualien_Airport', '花蓮機場'),
('0', 'Aeropuerto Internacional de Bahías de Huatulco', 'aerodrome:type=public
aeroway=aerodrome
ele=141
iata=HUX
ica', '96° 15'' 37.440', '15° 46'' 27.783', 'public', '141', 'HUX', 'MMBT', ' ', '-96.2604', '15.7743841', '4692', 'MMBT', 'medium_airport', 'Bahías de Huatulco International Airport', '15.7753', '-96.262604', '464', 'NA', 'MX', 'MX-OAX', 'Huatulco', 'yes', 'MMBT', 'HUX', 'HU1', '', 'https://en.wikipedia.org/wiki/Bah%C3%ADas_de_Huatulco_International_Airport', ''),
('0', 'Aéroport d''In Aménas Zerzaitine', 'aerodrome=international
aeroway=aerodrome
area=yes
iata=IAM
', '9° 38'' 42.060"', '28° 3'' 19.450"', ' ', '0', 'IAM', 'DAUZ', ' ', '9.6450166', '28.0554028', '2086', 'DAUZ', 'medium_airport', 'Zarzaitine - In Aménas Airport', '28.0515', '9.64291', '1847', 'AF', 'DZ', 'DZ-33', 'In Aménas', 'yes', 'DAUZ', 'IAM', '', '', 'https://en.wikipedia.org/wiki/In_Amenas_Airport', ''),
('0', 'Aeropuerto Nacional Perales', 'aeroway=aerodrome
iata=IBE
icao=SKIB
name=Aeropuerto Naciona', '75° 8'' 7.743"', '4° 25'' 18.747"', ' ', '0', 'IBE', 'SKIB', ' ', '-75.1354843', '4.4218743', '6124', 'SKIB', 'medium_airport', 'Perales Airport', '4.42161', '-75.1333', '2999', 'SA', 'CO', 'CO-TOL', 'Ibagué', 'yes', 'SKIB', 'IBE', 'IBE', '', 'https://en.wikipedia.org/wiki/Perales_Airport', ''),
('0', 'Devi Ahilyabai Holkar Airport', 'aeroway=aerodrome
iata=IDR
icao=VAID
is_in:country=India
nam', '75° 47'' 48.317', '22° 43'' 10.877', ' ', '0', 'IDR', 'VAID', ' ', '75.7967546', '22.7196881', '26446', 'VAID', 'medium_airport', 'Devi Ahilyabai Holkar Airport', '22.7217998505', '75.8011016846', '1850', 'AS', 'IN', 'IN-MP', 'Indore', 'yes', 'VAID', 'IDR', '', 'http://aai.aero/allAirports/indore_technicalinfo.jsp', 'https://en.wikipedia.org/wiki/Devi_Ahilyabai_Holkar_International_Airport', ''),
('0', 'Inagua Airport', 'addr:city=Matthew Town
addr:country=BS
aeroway=aerodrome
ele', '73° 40'' 2.208"', '20° 58'' 27.858', ' ', '2', 'IGA', 'MYIG', ' ', '-73.66728', '20.974405', '4947', 'MYIG', 'medium_airport', 'Inagua Airport', '20.9750003814697', '-73.6669006347656', '8', 'NA', 'BS', 'BS-IN', 'Matthew Town', 'yes', 'MYIG', 'IGA', '', '', 'https://en.wikipedia.org/wiki/Inagua_Airport', ''),
('0', 'Aeropuerto Internacional de Puerto Iguazú', 'aerodrome=public
aeroway=aerodrome
alt_name=Aeropuerto Catar', '54° 28'' 23.302', '25° 44'' 11.991', ' ', '279', 'IGR', 'SARI', ' ', '-54.4731394', '-25.7366641', '5806', 'SARI', 'medium_airport', 'Cataratas Del Iguazú International Airport', '-25.737301', '-54.4734', '916', 'SA', 'AR', 'AR-N', 'Puerto Iguazu', 'yes', 'SARI', 'IGR', 'IGU', 'http://www.aa2000.com.ar/iguazu', 'https://en.wikipedia.org/wiki/Cataratas_del_Iguaz%C3%BA_International_Airport', 'Iguaçu'),
('0', 'Aeroporto Internacional de Foz do Iguaçu/Cataratas', 'aeroway=aerodrome
iata=IGU
icao=SBFI
name=Aeroporto Internac', '54° 29'' 12.405', '25° 35'' 46.661', ' ', '0', 'IGU', 'SBFI', ' ', '-54.4867792', '-25.5962948', '5900', 'SBFI', 'medium_airport', 'Cataratas International Airport', '-25.6002788543701', '-54.4850006103516', '786', 'SA', 'BR', 'BR-PR', 'Foz Do Iguaçu', 'yes', 'SBFI', 'IGU', '', '', 'https://en.wikipedia.org/wiki/Foz_do_Igua%C3%A7u_International_Airport', ''),
('0', 'Iloilo International Airport', 'aerodrome:type=public
aeroway=aerodrome
iata=ILO
icao=RPVI
n', '122° 29'' 33.75', '10° 50'' 6.900"', 'public', '0', 'ILO', 'RPVI', ' ', '122.4927087', '10.8352501', '5739', 'RPVI', 'medium_airport', 'Iloilo International Airport', '10.833017', '122.493358', '27', 'AS', 'PH', 'PH-ILI', 'Iloilo (Cabatuan)', 'yes', 'RPVI', 'ILO', '', '', 'https://en.wikipedia.org/wiki/Iloilo_International_Airport', ''),
('0', 'Aéroport de Moué - Île des Pins', 'aeroway=aerodrome
iata=ILP
icao=NWWE
name=Aéroport de Moué', '167° 27'' 1.873', '22° 35'' 22.925', ' ', '0', 'ILP', 'NWWE', ' ', '167.4505202', '-22.5897014', '5014', 'NWWE', 'medium_airport', 'Île des Pins Airport', '-22.5888996124268', '167.455993652344', '315', 'OC', 'NC', 'NC-U-A', 'Île des Pins', 'yes', 'NWWE', 'ILP', '', '', 'https://en.wikipedia.org/wiki/%C3%8Ele_des_Pins_Airport', ''),
('0', 'Nauru International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Nauru
e', '166° 55'' 9.147', '0° 32'' 50.165"', 'public', '7', 'INU', 'ANYN', ' ', '166.9192076', '-0.547268', '8', 'ANYN', 'medium_airport', 'Nauru International Airport', '-0.547458', '166.919006', '22', 'OC', 'NR', 'NR-14', 'Yaren District', 'yes', 'ANYN', 'INU', '', '', 'https://en.wikipedia.org/wiki/Nauru_International_Airport', 'ANAU'),
('0', 'Aeroporto Regional do Vale do Aço', 'aeroway=aerodrome
alt_name=Aeroporto de Ipatinga
iata=IPN
ic', '42° 29'' 8.541"', '19° 28'' 14.322', ' ', '0', 'IPN', 'SBIP', ' ', '-42.4857058', '-19.470645', '5918', 'SBIP', 'medium_airport', 'Usiminas Airport', '-19.470699310303', '-42.487598419189', '784', 'SA', 'BR', 'BR-MG', 'Ipatinga', 'yes', 'SBIP', 'IPN', '', '', 'https://en.wikipedia.org/wiki/Usiminas_Airport', ''),
('0', '庆阳机场', 'aeroway=aerodrome
iata=IQN
icao=ZLQY
name=庆阳机场
name:', '107° 36'' 0.208', '35° 48'' 6.867"', ' ', '0', 'IQN', 'ZLQY', ' ', '107.6000578', '35.8019076', '31685', 'ZLQY', 'medium_airport', 'Qingyang Airport', '35.799702', '107.602997', NULL, 'AS', 'CN', 'CN-62', 'Qingyang', 'yes', 'ZLQY', 'IQN', '', '', 'https://en.wikipedia.org/wiki/Qingyang_Airport', 'Dingxi Air Base'),
('0', 'Aeropuerto Diego Aracena', 'aeroway=aerodrome
alt_name=Aeropuerto Iquique
iata=IQQ
icao=', '70° 10'' 59.020', '20° 32'' 2.135"', ' ', '0', 'IQQ', 'SCDA', ' ', '-70.183061', '-20.5339265', '6012', 'SCDA', 'medium_airport', 'Diego Aracena Airport', '-20.5352001190186', '-70.1812973022461', '155', 'SA', 'CL', 'CL-TA', 'Iquique', 'yes', 'SCDA', 'IQQ', '', '', '', ''),
('0', 'Crnl. FAP Francisco Secada Vignetta International Airport', 'aeroway=aerodrome
closest_town=Iquitos
ele=124
iata=IQT
icao', '73° 18'' 37.191', '3° 47'' 8.533"', ' ', '124', 'IQT', 'SPQT', ' ', '-73.3103308', '-3.7857037', '6232', 'SPQT', 'medium_airport', 'Coronel FAP Francisco Secada Vignetta International Airport', '-3.78473997116089', '-73.3087997436523', '306', 'SA', 'PE', 'PE-LOR', 'Iquitos', 'yes', 'SPQT', 'IQT', '', '', 'https://en.wikipedia.org/wiki/Crnl._FAP_Francisco_Secada_Vignetta_International_Airport', ''),
('0', 'Mount Isa Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Mount I', '139° 29'' 20.26', '20° 39'' 58.152', 'public', '342', 'ISA', 'YBMA', ' ', '139.488962', '-20.6661532', '26917', 'YBMA', 'medium_airport', 'Mount Isa Airport', '-20.6639003754', '139.488998413', '1121', 'OC', 'AU', 'AU-QLD', 'Mount Isa', 'yes', 'YBMA', 'ISA', '', '', 'https://en.wikipedia.org/wiki/Mount_Isa_Airport', ''),
('0', 'اسلام آباد بین الاقوامی ہوائی اڈ', 'addr:city=Islamabad
addr:housenumber=1-20
addr:street=Airpor', '72° 49'' 36.316', '33° 33'' 4.536"', ' ', '0', 'ISB', 'OPRN', ' ', '72.8267544', '33.5512601', '333692', 'OPIS', 'large_airport', 'Islamabad International Airport', '33.549', '72.82566', '1761', 'AS', 'PK', 'PK-PB', 'Islamabad', 'yes', 'OPIS', 'ISB', '', 'http://www.islamabadairport.com.pk/', 'https://en.wikipedia.org/wiki/Islamabad_International_Airport', ''),
('0', '南ぬ島石垣空港', 'aeroway=aerodrome
iata=ISG
icao=ROIG
name=南ぬ島石垣空', '124° 14'' 43.12', '24° 23'' 47.993', ' ', '0', 'ISG', 'ROIG', ' ', '124.2453112', '24.3966647', '327608', 'ISG', 'medium_airport', 'New Ishigaki Airport', '24.396389', '124.245', '102', 'AS', 'JP', 'JP-47', 'Ishigaki', 'yes', 'ROIG', 'ISG', '', '', 'https://en.wikipedia.org/wiki/New_Ishigaki_Airport', ''),
('0', 'Nashik Gandhinagar Airport', 'addr:country=IN
aeroway=aerodrome
area=yes
ele=1959 feet
iat', '73° 48'' 31.564', '19° 58'' 0.199"', ' ', '0', 'ISK', 'VANR', ' ', '73.8087679', '19.966722', '3301', 'VAOZ', 'medium_airport', 'Nashik Airport', '20.119101', '73.912903', '1900', 'AS', 'IN', 'IN-MM', 'Nasik', 'yes', 'VAOZ', 'ISK', '', '', 'https://en.wikipedia.org/wiki/Nashik_Airport', 'VA35'),
('0', 'Hilo International Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
ele=10', '155° 2'' 41.949', '19° 43'' 8.913"', 'public', '10', 'ITO', 'PHTO', ' ', '-155.0449857', '19.7191426', '5457', 'PHTO', 'medium_airport', 'Hilo International Airport', '19.721399307251', '-155.048004150391', '38', 'NA', 'US', 'US-HI', 'Hilo', 'yes', 'PHTO', 'ITO', 'ITO', '', 'https://en.wikipedia.org/wiki/Hilo_International_Airport', ''),
('0', 'Niue International Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Hanan Inter', '169° 55'' 32.35', '19° 4'' 44.432"', 'public', '64', 'IUE', 'NIUE', ' ', '-169.9256554', '-19.0790088', '4974', 'NIUE', 'medium_airport', 'Niue International Airport', '-19.0790309906006', '-169.925598144531', '209', 'OC', 'NU', 'NU-U-A', 'Alofi', 'yes', 'NIUE', 'IUE', '', '', 'https://en.wikipedia.org/wiki/Niue_International_Airport', 'Hanan International Airport'),
('0', 'Invercargill Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '168° 18'' 59.13', '46° 24'' 58.878', ' ', '2', 'IVC', 'NZNV', ' ', '168.316425', '-46.416355', '5045', 'NZNV', 'medium_airport', 'Invercargill Airport', '-46.4123992919922', '168.313003540039', '5', 'OC', 'NZ', 'NZ-STL', 'Invercargill', 'yes', 'NZNV', 'IVC', '', 'http://www.invercargillairport.co.nz/index.htm', 'https://en.wikipedia.org/wiki/Invercargill_Airport', ''),
('0', '石見空港', 'KSJ2:AAC=32204
KSJ2:AAC_label=島根県益田市
KSJ2:AD2=5
', '131° 47'' 27.57', '34° 40'' 35.068', ' ', '54', 'IWJ', 'RJOW', ' ', '131.790993', '34.6764077', '5602', 'RJOW', 'medium_airport', 'Iwami Airport', '34.676399231', '131.789993286', '184', 'AS', 'JP', 'JP-32', 'Masuda', 'yes', 'RJOW', 'IWJ', '', '', 'https://en.wikipedia.org/wiki/Iwami_Airport', ''),
('0', 'Bagdogra Airport', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Ci', '88° 19'' 47.277', '26° 40'' 54.473', 'military/public', '126', 'IXB', 'VEBD', ' ', '88.3297991', '26.6817981', '26491', 'VEBD', 'medium_airport', 'Bagdogra Airport', '26.6812000274658', '88.3285980224609', '412', 'AS', 'IN', 'IN-WB', 'Siliguri', 'yes', 'VEBD', 'IXB', '', 'http://aai.aero/allAirports/bagdogra_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Bagdogra_Airport', 'Bagdogra Air Force Station'),
('0', 'Chandigarh Airport', 'aeroway=aerodrome
barrier=wall
iata=IXC
icao=VICG
is_in:coun', '76° 47'' 21.979', '30° 40'' 27.334', ' ', '0', 'IXC', 'VICG', ' ', '76.7894385', '30.6742594', '26550', 'VICG', 'medium_airport', 'Chandigarh Airport', '30.6735000610352', '76.7884979248047', '1012', 'AS', 'IN', 'IN-CH', 'Chandigarh', 'yes', 'VICG', 'IXC', '', '', 'https://en.wikipedia.org/wiki/Chandigarh_Airport', 'Chandigarh Air Force Station'),
('0', 'Allahabad Bamrauli Airport', 'aeroway=aerodrome
iata=IXD
icao=VEAB
is_in:country=India
mil', '81° 44'' 13.483', '25° 26'' 19.229', ' ', '0', 'IXD', 'VEAB', ' ', '81.7370785', '25.4386747', '26538', 'VIAL', 'medium_airport', 'Allahabad Airport', '25.4401', '81.733902', '322', 'AS', 'IN', 'IN-UP', 'Allahabad', 'yes', 'VEAB', 'IXD', '', 'https://www.aai.aero/en/node/75695', 'https://en.wikipedia.org/wiki/Allahabad_Airport', 'VIAL, Allahabad Bamrauli Airport, Bamrauli Air Force Station'),
('0', 'Mangalore International Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Kenjar Airp', '74° 53'' 1.832"', '12° 57'' 19.812', 'public', '103', 'IXE', 'VOML', ' ', '74.8838422', '12.9555034', '26617', 'VOML', 'medium_airport', 'Mangalore International Airport', '12.9612998962', '74.8900985718', '337', 'AS', 'IN', 'IN-KA', 'Mangalore', 'yes', 'VOML', 'IXE', '', '', 'https://en.wikipedia.org/wiki/Mangalore_International_Airport', 'Bajpe Airport'),
('0', 'Leh Kushok Bakula Rimpochee Airport', 'aeroway=aerodrome
alt_name=Leh Airport
ele=3256
iata=IXL
ica', '77° 32'' 46.416', '34° 8'' 6.974"', ' ', '3256', 'IXL', 'VILH', ' ', '77.5462267', '34.1352705', '26569', 'VILH', 'medium_airport', 'Leh Kushok Bakula Rimpochee Airport', '34.1358985901', '77.5465011597', '10682', 'AS', 'IN', 'IN-JK', 'Leh', 'yes', 'VILH', 'IXL', '', 'http://airportsindia.org.in/allAirports/leh_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Leh_Kushok_Bakula_Rimpochee_Airport', 'Leh Air Force Station'),
('0', 'Madurai International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Madurai', '78° 5'' 22.908"', '9° 50'' 10.727"', 'public', '136', 'IXM', 'VOMD', ' ', '78.0896966', '9.8363131', '26616', 'VOMD', 'medium_airport', 'Madurai Airport', '9.83450984955', '78.0933990479', '459', 'AS', 'IN', 'IN-TN', 'Madurai', 'yes', 'VOMD', 'IXM', '', 'http://www.airportsindia.org.in/allAirports/madurai_airpo_gi.jsp', 'https://en.wikipedia.org/wiki/Madurai_Airport', 'Madurai Air Force Station'),
('0', 'Ranchi Birsa Munda Airport', 'aeroway=aerodrome
ele=654
iata=IXR
icao=VERC
is_in:country=I', '85° 19'' 17.229', '23° 18'' 53.616', ' ', '654', 'IXR', 'VERC', ' ', '85.3214526', '23.3148934', '26519', 'VERC', 'medium_airport', 'Birsa Munda Airport', '23.3143005371', '85.3217010498', '2148', 'AS', 'IN', 'IN-JH', 'Ranchi', 'yes', 'VERC', 'IXR', '', 'http://airportsindia.org.in/allAirports/ranchi.jsp', 'https://en.wikipedia.org/wiki/Birsa_Munda_Airport', ''),
('0', 'Silchar Airport', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '92° 58'' 49.596', '24° 54'' 45.039', 'military/public', '107', 'IXS', 'VEKU', ' ', '92.9804433', '24.9125108', '26509', 'VEKU', 'medium_airport', 'Silchar Airport', '24.9129009247', '92.9786987305', '352', 'AS', 'IN', 'IN-AS', 'Silchar', 'yes', 'VEKU', 'IXS', '', 'http://aai.aero/allAirports/silchar_general.jsp', 'https://en.wikipedia.org/wiki/Silchar_Airport', 'Kumbhigram Air Force Station'),
('0', 'Aurangabad Chikkalthana Airport', 'aeroway=aerodrome
ele=582
iata=IXU
icao=VAAU
is_in=Aurangaba', '75° 23'' 37.832', '19° 51'' 52.741', ' ', '582', 'IXU', 'VAAU', ' ', '75.3938422', '19.8646504', '26433', 'VAAU', 'medium_airport', 'Aurangabad Airport', '19.862699508667', '75.3981018066406', '1911', 'AS', 'IN', 'IN-MM', 'Aurangabad', 'yes', 'VAAU', 'IXU', '', 'http://aai.aero/allAirports/aurangabad_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Aurangabad_Airport', 'Chikkalthana Airport'),
('0', 'Kandla Airport', 'aeroway=aerodrome
ele=29
iata=IXY
icao=VAKE
is_in:country=In', '70° 6'' 2.605"', '23° 6'' 44.754"', ' ', '29', 'IXY', 'VAKE', ' ', '70.1007236', '23.1124317', '26449', 'VAKE', 'medium_airport', 'Kandla Airport', '23.1127', '70.100304', '96', 'AS', 'IN', 'IN-GJ', 'Kandla', 'yes', 'VAKE', 'IXY', '', 'http://aai.aero/allAirports/kandla_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Kandla_Airport', ''),
('0', 'Vir Savakar International Airport', 'addr:street=Lambaline
aeroway=aerodrome
iata=IXZ
icao=VOPB
i', '92° 43'' 45.858', '11° 38'' 27.222', ' ', '0', 'IXZ', 'VOPB', ' ', '92.729405', '11.640895', '26620', 'VOPB', 'medium_airport', 'Vir Savarkar International Airport', '11.6412000656128', '92.7296981811523', '14', 'AS', 'IN', 'IN-AN', 'Port Blair', 'yes', 'VOPB', 'IXZ', '', '', 'https://en.wikipedia.org/wiki/Vir_Savarkar_Airport', 'Port Blair Airport, Port Blair Air Force Station'),
('0', 'Aeroporto Regional Presidente Itamar Franco', 'aeroway=aerodrome
alt_name=Aeroporto Regional da Zona da Mat', '43° 10'' 11.488', '21° 30'' 40.256', ' ', '0', 'IZA', 'SBZM', ' ', '-43.1698577', '-21.5111821', '35836', 'SDZY', 'medium_airport', 'Presidente Itamar Franco Airport', '-21.513056', '-43.173058', '1348', 'SA', 'BR', 'BR-MG', 'Juiz de Fora', 'yes', 'SBZM', 'IZA', 'MG0006', '', 'https://en.wikipedia.org/wiki/Zona_da_Mata_Regional_Airport', 'SDSY, Zona da Mata Regional, Goianá'),
('0', '出雲空港', 'KSJ2:AAC=32401
KSJ2:AAC_label=島根県簸川郡斐川町
KS', '132° 53'' 21.21', '35° 24'' 47.960', 'public', '2', 'IZO', 'RJOC', 'Аэропорт Изумо', '132.8892252', '35.4133223', '5592', 'RJOC', 'medium_airport', 'Izumo Enmusubi Airport', '35.413601', '132.889999', '15', 'AS', 'JP', 'JP-32', 'Izumo', 'yes', 'RJOC', 'IZO', '', '', 'https://en.wikipedia.org/wiki/Izumo_Airport', 'izumo, enmusubi, en-musubi'),
('0', 'Base Aerea Militar #2 Ixtepec', 'aeroway=aerodrome
iata=IZT
icao=MMIT
name=Base Aerea Militar', '95° 5'' 19.692"', '16° 27'' 2.398"', ' ', '0', 'IZT', 'MMIT', ' ', '-95.0888032', '16.450666', '4717', 'MMIT', 'medium_airport', 'Ixtepec Airport', '16.449301', '-95.093697', '164', 'NA', 'MX', 'MX-OAX', 'Ixtepec', 'yes', 'MMIT', 'IZT', '', '', 'https://en.wikipedia.org/wiki/Ixtepec_Airport', ''),
('0', 'Jaipur International Airport', 'aeroway=aerodrome
iata=JAI
icao=VIJP
is_in:country=India
nam', '75° 49'' 8.815"', '26° 49'' 41.055', ' ', '0', 'JAI', 'VIJP', ' ', '75.8191154', '26.8280709', '26563', 'VIJP', 'medium_airport', 'Jaipur International Airport', '26.8242', '75.812202', '1263', 'AS', 'IN', 'IN-RJ', 'Jaipur', 'yes', 'VIJP', 'JAI', '', 'https://www.jaipurairport.com/', 'https://en.wikipedia.org/wiki/Jaipur_International_Airport', ''),
('0', 'Aéroport de Jacmel', 'addr:country=HT
aeroway=aerodrome
area=yes
city_served=Jacme', '72° 31'' 6.651"', '18° 14'' 27.614', ' ', '51', 'JAK', 'MTJA', ' ', '-72.5185141', '18.2410039', '4824', 'MTJA', 'medium_airport', 'Jacmel Airport', '18.2411003112793', '-72.5185012817383', '167', 'NA', 'HT', 'HT-SE', 'Jacmel', 'yes', 'MTJA', 'JAK', '', '', 'https://en.wikipedia.org/wiki/Jacmel_Airport', ''),
('0', 'Francisco Carle', 'aeroway=aerodrome
closest_town=Jauja
ele=3363
iata=JAU
icao=', '75° 28'' 24.264', '11° 47'' 0.422"', ' ', '3363', 'JAU', 'SPJJ', ' ', '-75.4734067', '-11.7834505', '6221', 'SPJJ', 'medium_airport', 'Francisco Carle Airport', '-11.7831001282', '-75.4733963013', '11034', 'SA', 'PE', 'PE-JUN', 'Jauja', 'yes', 'SPJJ', 'JAU', '', '', 'https://en.wikipedia.org/wiki/Francisco_Carle_Airport', ''),
('0', 'Aeropuerto Internacional Joaquín Balaguer', 'aeroway=aerodrome
iata=JBQ
icao=MDJB
name=Aeropuerto Interna', '69° 59'' 2.131"', '18° 34'' 16.194', ' ', '0', 'JBQ', 'MDJB', ' ', '-69.9839253', '18.5711651', '4634', 'MDJB', 'medium_airport', 'La Isabela International Airport', '18.5725002288818', '-69.9856033325195', '98', 'NA', 'DO', 'DO-01', 'La Isabela', 'yes', 'MDJB', 'JBQ', '', '', 'https://en.wikipedia.org/wiki/La_Isabela_International_Airport', 'MDLI, SDI, Dr Joaquin Balaguer International Airport'),
('0', 'Jodhpur Airbase', 'aeroway=aerodrome
barrier=fence
closest_town=Jodhpur
ele=219', '73° 3'' 6.090"', '26° 15'' 4.834"', ' ', '219', 'JDH', 'VIJO', ' ', '73.0516916', '26.2513428', '26562', 'VIJO', 'medium_airport', 'Jodhpur Airport', '26.2511005401611', '73.0488967895508', '717', 'AS', 'IN', 'IN-RJ', 'Jodhpur', 'yes', 'VIJO', 'JDH', '', '', 'https://en.wikipedia.org/wiki/Jodhpur_Airport', 'Jodhpur Air Force Station'),
('0', '景德镇罗家机场', 'aeroway=aerodrome
iata=JDZ
icao=ZSJD
name=景德镇罗家机', '117° 10'' 34.59', '29° 20'' 23.007', ' ', '0', 'JDZ', 'ZSJD', ' ', '117.1762768', '29.3397242', '31695', 'ZSJD', 'medium_airport', 'Jingdezhen Airport', '29.3386001587', '117.176002502', '112', 'AS', 'CN', 'CN-36', 'Jingdezhen', 'yes', 'ZSJD', 'JDZ', '', '', 'https://en.wikipedia.org/wiki/Jingdezhen_Luojia_Airport', 'Fouliang Air Base'),
('0', 'Govardhanpur Airport Jamnagar', 'aerodrome:type=military/public
aeroway=aerodrome
ele=21
iata', '70° 0'' 29.331"', '22° 27'' 59.448', 'military/public', '21', 'JGA', 'VAJM', ' ', '70.0081474', '22.4665132', '26448', 'VAJM', 'medium_airport', 'Jamnagar Airport', '22.4654998779297', '70.0126037597656', '69', 'AS', 'IN', 'IN-GJ', 'Jamnagar', 'yes', 'VAJM', 'JGA', '', '', 'https://en.wikipedia.org/wiki/Jamnagar_Airport', 'Jamnagar Air Force Station'),
('0', '嘉峪关机场', 'aerodrome:type=military/public
aeroway=aerodrome
ele=1558.1
', '98° 20'' 18.426', '39° 51'' 34.698', 'military/public', '1558', 'JGN', 'ZLJQ', ' ', '98.3384518', '39.8596383', '31699', 'ZLJQ', 'medium_airport', 'Jiayuguan Airport', '39.856899', '98.3414', '5112', 'AS', 'CN', 'CN-62', 'Jiayuguan', 'yes', 'ZLJQ', 'JGN', '', '', 'https://en.wikipedia.org/wiki/Jiayuguan_Airport', ''),
('0', 'Senai Airport', 'aeroway=aerodrome
iata=JHB
icao=WMKJ
name=Senai Airport
name', '103° 40'' 11.31', '1° 38'' 22.462"', ' ', '0', 'JHB', 'WMKJ', ' ', '103.6698107', '1.6395727', '26873', 'WMKJ', 'medium_airport', 'Senai International Airport', '1.64131', '103.669998', '135', 'AS', 'MY', 'MY-01', 'Johor Bahru', 'yes', 'WMKJ', 'JHB', '', '', 'https://en.wikipedia.org/wiki/Senai_International_Airport', 'Sultan Ismail Airport'),
('0', '西双版纳嘎洒机场', 'aeroway=aerodrome
alt_name=景洪机场
alt_name:en=Jinghong', '100° 45'' 44.84', '21° 58'' 29.689', ' ', '0', 'JHG', 'ZPJH', ' ', '100.7624555', '21.9749135', '27213', 'ZPJH', 'medium_airport', 'Xishuangbanna Gasa Airport', '21.9738998413086', '100.76000213623', '1815', 'AS', 'CN', 'CN-53', 'Jinghong', 'yes', 'ZPJH', 'JHG', '', '', 'https://en.wikipedia.org/wiki/Xishuangbanna_Gasa_Airport', ''),
('0', 'Kapalua-West Maui Airport', 'addr:city=Lahaina
addr:country=US
addr:housename=Kapalua-Wes', '156° 40'' 23.64', '20° 57'' 46.497', 'public', '75', 'JHM', 'PHJH', ' ', '-156.6732336', '20.9629159', '5446', 'PHJH', 'medium_airport', 'Kapalua Airport', '20.9629001617432', '-156.673004150391', '256', 'NA', 'US', 'US-HI', 'Lahaina', 'yes', 'PHJH', 'JHM', 'JHM', '', 'https://en.wikipedia.org/wiki/Kapalua_Airport', ''),
('0', 'Jabalpur', 'aeroway=aerodrome
iata=JLR
icao=VAJB
is_in:country=India
nam', '80° 3'' 7.856"', '23° 10'' 48.647', ' ', '0', 'JLR', 'VAJB', ' ', '80.0521823', '23.1801796', '26447', 'VAJB', 'medium_airport', 'Jabalpur Airport', '23.1777992248535', '80.052001953125', '1624', 'AS', 'IN', 'IN-MP', '', 'yes', 'VAJB', 'JLR', '', '', 'https://en.wikipedia.org/wiki/Jabalpur_Airport', ''),
('0', 'O.R. Tambo International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '28° 14'' 25.238', '26° 8'' 9.719"', 'public', '1694', 'JNB', 'FAOR', ' ', '28.2403438', '-26.136033', '31055', 'FAOR', 'large_airport', 'OR Tambo International Airport', '-26.1392', '28.246', '5558', 'AF', 'ZA', 'ZA-GT', 'Johannesburg', 'yes', 'FAOR', 'JNB', '', '', '', 'Johannesburg International Airport'),
('0', '濟寧曲阜機場', 'aerodrome:type=military/public
aeroway=aerodrome
iata=JNG
ic', '116° 20'' 37.15', '35° 17'' 39.772', 'military/public', '0', 'JNG', 'ZSJG', ' ', '116.3436552', '35.2943812', '300515', 'ZLJN', 'medium_airport', 'Jining Qufu Airport', '35.292778', '116.346667', '134', 'AS', 'CN', 'CN-37', 'Jining', 'yes', 'ZSJG', 'JNG', '', '', 'https://en.wikipedia.org/wiki/Jining_Qufu_Airport', ''),
('0', 'Aeroporto de Joinville Lauro Carneiro de Loyola', 'addr:city=Joinville
addr:housenumber=9000
addr:postcode=8922', '48° 47'' 50.646', '26° 13'' 29.320', ' ', '0', 'JOI', 'SBJV', ' ', '-48.7974018', '-26.2248112', '5925', 'SBJV', 'medium_airport', 'Lauro Carneiro de Loyola Airport', '-26.2245006561279', '-48.7974014282227', '15', 'SA', 'BR', 'BR-SC', 'Joinville', 'yes', 'SBJV', 'JOI', '', '', 'https://en.wikipedia.org/wiki/Joinville-Lauro_Carneiro_de_Loyola_Airport', ''),
('0', 'Yakubu Gowon Airport', 'aerodrome:type=public
aeroway=aerodrome
iata=JOS
icao=DNJO
n', '8° 52'' 4.972"', '9° 38'' 23.658"', 'public', '0', 'JOS', 'DNJO', ' ', '8.8680479', '9.639905', '2113', 'DNJO', 'medium_airport', 'Yakubu Gowon Airport', '9.63982963562012', '8.86905002593994', '4232', 'AF', 'NG', 'NG-PL', 'Jos', 'yes', 'DNJO', 'JOS', '', 'http://www.faannigeria.org/nigeria-airport.php?airport=15', 'https://en.wikipedia.org/wiki/Yakubu_Gowon_Airport', ''),
('0', 'Aeroporto Internacional de João Pessoa - Presidente Castro', 'aeroway=aerodrome
iata=JPA
icao=SBJP
name=Aeroporto Internac', '34° 57'' 3.624"', '7° 8'' 54.332"', ' ', '0', 'JPA', 'SBJP', ' ', '-34.9510066', '-7.1484256', '5923', 'SBJP', 'medium_airport', 'Presidente Castro Pinto International Airport', '-7.14583301544', '-34.9486122131', '217', 'SA', 'BR', 'BR-PB', 'João Pessoa', 'yes', 'SBJP', 'JPA', '', '', 'https://en.wikipedia.org/wiki/Presidente_Castro_Pinto_International_Airport', ''),
('0', 'Aeroporto Estadual de Bauru-Arealva - Moussa Nakhl Tobias', 'aeroway=aerodrome
alt_name=Aeroporto de Bauru-Arealva
iata=J', '49° 4'' 18.923"', '22° 9'' 26.464"', ' ', '0', 'JTC', 'SBAE', ' ', '-49.0719231', '-22.157351', '32305', 'SJTC', 'medium_airport', 'Bauru/Arealva–Moussa Nakhal Tobias State Airport', '-22.160755', '-49.070325', '1962', 'SA', 'BR', 'BR-SP', 'Bauru', 'yes', 'SBAE', 'JTC', 'SP0010', 'http://www.daesp.sp.gov.br/aeroporto-estadual-de-bauru-arealva-moussa-nakhal-tobias/', 'https://en.wikipedia.org/wiki/Bauru-Arealva_Airport', 'SJCT'),
('0', 'Aeropuerto Internacional Gobernador Horacio Guzmán', 'aeroway=aerodrome
barrier=fence
iata=JUJ
icao=SASJ
name=Aero', '65° 5'' 40.367"', '24° 23'' 36.000', ' ', '0', 'JUJ', 'SASJ', ' ', '-65.0945463', '-24.3933334', '5812', 'SASJ', 'medium_airport', 'Gobernador Horacio Guzman International Airport', '-24.392799', '-65.097801', '3019', 'SA', 'AR', 'AR-Y', 'San Salvador de Jujuy', 'yes', 'SASJ', 'JUJ', 'JUJ', '', 'https://en.wikipedia.org/wiki/Gobernador_Horacio_Guzm%C3%A1n_International_Airport', ''),
('0', 'Inca Manco Capac International Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=3826
iata=JUL
ic', '70° 9'' 25.442"', '15° 28'' 2.932"', 'public', '3826', 'JUL', 'SPJL', ' ', '-70.1570671', '-15.4674811', '6222', 'SPJL', 'medium_airport', 'Inca Manco Capac International Airport', '-15.4671001434326', '-70.158203125', '12552', 'SA', 'PE', 'PE-PUN', 'Juliaca', 'yes', 'SPJL', 'JUL', '', '', 'https://en.wikipedia.org/wiki/Inca_Manco_Capac_International_Airport', ''),
('0', '衢州机场', 'aeroway=aerodrome
iata=JUZ
icao=ZSJU
name=衢州机场
name:', '118° 53'' 46.42', '28° 57'' 57.723', ' ', '0', 'JUZ', 'ZSJU', ' ', '118.8962288', '28.9660341', '31714', 'ZSJU', 'medium_airport', 'Quzhou Airport', '28.965799', '118.899002', NULL, 'AS', 'CN', 'CN-33', 'Quzhou', 'yes', 'ZSJU', 'JUZ', '', '', 'https://en.wikipedia.org/wiki/Quzhou_Airport', ''),
('0', 'Jwaneng Airport', 'aeroway=aerodrome
iata=JWA
icao=FBJW
name=Jwaneng Airport
na', '24° 41'' 29.608', '24° 36'' 4.281"', ' ', '0', 'JWA', 'FBJW', ' ', '24.6915577', '-24.6011891', '2861', 'FBJW', 'medium_airport', 'Jwaneng Airport', '-24.602301', '24.691', '3900', 'AF', 'BW', 'BW-SO', 'Jwaneng', 'yes', 'FBJW', 'JWA', '', '', 'https://en.wikipedia.org/wiki/Jwaneng_Airport', ''),
('0', 'Mallam Aminu Kano International Airport', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '8° 31'' 14.172"', '12° 2'' 50.024"', 'military/public', '476', 'KAN', 'DNKN', ' ', '8.5206033', '12.047229', '2115', 'DNKN', 'large_airport', 'Mallam Aminu International Airport', '12.0476', '8.52462', '1562', 'AF', 'NG', 'NG-KN', 'Kano', 'yes', 'DNKN', 'KAN', '', 'http://www.faannigeria.org/nigeria-airport.php?airport=2', 'https://en.wikipedia.org/wiki/Mallam_Aminu_Kano_International_Airport', ''),
('0', 'Hamid Karzai International Airport', 'aerodrome=international
aerodrome:type=military/public
aerow', '69° 12'' 48.151', '34° 33'' 51.227', 'military/public', '1789', 'KBL', 'OAKB', ' ', '69.2133754', '34.5642298', '5072', 'OAKB', 'medium_airport', 'Hamid Karzai International Airport', '34.565899', '69.212303', '5877', 'AS', 'AF', 'AF-KAB', 'Kabul', 'yes', 'OAKB', 'KBL', '', 'http://hamidkarzaiairport.com/', 'https://en.wikipedia.org/wiki/Hamid_Karzai_International_Airport', 'Kabul International Airport, Khwaja Rawash Airport'),
('0', 'ท่าอากาศยานนานาชาติก', 'aeroway=aerodrome
iata=KBV
icao=VTSG
name=ท่าอาก', '98° 59'' 8.485"', '8° 5'' 45.333"', ' ', '0', 'KBV', 'VTSG', ' ', '98.9856904', '8.0959259', '26669', 'VTSG', 'medium_airport', 'Krabi Airport', '8.09912014008', '98.9861984253', '82', 'AS', 'TH', 'TH-81', 'Krabi', 'yes', 'VTSG', 'KBV', '', '', 'https://en.wikipedia.org/wiki/Krabi_Airport', ''),
('0', 'Kuching International Airport', 'addr:city=Kuching
addr:postcode=93722
aeroway=aerodrome
ele=', '110° 21'' 19.21', '1° 29'' 17.727"', ' ', '27', 'KCH', 'WBGG', ' ', '110.3553379', '1.4882576', '26808', 'WBGG', 'medium_airport', 'Kuching International Airport', '1.48469996452332', '110.34700012207', '89', 'AS', 'MY', 'MY-13', 'Kuching', 'yes', 'WBGG', 'KCH', '', '', 'https://en.wikipedia.org/wiki/Kuching_International_Airport', ''),
('0', '高知龍馬空港', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Nankoku', '133° 40'' 7.874', '33° 32'' 47.725', 'public', '9', 'KCZ', 'RJOK', ' ', '133.6688539', '33.5465903', '5596', 'RJOK', 'medium_airport', 'Kochi Ryoma Airport', '33.546101', '133.669006', '42', 'AS', 'JP', 'JP-39', 'Nankoku', 'yes', 'RJOK', 'KCZ', '', '', 'https://en.wikipedia.org/wiki/K%C5%8Dchi_Airport', 'Kochi, Nankoku, RJOK, KCZ'),
('0', 'Kalgoorlie-Boulder Airport', 'aerodrome=continental
aerodrome:type=public
aeroway=aerodrom', '121° 27'' 39.26', '30° 47'' 15.322', 'public', '0', 'KGI', 'YPKG', ' ', '121.460907', '-30.7875894', '27108', 'YPKG', 'medium_airport', 'Kalgoorlie Boulder Airport', '-30.7894001007', '121.461997986', '1203', 'OC', 'AU', 'AU-WA', 'Kalgoorlie', 'yes', 'YPKG', 'KGI', '', '', 'https://en.wikipedia.org/wiki/Kalgoorlie-Boulder_Airport', ''),
('0', '喀什机场', 'aeroway=aerodrome
iata=KHG
icao=ZWSH
name=喀什机场
name:', '76° 1'' 17.111"', '39° 32'' 26.180', ' ', '0', 'KHG', 'ZWSH', 'Аэропорт Кашгар', '76.0214196', '39.5406055', '27234', 'ZWSH', 'medium_airport', 'Kashgar Airport', '39.5429000854', '76.0199966431', '4529', 'AS', 'CN', 'CN-65', 'Kashgar', 'yes', 'ZWSH', 'KHG', '', '', 'https://en.wikipedia.org/wiki/Kashgar_Airport', 'Kashi Airport'),
('0', '高雄國際機場', 'aeroway=aerodrome
iata=KHH
icao=RCKH
name=高雄國際機場', '120° 21'' 1.484', '22° 34'' 27.537', ' ', '0', 'KHH', 'RCKH', ' ', '120.3504122', '22.5743159', '5516', 'RCKH', 'large_airport', 'Kaohsiung International Airport', '22.577101', '120.349998', '31', 'AS', 'TW', 'TW-KHH', 'Kaohsiung (Xiaogang)', 'yes', 'RCKH', 'KHH', '', 'http://www.kia.gov.tw/english/e_index.asp', 'https://en.wikipedia.org/wiki/Kaohsiung_International_Airport', 'Siaogang International Airport, 高雄國際航空站, 小港國際機場'),
('0', '南昌昌北国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=KHN
icao', '115° 54'' 25.65', '28° 51'' 42.603', 'international', '0', 'KHN', 'ZSCN', ' ', '115.9071272', '28.8618341', '27216', 'ZSCN', 'medium_airport', 'Nanchang Changbei International Airport', '28.8649997711182', '115.900001525879', '143', 'AS', 'CN', 'CN-36', 'Nanchang', 'yes', 'ZSCN', 'KHN', '', '', 'https://en.wikipedia.org/wiki/Nanchang_Changbei_International_Airport', ''),
('0', 'Kimberley Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=1203.96
iata=KIM', '24° 45'' 50.700', '28° 48'' 25.425', 'public', '1204', 'KIM', 'FAKM', ' ', '24.7640833', '-28.8070626', '2794', 'FAKM', 'medium_airport', 'Kimberley Airport', '-28.8027992249', '24.7651996613', '3950', 'AF', 'ZA', 'ZA-NC', 'Kimberley', 'yes', 'FAKM', 'KIM', '', '', 'https://en.wikipedia.org/wiki/Kimberley_Airport', ''),
('0', 'Norman Manley International Airport', 'aerodrome=international
aeroway=aerodrome
ele=3
iata=KIN
ica', '76° 47'' 11.747', '17° 56'' 8.790"', ' ', '3', 'KIN', 'MKJP', ' ', '-76.7865965', '17.9357751', '4680', 'MKJP', 'large_airport', 'Norman Manley International Airport', '17.9356994628906', '-76.7874984741211', '10', 'NA', 'JM', 'JM-01', 'Kingston', 'yes', 'MKJP', 'KIN', '', '', 'https://en.wikipedia.org/wiki/Norman_Manley_International_Airport', ''),
('0', 'ท่าอากาศยานขอนแก่น', 'aerodrome=domestic
aerodrome:type=military/public
aeroway=ae', '102° 47'' 0.092', '16° 27'' 58.398', 'military/public', '204', 'KKC', 'VTUK', ' ', '102.7833588', '16.4662216', '26681', 'VTUK', 'medium_airport', 'Khon Kaen Airport', '16.4666004181', '102.783996582', '670', 'AS', 'TH', 'TH-40', 'Khon Kaen', 'yes', 'VTUK', 'KKC', '', '', 'https://en.wikipedia.org/wiki/Khon_Kaen_Airport', ''),
('0', 'Bay of Islands Airport', 'LINZ:source_version=2012-06-06
aeroway=aerodrome
alt_name=Ke', '173° 54'' 50.84', '35° 15'' 29.645', ' ', '0', 'KKE', 'NZKK', ' ', '173.9141226', '-35.2582346', '5036', 'NZKK', 'medium_airport', 'Kerikeri Airport', '-35.2627983093262', '173.912002563477', '492', 'OC', 'NZ', 'NZ-NTL', 'Kerikeri', 'yes', 'NZKK', 'KKE', '', 'http://www.kerikeri-airport.co.nz/', 'https://en.wikipedia.org/wiki/Kerikeri_Airport', ''),
('0', 'Aéroport de Kikwit', 'aeroway=aerodrome
iata=KKW
icao=FZCA
name=Aéroport de Kikwi', '18° 47'' 6.803"', '5° 2'' 8.814" S', ' ', '0', 'KKW', 'FZCA', ' ', '18.7852231', '-5.0357817', '3045', 'FZCA', 'medium_airport', 'Kikwit Airport', '-5.03577', '18.785601', '1572', 'AF', 'CD', 'CD-BN', 'Kikwit', 'yes', 'FZCA', 'KKW', '', '', 'https://en.wikipedia.org/wiki/Kikwit_Airport', ''),
('0', '喜界空港', 'KSJ2:AAC=46529
KSJ2:AAC_label=鹿児島県大島郡喜界町', '129° 55'' 40.39', '28° 19'' 17.565', ' ', '0', 'KKX', 'RJKI', ' ', '129.9278878', '28.3215459', '5579', 'RJKI', 'medium_airport', 'Kikai Airport', '28.3213005066', '129.927993774', '21', 'AS', 'JP', 'JP-46', 'Kikai', 'yes', 'RJKI', 'KKX', '', '', 'https://en.wikipedia.org/wiki/Kikai_Airport', ''),
('0', 'Kerema Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=3
iata=KMA
icao=', '145° 46'' 17.11', '7° 57'' 46.040"', 'public', '3', 'KMA', 'AYKM', ' ', '145.7714197', '-7.962789', '60', 'AYKM', 'medium_airport', 'Kerema Airport', '-7.96361017227', '145.770996094', '10', 'OC', 'PG', 'PG-GPK', 'Kerema', 'yes', 'AYKM', 'KMA', '', '', 'https://en.wikipedia.org/wiki/Kerema_Airport', ''),
('0', '宮崎ブーゲンビリア空港', 'KSJ2:AAC=45201
KSJ2:AAC_label=宮崎県宮崎市
KSJ2:AD2=1
', '131° 26'' 52.37', '31° 52'' 33.686', ' ', '0', 'KMI', 'RJFM', ' ', '131.447882', '31.8760239', '5567', 'RJFM', 'medium_airport', 'Miyazaki Airport', '31.877199173', '131.449005127', '20', 'AS', 'JP', 'JP-45', 'Miyazaki', 'yes', 'RJFM', 'KMI', '', '', 'https://en.wikipedia.org/wiki/Miyazaki_Airport', ''),
('0', '熊本空港', 'KSJ2:AAC=43443
KSJ2:AAC_label=熊本県上益城郡益城町', '130° 51'' 6.030', '32° 50'' 10.450', ' ', '0', 'KMJ', 'RJFT', ' ', '130.8516751', '32.8362361', '5572', 'RJFT', 'medium_airport', 'Kumamoto Airport', '32.8372993469238', '130.854995727539', '642', 'AS', 'JP', 'JP-43', 'Kumamoto', 'yes', 'RJFT', 'KMJ', '', '', 'https://en.wikipedia.org/wiki/Kumamoto_Airport', ''),
('0', 'Kumasi Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '1° 35'' 25.692"', '6° 42'' 55.449"', 'public', '287', 'KMS', 'DGSI', ' ', '-1.5904701', '6.7154026', '2093', 'DGSI', 'medium_airport', 'Kumasi Airport', '6.71456003189087', '-1.59081995487213', '942', 'AF', 'GH', 'GH-AH', 'Kumasi', 'yes', 'DGSI', 'KMS', '', '', 'https://en.wikipedia.org/wiki/Kumasi_Airport', ''),
('0', 'Aérodrome de Koné', 'aeroway=aerodrome
iata=KNQ
icao=NWWD
name=Aérodrome de Kon', '164° 50'' 23.20', '21° 3'' 15.685"', ' ', '0', 'KNQ', 'NWWD', ' ', '164.8397788', '-21.0543569', '5013', 'NWWD', 'medium_airport', 'Koné Airport', '-21.0543003082275', '164.837005615234', '23', 'OC', 'NC', 'NC-U-A', 'Koné', 'yes', 'NWWD', 'KNQ', '', '', 'https://en.wikipedia.org/wiki/Kon%C3%A9_Airport', ''),
('0', 'King Island Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Currie,', '143° 52'' 42.24', '39° 52'' 39.949', 'public', '40', 'KNS', 'YKII', ' ', '143.8784004', '-39.8777635', '27023', 'YKII', 'medium_airport', 'King Island Airport', '-39.877498626709', '143.878005981445', '132', 'OC', 'AU', 'AU-TAS', '', 'yes', 'YKII', 'KNS', '', '', 'https://en.wikipedia.org/wiki/King_Island_Airport', ''),
('0', 'East Kimberley Regional Airport', 'aerodrome=regional
aerodrome:type=public
aeroway=aerodrome
i', '128° 42'' 25.31', '15° 46'' 37.949', 'public', '0', 'KNX', 'YPKU', ' ', '128.7070314', '-15.7772081', '27111', 'YPKU', 'medium_airport', 'Kununurra Airport', '-15.7781000137', '128.707992554', '145', 'OC', 'AU', 'AU-WA', 'Kununurra', 'yes', 'YPKU', 'KNX', '', '', 'https://en.wikipedia.org/wiki/Kununurra_Airport', ''),
('0', 'Kona International Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
ele=14', '156° 2'' 21.955', '19° 44'' 30.357', 'public', '14', 'KOA', 'PHKO', ' ', '-156.0394319', '19.7417658', '5448', 'PHKO', 'medium_airport', 'Ellison Onizuka Kona International Airport at Keahole', '19.738783', '-156.045603', '47', 'NA', 'US', 'US-HI', 'Kailua-Kona', 'yes', 'PHKO', 'KOA', 'KOA', '', 'https://en.wikipedia.org/wiki/Kona_International_Airport', ''),
('0', 'Karratha Airport', 'aerodrome=continental
aerodrome:type=public
aeroway=aerodrom', '116° 46'' 19.01', '20° 42'' 41.868', 'public', '0', 'KTA', 'YPKA', ' ', '116.7719487', '-20.71163', '27107', 'YPKA', 'medium_airport', 'Karratha Airport', '-20.7122001648', '116.773002625', '29', 'OC', 'AU', 'AU-WA', 'Karratha', 'yes', 'YPKA', 'KTA', '', '', 'https://en.wikipedia.org/wiki/Karratha_Airport', ''),
('0', '北大東空港', 'KSJ2:AAC=47358
KSJ2:AAC_label=沖縄県島尻郡北大東村', '131° 19'' 34.92', '25° 56'' 39.489', ' ', '0', 'KTD', 'RORK', ' ', '131.326369', '25.9443025', '5679', 'RORK', 'medium_airport', 'Kitadaito Airport', '25.9447002411', '131.32699585', '80', 'AS', 'JP', 'JP-47', 'Kitadaitōjima', 'yes', 'RORK', 'KTD', '', '', 'https://en.wikipedia.org/wiki/Kitadaito_Airport', ''),
('0', 'त्रिभुवन अन्तर्राष्ट', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '85° 21'' 33.287', '27° 41'' 37.475', 'public', '1338', 'KTM', 'VNKT', ' ', '85.3592464', '27.6937431', '26596', 'VNKT', 'large_airport', 'Tribhuvan International Airport', '27.6966', '85.3591', '4390', 'AS', 'NP', 'NP-BA', 'Kathmandu', 'yes', 'VNKT', 'KTM', '', 'http://www.tiairport.com.np/index.php', 'https://en.wikipedia.org/wiki/Tribhuvan_International_Airport', 'gaucharan'),
('0', 'Tinson Pen Aerodrome', 'aeroway=aerodrome
closest_town=Kingston, Jamaica
ele=5
iata=', '76° 49'' 29.478', '17° 59'' 13.921', ' ', '5', 'KTP', 'MKTP', ' ', '-76.824855', '17.9872003', '4683', 'MKTP', 'medium_airport', 'Tinson Pen Airport', '17.9885997772217', '-76.8237991333008', '16', 'NA', 'JM', 'JM-02', 'Tinson Pen', 'yes', 'MKTP', 'KTP', '', '', 'https://en.wikipedia.org/wiki/Tinson_Pen_Airport', ''),
('0', '釧路空港', 'KSJ2:AAC=01206
KSJ2:AAC_label=北海道釧路市
KSJ2:AD2=1
', '144° 11'' 33.16', '43° 2'' 34.606"', 'public', '94', 'KUH', 'RJCK', 'Аэропорт Куширо', '144.192547', '43.042946', '5547', 'RJCK', 'medium_airport', 'Kushiro Airport', '43.0410003662', '144.192993164', '327', 'AS', 'JP', 'JP-01', 'Kushiro', 'yes', 'RJCK', 'KUH', '', '', 'https://en.wikipedia.org/wiki/Kushiro_Airport', ''),
('0', '군산공항', 'aeroway=aerodrome
iata=KUV
icao=RKJK
landuse=military
milita', '126° 37'' 37.84', '35° 54'' 25.579', ' ', '0', 'KUV', 'RKJK', ' ', '126.6271788', '35.9071052', '5631', 'RKJK', 'large_airport', 'Kunsan Air Base', '35.9038009643555', '126.615997314453', '29', 'AS', 'KR', 'KR-45', 'Kunsan', 'yes', 'RKJK', 'KUV', '', '', 'https://en.wikipedia.org/wiki/Gunsan_Airport', ''),
('0', 'Kavieng Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Kavieng', '150° 48'' 28.17', '2° 34'' 45.821"', 'public', '2', 'KVG', 'AYKV', ' ', '150.8078256', '-2.5793946', '61', 'AYKV', 'medium_airport', 'Kavieng Airport', '-2.57940006256', '150.807998657', '7', 'OC', 'PG', 'PG-NIK', 'Kavieng', 'yes', 'AYKV', 'KVG', '', '', 'https://en.wikipedia.org/wiki/Kavieng_Airport', ''),
('0', '광주공항', 'addr:city=광주광역시 광산구
addr:housenumber=420-25
', '126° 48'' 16.97', '35° 7'' 27.597"', ' ', '0', 'KWJ', 'RKJJ', ' ', '126.8047138', '35.1243326', '5630', 'RKJJ', 'medium_airport', 'Gwangju Airport', '35.123173', '126.805444', '39', 'AS', 'KR', 'KR-29', 'Gwangju', 'yes', 'RKJJ', 'KWJ', '', 'http://gwangju.airport.co.kr/doc/gwangju/index.jsp', 'https://en.wikipedia.org/wiki/Gwangju_Airport', ''),
('0', 'Kowanyama Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Kowanya', '141° 45'' 7.565', '15° 29'' 6.626"', 'public', '35', 'KWM', 'YKOW', ' ', '141.7521015', '-15.485174', '27026', 'YKOW', 'medium_airport', 'Kowanyama Airport', '-15.4856004714966', '141.751007080078', '35', 'OC', 'AU', 'AU-QLD', 'Kowanyama', 'yes', 'YKOW', 'KWM', '', '', 'https://en.wikipedia.org/wiki/Kowanyama_Airport', ''),
('0', 'Kolwezi', 'aeroway=aerodrome
iata=KWZ
icao=FZQM
name=Kolwezi
wikidata=Q', '25° 30'' 33.420', '10° 45'' 59.460', ' ', '0', 'KWZ', 'FZQM', ' ', '25.5092833', '-10.7665168', '3060', 'FZQM', 'medium_airport', 'Kolwezi Airport', '-10.7658996582031', '25.5056991577148', '5007', 'AF', 'CD', 'CD-KA', '', 'yes', 'FZQM', 'KWZ', '', '', 'https://en.wikipedia.org/wiki/Kolwezi_Airport', ''),
('0', '蘭嶼機場(Lanyu Airport)', 'aeroway=aerodrome
barrier=wall
iata=KYD
icao=RCLY
int_name=L', '121° 32'' 2.222', '22° 1'' 43.011"', ' ', '0', 'KYD', 'RCLY', ' ', '121.5339506', '22.0286143', '5519', 'RCLY', 'medium_airport', 'Lanyu Airport', '22.0270004272461', '121.535003662109', '44', 'AS', 'TW', 'TW-TTT', 'Orchid Island', 'yes', 'RCLY', 'KYD', '', '', 'https://en.wikipedia.org/wiki/Lanyu_Airport', 'Lanyu Auxiliary Station'),
('0', 'Aéroport de Kayes', 'aeroway=aerodrome
barrier=fence
ele=50
iata=KYS
icao=GAKY
in', '11° 23'' 55.796', '14° 28'' 54.642', ' ', '50', 'KYS', 'GAKY', ' ', '-11.3988322', '14.4818449', '3068', 'GAKY', 'medium_airport', 'Kayes Dag Dag Airport', '14.4812002182007', '-11.4043998718262', '164', 'AF', 'ML', 'ML-1', '', 'yes', 'GAKY', 'KYS', '', '', 'https://en.wikipedia.org/wiki/Kayes_Airport', ''),
('0', 'Aeroporto Internacional 4 de Fevereiro', 'aerodrome=international
aerodrome:type=military/public
aerow', '13° 13'' 51.843', '8° 51'' 42.489"', 'military/public', '74', 'LAD', 'FNLU', ' ', '13.2310676', '-8.8618025', '2942', 'FNLU', 'large_airport', 'Quatro de Fevereiro Airport', '-8.85837', '13.2312', '243', 'AF', 'AO', 'AO-LUA', 'Luanda', 'yes', 'FNLU', 'LAD', '', '', 'https://en.wikipedia.org/wiki/Quatro_de_Fevereiro_Airport', '4 de Fevereiro'),
('0', 'Lae Nadzab Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Lae
ele', '146° 43'' 34.41', '6° 34'' 11.353"', 'public', '73', 'LAE', 'AYNZ', ' ', '146.7262268', '-6.5698202', '66', 'AYNZ', 'medium_airport', 'Nadzab Airport', '-6.569803', '146.725977', '239', 'OC', 'PG', 'PG-MPL', 'Lae', 'yes', 'AYNZ', 'LAE', '', '', 'https://en.wikipedia.org/wiki/Lae_Nadzab_Airport', 'MFO'),
('0', 'Manuel Márquez de León International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=La Paz,', '110° 21'' 50.95', '24° 4'' 31.157"', 'public', '21', 'LAP', 'MMLP', ' ', '-110.3641528', '24.0753214', '4723', 'MMLP', 'medium_airport', 'Manuel Márquez de León International Airport', '24.0727005005', '-110.361999512', '69', 'NA', 'MX', 'MX-BCS', 'La Paz', 'yes', 'MMLP', 'LAP', '', 'http://aeropuertosgap.com.mx/aeropuertos/lapaz?lang=eng', 'https://en.wikipedia.org/wiki/Manuel_M%C3%A1rquez_de_Le%C3%B3n_International_Airport', ''),
('0', 'Khudzhand Airport', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '69° 41'' 49.080', '40° 13'' 11.199', 'military/public', '0', 'LBD', 'UTDL', 'Аэропорт Худжанд', '69.6969667', '40.2197774', '26386', 'UTDL', 'medium_airport', 'Khujand Airport', '40.215401', '69.694702', '1450', 'AS', 'TJ', 'TJ-SU', 'Khujand', 'yes', 'UTDL', 'LBD', '', '', 'https://en.wikipedia.org/wiki/Khudzhand_Airport', 'Khodjend, Khodzhent, Leninabad, Khudzhand'),
('0', 'Labasa Airport', 'aeroway=aerodrome
iata=LBS
icao=NFNL
name=Labasa Airport', '179° 20'' 23.01', '16° 28'' 4.120"', ' ', '0', 'LBS', 'NFNL', ' ', '179.3397259', '-16.4678112', '4961', 'NFNL', 'medium_airport', 'Labasa Airport', '-16.4666996002197', '179.339996337891', '44', 'OC', 'FJ', 'FJ-N', '', 'yes', 'NFNL', 'LBS', '', '', 'https://en.wikipedia.org/wiki/Lambasa_Airport', ''),
('0', 'Libreville Leon M''ba International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '9° 24'' 40.067"', '0° 27'' 43.352"', 'public', '12', 'LBV', 'FOOL', ' ', '9.4111296', '0.4620423', '2966', 'FOOL', 'medium_airport', 'Libreville Leon M''ba International Airport', '0.458600014448', '9.4122800827', '39', 'AF', 'GA', 'GA-1', 'Libreville', 'yes', 'FOOL', 'LBV', '', 'http://www.adlgabon.com', 'https://en.wikipedia.org/wiki/Libreville_International_Airport', ''),
('0', 'Golosón International Airport', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '86° 51'' 8.151"', '15° 44'' 32.792', 'military/public', '15', 'LCE', 'MHLC', ' ', '-86.8522643', '15.7424421', '4660', 'MHLC', 'medium_airport', 'Goloson International Airport', '15.7425', '-86.852997', '39', 'NA', 'HN', 'HN-AT', 'La Ceiba', 'yes', 'MHLC', 'LCE', '', '', 'https://en.wikipedia.org/wiki/Golos%C3%B3n_International_Airport', 'Hector C Moncada Air Base'),
('0', 'Aeroporto Governador José Richa', 'aeroway=aerodrome
alt_name=Aeroporto de Londrina
iata=LDB
ic', '51° 7'' 50.090"', '23° 19'' 54.279', ' ', '0', 'LDB', 'SBLO', ' ', '-51.1305805', '-23.3317442', '5931', 'SBLO', 'medium_airport', 'Governador José Richa Airport', '-23.3335990906', '-51.1301002502', '1867', 'SA', 'BR', 'BR-PR', 'Londrina', 'yes', 'SBLO', 'LDB', '', '', 'https://en.wikipedia.org/wiki/Londrina_Airport', 'Londrina Airport'),
('0', 'RAAF Base Learmonth', 'aeroway=aerodrome
closest_town=Exmouth, Western Australia
el', '114° 4'' 58.985', '22° 13'' 57.190', ' ', '6', 'LEA', 'YPLM', ' ', '114.0830513', '-22.2325527', '27113', 'YPLM', 'medium_airport', 'Learmonth Airport', '-22.2355995178', '114.088996887', '19', 'OC', 'AU', 'AU-WA', 'Exmouth', 'yes', 'YPLM', 'LEA', '', '', 'https://en.wikipedia.org/wiki/RAAF_Learmonth', 'RAAF Learmonth'),
('0', 'Aéroport International Gnassingbé Eyadéma de Lomé Tokoin', 'aerodrome=international
aerodrome:type=military/public
aerow', '1° 15'' 24.664"', '6° 10'' 12.485"', 'military/public', '22', 'LFW', 'DXXX', ' ', '1.2568512', '6.1701347', '2144', 'DXXX', 'medium_airport', 'Lomé–Tokoin International Airport', '6.16561', '1.25451', '72', 'AF', 'TG', 'TG-M', 'Lomé', 'yes', 'DXXX', 'LFW', '', '', 'https://en.wikipedia.org/wiki/Lom%C3%A9%E2%80%93Tokoin_International_Airport', 'Gnassingbe Eyadema International Airport'),
('0', 'Lightning Ridge Aerodrome', 'aeroway=aerodrome
iata=LHG
icao=YLRD
name=Lightning Ridge Ae', '147° 58'' 39.78', '29° 27'' 11.235', ' ', '0', 'LHG', 'YLRD', ' ', '147.977717', '-29.4531209', '27037', 'YLRD', 'medium_airport', 'Lightning Ridge Airport', '-29.4566993713379', '147.983993530273', '540', 'OC', 'AU', 'AU-NSW', '', 'yes', 'YLRD', 'LHG', '', '', 'https://en.wikipedia.org/wiki/Lightning_Ridge_Airport', ''),
('0', 'Aérodrome de Lifou Wanaham', 'aeroway=aerodrome
iata=LIF
icao=NWWL
name=Aérodrome de Lifo', '167° 14'' 21.60', '20° 46'' 35.562', ' ', '0', 'LIF', 'NWWL', ' ', '167.2393354', '-20.7765449', '5016', 'NWWL', 'medium_airport', 'Lifou Airport', '-20.7747993469238', '167.240005493164', '92', 'OC', 'NC', 'NC-U-A', 'Lifou', 'yes', 'NWWL', 'LIF', '', '', 'https://en.wikipedia.org/wiki/Lifou_Airport', ''),
('0', 'Lihue Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
ele=42', '159° 20'' 36.73', '21° 58'' 19.287', 'public', '42', 'LIH', 'PHLI', ' ', '-159.3435362', '21.9720241', '5449', 'PHLI', 'medium_airport', 'Lihue Airport', '21.9759998321533', '-159.339004516602', '153', 'NA', 'US', 'US-HI', 'Lihue', 'yes', 'PHLI', 'LIH', 'LIH', '', 'https://en.wikipedia.org/wiki/Lihu''e_Airport', ''),
('0', 'Aeropuerto Internacional Daniel Oduber Quirós', 'addr:city=Liberia
aeroway=aerodrome
iata=LIR
icao=MRLB
int_n', '85° 32'' 41.847', '10° 35'' 42.419', ' ', '0', 'LIR', 'MRLB', ' ', '-85.5449574', '10.5951163', '4806', 'MRLB', 'large_airport', 'Daniel Oduber Quiros International Airport', '10.5933', '-85.544403', '270', 'NA', 'CR', 'CR-G', 'Liberia', 'yes', 'MRLB', 'LIR', '', '', 'https://en.wikipedia.org/wiki/Daniel_Oduber_Quir%C3%B3s_International_Airport', ''),
('0', '丽江机场', 'aeroway=aerodrome
alt_name=丽江三义机场
description=Li', '100° 14'' 40.55', '26° 40'' 36.765', ' ', '0', 'LJG', 'ZPLJ', ' ', '100.2445973', '26.6768791', '31828', 'ZPLJ', 'medium_airport', 'Lijiang Airport', '26.6800003052', '100.246002197', NULL, 'AS', 'CN', 'CN-53', 'Lijiang', 'yes', 'ZPLJ', 'LJG', '', '', 'https://en.wikipedia.org/wiki/Lijiang_Sanyi_Airport', 'Yunlong Air Base'),
('0', 'Limbang Airport', 'aeroway=aerodrome
iata=LMN
icao=WBGJ
name=Limbang Airport
na', '115° 0'' 35.349', '4° 48'' 24.952"', ' ', '0', 'LMN', 'WBGJ', ' ', '115.0098193', '4.8069311', '26809', 'WBGJ', 'medium_airport', 'Limbang Airport', '4.80830001831055', '115.010002136231', '14', 'AS', 'MY', 'MY-13', 'Limbang', 'yes', 'WBGJ', 'LMN', '', '', 'https://en.wikipedia.org/wiki/Limbang_Airport', ''),
('0', 'Crater Lake Klamath Regional Airport', 'aeroway=aerodrome
iata=LMT
icao=KLMT
name=Crater Lake Klamat', '121° 44'' 4.446', '42° 9'' 23.384"', ' ', '0', 'LMT', 'KLMT', ' ', '-121.7345683', '42.1564955', '3646', 'KLMT', 'medium_airport', 'Crater Lake-Klamath Regional Airport', '42.156101', '-121.733002', '4095', 'NA', 'US', 'US-OR', 'Klamath Falls', 'yes', 'KLMT', 'LMT', 'LMT', '', 'https://en.wikipedia.org/wiki/Klamath_Falls_Airport', 'Klamath Falls Airport'),
('0', '临沧机场', 'aeroway=aerodrome
iata=LNJ
icao=ZPLC
name=临沧机场
name:', '100° 1'' 26.643', '23° 44'' 17.937', ' ', '0', 'LNJ', 'ZPLC', ' ', '100.0240675', '23.7383158', '44116', 'CN-0040', 'medium_airport', 'Lintsang Airfield', '23.7381000519', '100.025001526', '6230', 'AS', 'CN', 'CN-53', 'Lincang', 'yes', 'ZPLC', 'LNJ', '', '', 'https://en.wikipedia.org/wiki/Lincang_Airport', 'Lincang Airport'),
('0', 'Lāna‘i Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
city_s', '156° 57'' 1.536', '20° 47'' 11.732', 'public', '397', 'LNY', 'PHNY', ' ', '-156.9504268', '20.7865921', '5454', 'PHNY', 'medium_airport', 'Lanai Airport', '20.7856006622314', '-156.95100402832', '1308', 'NA', 'US', 'US-HI', 'Lanai City', 'yes', 'PHNY', 'LNY', 'LNY', '', 'https://en.wikipedia.org/wiki/Lanai_Airport', ''),
('0', 'Bandar Udara Internasional Lombok', 'aeroway=aerodrome
alt_name=BIL
ele=97
iata=LOP
icao=WADL
int', '116° 16'' 32.18', '8° 45'' 27.020"', ' ', '97', 'LOP', 'WADL', ' ', '116.2756074', '-8.7575056', '299629', 'WADL', 'medium_airport', 'Lombok International Airport', '-8.757322', '116.276675', '319', 'AS', 'ID', 'ID-NB', 'Mataram', 'yes', 'WADL', 'LOP', '', 'http://www.lombok-airport.co.id/', 'https://en.wikipedia.org/wiki/Lombok_International_Airport', 'Bandara Udara Internasional Lombok'),
('0', 'Venustiano Carranza International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Monclov', '101° 28'' 17.30', '26° 57'' 12.851', 'public', '568', 'LOV', 'MMMV', ' ', '-101.471473', '26.9535697', '4730', 'MMMV', 'medium_airport', 'Monclova International Airport', '26.9557', '-101.470001', '1864', 'NA', 'MX', 'MX-COA', '', 'yes', 'MMMV', 'LOV', 'MOV', '', 'https://en.wikipedia.org/wiki/Venustiano_Carranza_International_Airport', ''),
('0', 'Aeropuerto Internacional El Alto', 'aeroway=aerodrome
barrier=fence
ele=4061.5
fence_type=chain_', '68° 11'' 24.456', '16° 30'' 35.072', ' ', '4062', 'LPB', 'SLLP', ' ', '-68.1901267', '-16.5097422', '6184', 'SLLP', 'medium_airport', 'El Alto International Airport', '-16.5132999420166', '-68.1922988891602', '13355', 'SA', 'BO', 'BO-L', 'La Paz / El Alto', 'yes', 'SLLP', 'LPB', '', '', 'https://en.wikipedia.org/wiki/El_Alto_International_Airport', ''),
('0', 'Aeropuerto Internacional La Romana', 'aeroway=aerodrome
iata=LRM
icao=MDLR
name=Aeropuerto Interna', '68° 54'' 43.945', '18° 27'' 6.374"', ' ', '0', 'LRM', 'MDLR', ' ', '-68.912207', '18.4517705', '4635', 'MDLR', 'medium_airport', 'Casa De Campo International Airport', '18.4507007598877', '-68.9117965698242', '240', 'NA', 'DO', 'DO-12', 'La Romana', 'yes', 'MDLR', 'LRM', '', '', 'https://en.wikipedia.org/wiki/La_Romana_International_Airport', ''),
('0', 'Aeropuerto La Florida', 'aeroway=aerodrome
iata=LSC
icao=SCSE
name=Aeropuerto La Flor', '71° 11'' 59.542', '29° 54'' 56.205', ' ', '0', 'LSC', 'SCSE', ' ', '-71.1998727', '-29.9156125', '6040', 'SCSE', 'medium_airport', 'La Florida Airport', '-29.916201', '-71.199501', '481', 'SA', 'CL', 'CL-CO', 'La Serena-Coquimbo', 'yes', 'SCSE', 'LSC', '', '', 'https://en.wikipedia.org/wiki/La_Florida_Airport_(Chile)', 'COW'),
('0', 'Launceston Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Launces', '147° 12'' 42.89', '41° 32'' 37.542', 'public', '171', 'LST', 'YMLT', ' ', '147.2119161', '-41.5437618', '27063', 'YMLT', 'medium_airport', 'Launceston Airport', '-41.54529953', '147.214004517', '562', 'OC', 'AU', 'AU-TAS', 'Launceston', 'yes', 'YMLT', 'LST', '', 'http://www.launcestonairport.com.au/', 'https://en.wikipedia.org/wiki/Launceston_Airport', ''),
('0', 'Lismore Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=10
iata=LSY
icao', '153° 15'' 33.09', '28° 49'' 51.188', 'public', '10', 'LSY', 'YLIS', ' ', '153.2591916', '-28.8308856', '27035', 'YLIS', 'medium_airport', 'Lismore Airport', '-28.8302993774', '153.259994507', '35', 'OC', 'AU', 'AU-NSW', 'Lismore', 'yes', 'YLIS', 'LSY', '', '', 'https://en.wikipedia.org/wiki/Lismore_Airport', ''),
('0', 'Aeropuerto Internacional de Loreto', 'aeroway=aerodrome
ele=10
fuel=100LL - Jet A
iata=LTO
icao=MM', '111° 21'' 1.684', '25° 59'' 18.343', ' ', '10', 'LTO', 'MMLT', ' ', '-111.3504679', '25.9884287', '4724', 'MMLT', 'medium_airport', 'Loreto International Airport', '25.989200592041', '-111.347999572754', '34', 'NA', 'MX', 'MX-BCS', 'Loreto', 'yes', 'MMLT', 'LTO', '', '', 'https://en.wikipedia.org/wiki/Loreto_International_Airport', ''),
('0', 'Lüderitz Airport', 'aeroway=aerodrome
ele=139
iata=LUD
icao=FYLZ
name=Lüderitz', '15° 14'' 50.406', '26° 41'' 1.297"', ' ', '139', 'LUD', 'FYLZ', ' ', '15.2473351', '-26.6836935', '3029', 'FYLZ', 'medium_airport', 'Luderitz Airport', '-26.6874008178711', '15.2428998947144', '457', 'AF', 'NA', 'NA-KA', 'Luderitz', 'yes', 'FYLZ', 'LUD', '', '', 'https://en.wikipedia.org/wiki/L%C3%BCderitz_Airport', ''),
('0', 'Kenneth Kaunda International Airport', 'aerodrome=international
aerodrome:type=military/public
aerow', '28° 27'' 15.354', '15° 19'' 44.667', 'military/public', '1152', 'LUN', 'FLLS', ' ', '28.4542651', '-15.3290741', '2894', 'FLLS', 'large_airport', 'Kenneth Kaunda International Airport', '-15.3308', '28.4526', '3779', 'AF', 'ZM', 'ZM-09', 'Lusaka', 'yes', 'FLKK', 'LUN', '', 'http://www.nacl.co.zm/?page_id=336', 'https://en.wikipedia.org/wiki/Kenneth_Kaunda_International_Airport', ''),
('0', 'Aeropuerto Brigadier Mayor Cesar Raúl Ojeda', 'aeroway=aerodrome
alt_name=Aeropuerto de San Luis
iata=LUQ
i', '66° 21'' 9.035"', '33° 16'' 23.313', ' ', '0', 'LUQ', 'SAOU', ' ', '-66.3525097', '-33.2731426', '5802', 'SAOU', 'medium_airport', 'Brigadier Mayor D Cesar Raul Ojeda Airport', '-33.2732009888', '-66.3563995361', '2328', 'SA', 'AR', 'AR-D', 'San Luis', 'yes', 'SAOU', 'LUQ', 'UIS', '', '', ''),
('0', 'Karel Sadsuitubun Airport', 'aeroway=aerodrome
iata=LUV
icao=WAPF
name=Karel Sadsuitubun', '132° 45'' 37.62', '5° 45'' 38.659"', ' ', '0', 'LUV', 'WAPF', ' ', '132.7604521', '-5.7607385', '333039', 'WAPF', 'medium_airport', 'Karel Sadsuitubun Airport', '-5.760278', '132.759444', '78', 'AS', 'ID', 'ID-MA', 'Langgur', 'yes', 'WAPF', 'LUV', '', '', 'https://en.wikipedia.org/wiki/Karel_Sadsuitubun_Airport', 'Tual Baru'),
('0', 'Harry Mwanga Nkumbula International Airport', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '25° 49'' 0.892"', '17° 49'' 4.128"', 'military/public', '991', 'LVI', 'FLLI', ' ', '25.8169145', '-17.8178134', '2893', 'FLLI', 'medium_airport', 'Harry Mwanga Nkumbula International Airport', '-17.8218', '25.822701', '3302', 'AF', 'ZM', 'ZM-07', 'Livingstone', 'yes', 'FLHN', 'LVI', '', '', 'https://en.wikipedia.org/wiki/Harry_Mwanga_Nkumbula_International_Airport', 'FLLI'),
('0', '拉萨贡嘎机场 ལྷ་ས་གོང་དཀར་', 'aeroway=aerodrome
ele=3571
iata=LXA
icao=ZULS
int_name=Lhasa', '90° 54'' 41.782', '29° 17'' 39.246', ' ', '3571', 'LXA', 'ZULS', ' ', '90.9116061', '29.2942351', '31867', 'ZULS', 'medium_airport', 'Lhasa Gonggar Airport', '29.2978000641', '90.9119033813', '11713', 'AS', 'CN', 'CN-54', 'Lhasa', 'yes', 'ZULS', 'LXA', '', '', 'https://en.wikipedia.org/wiki/Lhasa_Gonggar_Airport', ''),
('0', 'Aeropuerto Lázaro Cárdenas', 'aeroway=aerodrome
ele=12
iata=LZC
icao=MMLC
name=Aeropuerto', '102° 13'' 11.25', '18° 0'' 10.026"', ' ', '12', 'LZC', 'MMLC', ' ', '-102.2197941', '18.002785', '4720', 'MMLC', 'medium_airport', 'Lázaro Cárdenas Airport', '18.0016994476', '-102.221000671', '39', 'NA', 'MX', 'MX-MIC', 'Lázaro Cárdenas', 'yes', 'MMLC', 'LZC', '', '', 'https://en.wikipedia.org/wiki/L%C3%A1zaro_C%C3%A1rdenas_Airport', ''),
('0', '柳州白莲机场', 'aeroway=aerodrome
ele=90
iata=LZH
icao=ZGZH
name=柳州白', '109° 23'' 29.64', '24° 12'' 27.488', ' ', '90', 'LZH', 'ZGZH', ' ', '109.3915686', '24.2076356', '31874', 'ZGZH', 'medium_airport', 'Liuzhou Bailian Airport', '24.2075', '109.390999', '295', 'AS', 'CN', 'CN-45', 'Liuzhou (Liujiang)', 'yes', 'ZGZH', 'LZH', '', '', 'https://en.wikipedia.org/wiki/Liuzhou_Bailian_Airport', 'Liujiang-Liuzhou Air Base'),
('0', '南竿機場', 'aerodrome:type=public
aeroway=aerodrome
closest_town=南竿
', '119° 57'' 30.22', '26° 9'' 34.995"', 'public', '71', 'LZN', 'RCFG', ' ', '119.958396', '26.1597209', '5513', 'RCFG', 'medium_airport', 'Matsu Nangan Airport', '26.1598', '119.958', '232', 'AS', 'TW', 'TW-X-LK', 'Matsu (Nangan)', 'yes', 'RCFG', 'LZN', '', 'http://www.tsa.gov.tw/tsaLZN/zh/home.aspx', 'https://en.wikipedia.org/wiki/Matsu_Nangan_Airport', 'Matsu Islands'),
('0', '林芝米林机场', 'aeroway=aerodrome
ele=2949
iata=LZY
icao=ZUNZ
name=林芝米', '94° 20'' 7.714"', '29° 18'' 12.351', ' ', '2949', 'LZY', 'ZUNZ', ' ', '94.3354762', '29.3034309', '31876', 'ZUNZ', 'medium_airport', 'Nyingchi Airport', '29.3033008575439', '94.3352966308594', '9675', 'AS', 'CN', 'CN-54', 'Nyingchi', 'yes', 'ZUNZ', 'LZY', '', '', 'https://en.wikipedia.org/wiki/Nyingchi_Airport', 'Linzhi, Kang Ko'),
('0', 'Chennai International Airport', 'aerodrome=international
aeroway=aerodrome
iata=MAA
icao=VOMM', '80° 10'' 2.246"', '12° 59'' 36.120', ' ', '0', 'MAA', 'VOMM', ' ', '80.1672905', '12.9933666', '26618', 'VOMM', 'large_airport', 'Chennai International Airport', '12.990005', '80.169296', '52', 'AS', 'IN', 'IN-TN', 'Chennai', 'yes', 'VOMM', 'MAA', '', '', 'https://en.wikipedia.org/wiki/Chennai_International_Airport', ''),
('0', 'Aeroporto João Correa da Rocha', 'aeroway=aerodrome
alt_name=Aeroporto de Marabá
iata=MAB
ica', '49° 8'' 23.044"', '5° 22'' 4.741"', ' ', '0', 'MAB', 'SBMA', ' ', '-49.1397345', '-5.3679835', '5934', 'SBMA', 'medium_airport', 'João Correa da Rocha Airport', '-5.36858987808', '-49.1380004883', '357', 'SA', 'BR', 'BR-PA', 'Marabá', 'yes', 'SBMA', 'MAB', '', '', 'https://en.wikipedia.org/wiki/Marab%C3%A1_Airport', 'Marabá Airport'),
('0', 'General Servando Canales International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Matamor', '97° 31'' 38.502', '25° 46'' 16.833', 'public', '8', 'MAM', 'MMMA', ' ', '-97.5273617', '25.7713424', '4725', 'MMMA', 'medium_airport', 'General Servando Canales International Airport', '25.7698993683', '-97.5252990723', '25', 'NA', 'MX', 'MX-TAM', 'Matamoros', 'yes', 'MMMA', 'MAM', '', '', 'https://en.wikipedia.org/wiki/General_Servando_Canales_International_Airport', ''),
('0', 'Aeroporto Internacional Eduardo Gomes', 'aerodrome:type=public
aeroway=aerodrome
ele=80.5
iata=MAO
ic', '60° 2'' 48.526"', '3° 2'' 8.249" S', 'public', '80', 'MAO', 'SBEG', ' ', '-60.0468128', '-3.0356246', '5897', 'SBEG', 'large_airport', 'Eduardo Gomes International Airport', '-3.03861', '-60.049702', '264', 'SA', 'BR', 'BR-AM', 'Manaus', 'yes', 'SBEG', 'MAO', '', 'https://www4.infraero.gov.br/aeroportos/aeroporto-internacional-de-manaus-eduardo-gomes/', 'https://en.wikipedia.org/wiki/Eduardo_Gomes_International_Airport', ''),
('0', 'Aéroport d''Ourossogui', 'aeroway=aerodrome
alt_name=Ouro Sogui
iata=MAX
icao=GOSM
nam', '13° 19'' 24.033', '15° 35'' 40.564', ' ', '0', 'MAX', 'GOSM', ' ', '-13.3233425', '15.5946012', '31879', 'GOSM', 'medium_airport', 'Ouro Sogui Airport', '15.5936', '-13.3228', '85', 'AF', 'SN', 'SN-MT', 'Ouro Sogui', 'yes', 'GOSM', 'MAX', '', '', 'https://en.wikipedia.org/wiki/Ouro_Sogui_Airport', ''),
('0', 'Eugenio María de Hostos Airport', 'aeroway=aerodrome
ele=9
gnis:county_name=Mayaguez
gnis:creat', '67° 8'' 53.386"', '18° 15'' 18.717', ' ', '9', 'MAZ', 'TJMZ', ' ', '-67.1481629', '18.2551991', '6382', 'TJMZ', 'medium_airport', 'Eugenio Maria De Hostos Airport', '18.2556991577148', '-67.1484985351563', '28', 'NA', 'PR', 'PR-U-A', 'Mayaguez', 'yes', 'TJMZ', 'MAZ', 'MAZ', '', 'https://en.wikipedia.org/wiki/Eugenio_Mar%C3%ADa_de_Hostos_Airport', ''),
('0', 'Mafikeng International Airport', 'aeroway=aerodrome
gliding=aerotow
iata=MBD
icao=FAMM
ifr=yes', '25° 32'' 29.037', '25° 48'' 17.866', ' ', '0', 'MBD', 'FAMM', ' ', '25.5413992', '-25.8049629', '2812', 'FAMM', 'medium_airport', 'Mmabatho International Airport', '-25.7984008789', '25.5480003357', '4181', 'AF', 'ZA', 'ZA-NW', 'Mafeking', 'yes', 'FAMM', 'MBD', '', '', 'https://en.wikipedia.org/wiki/Mmabatho_International_Airport', 'Mafikeng'),
('0', '紋別空港', 'KSJ2:AAC=01219
KSJ2:AAC_label=北海道紋別市
KSJ2:AD2=5
', '143° 24'' 19.02', '44° 18'' 9.544"', 'public', '18', 'MBE', 'RJEB', 'Аэропорт Монбетсу', '143.4052847', '44.3026512', '5557', 'RJEB', 'medium_airport', 'Monbetsu Airport', '44.3039016724', '143.404006958', '80', 'AS', 'JP', 'JP-01', 'Monbetsu', 'yes', 'RJEB', 'MBE', '', '', 'https://en.wikipedia.org/wiki/Monbetsu_Airport', ''),
('0', 'Sangster International Airport', 'aerodrome=international
aeroway=aerodrome
alt_name=Montego B', '77° 54'' 58.356', '18° 30'' 6.196"', ' ', '1', 'MBJ', 'MKJS', ' ', '-77.9162101', '18.5017212', '4681', 'MKJS', 'medium_airport', 'Sangster International Airport', '18.5037002563477', '-77.9133987426758', '4', 'NA', 'JM', 'JM-08', 'Montego Bay', 'yes', 'MKJS', 'MBJ', '', '', 'https://en.wikipedia.org/wiki/Sangster_International_Airport', ''),
('0', 'Moises R. Espinosa Airport', 'addr:city=Masbate
aeroway=aerodrome
alt_name=Masbate Airport', '123° 37'' 48.00', '12° 22'' 10.722', ' ', '0', 'MBT', 'RPVJ', ' ', '123.6300006', '12.3696449', '5740', 'RPVJ', 'medium_airport', 'Moises R. Espinosa Airport', '12.369682', '123.630095', '49', 'AS', 'PH', 'PH-MAS', 'Masbate', 'yes', 'RPVJ', 'MBT', '', '', 'https://en.wikipedia.org/wiki/Masbate_Airport', 'Masbate, Moises R. Espinoza'),
('0', 'Moorabbin Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Harry Hawke', '145° 5'' 49.381', '37° 58'' 40.416', 'public', '0', 'MBW', 'YMMB', ' ', '145.0970502', '-37.9778934', '27064', 'YMMB', 'medium_airport', 'Melbourne Moorabbin Airport', '-37.9757995605469', '145.102005004883', '50', 'OC', 'AU', 'AU-VIC', 'Melbourne', 'yes', 'YMMB', 'MBW', '', 'http://www.moorabbinairport.com.au/', 'https://en.wikipedia.org/wiki/Moorabbin_Airport', ''),
('0', 'Sunshine Coast Airport', 'aeroway=aerodrome
closest_town=Sunshine Coast, Queensland
el', '153° 5'' 17.724', '26° 36'' 4.846"', ' ', '5', 'MCY', 'YBMC', ' ', '153.0882567', '-26.601346', '26918', 'YBSU', 'medium_airport', 'Sunshine Coast Airport', '-26.6033', '153.091003', '15', 'OC', 'AU', 'AU-QLD', 'Maroochydore', 'yes', 'YBSU', 'MCY', '', 'http://www.sunshinecoastairport.com.au/', 'https://en.wikipedia.org/wiki/Sunshine_Coast_Airport', 'YBSU'),
('0', 'Aeroporto Internacional Zumbi dos Palmares', 'aerodrome:type=public
aeroway=aerodrome
ele=118
iata=MCZ
ica', '35° 47'' 33.173', '9° 30'' 53.898"', 'public', '118', 'MCZ', 'SBMO', ' ', '-35.792548', '-9.5149716', '5942', 'SBMO', 'medium_airport', 'Zumbi dos Palmares Airport', '-9.51080989837647', '-35.7916984558105', '387', 'SA', 'BR', 'BR-AL', 'Maceió', 'yes', 'SBMO', 'MCZ', '', '', 'https://en.wikipedia.org/wiki/Zumbi_dos_Palmares_International_Airport', ''),
('0', 'José María Córdova', 'aerodrome=international
aeroway=aerodrome
closest_city=Medel', '75° 25'' 34.391', '6° 9'' 59.097"', ' ', '2142', 'MDE', 'SKRG', ' ', '-75.4262198', '6.1664157', '6158', 'SKRG', 'medium_airport', 'Jose Maria Córdova International Airport', '6.16454', '-75.4231', '6955', 'SA', 'CO', 'CO-ANT', 'Medellín', 'yes', 'SKRG', 'MDE', 'MDE', '', 'https://en.wikipedia.org/wiki/Jos%C3%A9_Mar%C3%ADa_C%C3%B3rdova_International_Airport', ''),
('0', '牡丹江海浪机场', 'aerodrome:type=international
aeroway=aerodrome
iata=MDG
icao', '129° 34'' 9.494', '44° 31'' 32.306', 'international', '0', 'MDG', 'ZYMD', 'Аэропорт Муданьцзян', '129.569304', '44.5256406', '27240', 'ZYMD', 'medium_airport', 'Mudanjiang Hailang International Airport', '44.5241012573', '129.569000244', '883', 'AS', 'CN', 'CN-23', 'Mudanjiang', 'yes', 'ZYMD', 'MDG', '', '', 'https://en.wikipedia.org/wiki/Mudanjiang_Airport', 'Mudanjiang'),
('0', 'Makurdi Airport', 'GNS:dsg_code=AIRP
GNS:dsg_string=airport
GNS:id=-2018677
aer', '8° 36'' 45.706"', '7° 42'' 8.422"', ' ', '0', 'MDI', 'DNMK', ' ', '8.6126961', '7.7023394', '2117', 'DNMK', 'medium_airport', 'Makurdi Airport', '7.70388', '8.61394', '371', 'AF', 'NG', 'NG-BE', 'Makurdi', 'yes', 'DNMK', 'MDI', '', 'http://www.faannigeria.org/nigeria-airport.php?airport=23', 'https://en.wikipedia.org/wiki/Makurdi_Airport', ''),
('0', 'မန္တလေး အပြည်ပြည်ဆို', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '95° 58'' 40.289', '21° 42'' 3.177"', 'public', '91', 'MDL', 'VYMD', ' ', '95.9778581', '21.7008826', '26727', 'VYMD', 'large_airport', 'Mandalay International Airport', '21.7021999359131', '95.977897644043', '300', 'AS', 'MM', 'MM-04', 'Mandalay', 'yes', 'VYMD', 'MDL', '', '', 'https://en.wikipedia.org/wiki/Mandalay_International_Airport', ''),
('0', 'Aeropuerto Internacional Astor Piazolla', 'aeroway=aerodrome
faa=MDP
iata=MDQ
icao=SAZM
name=Aeropuerto', '57° 34'' 18.191', '37° 56'' 5.339"', ' ', '0', 'MDQ', 'SAZM', ' ', '-57.5717198', '-37.9348164', '5847', 'SAZM', 'medium_airport', 'Ástor Piazzola International Airport', '-37.9342', '-57.5733', '72', 'SA', 'AR', 'AR-B', 'Mar del Plata', 'yes', 'SAZM', 'MDQ', 'MDP', '', 'https://en.wikipedia.org/wiki/%C3%81stor_Piazzola_International_Airport', 'Brigadier General Bartolome De La Colina International Airport'),
('0', 'Mendi Airport', 'aeroway=aerodrome
ele=1731
iata=MDU
icao=AYMN
is_in=Mendi, P', '143° 39'' 30.17', '6° 8'' 47.474"', ' ', '1731', 'MDU', 'AYMN', ' ', '143.6583828', '-6.1465206', '64', 'AYMN', 'medium_airport', 'Mendi Airport', '-6.14774', '143.656998', '5680', 'OC', 'PG', 'PG-SHM', 'Mendi', 'yes', 'AYMN', 'MDU', '', '', 'https://en.wikipedia.org/wiki/Mendi_Airport', ''),
('0', 'Aeropuerto Internacional Gobernador Francisco Gabrielli', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '68° 47'' 43.134', '32° 49'' 50.407', 'public', '704', 'MDZ', 'SAME', ' ', '-68.7953151', '-32.8306686', '5786', 'SAME', 'medium_airport', 'El Plumerillo Airport', '-32.8316993713', '-68.7929000854', '2310', 'SA', 'AR', 'AR-M', 'Mendoza', 'yes', 'SAME', 'MDZ', 'DOZ', '', 'https://en.wikipedia.org/wiki/Governor_Francisco_Gabrielli_International_Airport', ''),
('0', 'Aeroporto de Macaé', 'addr:city=Macaé
addr:postcode=27963-840
addr:street=Rua Hil', '41° 45'' 55.772', '22° 20'' 35.801', ' ', '3', 'MEA', 'SBME', ' ', '-41.7654921', '-22.343278', '5937', 'SBME', 'medium_airport', 'Macaé Airport', '-22.343000412', '-41.7659988403', '8', 'SA', 'BR', 'BR-RJ', 'Macaé', 'yes', 'SBME', 'MEA', '', '', 'https://en.wikipedia.org/wiki/Maca%C3%A9_Airport', ''),
('0', 'Essendon Airport', 'aerodrome:type=public
aeroway=aerodrome
city_served=Melbourn', '144° 54'' 14.23', '37° 43'' 35.899', 'public', '86', 'MEB', 'YMEN', ' ', '144.9039545', '-37.7266387', '27051', 'YMEN', 'medium_airport', 'Melbourne Essendon Airport', '-37.7281', '144.901993', '282', 'OC', 'AU', 'AU-VIC', 'Melbourne', 'yes', 'YMEN', 'MEB', '', 'http://www.essendonairport.com.au/', 'https://en.wikipedia.org/wiki/Essendon_Airport', ''),
('0', 'Aeropuerto Eloy Alfaro', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '80° 40'' 48.498', '0° 56'' 48.833"', 'military/public', '15', 'MEC', 'SEMT', ' ', '-80.6801382', '-0.946898', '6066', 'SEMT', 'medium_airport', 'Eloy Alfaro International Airport', '-0.946078002452851', '-80.6788024902344', '48', 'SA', 'EC', 'EC-M', 'Manta', 'yes', 'SEMT', 'MEC', '', '', 'https://en.wikipedia.org/wiki/Manta_Air_Base', ''),
('0', 'Aéroport de La Roche Maré', 'aeroway=aerodrome
iata=MEE
icao=NWWR
name=Aéroport de La Ro', '168° 2'' 10.980', '21° 28'' 55.401', ' ', '0', 'MEE', 'NWWR', ' ', '168.0363833', '-21.4820557', '5018', 'NWWR', 'medium_airport', 'Maré Airport', '-21.4817008972168', '168.037994384766', '141', 'OC', 'NC', 'NC-U-A', 'Maré', 'yes', 'NWWR', 'MEE', '', '', 'https://en.wikipedia.org/wiki/Mar%C3%A9_Airport', ''),
('0', 'Malanje Airport', 'aeroway=aerodrome
iata=MEG
icao=FNMA
name=Malanje Airport
re', '16° 18'' 43.167', '9° 31'' 30.130"', ' ', '0', 'MEG', 'FNMA', ' ', '16.3119907', '-9.5250361', '2944', 'FNMA', 'medium_airport', 'Malanje Airport', '-9.52509021759033', '16.3124008178711', '3868', 'AF', 'AO', 'AO-MAL', 'Malanje', 'yes', 'FNMA', 'MEG', '', '', 'https://en.wikipedia.org/wiki/Malanje_Airport', ''),
('0', 'Melbourne Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '144° 50'' 12.11', '37° 40'' 2.304"', 'public', '132', 'MEL', 'YMML', ' ', '144.8366997', '-37.6673066', '27066', 'YMML', 'large_airport', 'Melbourne International Airport', '-37.673302', '144.843002', '434', 'OC', 'AU', 'AU-VIC', 'Melbourne', 'yes', 'YMML', 'MEL', '', 'http://melbourneairport.com.au/', 'https://en.wikipedia.org/wiki/Melbourne_Airport', ''),
('0', 'Aeroporto de Monte Dourado - Serra do Areão', 'aeroway=aerodrome
iata=MEU
icao=SBMD
name=Aeroporto de Monte', '52° 36'' 5.126"', '0° 53'' 22.798"', ' ', '0', 'MEU', 'SBMD', ' ', '-52.6014238', '-0.889666', '5936', 'SBMD', 'medium_airport', 'Monte Dourado Airport', '-0.889839', '-52.6022', '677', 'SA', 'BR', 'BR-PA', 'Almeirim', 'yes', 'SBMD', 'MEU', '', '', 'https://en.wikipedia.org/wiki/Monte_Dourado_Airport', ''),
('0', 'Aeropuerto Internacional de la Ciudad de México', 'aerodrome:type=public
aeroway=aerodrome
barrier=fence
ele=22', '99° 4'' 10.544"', '19° 26'' 3.648"', 'public', '2238', 'MEX', 'MMMX', ' ', '-99.0695955', '19.4343467', '4731', 'MMMX', 'large_airport', 'Licenciado Benito Juarez International Airport', '19.4363', '-99.072098', '7316', 'NA', 'MX', 'MX-DIF', 'Mexico City', 'yes', 'MMMX', 'MEX', 'ME1', 'https://www.aicm.com.mx', 'https://en.wikipedia.org/wiki/Mexico_City_International_Airport', 'AICM'),
('0', 'McAllen Miller International Airport', 'addr:state=TX
aerodrome:type=public
aeroway=aerodrome
alt_na', '98° 14'' 21.952', '26° 10'' 33.253', 'public', '31', 'MFE', 'KMFE', ' ', '-98.2394311', '26.1759037', '3678', 'KMFE', 'medium_airport', 'Mc Allen Miller International Airport', '26.17580032', '-98.23860168', '107', 'NA', 'US', 'US-TX', 'Mc Allen', 'yes', 'KMFE', 'MFE', 'MFE', '', 'https://en.wikipedia.org/wiki/McAllen-Miller_International_Airport', ''),
('0', 'Muzaffarabad aerodrome', 'aeroway=aerodrome
closest_town=Muzaffarabad
ele=820
iata=MFG', '73° 30'' 31.810', '34° 20'' 19.658', ' ', '820', 'MFG', 'OPMF', ' ', '73.5088362', '34.3387939', '5260', 'OPMF', 'medium_airport', 'Muzaffarabad Airport', '34.3390007019043', '73.5085983276367', '2691', 'AS', 'PK', 'PK-JK', 'Muzaffarabad', 'yes', 'OPMF', 'MFG', '', '', 'https://en.wikipedia.org/wiki/Muzaffarabad_Airport', ''),
('0', '馬祖 北竿機場', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Matsu I', '120° 0'' 9.183"', '26° 13'' 27.825', 'public', '12', 'MFK', 'RCMT', ' ', '120.0025507', '26.2243958', '5521', 'RCMT', 'medium_airport', 'Matsu Beigan Airport', '26.224199', '120.002998', '41', 'AS', 'TW', 'TW-X-LK', 'Matsu (Beigan)', 'yes', 'RCMT', 'MFK', '', 'http://www.tsa.gov.tw/tsaMFK/en/page.aspx?id=1174', 'https://en.wikipedia.org/wiki/Matsu_Beigan_Airport', 'Matsu Islands'),
('0', '澳門國際機場 Macau International Airport', 'aerodrome:type=international
aeroway=aerodrome
ele=6
iata=MF', '113° 35'' 2.538', '22° 8'' 59.565"', 'international', '6', 'MFM', 'VMMC', ' ', '113.5840382', '22.1498791', '26589', 'VMMC', 'large_airport', 'Macau International Airport', '22.149599', '113.592003', '20', 'AS', 'MO', 'MO-U-A', 'Freguesia de Nossa Senhora do Carmo (Taipa)', 'yes', 'VMMC', 'MFM', '', 'http://www.macau-airport.gov.mo/en/index.php', 'https://en.wikipedia.org/wiki/Macau_International_Airport', '澳門國際機場'),
('0', 'Rogue Valley International-Medford Airport', 'addr:state=OR
aeroway=aerodrome
alt_name=Medford-Jackson Cou', '122° 52'' 24.00', '42° 22'' 22.455', ' ', '406', 'MFR', 'KMFR', ' ', '-122.8733349', '42.3729043', '3679', 'KMFR', 'medium_airport', 'Rogue Valley International Medford Airport', '42.3741989135742', '-122.873001098633', '1335', 'NA', 'US', 'US-OR', 'Medford', 'yes', 'KMFR', 'MFR', 'MFR', '', 'https://en.wikipedia.org/wiki/Rogue_Valley_International-Medford_Airport', ''),
('0', 'Aeropuerto Internacional Augusto C. Sandino', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Ae', '86° 10'' 8.098"', '12° 8'' 25.745"', 'military/public', '59', 'MGA', 'MNMG', ' ', '-86.1689162', '12.1404846', '4774', 'MNMG', 'medium_airport', 'Augusto C. Sandino (Managua) International Airport', '12.1415004730225', '-86.1681976318359', '194', 'NA', 'NI', 'NI-MN', 'Managua', 'yes', 'MNMG', 'MGA', '', '', 'https://en.wikipedia.org/wiki/Augusto_C._Sandino_International_Airport', ''),
('0', 'Mount Gambier Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=64
iata=MGB
icao', '140° 47'' 4.097', '37° 44'' 50.294', 'public', '64', 'MGB', 'YMTG', ' ', '140.7844715', '-37.747304', '27076', 'YMTG', 'medium_airport', 'Mount Gambier Airport', '-37.7456016540527', '140.785003662109', '212', 'OC', 'AU', 'AU-SA', '', 'yes', 'YMTG', 'MGB', '', '', 'https://en.wikipedia.org/wiki/Mount_Gambier_Airport', ''),
('0', 'Aeroporto Regional de Maringá Silvio Name Júnior', 'addr:street=Avenida Vladimir Babkov
aeroway=aerodrome
iata=M', '52° 0'' 58.333"', '23° 28'' 33.995', ' ', '0', 'MGF', 'SBMG', ' ', '-52.0162036', '-23.4761097', '5938', 'SBMG', 'medium_airport', 'Regional de Maringá - Sílvio Name Júnior Airport', '-23.47606', '-52.016187', '1788', 'SA', 'BR', 'BR-PR', 'Maringá', 'yes', 'SBMG', 'MGF', 'PR0004', '', 'https://en.wikipedia.org/wiki/Maring%C3%A1_Domestic_Airport', ''),
('0', 'Margate Airport', 'aeroway=aerodrome
iata=MGH
icao=FAMG
ifr=yes
licensed:sacaa=', '30° 20'' 36.493', '30° 51'' 26.510', ' ', '0', 'MGH', 'FAMG', ' ', '30.3434702', '-30.8573639', '2809', 'FAMG', 'medium_airport', 'Margate Airport', '-30.8574008942', '30.343000412', '495', 'AF', 'ZA', 'ZA-NL', 'Margate', 'yes', 'FAMG', 'MGH', '', '', 'https://en.wikipedia.org/wiki/Margate_Airport', ''),
('0', 'Mount Hotham Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Bright,', '147° 20'' 6.048', '37° 2'' 53.142"', 'public', '1298', 'MHU', 'YHOT', ' ', '147.3350133', '-37.048095', '27008', 'YHOT', 'medium_airport', 'Mount Hotham Airport', '-37.0475006104', '147.333999634', '4260', 'OC', 'AU', 'AU-VIC', 'Mount Hotham', 'yes', 'YHOT', 'MHU', '', '', 'https://en.wikipedia.org/wiki/Mount_Hotham_Airport', ''),
('0', 'Miami International Airport', 'addr:city=Miami
addr:country=US
addr:housenumber=2100
addr:p', '80° 17'' 30.714', '25° 47'' 43.946', ' ', '2', 'MIA', 'KMIA', ' ', '-80.2918651', '25.7955406', '3685', 'KMIA', 'large_airport', 'Miami International Airport', '25.7931995391846', '-80.2906036376953', '8', 'NA', 'US', 'US-FL', 'Miami', 'yes', 'KMIA', 'MIA', 'MIA', 'http://www.miami-airport.com/', 'https://en.wikipedia.org/wiki/Miami_International_Airport', 'MFW, South Florida'),
('0', 'Merimbula Airport', 'aerodrome:category=certified
aerodrome:type=public
aeroway=a', '149° 54'' 8.353', '36° 54'' 25.942', 'public', '0', 'MIM', 'YMER', ' ', '149.9023204', '-36.907206', '27052', 'YMER', 'medium_airport', 'Merimbula Airport', '-36.9085998535', '149.901000977', '7', 'OC', 'AU', 'AU-NSW', 'Merimbula', 'yes', 'YMER', 'MIM', '', '', 'https://en.wikipedia.org/wiki/Merimbula_Airport', ''),
('0', 'Shark Bay Airport', 'aerodrome:type=public
aeroway=aerodrome
iata=MJK
icao=YSHK
n', '113° 34'' 34.45', '25° 53'' 46.270', 'public', '0', 'MJK', 'YSHK', ' ', '113.576238', '-25.896186', '27137', 'YSHK', 'medium_airport', 'Shark Bay Airport', '-25.8938999176', '113.577003479', '111', 'OC', 'AU', 'AU-WA', 'Denham', 'yes', 'YSHK', 'MJK', '', '', 'https://en.wikipedia.org/wiki/Shark_Bay_Airport', 'DNM, Denham Airport, Monkey Mia Airport'),
('0', 'Molokai Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
ele=13', '157° 5'' 41.655', '21° 9'' 10.643"', 'public', '136', 'MKK', 'PHMK', ' ', '-157.0949041', '21.1529565', '5450', 'PHMK', 'medium_airport', 'Molokai Airport', '21.1529006958008', '-157.095993041992', '454', 'NA', 'US', 'US-HI', 'Kaunakakai', 'yes', 'PHMK', 'MKK', 'MKK', '', 'https://en.wikipedia.org/wiki/Molokai_Airport', ''),
('0', 'Mackay', 'aeroway=aerodrome
iata=MKY
icao=YBMK
name=Mackay', '149° 10'' 57.09', '21° 10'' 14.365', ' ', '0', 'MKY', 'YBMK', ' ', '149.1825266', '-21.1706569', '26919', 'YBMK', 'medium_airport', 'Mackay Airport', '-21.1716995239', '149.179992676', '19', 'OC', 'AU', 'AU-QLD', 'Mackay', 'yes', 'YBMK', 'MKY', '', 'http://www.mackayairport.com.au/', 'https://en.wikipedia.org/wiki/Mackay_Airport', ''),
('0', 'Melbourne International Airport', 'addr:state=FL
aeroway=aerodrome
ele=7
gnis:county_name=Breva', '80° 38'' 49.618', '28° 6'' 11.247"', ' ', '7', 'MLB', 'KMLB', ' ', '-80.6471162', '28.1031243', '3693', 'KMLB', 'medium_airport', 'Melbourne International Airport', '28.1028003692627', '-80.6453018188477', '33', 'NA', 'US', 'US-FL', 'Melbourne', 'yes', 'KMLB', 'MLB', 'MLB', '', 'https://en.wikipedia.org/wiki/Melbourne_International_Airport', ''),
('0', 'ވެލާނާ ބައިނަލްއަޤުވާމީ ވައިގެ', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Malé
e', '73° 31'' 44.565', '4° 11'' 28.605"', 'public', '2', 'MLE', 'VRMM', ' ', '73.5290459', '4.1912793', '26636', 'VRMM', 'large_airport', 'Malé International Airport', '4.19183015823364', '73.5290985107422', '6', 'AS', 'MV', 'MV-MLE', 'Malé', 'yes', 'VRMM', 'MLE', '', 'http://www.airports.com.mv/', 'https://en.wikipedia.org/wiki/Mal%C3%A9_International_Airport', ''),
('0', 'Aeropuerto Internacional General Francisco J. Mujica', 'addr:city=Alavaro Obregón
addr:country=MX
addr:postcode=589', '101° 1'' 33.293', '19° 50'' 57.929', 'public', '1839', 'MLM', 'MMMM', ' ', '-101.0259148', '19.8494246', '4728', 'MMMM', 'medium_airport', 'General Francisco J. Mujica International Airport', '19.849899292', '-101.025001526', '6033', 'NA', 'MX', 'MX-MIC', 'Morelia', 'yes', 'MMMM', 'MLM', '', '', 'https://en.wikipedia.org/wiki/General_Francisco_J._Mujica_International_Airport', ''),
('0', 'James Spriggs Payne Airport', 'aeroway=aerodrome
iata=MLW
icao=GLMR
name=James Spriggs Payn', '10° 45'' 32.981', '6° 17'' 20.353"', ' ', '0', 'MLW', 'GLMR', ' ', '-10.7591613', '6.288987', '3098', 'GLMR', 'medium_airport', 'Spriggs Payne Airport', '6.28906011581421', '-10.7587003707886', '25', 'AF', 'LR', 'LR-MO', 'Monrovia', 'yes', 'GLMR', 'MLW', '', '', 'https://en.wikipedia.org/wiki/Spriggs_Payne_Airport', ''),
('0', '女満別空港', 'KSJ2:AAC=01542
KSJ2:AAC_label=北海道, *
KSJ2:AD2=5
KSJ2:A', '144° 9'' 48.924', '43° 52'' 46.195', ' ', '0', 'MMB', 'RJCM', ' ', '144.16359', '43.8794986', '5548', 'RJCM', 'medium_airport', 'Memanbetsu Airport', '43.8805999756', '144.164001465', '135', 'AS', 'JP', 'JP-01', 'Ōzora', 'yes', 'RJCM', 'MMB', '', '', 'https://en.wikipedia.org/wiki/Memanbetsu_Airport', ''),
('0', '南大東空港', 'KSJ2:AAC=47357
KSJ2:AAC_label=沖縄県島尻郡南大東村', '131° 15'' 48.46', '25° 50'' 47.436', ' ', '0', 'MMD', 'ROMD', ' ', '131.2634615', '25.84651', '5676', 'ROMD', 'medium_airport', 'Minamidaito Airport', '25.8465', '131.263', '167', 'AS', 'JP', 'JP-47', 'Minamidaito', 'yes', 'ROMD', 'MMD', '', '', 'https://en.wikipedia.org/wiki/Minami-Daito_Airport#Airlines', ''),
('0', '松本空港', 'KSJ2:AAC=20202
KSJ2:AAC_label=長野県松本市
KSJ2:AD2=5
', '137° 55'' 26.36', '36° 9'' 58.338"', ' ', '0', 'MMJ', 'RJAF', ' ', '137.92399', '36.166205', '5532', 'RJAF', 'medium_airport', 'Shinshu-Matsumoto Airport', '36.166801', '137.923004', '2182', 'AS', 'JP', 'JP-20', 'Matsumoto', 'yes', 'RJAF', 'MMJ', '', 'http://www.matsumoto-airport.co.jp/', 'https://en.wikipedia.org/wiki/Matsumoto_Airport', ''),
('0', 'John A. Osborne Airport', 'aeroway=aerodrome
closest_town=Montserrat
ele=168
iata=MNI
i', '62° 11'' 36.623', '16° 47'' 30.220', ' ', '168', 'MNI', 'TRPG', ' ', '-62.1935063', '16.7917279', '43074', 'TRPG', 'medium_airport', 'John A. Osborne Airport', '16.7914009094238', '-62.1932983398438', '550', 'NA', 'MS', 'MS-U-A', 'Gerald''s Park', 'yes', 'TRPG', 'MNI', '', '', 'https://en.wikipedia.org/wiki/John_A._Osborne_Airport', 'Gerald''s Airport'),
('0', 'Aeroporto de Montes Claros - Mário Ribeiro', 'aeroway=aerodrome
alt_name=Aeroporto de Montes Claros
iata=M', '43° 49'' 2.371"', '16° 42'' 18.045', ' ', '0', 'MOC', 'SBMK', ' ', '-43.8173252', '-16.7050124', '5939', 'SBMK', 'medium_airport', 'Mário Ribeiro Airport', '-16.7068996429', '-43.818901062', '2191', 'SA', 'BR', 'BR-MG', 'Montes Claros', 'yes', 'SBMK', 'MOC', '', '', 'https://en.wikipedia.org/wiki/Montes_Claros_Airport', 'Montes Claros Airport'),
('0', 'Moorea-Temae', 'aeroway=aerodrome
iata=MOZ
icao=NTTM
name=Moorea-Temae
sourc', '149° 45'' 40.88', '17° 29'' 23.439', ' ', '0', 'MOZ', 'NTTM', ' ', '-149.7613573', '-17.4898441', '5006', 'NTTM', 'medium_airport', 'Moorea Airport', '-17.49', '-149.761993', '9', 'OC', 'PF', 'PF-U-A', '', 'yes', 'NTTM', 'MOZ', '', '', 'https://en.wikipedia.org/wiki/Moorea_Airport', 'Temae Airport, Moorea Temae Airport'),
('0', 'RAF Mount Pleasant', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Mo', '58° 27'' 23.509', '51° 49'' 15.160', 'military/public', '74', 'MPN', 'EGYP', ' ', '-58.4565304', '-51.8208779', '2512', 'EGYP', 'medium_airport', 'Mount Pleasant Airport', '-51.8227996826172', '-58.4472007751465', '244', 'SA', 'FK', 'FK-U-A', 'Mount Pleasant', 'yes', 'EGYP', 'MPN', '', '', 'https://en.wikipedia.org/wiki/RAF_Mount_Pleasant', 'RAF Mount Pleasant'),
('0', 'Mildura Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Mildura', '142° 5'' 4.355"', '34° 13'' 50.885', 'public', '0', 'MQL', 'YMIA', ' ', '142.084543', '-34.2308014', '27060', 'YMIA', 'medium_airport', 'Mildura Airport', '-34.2291984558', '142.085998535', '167', 'OC', 'AU', 'AU-VIC', 'Mildura', 'yes', 'YMIA', 'MQL', '', 'http://www.milduraairport.com.au/', 'https://en.wikipedia.org/wiki/Mildura_Airport', ''),
('0', 'Kruger Mpumalanga International Airport', 'aeroway=aerodrome
iata=MQP
icao=FAKN
ifr=yes
licensed:sacaa=', '31° 6'' 18.217"', '25° 22'' 57.083', ' ', '0', 'MQP', 'FAKN', ' ', '31.1050603', '-25.3825231', '2795', 'FAKN', 'medium_airport', 'Kruger Mpumalanga International Airport', '-25.3831996918', '31.1056003571', '2829', 'AF', 'ZA', 'ZA-MP', 'Mpumalanga', 'yes', 'FAKN', 'MQP', '', '', 'https://en.wikipedia.org/wiki/Kruger_Mpumalanga_International_Airport', ''),
('0', 'Monterey Regional Airport', 'addr:state=CA
aerodrome:type=public
aeroway=aerodrome
alt_na', '121° 50'' 46.97', '36° 35'' 15.837', 'public', '73', 'MRY', 'KMRY', ' ', '-121.8463809', '36.5877324', '3705', 'KMRY', 'medium_airport', 'Monterey Peninsula Airport', '36.5870018005371', '-121.843002319336', '257', 'NA', 'US', 'US-CA', 'Monterey', 'yes', 'KMRY', 'MRY', 'MRY', '', 'https://en.wikipedia.org/wiki/Monterey_Peninsula_Airport', ''),
('0', 'Moree Aerodrome', 'aerodrome:type=public
aeroway=aerodrome
ele=214
iata=MRZ
ica', '149° 50'' 32.36', '29° 30'' 0.183"', 'public', '214', 'MRZ', 'YMOR', ' ', '149.8423231', '-29.5000508', '27072', 'YMOR', 'medium_airport', 'Moree Airport', '-29.4988994598', '149.845001221', '701', 'OC', 'AU', 'AU-NSW', 'Moree', 'yes', 'YMOR', 'MRZ', '', '', 'https://en.wikipedia.org/wiki/Moree_Airport', ''),
('0', '三沢飛行場', 'KSJ2:AAC=02207
KSJ2:AAC_label=青森県三沢市
KSJ2:AD2=3
', '141° 22'' 6.254', '40° 42'' 4.891"', 'public', '36', 'MSJ', 'RJSM', 'Аэродром Мисава', '141.3684038', '40.7013585', '5612', 'RJSM', 'medium_airport', 'Misawa Air Base / Misawa Airport', '40.703201', '141.367996', '119', 'AS', 'JP', 'JP-02', 'Misawa', 'yes', 'RJSM', 'MSJ', '', '', 'https://en.wikipedia.org/wiki/Misawa_Air_Base', ''),
('0', 'Moshoeshoe I International Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Moshoeshoe', '27° 33'' 16.756', '29° 27'' 22.260', 'public', '1630', 'MSU', 'FXMM', ' ', '27.5546545', '-29.4561833', '3022', 'FXMM', 'medium_airport', 'Moshoeshoe I International Airport', '-29.4622993469238', '27.5524997711182', '5348', 'AF', 'LS', 'LS-A', 'Maseru', 'yes', 'FXMM', 'MSU', '', '', 'https://en.wikipedia.org/wiki/Moshoeshoe_I_International_Airport', ''),
('0', 'Aeropuerto Internacional de Minatitlán', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Minatit', '94° 34'' 48.762', '18° 5'' 57.842"', 'public', '11', 'MTT', 'MMMT', ' ', '-94.5802116', '18.0994005', '4729', 'MMMT', 'medium_airport', 'Minatitlán/Coatzacoalcos National Airport', '18.1033992767', '-94.5807037354', '36', 'NA', 'MX', 'MX-VER', 'Minatitlán', 'yes', 'MMMT', 'MTT', '', 'http://www.asur.com.mx/asur/ingles/aeropuertos/minatitlan/minatitlan.asp', 'https://en.wikipedia.org/wiki/Minatitl%C3%A1n/Coatzacoalcos_National_Airport', ''),
('0', 'Aeropuerto Internacional General Mariano Escobedo', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Monterr', '100° 6'' 31.668', '25° 46'' 36.936', 'public', '390', 'MTY', 'MMMY', ' ', '-100.1087968', '25.7769268', '4732', 'MMMY', 'large_airport', 'General Mariano Escobedo International Airport', '25.7784996033', '-100.107002258', '1278', 'NA', 'MX', 'MX-NLE', 'Monterrey', 'yes', 'MMMY', 'MTY', '', '', 'https://en.wikipedia.org/wiki/General_Mariano_Escobedo_International_Airport', ''),
('0', 'Munda Airport', 'aerodrome:type=private
aeroway=aerodrome
ele=3
iata=MUA
icao', '157° 15'' 47.05', '8° 19'' 37.048"', 'private', '3', 'MUA', 'AGGM', ' ', '157.2630716', '-8.3269577', '4', 'AGGM', 'medium_airport', 'Munda Airport', '-8.32797', '157.263', '10', 'OC', 'SB', 'SB-WE', 'Munda', 'yes', 'AGGM', 'MUA', '', '', 'https://en.wikipedia.org/wiki/Munda_Airport', ''),
('0', 'Maun Airport', 'aerodrome:type=public
aeroway=aerodrome
city_served=Maun
ele', '23° 25'' 54.534', '19° 58'' 16.921', 'public', '943', 'MUB', 'FBMN', ' ', '23.4318151', '-19.9713669', '2863', 'FBMN', 'medium_airport', 'Maun Airport', '-19.9726009368896', '23.4311008453369', '3093', 'AF', 'BW', 'BW-NW', 'Maun', 'yes', 'FBMN', 'MUB', '', '', 'https://en.wikipedia.org/wiki/Maun_Airport', ''),
('0', 'Waimea-Kohala Airport', 'addr:city=Kamuela
addr:postcode=96743
addr:state=HI
addr:str', '155° 40'' 6.050', '20° 0'' 4.872"', 'public', '813', 'MUE', 'PHMU', ' ', '-155.6683473', '20.0013534', '5451', 'PHMU', 'medium_airport', 'Waimea Kohala Airport', '20.001301', '-155.667999', '2671', 'NA', 'US', 'US-HI', 'Waimea (Kamuela)', 'yes', 'PHMU', 'MUE', 'MUE', '', 'https://en.wikipedia.org/wiki/Waimea-Kohala_Airport', ''),
('0', 'Multan International Airport', 'aerodrome=international
aeroway=aerodrome
barrier=fence
clos', '71° 25'' 13.116', '30° 12'' 19.974', ' ', '122', 'MUX', 'OPMT', ' ', '71.4203099', '30.2055482', '5264', 'OPMT', 'medium_airport', 'Multan International Airport', '30.2031993865967', '71.4190979003906', '403', 'AS', 'PK', 'PK-PB', 'Multan', 'yes', 'OPMT', 'MUX', '', '', 'https://en.wikipedia.org/wiki/Multan_International_Airport', 'Multan Air Base'),
('0', 'Aeropuerto Internacional de Carrasco', 'aerodrome:type=military/public
aeroway=aerodrome
iata=MVD
ic', '56° 1'' 34.301"', '34° 49'' 53.718', 'military/public', '0', 'MVD', 'SUMU', ' ', '-56.0261947', '-34.8315884', '6247', 'SUMU', 'large_airport', 'Carrasco International /General C L Berisso Airport', '-34.838402', '-56.0308', '105', 'SA', 'UY', 'UY-CA', 'Montevideo', 'yes', 'SUMU', 'MVD', '', '', 'https://en.wikipedia.org/wiki/Carrasco_International_Airport', ''),
('0', '무안국제공항 (Muan International Airport)', 'aeroway=aerodrome
alt_name:ko=무안공항
area=yes
iata=MWX', '126° 23'' 9.268', '34° 59'' 29.239', ' ', '0', 'MWX', 'RKJB', ' ', '126.3859078', '34.9914553', '302303', 'RKJB', 'large_airport', 'Muan International Airport', '34.991406', '126.382814', '35', 'AS', 'KR', 'KR-46', 'Piseo-ri (Muan)', 'yes', 'RKJB', 'MWX', '', 'http://muan.airport.co.kr/doc/muan_eng/', 'https://en.wikipedia.org/wiki/Muan_International_Airport', 'Gwangju, Mokpo, Muan, MWX, RKJB'),
('0', 'Moruya Airport', 'aeroway=aerodrome
iata=MYA
icao=YMRY
name=Moruya Airport
sou', '150° 8'' 36.670', '35° 53'' 47.768', ' ', '0', 'MYA', 'YMRY', ' ', '150.1435194', '-35.8966021', '27075', 'YMRY', 'medium_airport', 'Moruya Airport', '-35.8978004456', '150.143997192', '14', 'OC', 'AU', 'AU-NSW', 'Moruya', 'yes', 'YMRY', 'MYA', '', '', 'https://en.wikipedia.org/wiki/Moruya_Airport', 'RAAF Moruya'),
('0', '三宅島空港', 'KSJ2:AAC=13381
KSJ2:AAC_label=東京都三宅島三宅村
KS', '139° 33'' 34.80', '34° 4'' 19.883"', ' ', '0', 'MYE', 'RJTQ', ' ', '139.5596669', '34.0721898', '5625', 'RJTQ', 'medium_airport', 'Miyakejima Airport', '34.073600769', '139.559997559', '67', 'AS', 'JP', 'JP-13', 'Miyakejima', 'yes', 'RJTQ', 'MYE', '', '', 'https://en.wikipedia.org/wiki/Miyakejima_Airport', ''),
('0', 'Mayaguana Airfield', 'aeroway=aerodrome
iata=MYG
icao=MYMM
name=Mayaguana Airfield', '73° 0'' 48.709"', '22° 22'' 45.675', ' ', '0', 'MYG', 'MYMM', ' ', '-73.0135304', '22.3793542', '4950', 'MYMM', 'medium_airport', 'Mayaguana Airport', '22.379499', '-73.013494', '11', 'NA', 'BS', 'BS-MG', 'Abrahams Bay', 'yes', 'MYMM', 'MYG', '', '', 'https://en.wikipedia.org/wiki/Mayaguana_Airport', '');
INSERT INTO "untitled_name" ("Id", "Name", "Descript", "LONG", "LAT", "aerodrome_", "ele", "iata", "icao", "name_ru", "x", "y", "id", "ident", "type", "name", "latitude_deg", "longitude_deg", "elevation_ft", "continent", "iso_country", "iso_region", "municipality", "scheduled_service", "gps_code", "iata_code", "local_code", "home_link", "wikipedia_link", "keywords") VALUES
('0', '松山空港', 'KSJ2:AAC=38201
KSJ2:AAC_label=愛媛県松山市
KSJ2:AD2=1
', '132° 41'' 59.81', '33° 49'' 38.425', ' ', '0', 'MYJ', 'RJOM', ' ', '132.6999473', '33.8273404', '5597', 'RJOM', 'medium_airport', 'Matsuyama Airport', '33.8272018432617', '132.699996948242', '25', 'AS', 'JP', 'JP-38', 'Matsuyama', 'yes', 'RJOM', 'MYJ', '', '', 'https://en.wikipedia.org/wiki/Matsuyama_Airport', ''),
('0', 'မြစ်ကြီးနားလေဆိပ်', 'aeroway=aerodrome
iata=MYT
icao=VYMK
name=မြစ်ကြ', '97° 21'' 12.513', '25° 23'' 1.980"', ' ', '0', 'MYT', 'VYMK', ' ', '97.3534759', '25.3838832', '26729', 'VYMK', 'medium_airport', 'Myitkyina Airport', '25.3836002349853', '97.3518981933594', '475', 'AS', 'MM', 'MM-11', 'Myitkyina', 'yes', 'VYMK', 'MYT', '', '', 'https://en.wikipedia.org/wiki/Mtwara_Airport', ''),
('0', 'Aéroport international de Mopti Ambodédjo', 'aeroway=aerodrome
barrier=wall
ele=276
iata=MZI
icao=GAMB
na', '4° 4'' 43.287"', '14° 30'' 40.519', ' ', '276', 'MZI', 'GAMB', ' ', '-4.0786909', '14.5112554', '3069', 'GAMB', 'medium_airport', 'Mopti Airport', '14.5128002167', '-4.07955980301', '906', 'AF', 'ML', 'ML-5', '', 'yes', 'GAMB', 'MZI', '', '', 'https://en.wikipedia.org/wiki/Mopti_Airport', 'Ambodedjo Airport'),
('0', 'La Nubia', 'aeroway=aerodrome
ele=2012
iata=MZL
icao=SKMZ
is_in=Manizale', '75° 27'' 52.515', '5° 1'' 47.015"', ' ', '2012', 'MZL', 'SKMZ', ' ', '-75.4645875', '5.0297264', '6140', 'SKMZ', 'medium_airport', 'La Nubia Airport', '5.0296', '-75.4647', '6871', 'SA', 'CO', 'CO-CAL', 'Manizales', 'yes', 'SKMZ', 'MZL', 'MZL', '', 'https://en.wikipedia.org/wiki/La_Nubia_Airport', ''),
('0', 'Aeropuerto Internacional Sierra Maestra', 'aeroway=aerodrome
alt_name=Aeropuerto de Manzanillo
iata=MZO', '77° 5'' 21.736"', '20° 17'' 16.228', ' ', '0', 'MZO', 'MUMZ', ' ', '-77.089371', '20.2878412', '4847', 'MUMZ', 'medium_airport', 'Sierra Maestra Airport', '20.2880992889404', '-77.0892028808594', '112', 'NA', 'CU', 'CU-12', 'Manzanillo', 'yes', 'MUMZ', 'MZO', '', '', 'https://en.wikipedia.org/wiki/Sierra_Maestra_Airport', ''),
('0', 'میدان هوائی مزار شریف', 'aeroway=aerodrome
closest_town=Mazari Sharif
ele=391
iata=MZ', '67° 12'' 45.589', '36° 42'' 18.695', ' ', '391', 'MZR', 'OAMS', 'Аэропорт Мазари-Шариф', '67.2126635', '36.7051931', '5075', 'OAMS', 'medium_airport', 'Mazar-i-Sharif International Airport', '36.706902', '67.209702', '1284', 'AS', 'AF', 'AF-BAL', 'Mazar-i-Sharif', 'yes', 'OAMS', 'MZR', '', '', 'https://en.wikipedia.org/wiki/Mazar-i-Sharif_International_Airport', 'Mazari Sharif'),
('0', 'Aeropuerto Internacional General Rafael Buelna', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '106° 16'' 3.774', '23° 9'' 53.016"', 'public', '12', 'MZT', 'MMMZ', ' ', '-106.267715', '23.1647268', '4733', 'MMMZ', 'large_airport', 'General Rafael Buelna International Airport', '23.1614', '-106.265999', '38', 'NA', 'MX', 'MX-SIN', 'Mazatlán', 'yes', 'MMMZ', 'MZT', '', '', 'https://en.wikipedia.org/wiki/General_Rafael_Buelna_International_Airport', ''),
('0', 'Narrabri Aerodrome', 'aeroway=aerodrome
iata=NAA
icao=YNBR
name=Narrabri Aerodrome', '149° 49'' 50.12', '30° 19'' 20.551', ' ', '0', 'NAA', 'YNBR', ' ', '149.8305903', '-30.3223752', '27082', 'YNBR', 'medium_airport', 'Narrabri Airport', '-30.3192005157', '149.82699585', '788', 'OC', 'AU', 'AU-NSW', 'Narrabri', 'yes', 'YNBR', 'NAA', '', 'http://narrabri.cfm.predelegation.com/index.cfm?page_id=1249', 'https://en.wikipedia.org/wiki/Narrabri_Airport', ''),
('0', 'Dr. Babasaheb Ambedkar International Airport', 'aerodrome=international
aeroway=aerodrome
alt_name=Nagpur Ai', '79° 2'' 37.705"', '21° 5'' 41.169"', ' ', '315', 'NAG', 'VANP', ' ', '79.0438069', '21.0947691', '26453', 'VANP', 'medium_airport', 'Dr. Babasaheb Ambedkar International Airport', '21.092199', '79.047203', '1033', 'AS', 'IN', 'IN-MM', 'Nagpur', 'yes', 'VANP', 'NAG', '', 'http://aai.aero/allAirports/nagpur1_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Dr._Babasaheb_Ambedkar_International_Airport', 'Sonegaon Airport, Sonegaon Air Force Station'),
('0', 'Nadi International Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=18
iata=NAN
icao', '177° 26'' 30.29', '17° 45'' 41.934', 'public', '18', 'NAN', 'NFFN', ' ', '177.4417481', '-17.7616484', '4959', 'NFFN', 'medium_airport', 'Nadi International Airport', '-17.7553997039795', '177.442993164063', '59', 'OC', 'FJ', 'FJ-W', 'Nadi', 'yes', 'NFFN', 'NAN', '', '', 'https://en.wikipedia.org/wiki/Nadi_International_Airport', ''),
('0', 'Aeroporto Internacional Governador Aluizio Alves', 'addr:housenumber=3100
addr:postcode=59290-000
addr:street=Av', '35° 22'' 23.280', '5° 45'' 39.930"', ' ', '0', 'NAT', 'SBSG', ' ', '-35.3731332', '-5.7610916', '313262', 'SBSG', 'large_airport', 'São Gonçalo do Amarante - Governador Aluízio Alves International Airport', '-5.769804', '-35.366578', '272', 'SA', 'BR', 'BR-RN', 'Natal', 'yes', 'SBSG', 'NAT', 'RN0001', 'http://www.natal.aero', 'https://en.wikipedia.org/wiki/Greater_Natal_International_Airport', 'Greater Natal International Airport,  Augusto Severo International Airport'),
('0', 'ท่าอากาศยานนราธิวาส', 'aerodrome=domestic
aerodrome:type=military/public
aeroway=ae', '101° 44'' 33.54', '6° 30'' 56.084"', 'military/public', '5', 'NAW', 'VTSC', ' ', '101.7426511', '6.5155789', '26666', 'VTSC', 'medium_airport', 'Narathiwat Airport', '6.51991987228394', '101.74299621582', '16', 'AS', 'TH', 'TH-96', '', 'yes', 'VTSC', 'NAW', '', '', 'https://en.wikipedia.org/wiki/Narathiwat_Airport', ''),
('0', '长白山机场', 'aeroway=aerodrome
amenity=airport
iata=NBS
icao=ZYBS
name=', '127° 36'' 19.30', '42° 4'' 8.670"', ' ', '0', 'NBS', 'ZYBS', 'аэропорт Чанбайшань', '127.6053619', '42.069075', '300544', 'NBS', 'medium_airport', 'Changbaishan Airport', '42.066944', '127.602222', '2874', 'AS', 'CN', 'CN-22', 'Baishan', 'yes', 'ZYBS', 'NBS', '', '', 'https://en.wikipedia.org/wiki/Changbaishan_Airport', ''),
('0', 'مطار نواذيبو الدولي', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '17° 1'' 53.962"', '20° 55'' 48.323', 'public', '0', 'NDB', 'GQPP', ' ', '-17.0316561', '20.9300898', '3138', 'GQPP', 'medium_airport', 'Nouadhibou International Airport', '20.9330997467041', '-17.0300006866455', '24', 'AF', 'MR', 'MR-08', 'Nouadhibou', 'yes', 'GQPP', 'NDB', '', '', 'https://en.wikipedia.org/wiki/Nouadhibou_International_Airport', ''),
('0', 'Rundu Airport', 'aeroway=aerodrome
ele=1106
iata=NDU
icao=FYRU
name=Rundu Air', '19° 43'' 10.090', '17° 57'' 24.780', ' ', '1106', 'NDU', 'FYRU', ' ', '19.7194694', '-17.9568833', '3035', 'FYRU', 'medium_airport', 'Rundu Airport', '-17.956499099731', '19.719400405884', '3627', 'AF', 'NA', 'NA-KE', 'Rundu', 'yes', 'FYRU', 'NDU', '', '', 'http://ourairports.com/airports/FYRU/#lat=-17.956499099731445,lon=19.71940040588379,zoom=13,type=Satellite,airport=FYRU', ''),
('0', 'Vance W. Amory International Airport', 'aeroway=aerodrome
closest_town=Charlestown, Nevis
ele=4
iata', '62° 35'' 22.267', '17° 12'' 18.735', ' ', '4', 'NEV', 'TKPN', ' ', '-62.5895185', '17.2052042', '6387', 'TKPN', 'medium_airport', 'Vance W. Amory International Airport', '17.2056999206543', '-62.589900970459', '14', 'NA', 'KN', 'KN-U-A', 'Charlestown', 'yes', 'TKPN', 'NEV', '', '', 'https://en.wikipedia.org/wiki/Vance_W._Amory_International_Airport', 'Nevis, Bambooshay Airport, Newcastle Airport'),
('0', '中部国際空港', 'KSJ2:AAC=23216
KSJ2:AAC_label=愛知県常滑市
KSJ2:AD2=4
', '136° 48'' 40.84', '34° 51'' 30.886', 'public', '5', 'NGO', 'RJGG', ' ', '136.8113468', '34.8585795', '5576', 'RJGG', 'large_airport', 'Chubu Centrair International Airport', '34.8583984375', '136.804992675781', '15', 'AS', 'JP', 'JP-23', 'Tokoname', 'yes', 'RJGG', 'NGO', '', '', 'https://en.wikipedia.org/wiki/Ch%C5%ABbu_Centrair_International_Airport', ''),
('0', '長崎空港', 'KSJ2:AAC=42205
KSJ2:AAC_label=長崎県大村市
KSJ2:AD2=1
', '129° 54'' 48.63', '32° 55'' 2.453"', 'military/public', '5', 'NGS', 'RJFU', ' ', '129.9135096', '32.917348', '5573', 'RJFU', 'medium_airport', 'Nagasaki Airport', '32.9169006348', '129.914001465', '15', 'AS', 'JP', 'JP-42', 'Nagasaki', 'yes', 'RJFU', 'NGS', '', '', 'https://en.wikipedia.org/wiki/Nagasaki_Airport', ''),
('0', 'Nuku Ataha', 'aeroway=aerodrome
iata=NHV
icao=NTMD
name=Nuku Ataha', '140° 13'' 43.89', '8° 47'' 44.489"', ' ', '0', 'NHV', 'NTMD', ' ', '-140.2288603', '-8.7956914', '5001', 'NTMD', 'medium_airport', 'Nuku Hiva Airport', '-8.79559993743897', '-140.22900390625', '220', 'OC', 'PF', 'PF-U-A', '', 'yes', 'NTMD', 'NHV', '', '', 'https://en.wikipedia.org/wiki/Nuku_Hiva_Airport', ''),
('0', 'مطار نواكشوط الدولي', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '15° 56'' 47.973', '18° 5'' 45.591"', 'public', '2', 'NKC', 'GQNN', ' ', '-15.9466592', '18.0959975', '323315', 'GQNO', 'large_airport', 'Nouakchott–Oumtounsy International Airport', '18.31', '-15.9697222', '9', 'AF', 'MR', 'MR-NKC', 'Nouakchott', 'yes', 'GQNO', 'NKC', '', '', 'https://en.wikipedia.org/wiki/Nouakchott%E2%80%93Oumtounsy_International_Airport', 'Nouakchott,Oumtounsy'),
('0', '南京禄口国际机场', 'aerodrome:type=international
aeroway=aerodrome
closest_town=', '118° 51'' 48.31', '31° 43'' 56.905', 'international', '15', 'NKG', 'ZSNJ', ' ', '118.8634206', '31.7324737', '27221', 'ZSNJ', 'large_airport', 'Nanjing Lukou Airport', '31.742000579834', '118.861999511719', '49', 'AS', 'CN', 'CN-32', 'Nanjing', 'yes', 'ZSNJ', 'NKG', '', '', 'https://en.wikipedia.org/wiki/Nanjing_Lukou_International_Airport', ''),
('0', 'Simon Mwansa Kapwepwe International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Ndola
e', '28° 40'' 1.420"', '12° 59'' 48.344', 'public', '1270', 'NLA', 'FLND', ' ', '28.667061', '-12.9967623', '2897', 'FLND', 'medium_airport', 'Simon Mwansa Kapwepwe International Airport', '-12.998100280762', '28.66489982605', '4167', 'AF', 'ZM', 'ZM-08', 'Ndola', 'yes', 'FLSK', 'NLA', '', '', 'https://en.wikipedia.org/wiki/Ndola_Airport', 'FLND, Ndola Airport'),
('0', 'Aeropuerto Intl. Quetzalcóatl', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Nuevo L', '99° 34'' 10.940', '27° 26'' 38.760', 'public', '148', 'NLD', 'MMNL', ' ', '-99.5697056', '27.4441', '4735', 'MMNL', 'medium_airport', 'Quetzalcóatl International Airport', '27.4438991547', '-99.5705032349', '484', 'NA', 'MX', 'MX-TAM', 'Nuevo Laredo', 'yes', 'MMNL', 'NLD', '', '', 'https://en.wikipedia.org/wiki/Quetzalc%C3%B3atl_International_Airport', ''),
('0', 'Norfolk Island Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Norfolk', '167° 56'' 17.53', '29° 2'' 28.995"', 'public', '113', 'NLK', 'YSNF', ' ', '167.9382039', '-29.0413876', '27141', 'YSNF', 'medium_airport', 'Norfolk Island International Airport', '-29.041887', '167.939616', '371', 'OC', 'NF', 'NF-U-A', 'Burnt Pine', 'yes', 'YSNF', 'NLK', '', 'http://www.airport.gov.nf/', 'https://en.wikipedia.org/wiki/Norfolk_Island_Airport', ''),
('0', 'Aéroport de Nouméa - La Tontouta', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Nouméa', '166° 12'' 50.17', '22° 0'' 53.634"', 'public', '16', 'NOU', 'NWWW', ' ', '166.2139373', '-22.0148982', '5021', 'NWWW', 'medium_airport', 'La Tontouta International Airport', '-22.0146007537842', '166.212997436523', '52', 'OC', 'NC', 'NC-U-A', 'Nouméa', 'yes', 'NWWW', 'NOU', '', 'http://www.cci-nc.com/tontouta/index.php', 'https://en.wikipedia.org/wiki/La_Tontouta_International_Airport', 'Aéroport de Nouméa - La Tontouta'),
('0', 'Aeroporto Albano Machado', 'aeroway=aerodrome
alt_name=Aeroporto Nova Lisboa
alt_name:2=', '15° 45'' 5.873"', '12° 48'' 18.246', ' ', '0', 'NOV', 'FNHU', ' ', '15.7516315', '-12.8050683', '2939', 'FNHU', 'medium_airport', 'Nova Lisboa Airport', '-12.8088998794556', '15.7604999542236', '5587', 'AF', 'AO', 'AO-HUA', 'Huambo', 'yes', 'FNHU', 'NOV', '', '', 'https://en.wikipedia.org/wiki/Nova_Lisboa_Airport', 'Nova Lisbo Airport'),
('0', 'Hawke''s Bay Airport', 'aeroway=aerodrome
iata=NPE
icao=NZNR
name=Hawke''s Bay Airpor', '176° 51'' 54.56', '39° 28'' 9.696"', ' ', '0', 'NPE', 'NZNR', ' ', '176.8651563', '-39.4693601', '5043', 'NZNR', 'medium_airport', 'Hawke''s Bay Airport', '-39.465801', '176.869995', '6', 'OC', 'NZ', 'NZ-HKB', 'Napier', 'yes', 'NZNR', 'NPE', '', 'http://www.hawkesbay-airport.co.nz/', 'https://en.wikipedia.org/wiki/Napier_Airport', ''),
('0', 'New Plymouth Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '174° 10'' 40.69', '39° 0'' 21.279"', ' ', '0', 'NPL', 'NZNP', ' ', '174.1779704', '-39.0059107', '5042', 'NZNP', 'medium_airport', 'New Plymouth Airport', '-39.0085983276367', '174.179000854492', '97', 'OC', 'NZ', 'NZ-TKI', 'New Plymouth', 'yes', 'NZNP', 'NPL', '', '', 'https://en.wikipedia.org/wiki/New_Plymouth_Airport', ''),
('0', 'Aeropuerto Presidente Juan Domingo Perón', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Ae', '68° 9'' 15.989"', '38° 57'' 4.785"', 'military/public', '293', 'NQN', 'SAZN', ' ', '-68.1544415', '-38.9513291', '5848', 'SAZN', 'medium_airport', 'Presidente Peron Airport', '-38.949001', '-68.155701', '895', 'SA', 'AR', 'AR-Q', 'Neuquen', 'yes', 'SAZN', 'NQN', 'NEU', '', 'https://en.wikipedia.org/wiki/Presidente_Per%C3%B3n_International_Airport', ''),
('0', 'Narrandera-Leeton Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Narrand', '146° 30'' 42.46', '34° 42'' 14.864', 'public', '144', 'NRA', 'YNAR', ' ', '146.5117954', '-34.7041288', '27081', 'YNAR', 'medium_airport', 'Narrandera Airport', '-34.7022018433', '146.511993408', '474', 'OC', 'AU', 'AU-NSW', 'Narrandera', 'yes', 'YNAR', 'NRA', '', '', 'https://en.wikipedia.org/wiki/Narrandera_Airport', ''),
('0', '成田国際空港', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '140° 22'' 57.77', '35° 46'' 33.210', 'public', '43', 'NRT', 'RJAA', 'Международный аэропорт Нарит', '140.3827142', '35.7758917', '5531', 'RJAA', 'large_airport', 'Narita International Airport', '35.764702', '140.386002', '141', 'AS', 'JP', 'JP-12', 'Tokyo', 'yes', 'RJAA', 'NRT', '', '', 'https://en.wikipedia.org/wiki/Narita_International_Airport', 'TYO, Tokyo Narita Airport, New Tokyo International Airport'),
('0', 'Aéroport International de Yaoundé-Nsimalen', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '11° 33'' 4.798"', '3° 43'' 22.622"', 'public', '694', 'NSI', 'FKYS', ' ', '11.5513328', '3.7229506', '2890', 'FKYS', 'medium_airport', 'Yaoundé Nsimalen International Airport', '3.72255992889404', '11.5532999038696', '2278', 'AF', 'CM', 'CM-CE', 'Yaoundé', 'yes', 'FKYS', 'NSI', '', '', 'https://en.wikipedia.org/wiki/Yaound%C3%A9_Nsimalen_International_Airport', ''),
('0', 'Nelson Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '173° 13'' 26.77', '41° 17'' 48.873', 'public', '5', 'NSN', 'NZNS', ' ', '173.224103', '-41.2969093', '5044', 'NZNS', 'medium_airport', 'Nelson Airport', '-41.2983016967773', '173.220993041992', '17', 'OC', 'NZ', 'NZ-NSN', 'Nelson', 'yes', 'NZNS', 'NSN', '', 'http://www.nelsonairport.co.nz/', 'https://en.wikipedia.org/wiki/Nelson_Airport_(New_Zealand)', ''),
('0', 'Newcastle Airport', 'addr:city=Williamtown
addr:housenumber=55
addr:postcode=2318', '151° 50'' 25.18', '32° 48'' 22.115', 'public', '9', 'NTL', 'YWLM', ' ', '151.8403292', '-32.8061431', '27175', 'YWLM', 'medium_airport', 'Newcastle Airport', '-32.7949981689453', '151.833999633789', '31', 'OC', 'AU', 'AU-NSW', 'Williamtown', 'yes', 'YWLM', 'NTL', '', '', 'https://en.wikipedia.org/wiki/Newcastle_Airport_(Williamtown)', 'RAAF Base Williamtown'),
('0', 'Normanton Aerodrome', 'aeroway=aerodrome
iata=NTN
icao=YNTN
name=Normanton Aerodrom', '141° 4'' 18.300', '17° 41'' 14.243', ' ', '0', 'NTN', 'YNTN', ' ', '141.07175', '-17.6872898', '27088', 'YNTN', 'medium_airport', 'Normanton Airport', '-17.68409', '141.069664', '73', 'OC', 'AU', 'AU-QLD', 'Normanton', 'yes', 'YNTN', 'NTN', '', '', 'https://en.wikipedia.org/wiki/Normanton_Airport', ''),
('0', 'のと里山空港', 'KSJ2:AAC=17204
KSJ2:AAC_label=石川県輪島市
KSJ2:AD2=5
', '136° 57'' 53.66', '37° 17'' 37.657', ' ', '0', 'NTQ', 'RJNW', ' ', '136.9649073', '37.2937936', '5588', 'RJNW', 'medium_airport', 'Noto Satoyama Airport', '37.293098', '136.962006', '718', 'AS', 'JP', 'JP-17', 'Wajima', 'yes', 'RJNW', 'NTQ', '', '', 'https://en.wikipedia.org/wiki/Noto_Airport', 'Noto, Wajima'),
('0', 'Aeroporto Internacional de Navegantes Ministro Victor Konder', 'addr:housenumber=1297
addr:postcode=88375-000
addr:street=Ru', '48° 39'' 1.177"', '26° 52'' 44.030', ' ', '0', 'NVT', 'SBNF', ' ', '-48.650327', '-26.8788971', '5947', 'SBNF', 'medium_airport', 'Ministro Victor Konder International Airport', '-26.879999', '-48.651402', '18', 'SA', 'BR', 'BR-SC', 'Navegantes', 'yes', 'SBNF', 'NVT', '', '', 'https://en.wikipedia.org/wiki/Navegantes_Airport', ''),
('0', 'Sunyani Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=309
iata=NYI
ica', '2° 19'' 43.548"', '7° 21'' 42.512"', 'public', '309', 'NYI', 'DGSN', ' ', '-2.3287634', '7.3618089', '2094', 'DGSN', 'medium_airport', 'Sunyani Airport', '7.36183023452759', '-2.32875990867615', '1014', 'AF', 'GH', 'GH-BA', 'Sunyani', 'yes', 'DGSN', 'NYI', '', '', 'https://en.wikipedia.org/wiki/Sunyani_Airport', ''),
('0', 'Metropolitan Oakland International Airport', 'addr:state=CA
aerodrome=international
aeroway=aerodrome
ele=', '122° 13'' 31.22', '37° 43'' 20.454', ' ', '2', 'OAK', 'KOAK', ' ', '-122.2253404', '37.7223484', '3744', 'KOAK', 'large_airport', 'Metropolitan Oakland International Airport', '37.721298', '-122.221001', '9', 'NA', 'US', 'US-CA', 'Oakland', 'yes', 'KOAK', 'OAK', 'OAK', 'http://www.oaklandairport.com/', 'https://en.wikipedia.org/wiki/Oakland_International_Airport', 'QSF, QBA'),
('0', 'Oamaru Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '171° 5'' 5.360"', '44° 58'' 5.632"', 'public', '30', 'OAM', 'NZOU', ' ', '171.0848223', '-44.968231', '5047', 'NZOU', 'medium_airport', 'Oamaru Airport', '-44.9700012207031', '171.082000732422', '99', 'OC', 'NZ', 'NZ-OTA', '', 'yes', 'NZOU', 'OAM', '', '', 'https://en.wikipedia.org/wiki/Oamaru_Aerodrome', ''),
('0', 'Aeropuerto Internacional Xoxocotlán', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '96° 43'' 32.230', '16° 59'' 57.484', 'public', '1521', 'OAX', 'MMOX', ' ', '-96.7256194', '16.9993012', '4736', 'MMOX', 'medium_airport', 'Xoxocotlán International Airport', '16.9999008179', '-96.726600647', '4989', 'NA', 'MX', 'MX-OAX', 'Oaxaca', 'yes', 'MMOX', 'OAX', '', 'http://www.asur.com.mx/asur/ingles/aeropuertos/oaxaca/oaxaca.asp', 'https://en.wikipedia.org/wiki/Xoxocotl%C3%A1n_International_Airport', ''),
('0', 'とかち帯広空港', 'KSJ2:AAC=01207
KSJ2:AAC_label=北海道帯広市
KSJ2:AD2=6
', '143° 13'' 1.801', '42° 43'' 57.452', ' ', '0', 'OBO', 'RJCB', ' ', '143.217167', '42.7326255', '5543', 'RJCB', 'medium_airport', 'Tokachi-Obihiro Airport', '42.7332992554', '143.216995239', '505', 'AS', 'JP', 'JP-01', 'Obihiro', 'yes', 'RJCB', 'OBO', '', '', 'https://en.wikipedia.org/wiki/Obihiro_Airport', ''),
('0', 'Ian Fleming International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '76° 58'' 11.408', '18° 24'' 15.827', 'public', '27', 'OCJ', 'MKBS', ' ', '-76.9698355', '18.4043963', '4679', 'MKBS', 'medium_airport', 'Ian Fleming International Airport', '18.404079', '-76.969754', '90', 'NA', 'JM', 'JM-05', 'Boscobel', 'yes', 'MKBS', 'OCJ', '', 'http://www.aaj.com.jm/ourairports/index.php#boscobel', 'https://en.wikipedia.org/wiki/Ian_Fleming_International_Airport', 'Boscobel Aerodrome'),
('0', 'Kahului Airport', 'addr:state=HI
aerodrome:type=public
aeroway=aerodrome
ele=13', '156° 25'' 56.52', '20° 53'' 43.087', 'public', '13', 'OGG', 'PHOG', ' ', '-156.4323667', '20.895302', '5455', 'PHOG', 'medium_airport', 'Kahului Airport', '20.8986', '-156.429993', '54', 'NA', 'US', 'US-HI', 'Kahului', 'yes', 'PHOG', 'OGG', 'OGG', '', 'https://en.wikipedia.org/wiki/Kahului_Airport', ''),
('0', 'Eugene F. Correira International Airport', 'aeroway=aerodrome
ele=3
iata=OGL
icao=SYGO
name=Eugene F. Co', '58° 6'' 25.293"', '6° 48'' 27.550"', ' ', '3', 'OGL', 'SYGO', ' ', '-58.1070258', '6.8076527', '41513', 'SYGO', 'medium_airport', 'Eugene F. Correira International Airport', '6.80628', '-58.1059', '10', 'SA', 'GY', 'GY-DE', 'Ogle', 'yes', 'SYEC', 'OGL', '', '', 'https://en.wikipedia.org/wiki/Ogle_Airport', ''),
('0', '与那国空港', 'KSJ2:AAC=47382
KSJ2:AAC_label=沖縄県八重山郡与那国', '122° 58'' 45.71', '24° 28'' 2.295"', 'public', '21', 'OGN', 'ROYN', ' ', '122.9793643', '24.4673042', '5684', 'ROYN', 'medium_airport', 'Yonaguni Airport', '24.467298', '122.979827', '70', 'AS', 'JP', 'JP-47', 'Yonaguni', 'yes', 'ROYN', 'OGN', '', '', 'https://en.wikipedia.org/wiki/Yonaguni_Airport', ''),
('0', '丘珠空港', 'KSJ2:AAC=01103
KSJ2:AAC_label=北海道札幌市東区
KSJ2:', '141° 22'' 53.26', '43° 6'' 58.915"', ' ', '0', 'OKD', 'RJCO', 'Аэропорт Окадама', '141.3814625', '43.1163654', '5550', 'RJCO', 'medium_airport', 'Sapporo Okadama Airport', '43.117447', '141.38134', '25', 'AS', 'JP', 'JP-01', 'Sapporo', 'yes', 'RJCO', 'OKD', '', '', 'https://en.wikipedia.org/wiki/Okadama_Airport', 'RJCO, OKD, Sapporo, Okadama'),
('0', '隠岐空港', 'KSJ2:AAC=32528
KSJ2:AAC_label=島根県, *
KSJ2:AD2=5
KSJ2:A', '133° 19'' 23.67', '36° 10'' 43.214', ' ', '0', 'OKI', 'RJNO', ' ', '133.3232425', '36.1786706', '5586', 'RJNO', 'medium_airport', 'Oki Global Geopark Airport', '36.178388', '133.323566', '311', 'AS', 'JP', 'JP-32', 'Okinoshima', 'yes', 'RJNO', 'OKI', '', '', 'https://en.wikipedia.org/wiki/Oki_Airport', 'oki, okinoshima, geopark, global, sekai'),
('0', '岡山空港', 'KSJ2:AAC=33201
KSJ2:AD2=5
KSJ2:AD2_label=都道府県
KSJ2:A', '133° 51'' 19.94', '34° 45'' 25.711', 'public', '246', 'OKJ', 'RJOB', ' ', '133.8555389', '34.7571419', '5591', 'RJOB', 'medium_airport', 'Okayama Momotaro Airport', '34.756901', '133.854996', '806', 'AS', 'JP', 'JP-33', 'Okayama', 'yes', 'RJOB', 'OKJ', '', '', 'https://en.wikipedia.org/wiki/Okayama_Airport', 'okayama'),
('0', 'Oakey Army Aviation Centre', 'aeroway=aerodrome
boundary=fence
ele=437
iata=OKY
icao=YBOK
', '151° 44'' 15.10', '27° 24'' 33.830', ' ', '437', 'OKY', 'YBOK', ' ', '151.7375277', '-27.4093972', '26922', 'YBOK', 'medium_airport', 'Oakey Airport', '-27.4113998413086', '151.735000610352', '1335', 'OC', 'AU', 'AU-QLD', '', 'yes', 'YBOK', 'OKY', '', '', '', ''),
('0', '大館能代空港', 'KSJ2:AAC=05213
KSJ2:AAC_label=秋田県, *
KSJ2:AD2=5
KSJ2:A', '140° 22'' 18.97', '40° 11'' 36.490', ' ', '0', 'ONJ', 'RJSR', ' ', '140.3719365', '40.1934695', '5614', 'RJSR', 'medium_airport', 'Odate Noshiro Airport', '40.191898', '140.371002', '292', 'AS', 'JP', 'JP-05', 'Kitaakita', 'yes', 'RJSR', 'ONJ', '', '', 'https://en.wikipedia.org/wiki/Odate-Noshiro_Airport', ''),
('0', 'Gold Coast Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=6
iata=OOL
icao=', '153° 30'' 32.49', '28° 9'' 54.423"', 'public', '6', 'OOL', 'YBCG', ' ', '153.5090268', '-28.1651175', '26902', 'YBCG', 'medium_airport', 'Gold Coast Airport', '-28.1644001007', '153.505004883', '21', 'OC', 'AU', 'AU-QLD', 'Gold Coast', 'yes', 'YBCG', 'OOL', '', '', 'https://en.wikipedia.org/wiki/Gold_Coast_Airport', ''),
('0', 'Cooma - Snowy Mountains Aerodrome', 'access=customers
aeroway=aerodrome
ele=947
iata=OOM
icao=YCO', '148° 58'' 23.75', '36° 17'' 38.798', ' ', '947', 'OOM', 'YCOM', ' ', '148.9732651', '-36.2941106', '26962', 'YCOM', 'medium_airport', 'Cooma Snowy Mountains Airport', '-36.3005981445', '148.973999023', '3088', 'OC', 'AU', 'AU-NSW', 'Cooma', 'yes', 'YCOM', 'OOM', '', '', 'https://en.wikipedia.org/wiki/Cooma_-_Snowy_Mountains_Airport', ''),
('0', 'Международный аэропорт Ош', 'aeroway=aerodrome
iata=OSS
icao=UAFO
name=Междунаро', '72° 47'' 40.877', '40° 36'' 29.429', ' ', '0', 'OSS', 'UAFO', 'Международный аэропорт "Ош"', '72.794688', '40.6081748', '6427', 'UAFO', 'medium_airport', 'Osh Airport', '40.6090011597', '72.793296814', '2927', 'AS', 'KG', 'KG-O', 'Osh', 'yes', 'UCFO', 'OSS', '', 'http://www.airport.kg/eng/branches.htm', 'https://en.wikipedia.org/wiki/Osh_Airport', ''),
('0', 'Southwest Oregon Regional Airport', 'addr:state=OR
aeroway=aerodrome
closest_town=North Bend, Ore', '124° 15'' 1.081', '43° 24'' 55.830', ' ', '5', 'OTH', 'KOTH', ' ', '-124.2503002', '43.4155084', '3759', 'KOTH', 'medium_airport', 'Southwest Oregon Regional Airport', '43.4170989990234', '-124.246002197266', '17', 'NA', 'US', 'US-OR', 'North Bend', 'yes', 'KOTH', 'OTH', 'OTH', '', 'https://en.wikipedia.org/wiki/Southwest_Oregon_Regional_Airport', ''),
('0', 'Aéroport international de Ouagadougou', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Ou', '1° 30'' 48.726"', '12° 21'' 0.558"', 'military/public', '316', 'OUA', 'DFFD', ' ', '-1.5135349', '12.3501549', '2088', 'DFFD', 'large_airport', 'Ouagadougou Airport', '12.3532', '-1.51242', '1037', 'AF', 'BF', 'BF-KAD', 'Ouagadougou', 'yes', 'DFFD', 'OUA', '', '', 'https://en.wikipedia.org/wiki/Ouagadougou_Airport', ''),
('0', 'Aeroporto internacional Osvaldo Vieira', 'aerodrome:type=public
aeroway=aerodrome
area=yes
ele=39
iata', '15° 39'' 10.015', '11° 53'' 46.457', 'public', '39', 'OXB', 'GGOV', ' ', '-15.6527819', '11.8962381', '3097', 'GGOV', 'medium_airport', 'Osvaldo Vieira International Airport', '11.8948001861572', '-15.6536998748779', '129', 'AF', 'GW', 'GW-BS', 'Bissau', 'yes', 'GGOV', 'OXB', '', '', 'https://en.wikipedia.org/wiki/Osvaldo_Vieira_International_Airport', ''),
('0', 'Bilaspur', 'aeroway=aerodrome
iata=PAB
icao=VEBU
name=Bilaspur
name:ja=', '82° 6'' 47.087"', '21° 59'' 21.790', ' ', '0', 'PAB', 'VEBU', ' ', '82.1130797', '21.9893862', '26435', 'VABI', 'medium_airport', 'Bilaspur Airport', '21.9884', '82.111', '899', 'AS', 'IN', 'IN-CT', 'Bilaspur', 'yes', 'VEBU', 'PAB', '', '', 'https://en.wikipedia.org/wiki/Bilaspur_Airport', 'VABI'),
('0', 'Aeropuerto Internacional de Albrook "Marcos A. Gelabert"', 'aeroway=aerodrome
alt_name=Marcos A. Gelabert International', '79° 33'' 25.605', '8° 58'' 24.339"', ' ', '0', 'PAC', 'MPMG', ' ', '-79.5571126', '8.9734276', '4791', 'MPMG', 'medium_airport', 'Marcos A. Gelabert International Airport', '8.97334003448486', '-79.5556030273437', '31', 'NA', 'PA', 'PA-8', 'Albrook', 'yes', 'MPMG', 'PAC', '', '', '', 'Balboa. Albrook AFS. MPLB'),
('0', 'Aéroport International Toussaint Louverture', 'addr:country=HT
aeroway=aerodrome
closest_town=Port-au-Princ', '72° 17'' 35.605', '18° 34'' 43.874', ' ', '37', 'PAP', 'MTPP', ' ', '-72.2932235', '18.578854', '4825', 'MTPP', 'medium_airport', 'Toussaint Louverture International Airport', '18.5799999237061', '-72.2925033569336', '122', 'NA', 'HT', 'HT-OU', 'Port-au-Prince', 'yes', 'MTPP', 'PAP', '', '', 'https://en.wikipedia.org/wiki/Toussaint_Louverture_International_Airport', ''),
('0', 'Jaiprakash Narayan Airport', 'aeroway=aerodrome
iata=PAT
icao=VEPT
name=Jaiprakash Narayan', '85° 5'' 15.408"', '25° 35'' 30.832', ' ', '0', 'PAT', 'VEPT', ' ', '85.0876134', '25.5918977', '26517', 'VEPT', 'medium_airport', 'Lok Nayak Jayaprakash Airport', '25.591299057', '85.0879974365', '170', 'AS', 'IN', 'IN-BR', 'Patna', 'yes', 'VEPT', 'PAT', '', 'http://airportsindia.org.in/allAirports/patna_techinfo.jsp', 'https://en.wikipedia.org/wiki/Lok_Nayak_Jayaprakash_Airport', ''),
('0', 'Aeroporto de Paulo Afonso', 'aeroway=aerodrome
alt_name=Aeroporto Paulo Afonso
iata=PAV
i', '38° 15'' 2.038"', '9° 24'' 2.879"', ' ', '0', 'PAV', 'SBUF', ' ', '-38.2505661', '-9.4007996', '5991', 'SBUF', 'medium_airport', 'Paulo Afonso Airport', '-9.4008798599243', '-38.250598907471', '883', 'SA', 'BR', 'BR-BA', 'Paulo Afonso', 'yes', 'SBUF', 'PAV', '', '', 'https://en.wikipedia.org/wiki/Paulo_Afonso_Airport', ''),
('0', 'Aeropuerto Nacional El Tajín', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '97° 27'' 45.206', '20° 35'' 57.278', 'public', '151', 'PAZ', 'MMPA', ' ', '-97.4625572', '20.5992439', '4737', 'MMPA', 'medium_airport', 'El Tajín National Airport', '20.6026992798', '-97.4608001709', '497', 'NA', 'MX', 'MX-VER', 'Poza Rica', 'yes', 'MMPA', 'PAZ', '', '', 'https://en.wikipedia.org/wiki/El_Taj%C3%ADn_National_Airport', ''),
('0', 'Aeropuerto Internacional de Puebla', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Puebla,', '98° 22'' 21.499', '19° 9'' 19.450"', 'public', '2244', 'PBC', 'MMPB', ' ', '-98.3726386', '19.1554029', '4738', 'MMPB', 'medium_airport', 'Hermanos Serdán International Airport', '19.1581001282', '-98.3713989258', '7361', 'NA', 'MX', 'MX-PUE', 'Puebla', 'yes', 'MMPB', 'PBC', '', 'http://www.aeropuertopuebla.com/', 'https://en.wikipedia.org/wiki/Hermanos_Serd%C3%A1n_International_Airport', ''),
('0', 'Palm Beach International Airport', 'addr:state=FL
aeroway=aerodrome
ele=4
gnis:county_name=Palm', '80° 5'' 34.438"', '26° 41'' 8.980"', ' ', '4', 'PBI', 'KPBI', ' ', '-80.0928994', '26.6858278', '3766', 'KPBI', 'large_airport', 'Palm Beach International Airport', '26.6832008361816', '-80.0955963134766', '19', 'NA', 'US', 'US-FL', 'West Palm Beach', 'yes', 'KPBI', 'PBI', 'PBI', '', 'https://en.wikipedia.org/wiki/Palm_Beach_International_Airport', 'MFW, South Florida'),
('0', 'Paraburdoo Airport', 'aerodrome=continental
aerodrome:type=public
aeroway=aerodrom', '117° 44'' 42.94', '23° 10'' 19.535', 'public', '429', 'PBO', 'YPBO', ' ', '117.7452616', '-23.172093', '27099', 'YPBO', 'medium_airport', 'Paraburdoo Airport', '-23.1711006165', '117.745002747', '1406', 'OC', 'AU', 'AU-WA', 'Paraburdoo', 'yes', 'YPBO', 'PBO', '', '', 'https://en.wikipedia.org/wiki/Paraburdoo_Airport', ''),
('0', 'Plettenberg Bay Airport', 'aeroway=aerodrome
iata=PBZ
icao=FAPG
ifr=yes
licensed:sacaa=', '23° 19'' 44.332', '34° 5'' 16.903"', ' ', '0', 'PBZ', 'FAPG', ' ', '23.3289811', '-34.0880286', '2821', 'FAPG', 'medium_airport', 'Plettenberg Bay Airport', '-34.08816', '23.328723', '465', 'AF', 'ZA', 'ZA-WC', 'Plettenberg Bay', 'yes', 'FAPG', 'PBZ', '', '', '', ''),
('0', 'Aeropuerto Internacional Capitán FAP David Abenzur Rengifo', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Pucallp', '74° 34'' 26.832', '8° 22'' 39.554"', 'public', '156', 'PCL', 'SPCL', ' ', '-74.57412', '-8.3776538', '6205', 'SPCL', 'medium_airport', 'Cap FAP David Abenzur Rengifo International Airport', '-8.37794017791748', '-74.5743026733399', '513', 'SA', 'PE', 'PE-UCA', 'Pucallpa', 'yes', 'SPCL', 'PCL', '', '', 'https://en.wikipedia.org/wiki/Captain_Rolden_International_Airport', ''),
('0', 'Príncipe Airport', 'aeroway=aerodrome
fixme=Location not exact
iata=PCP
icao=FPP', '7° 24'' 42.200"', '1° 39'' 42.559"', ' ', '0', 'PCP', 'FPPR', ' ', '7.4117221', '1.661822', '2968', 'FPPR', 'medium_airport', 'Principe Airport', '1.66294', '7.41174', '591', 'AF', 'ST', 'ST-P', 'São Tomé & Príncipe', 'yes', 'FPPR', 'PCP', '', '', 'https://en.wikipedia.org/wiki/Pr%C3%ADncipe_Airport', ''),
('0', 'Germán Olano', 'aeroway=aerodrome
ele=55
iata=PCR
icao=SKPC
is_in:city=Puert', '67° 29'' 33.844', '6° 11'' 6.069"', ' ', '55', 'PCR', 'SKPC', ' ', '-67.4927344', '6.1850192', '6146', 'SKPC', 'medium_airport', 'German Olano Airport', '6.18472', '-67.4932', '177', 'SA', 'CO', 'CO-VID', 'Puerto Carreño', 'yes', 'SKPC', 'PCR', 'PCR', '', 'https://en.wikipedia.org/wiki/Germ%C3%A1n_Olano_Airport', 'Puerto Carreño Airport'),
('0', 'Minangkabau Airport', 'aeroway=aerodrome
iata=PDG
icao=WIPT
name=Minangkabau Airpor', '100° 16'' 53.96', '0° 47'' 19.442"', ' ', '0', 'PDG', 'WIPT', ' ', '100.2816575', '-0.7887338', '26858', 'WIPT', 'medium_airport', 'Minangkabau International Airport', '-0.786917', '100.280998', '18', 'AS', 'ID', 'ID-SB', 'Ketaping/Padang - Sumatra Island', 'yes', 'WIEE', 'PDG', '', 'http://minangkabau-airport.co.id/en/general/about-us', 'https://en.wikipedia.org/wiki/Minangkabau_International_Airport', ''),
('0', 'Portland International Airport', 'aerodrome=international
aeroway=aerodrome
alt_name=PDX
conta', '122° 35'' 50.38', '45° 35'' 13.966', ' ', '0', 'PDX', 'KPDX', ' ', '-122.5973279', '45.5872128', '3768', 'KPDX', 'large_airport', 'Portland International Airport', '45.58869934', '-122.5979996', '31', 'NA', 'US', 'US-OR', 'Portland', 'yes', 'KPDX', 'PDX', 'PDX', '', 'https://en.wikipedia.org/wiki/Portland_International_Airport', ''),
('0', 'Aeropuerto Internacional Matecaña', 'aeroway=aerodrome
closest_town=Pereira, Colombia
ele=1346
ia', '75° 44'' 23.716', '4° 48'' 44.678"', ' ', '1346', 'PEI', 'SKPE', ' ', '-75.739921', '4.8124106', '6148', 'SKPE', 'medium_airport', 'Matecaña International Airport', '4.81267', '-75.7395', '4416', 'SA', 'CO', 'CO-RIS', 'Pereira', 'yes', 'SKPE', 'PEI', 'PEI', 'http://aeromate.gov.co', 'https://en.wikipedia.org/wiki/Mateca%C3%B1a_International_Airport', ''),
('0', 'Aeroporto Internacional João Simões Lopes Neto', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '52° 19'' 36.267', '31° 43'' 10.299', 'public', '18', 'PET', 'SBPK', ' ', '-52.3267408', '-31.7195275', '5956', 'SBPK', 'medium_airport', 'João Simões Lopes Neto International Airport', '-31.718399', '-52.327702', '59', 'SA', 'BR', 'BR-RS', 'Pelotas', 'yes', 'SBPK', 'PET', '', '', 'https://en.wikipedia.org/wiki/Pelotas_International_Airport', 'Pelotas Airport'),
('0', 'Peshawar International Airport', 'aerodrome=international
aeroway=aerodrome
barrier=wall
ele=3', '71° 30'' 43.567', '33° 59'' 27.716', ' ', '353', 'PEW', 'OPPS', ' ', '71.5121019', '33.9910323', '5270', 'OPPS', 'medium_airport', 'Peshawar International Airport', '33.9939002990723', '71.5146026611328', '1158', 'AS', 'PK', 'PK-KP', 'Peshawar', 'yes', 'OPPS', 'PEW', '', '', 'https://en.wikipedia.org/wiki/Peshawar_International_Airport', ''),
('0', 'Aeroporto Lauro Kurtz', 'aerodrome=regional
aeroway=aerodrome
alt_name=Aeroporto de P', '52° 19'' 45.044', '28° 14'' 41.013', ' ', '724', 'PFB', 'SBPF', ' ', '-52.3291788', '-28.2447258', '5954', 'SBPF', 'medium_airport', 'Lauro Kurtz Airport', '-28.243999', '-52.326599', '2376', 'SA', 'BR', 'BR-RS', 'Passo Fundo', 'yes', 'SBPF', 'PFB', '', '', 'https://en.wikipedia.org/wiki/Lauro_Kurtz_Airport', ''),
('0', 'Port Hedland International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '118° 37'' 37.25', '20° 22'' 35.036', 'public', '10', 'PHE', 'YPPD', ' ', '118.627016', '-20.3763989', '27117', 'YPPD', 'medium_airport', 'Port Hedland International Airport', '-20.3777999878', '118.625999451', '33', 'OC', 'AU', 'AU-WA', 'Port Hedland', 'yes', 'YPPD', 'PHE', '', '', 'https://en.wikipedia.org/wiki/Port_Hedland_International_Airport', ''),
('0', 'St. Petersburg-Clearwater International Airport', 'addr:city=Clearwater
addr:country=US
addr:postcode=33762
add', '82° 41'' 17.236', '27° 54'' 36.559', ' ', '2', 'PIE', 'KPIE', ' ', '-82.6881211', '27.9101552', '3775', 'KPIE', 'medium_airport', 'St Petersburg Clearwater International Airport', '27.91020012', '-82.68740082', '11', 'NA', 'US', 'US-FL', 'St Petersburg-Clearwater', 'yes', 'KPIE', 'PIE', 'PIE', '', 'https://en.wikipedia.org/wiki/St._Petersburg-Clearwater_International_Airport', ''),
('0', 'Aeropuerto Capitán FAP Guillermo Concha Iberico', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Piura
e', '80° 36'' 54.688', '5° 12'' 22.535"', 'public', '53', 'PIU', 'SPUR', ' ', '-80.6151912', '-5.2062598', '6240', 'SPUR', 'medium_airport', 'Capitán FAP Guillermo Concha Iberico International Airport', '-5.20574998856', '-80.6164016724', '120', 'SA', 'PE', 'PE-PIU', 'Piura', 'yes', 'SPUR', 'PIU', '', '', 'https://en.wikipedia.org/wiki/Cap._FAP_Guillermo_Concha_Iberico_International_Airport', ''),
('0', 'Pokhara Airport', 'aeroway=aerodrome
ele=827
iata=PKR
icao=VNPK
name=Pokhara Ai', '83° 58'' 52.098', '28° 12'' 2.112"', ' ', '827', 'PKR', 'VNPK', ' ', '83.9811384', '28.2005866', '26598', 'VNPK', 'medium_airport', 'Pokhara Airport', '28.2008991241455', '83.9821014404297', '2712', 'AS', 'NP', 'NP-GA', 'Pokhara', 'yes', 'VNPK', 'PKR', '', '', 'https://en.wikipedia.org/wiki/Pokhara_Airport', ''),
('0', 'Bandar Udara Internasional Sultan Syarif Kasim II', 'aeroway=aerodrome
iata=PKU
icao=WIBB
int_name=Sultan Syarif', '101° 26'' 38.53', '0° 27'' 33.960"', ' ', '0', 'PKU', 'WIBB', ' ', '101.4440371', '0.4594334', '26821', 'WIBB', 'medium_airport', 'Sultan Syarif Kasim Ii (Simpang Tiga) Airport', '0.460786014795303', '101.444999694824', '102', 'AS', 'ID', 'ID-RI', 'Pekanbaru-Sumatra Island', 'yes', 'WIBB', 'PKU', '', '', 'https://en.wikipedia.org/wiki/Sultan_Syarif_Qasim_II_International_Airport', ''),
('0', 'Sultan Mahmud Badaruddin II Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Palemba', '104° 41'' 50.84', '2° 53'' 47.627"', 'public', '37', 'PLM', 'WIPP', ' ', '104.6974579', '-2.896563', '26855', 'WIPP', 'medium_airport', 'Sultan Mahmud Badaruddin II Airport', '-2.8982501029968', '104.69999694824', '49', 'AS', 'ID', 'ID-SS', 'Palembang-Sumatra Island', 'yes', 'WIPP', 'PLM', '', '', 'https://en.wikipedia.org/wiki/Sultan_Mahmud_Badaruddin_II_Airport', ''),
('0', 'Providenciales International Airport', 'aeroway=aerodrome
closest_town=Providenciales
ele=5
iata=PLS', '72° 16'' 7.380"', '21° 46'' 26.699', ' ', '5', 'PLS', 'MBPV', ' ', '-72.2687168', '21.774083', '4628', 'MBPV', 'medium_airport', 'Providenciales Airport', '21.7735996246338', '-72.2658996582031', '15', 'NA', 'TC', 'TC-PR', 'Providenciales Island', 'yes', 'MBPV', 'PLS', '', '', 'https://en.wikipedia.org/wiki/Providenciales_International_Airport', ''),
('0', 'Aeroporto de Belo Horizonte/Pampulha - MG - Carlos Drummond', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Ae', '43° 57'' 3.757"', '19° 51'' 3.346"', 'military/public', '789', 'PLU', 'SBBH', ' ', '-43.9510437', '-19.8509294', '5869', 'SBBH', 'medium_airport', 'Pampulha - Carlos Drummond de Andrade Airport', '-19.8512001037598', '-43.9505996704102', '2589', 'SA', 'BR', 'BR-MG', 'Belo Horizonte', 'yes', 'SBBH', 'PLU', '', 'http://www.infraero.gov.br/usa/aero_prev_home.php?ai=204', 'https://en.wikipedia.org/wiki/Pampulha_Domestic_Airport', 'BHZ'),
('0', 'Port Elizabeth Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '25° 36'' 39.636', '33° 59'' 23.371', 'public', '67', 'PLZ', 'FAPE', ' ', '25.6110099', '-33.9898253', '2820', 'FAPE', 'medium_airport', 'Port Elizabeth International Airport', '-33.984901', '25.6173', '226', 'AF', 'ZA', 'ZA-EC', 'Port Elizabeth', 'yes', 'FAPE', 'PLZ', '', 'http://www.acsa.co.za/home.asp?pid=236', 'https://en.wikipedia.org/wiki/Port_Elizabeth_International_Airport', 'Verwoerd International Airport'),
('0', 'Aeropuerto El Tepual', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '73° 5'' 43.467"', '41° 26'' 37.298', 'military/public', '90', 'PMC', 'SCTE', ' ', '-73.0954076', '-41.443694', '6043', 'SCTE', 'medium_airport', 'El Tepual Airport', '-41.4388999938965', '-73.0940017700195', '294', 'SA', 'CL', 'CL-LL', 'Puerto Montt', 'yes', 'SCTE', 'PMC', '', '', 'https://en.wikipedia.org/wiki/El_Tepual_Airport', ''),
('0', 'Aeroporto Internacional de Ponta Porã', 'aeroway=aerodrome
barrier=fence
ele=571
iata=PMG
icao=SBPP
n', '55° 42'' 10.766', '22° 32'' 59.927', ' ', '571', 'PMG', 'SBPP', ' ', '-55.7029905', '-22.5499797', '5959', 'SBPP', 'medium_airport', 'Ponta Porã Airport', '-22.5496006011963', '-55.7025985717773', '2156', 'SA', 'BR', 'BR-MS', 'Ponta Porã', 'yes', 'SBPP', 'PMG', '', '', '', ''),
('0', 'Palmerston North Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '175° 36'' 52.25', '40° 19'' 23.042', 'regional', '46', 'PMR', 'NZPM', ' ', '175.614514', '-40.3230671', '5049', 'NZPM', 'medium_airport', 'Palmerston North Airport', '-40.3205986022949', '175.617004394531', '151', 'OC', 'NZ', 'NZ-MWT', '', 'yes', 'NZPM', 'PMR', '', 'http://www.pnairport.co.nz/index.php', 'https://en.wikipedia.org/wiki/Palmerston_North_International_Airport', ''),
('0', 'Aeropuerto Internacional del Caribe Santiago Mariño', 'aeroway=aerodrome
iata=PMV
icao=SVMG
name=Aeropuerto Interna', '63° 58'' 8.588"', '10° 54'' 47.457', ' ', '0', 'PMV', 'SVMG', ' ', '-63.9690523', '10.9131825', '6299', 'SVMG', 'medium_airport', 'Del Caribe Santiago Mariño International Airport', '10.9126033782959', '-63.9665985107422', '74', 'SA', 'VE', 'VE-O', 'Isla Margarita', 'yes', 'SVMG', 'PMV', '', '', 'https://en.wikipedia.org/wiki/Del_Caribe_International_General_Santiago_Marino_Airport', ''),
('0', 'Aeroporto de Palmas – Brigadeiro Lysias Rodrigues', 'aerodrome=public
aeroway=aerodrome
ele=236
iata=PMW
icao=SBP', '48° 21'' 28.175', '10° 17'' 40.748', ' ', '236', 'PMW', 'SBPJ', ' ', '-48.3578265', '-10.2946522', '5955', 'SBPJ', 'medium_airport', 'Brigadeiro Lysias Rodrigues Airport', '-10.2915000916', '-48.3569984436', '774', 'SA', 'BR', 'BR-TO', 'Palmas', 'yes', 'SBPJ', 'PMW', '', '', 'https://en.wikipedia.org/wiki/Palmas_Airport', ''),
('0', 'អាកាសយានដ្ឋាន​អន្តរជ', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '104° 50'' 42.12', '11° 32'' 48.737', 'military/public', '12', 'PNH', 'VDPP', ' ', '104.8450357', '11.5468713', '26475', 'VDPP', 'large_airport', 'Phnom Penh International Airport', '11.5466', '104.844002', '40', 'AS', 'KH', 'KH-12', 'Phnom Penh (Pou Senchey)', 'yes', 'VDPP', 'PNH', '', 'http://www.cambodia-airports.com/phnompenh/en/', 'https://en.wikipedia.org/wiki/Phnom_Penh_International_Airport', 'Pochentong International Airport'),
('0', 'Bandar Udara Supadio', 'aeroway=aerodrome
iata=PNK
icao=WIOO
name=Bandar Udara Supad', '109° 24'' 15.59', '0° 9'' 6.542" S', ' ', '0', 'PNK', 'WIOO', ' ', '109.4043321', '-0.1518171', '26847', 'WIOO', 'medium_airport', 'Supadio Airport', '-0.150710999965668', '109.403999328613', '10', 'AS', 'ID', 'ID-KB', 'Pontianak-Borneo Island', 'yes', 'WIOO', 'PNK', '', '', 'https://en.wikipedia.org/wiki/Supadio_Airport', ''),
('0', 'Girua Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=95
iata=PNP
icao', '148° 18'' 32.40', '8° 48'' 16.200"', 'public', '95', 'PNP', 'AYGR', ' ', '148.309', '-8.8045', '58', 'AYGR', 'medium_airport', 'Girua Airport', '-8.80453968048', '148.309005737', '311', 'OC', 'PG', 'PG-NPP', 'Popondetta', 'yes', 'AYGR', 'PNP', '', '', 'https://en.wikipedia.org/wiki/Girua_Airport', ''),
('0', 'Aéroport International Agostinho-Neto', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '11° 53'' 7.371"', '4° 48'' 56.483"', 'public', '17', 'PNR', 'FCPP', ' ', '11.8853807', '-4.8156897', '2871', 'FCPP', 'medium_airport', 'Antonio Agostinho-Neto International Airport', '-4.81603', '11.8866', '55', 'AF', 'CG', 'CG-5', 'Pointe Noire', 'yes', 'FCPP', 'PNR', '', '', 'https://en.wikipedia.org/wiki/Agostinho-Neto_International_Airport', 'Pointe Noire'),
('0', 'Pondicherry Airport', 'aeroway=aerodrome
iata=PNY
icao=VOPC
name=Pondicherry Airpor', '79° 48'' 41.336', '11° 58'' 1.010"', ' ', '0', 'PNY', 'VOPC', ' ', '79.8114823', '11.9669472', '26621', 'VOPC', 'medium_airport', 'Pondicherry Airport', '11.968', '79.812', '134', 'AS', 'IN', 'IN-PY', 'Puducherry (Pondicherry)', 'yes', 'VOPC', 'PNY', '', '', 'https://en.wikipedia.org/wiki/Pondicherry_Airport', ''),
('0', 'Aeroporto Internacional de Porto Alegre', 'addr:postcode=90200-310
addr:street=Avenida Severo Dullius
a', '51° 10'' 5.449"', '29° 59'' 43.333', 'public', '3', 'POA', 'SBPA', 'Салгаду Филью (аэропорт)', '-51.1681802', '-29.9953704', '5951', 'SBPA', 'medium_airport', 'Salgado Filho Airport', '-29.9944000244141', '-51.1713981628418', '11', 'SA', 'BR', 'BR-RS', 'Porto Alegre', 'yes', 'SBPA', 'POA', '', '', 'https://en.wikipedia.org/wiki/Salgado_Filho_International_Airport', ''),
('0', 'Jacksons International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Port Mo', '147° 13'' 4.776', '9° 26'' 25.340"', 'public', '38', 'POM', 'AYPY', ' ', '147.2179933', '-9.4403722', '67', 'AYPY', 'large_airport', 'Port Moresby Jacksons International Airport', '-9.44338035583496', '147.220001220703', '146', 'OC', 'PG', 'PG-NCD', 'Port Moresby', 'yes', 'AYPY', 'POM', '', '', 'https://en.wikipedia.org/wiki/Jacksons_International_Airport', ''),
('0', 'Aeropuerto Internacional Gregorio Luperón', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Gr', '70° 34'' 10.855', '19° 45'' 27.011', 'military/public', '5', 'POP', 'MDPP', ' ', '-70.569682', '19.757503', '4637', 'MDPP', 'medium_airport', 'Gregorio Luperon International Airport', '19.7579002380371', '-70.5699996948242', '15', 'NA', 'DO', 'DO-18', 'Puerto Plata', 'yes', 'MDPP', 'POP', '', '', 'https://en.wikipedia.org/wiki/Gregorio_Luper%C3%B3n_International_Airport', 'La Union'),
('0', 'Aeroporto Estadual de Presidente Prudente', 'aeroway=aerodrome
iata=PPB
icao=SBDN
name=Aeroporto Estadual', '51° 25'' 29.262', '22° 10'' 30.272', ' ', '0', 'PPB', 'SBDN', ' ', '-51.424795', '-22.1750756', '5896', 'SBDN', 'medium_airport', 'Presidente Prudente Airport', '-22.1751003265', '-51.4245986938', '1477', 'SA', 'BR', 'BR-SP', 'Presidente Prudente', 'yes', 'SBDN', 'PPB', '', 'http://www.daesp.sp.gov.br/aeroporto-detalhe/?id=879', 'https://en.wikipedia.org/wiki/Presidente_Prudente_Airport', ''),
('0', 'Pago Pago International Airport', 'aerodrome=international
aeroway=aerodrome
ele=0
gnis:county_', '170° 42'' 44.23', '14° 19'' 42.230', ' ', '0', 'PPG', 'NSTU', ' ', '-170.7122885', '-14.3283971', '4978', 'NSTU', 'medium_airport', 'Pago Pago International Airport', '-14.3310003281', '-170.710006714', '32', 'OC', 'AS', 'AS-U-A', 'Pago Pago', 'yes', 'NSTU', 'PPG', 'PPG', '', 'https://en.wikipedia.org/wiki/Pago_Pago_International_Airport', ''),
('0', 'Proserpine Airport (Whitsunday Coast Airport)', 'aerodrome:type=public
aeroway=aerodrome
ele=25
iata=PPP
icao', '148° 33'' 7.132', '20° 29'' 25.462', 'public', '25', 'PPP', 'YBPN', ' ', '148.5519812', '-20.4904061', '26924', 'YBPN', 'medium_airport', 'Proserpine Whitsunday Coast Airport', '-20.4950008392', '148.552001953', '82', 'OC', 'AU', 'AU-QLD', 'Proserpine', 'yes', 'YBPN', 'PPP', '', '', 'https://en.wikipedia.org/wiki/Proserpine_/_Whitsunday_Coast_Airport', ''),
('0', 'Puerto Princesa International Airport', 'aeroway=aerodrome
iata=PPS
icao=RPVP
name=Puerto Princesa In', '118° 45'' 30.32', '9° 44'' 35.739"', ' ', '0', 'PPS', 'RPVP', ' ', '118.7584231', '9.7432609', '5744', 'RPVP', 'medium_airport', 'Puerto Princesa Airport', '9.74211978912354', '118.759002685547', '71', 'AS', 'PH', 'PH-PLW', 'Puerto Princesa City', 'yes', 'RPVP', 'PPS', '', '', 'https://en.wikipedia.org/wiki/Puerto_Princesa_Airport', ''),
('0', 'Port Macquarie Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=5 m
iata=PQQ
ica', '152° 51'' 53.72', '31° 26'' 9.008"', 'public', '0', 'PQQ', 'YPMQ', ' ', '152.8649231', '-31.4358356', '27115', 'YPMQ', 'medium_airport', 'Port Macquarie Airport', '-31.4358005524', '152.863006592', '12', 'OC', 'AU', 'AU-NSW', 'Port Macquarie', 'yes', 'YPMQ', 'PQQ', '', 'http://www.hastings.nsw.gov.au/www/html/455-welcome-to-port-macquarie-airport.asp?intLocationID=455', 'https://en.wikipedia.org/wiki/Port_Macquarie_Airport', ''),
('0', 'Tri-Cities Airport', 'addr:state=WA
aeroway=aerodrome
alt_name=Pasco Airport
faa=P', '119° 7'' 10.390', '46° 15'' 50.678', ' ', '0', 'PSC', 'KPSC', ' ', '-119.1195528', '46.2640771', '20870', 'KPSC', 'medium_airport', 'Tri Cities Airport', '46.2647018432617', '-119.119003295898', '410', 'NA', 'US', 'US-WA', 'Pasco', 'yes', 'KPSC', 'PSC', 'PSC', '', '', ''),
('0', 'Aeropuerto Internacional Mercedita', 'aeroway=aerodrome
ele=7
gnis:county_name=Ponce
gnis:created=', '66° 33'' 47.507', '18° 0'' 32.045"', ' ', '7', 'PSE', 'TJPS', ' ', '-66.5631965', '18.0089013', '6383', 'TJPS', 'medium_airport', 'Mercedita Airport', '18.00830078125', '-66.5630035400391', '29', 'NA', 'PR', 'PR-U-A', 'Ponce', 'yes', 'TJPS', 'PSE', 'PSE', '', 'https://en.wikipedia.org/wiki/Mercedita_Airport', ''),
('0', 'Antonio Nariño', 'aeroway=aerodrome
ele=1814
iata=PSO
icao=SKPS
name=Antonio N', '77° 17'' 26.416', '1° 23'' 48.865"', ' ', '1814', 'PSO', 'SKPS', ' ', '-77.2906712', '1.3969069', '6154', 'SKPS', 'medium_airport', 'Antonio Narino Airport', '1.39625', '-77.2915', '5951', 'SA', 'CO', 'CO-NAR', 'Pasto', 'yes', 'SKPS', 'PSO', 'PSO', '', 'https://en.wikipedia.org/wiki/Antonio_Nari%C3%B1o_Airport', ''),
('0', 'Aeropuerto de Posadas "Libertador General San Martín"', 'addr:city=Posadas
addr:postcode=3300
addr:street=Ruta Nacion', '55° 58'' 9.788"', '27° 23'' 0.530"', ' ', '131', 'PSS', 'SARP', ' ', '-55.9693855', '-27.3834805', '5809', 'SARP', 'medium_airport', 'Libertador Gral D Jose De San Martin Airport', '-27.3858', '-55.9707', '430', 'SA', 'AR', 'AR-N', 'Posadas', 'yes', 'SARP', 'PSS', 'POS', '', 'https://en.wikipedia.org/wiki/Libertador_General_Jos%C3%A9_de_San_Mart%C3%ADn_Airport', ''),
('0', 'Puerto Suárez - Cap Av Salvador Ogaya', 'addr:country=BO
aeroway=aerodrome
iata=PSZ
icao=SLPS
name=Pu', '57° 49'' 9.332"', '18° 58'' 26.076', ' ', '0', 'PSZ', 'SLPS', ' ', '-57.819259', '-18.9739099', '6187', 'SLPS', 'medium_airport', 'Capitán Av. Salvador Ogaya G. airport', '-18.975301', '-57.820599', '440', 'SA', 'BO', 'BO-S', 'Puerto Suárez', 'yes', 'SLPS', 'PSZ', '', '', 'https://en.wikipedia.org/wiki/Puerto_Su%C3%A1rez_International_Airport', ''),
('0', 'Polokwane International Airport', 'aeroway=aerodrome
iata=PTG
icao=FAPP
ifr=yes
licensed:sacaa=', '29° 27'' 29.097', '23° 50'' 42.051', ' ', '0', 'PTG', 'FAPP', ' ', '29.4580824', '-23.8450141', '2827', 'FAPP', 'medium_airport', 'Polokwane International Airport', '-23.845269', '29.458615', '4076', 'AF', 'ZA', 'ZA-NP', 'Polokwane', 'yes', 'FAPP', 'PTG', '', '', 'https://en.wikipedia.org/wiki/Polokwane_International_Airport', 'FAPB,  AFB Pietersburg,  Pietersburg International Airport, Gateway International Airport'),
('0', 'Portland Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Portlan', '141° 28'' 16.48', '38° 19'' 6.082"', 'public', '81', 'PTJ', 'YPOD', ' ', '141.4712463', '-38.3183561', '27116', 'YPOD', 'medium_airport', 'Portland Airport', '-38.3180999755859', '141.470993041992', '265', 'OC', 'AU', 'AU-VIC', '', 'yes', 'YPOD', 'PTJ', '', '', 'https://en.wikipedia.org/wiki/Portland_Airport_(Australia)', ''),
('0', 'Aeropuerto Internacional de Tocumen', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Aeropuerto', '79° 22'' 59.394', '9° 4'' 1.152" N', 'public', '41', 'PTY', 'MPTO', ' ', '-79.3831651', '9.0669866', '4793', 'MPTO', 'large_airport', 'Tocumen International Airport', '9.0713596344', '-79.3834991455', '135', 'NA', 'PA', 'PA-8', 'Tocumen', 'yes', 'MPTO', 'PTY', '', '', 'https://en.wikipedia.org/wiki/Tocumen_International_Airport', 'La Joya No 1'),
('0', 'Aeropuerto Internacional Punta Cana', 'aeroway=aerodrome
iata=PUJ
icao=MDPC
name=Aeropuerto Interna', '68° 21'' 47.185', '18° 34'' 10.647', ' ', '0', 'PUJ', 'MDPC', ' ', '-68.3631069', '18.5696242', '4636', 'MDPC', 'large_airport', 'Punta Cana International Airport', '18.5673999786', '-68.3634033203', '47', 'NA', 'DO', 'DO-11', 'Punta Cana', 'yes', 'MDPC', 'PUJ', '', '', 'https://en.wikipedia.org/wiki/Punta_Cana_International_Airport', ''),
('0', '김해국제공항', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name:ko', '128° 56'' 22.73', '35° 10'' 48.175', 'military/public', '2', 'PUS', 'RKPK', 'Аэропорт Гимхае', '128.9396489', '35.1800487', '5642', 'RKPK', 'large_airport', 'Gimhae International Airport', '35.1795005798', '128.93800354', '6', 'AS', 'KR', 'KR-26', 'Busan', 'yes', 'RKPK', 'PUS', '', 'http://gimhae.airport.co.kr/eng/index.jsp', 'https://en.wikipedia.org/wiki/Gimhae_International_Airport', 'Kimhae, Pusan'),
('0', 'Aeropuerto El Embrujo', 'aeroway=aerodrome
iata=PVA
icao=SKPV
is_in=Providencia, Colo', '81° 21'' 29.009', '13° 21'' 27.078', ' ', '0', 'PVA', 'SKPV', ' ', '-81.3580581', '13.3575216', '6155', 'SKPV', 'medium_airport', 'El Embrujo Airport', '13.3569', '-81.3583', '10', 'SA', 'CO', 'CO-SAP', 'Providencia', 'yes', 'SKPV', 'PVA', 'PVA', '', 'https://en.wikipedia.org/wiki/El_Embrujo_Airport', ''),
('0', 'Aeropuerto Internacional Gustavo Díaz Ordáz', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Puerto', '105° 15'' 11.88', '20° 40'' 47.280', 'public', '7', 'PVR', 'MMPR', ' ', '-105.2533', '20.6798', '4745', 'MMPR', 'large_airport', 'Licenciado Gustavo Díaz Ordaz International Airport', '20.6800994873047', '-105.253997802734', '23', 'NA', 'MX', 'MX-JAL', 'Puerto Vallarta', 'yes', 'MMPR', 'PVR', '', 'http://vallarta.aeropuertosgap.com.mx/index.php?lang=eng', 'https://en.wikipedia.org/wiki/Lic._Gustavo_D%C3%ADaz_Ordaz_International_Airport', ''),
('0', 'Aeropuerto Internacional de Puerto Escondido', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Puerto', '97° 5'' 22.589"', '15° 52'' 31.320', 'public', '90', 'PXM', 'MMPS', ' ', '-97.089608', '15.8753667', '4746', 'MMPS', 'medium_airport', 'Puerto Escondido International Airport', '15.8769', '-97.089103', '294', 'NA', 'MX', 'MX-OAX', 'Puerto Escondido', 'yes', 'MMPS', 'PXM', 'P1M', '', 'https://en.wikipedia.org/wiki/Puerto_Escondido_International_Airport', ''),
('0', 'Pietermaritzburg Airport', 'aeroway=aerodrome
alt_name=Oribi Airport
iata=PZB
icao=FAPM
', '30° 23'' 55.343', '29° 38'' 54.997', ' ', '0', 'PZB', 'FAPM', ' ', '30.3987064', '-29.6486103', '2825', 'FAPM', 'medium_airport', 'Pietermaritzburg Airport', '-29.6490001678', '30.3987007141', '2423', 'AF', 'ZA', 'ZA-NL', 'Pietermaritzburg', 'yes', 'FAPM', 'PZB', '', '', 'https://en.wikipedia.org/wiki/Pietermaritzburg_Airport', ''),
('0', 'Aeropuerto Intercontinental de Queretaro', 'aerodrome:type=public
aeroway=aerodrome
ele=1969
iata=QRO
ic', '100° 11'' 14.41', '20° 37'' 19.510', 'public', '1969', 'QRO', 'MMQT', ' ', '-100.187337', '20.6220861', '4747', 'MMQT', 'medium_airport', 'Querétaro Intercontinental Airport', '20.6173', '-100.185997', '6296', 'NA', 'MX', 'MX-QUE', 'Querétaro', 'yes', 'MMQT', 'QRO', 'QET', 'http://www.aiq.com.mx/', 'https://en.wikipedia.org/wiki/Quer%C3%A9taro_International_Airport', ''),
('0', 'Aeroporto Internacional Nelson Mandela', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Praia,', '23° 29'' 5.646"', '14° 56'' 28.686', 'public', '70', 'RAI', 'GVNP', ' ', '-23.4849016', '14.9413017', '3153', 'GVNP', 'medium_airport', 'Praia International Airport', '14.9245004653931', '-23.4934997558594', '230', 'AF', 'CV', 'CV-S', 'Praia', 'yes', 'GVNP', 'RAI', '', '', 'https://en.wikipedia.org/wiki/Francisco_Mendes_International_Airport', 'Santiago Island'),
('0', 'Rajkot Airport', 'aeroway=aerodrome
area=yes
barrier=wall
closest_town=Rajkot
', '70° 46'' 46.305', '22° 18'' 33.201', ' ', '134', 'RAJ', 'VARK', ' ', '70.7795291', '22.3092225', '26458', 'VARK', 'medium_airport', 'Rajkot Airport', '22.3092002869', '70.7795028687', '441', 'AS', 'IN', 'IN-GJ', 'Rajkot', 'yes', 'VARK', 'RAJ', '', 'http://www.airportsindia.org.in/allAirports/rajkot.jsp', 'https://en.wikipedia.org/wiki/Rajkot_Airport', ''),
('0', 'Aeroporto Doutor Leite Lopes de Ribeirão Preto', 'aeroway=aerodrome
alt_name=Aeroporto Internacional de Cargas', '47° 46'' 34.064', '21° 7'' 59.862"', ' ', '0', 'RAO', 'SBRP', ' ', '-47.7761288', '-21.1332949', '5968', 'SBRP', 'medium_airport', 'Leite Lopes Airport', '-21.1363887786865', '-47.776668548584', '1806', 'SA', 'BR', 'BR-SP', 'Ribeirão Preto', 'yes', 'SBRP', 'RAO', '', '', '', ''),
('0', 'Rarotonga International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '159° 48'' 20.54', '21° 12'' 5.346"', 'public', '6', 'RAR', 'NCRG', ' ', '-159.805706', '-21.2014851', '4958', 'NCRG', 'large_airport', 'Rarotonga International Airport', '-21.2026996613', '-159.805999756', '19', 'OC', 'CK', 'CK-U-A', 'Avarua', 'yes', 'NCRG', 'RAR', '', '', 'https://en.wikipedia.org/wiki/Rarotonga_International_Airport', ''),
('0', 'Almirante Padilla', 'aeroway=aerodrome
closest_town=Riohacha
ele=13
iata=RCH
icao', '72° 55'' 35.929', '11° 31'' 37.996', ' ', '13', 'RCH', 'SKRH', ' ', '-72.9266469', '11.527221', '6159', 'SKRH', 'medium_airport', 'Almirante Padilla Airport', '11.5262', '-72.926', '43', 'SA', 'CO', 'CO-LAG', 'Riohacha', 'yes', 'SKRH', 'RCH', 'RCH', '', 'https://en.wikipedia.org/wiki/Riohacha_Airport', ''),
('0', 'Redding Municipal Airport', 'Tiger:MTFCC=K2457
addr:state=CA
aeroway=aerodrome
closest_to', '122° 17'' 13.79', '40° 30'' 43.605', ' ', '152', 'RDD', 'KRDD', ' ', '-122.2871655', '40.5121125', '3841', 'KRDD', 'medium_airport', 'Redding Municipal Airport', '40.50899887', '-122.2929993', '505', 'NA', 'US', 'US-CA', 'Redding', 'yes', 'KRDD', 'RDD', 'RDD', '', 'https://en.wikipedia.org/wiki/Redding_Municipal_Airport', ''),
('0', 'Redmond Municipal Airport', 'addr:state=OR
aerodrome:type=public
aeroway=aerodrome
alt_na', '121° 8'' 58.696', '44° 15'' 12.780', 'public', '936', 'RDM', 'KRDM', ' ', '-121.1496379', '44.2535501', '3843', 'KRDM', 'medium_airport', 'Roberts Field', '44.2541008', '-121.1500015', '3080', 'NA', 'US', 'US-OR', 'Redmond', 'yes', 'KRDM', 'RDM', 'RDM', '', 'https://en.wikipedia.org/wiki/Roberts_Field', ''),
('0', 'Aeropuerto Almirante Marcos Zar', 'aerodrome:type=military/public
aeroway=aerodrome
ele=43
iata', '65° 16'' 16.455', '43° 12'' 15.239', 'military/public', '43', 'REL', 'SAVT', ' ', '-65.2712374', '-43.204233', '5827', 'SAVT', 'medium_airport', 'Almirante Marco Andres Zar Airport', '-43.2105', '-65.2703', '141', 'SA', 'AR', 'AR-U', 'Rawson', 'yes', 'SAVT', 'REL', 'TRE', '', 'https://en.wikipedia.org/wiki/Almirante_Marco_Andr%C3%A9s_Zar_Airport', 'Trelew Airport'),
('0', 'អាកាសយានដ្ឋាន​អន្តរជ', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '103° 48'' 45.71', '13° 24'' 39.171', 'military/public', '0', 'REP', 'VDSR', ' ', '103.8126975', '13.4108808', '26476', 'VDSR', 'large_airport', 'Siem Reap International Airport', '13.41155', '103.813044', '60', 'AS', 'KH', 'KH-17', 'Siem Reap', 'yes', 'VDSR', 'REP', '', 'http://www.cambodia-airports.com/siemreap/en', 'https://en.wikipedia.org/wiki/Siem_Reap_International_Airport', 'Angkor International Airport'),
('0', 'Aeropuerto Intl. General Lucio Blanco', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Reynosa', '98° 13'' 41.240', '26° 0'' 33.974"', 'public', '42', 'REX', 'MMRX', ' ', '-98.2281221', '26.0094371', '4748', 'MMRX', 'medium_airport', 'General Lucio Blanco International Airport', '26.0089', '-98.2285', '139', 'NA', 'MX', 'MX-TAM', 'Reynosa', 'yes', 'MMRX', 'REX', 'REI', '', 'https://en.wikipedia.org/wiki/General_Lucio_Blanco_International_Airport', ''),
('0', 'Aeropuerto Internacional Piloto Civil Norberto Fernández', 'aeroway=aerodrome
barrier=fence
iata=RGL
icao=SAWG
name=Aero', '69° 18'' 32.903', '51° 36'' 27.976', ' ', '0', 'RGL', 'SAWG', ' ', '-69.3091398', '-51.6077712', '5834', 'SAWG', 'medium_airport', 'Piloto Civil N. Fernández Airport', '-51.6089', '-69.3126', '61', 'SA', 'AR', 'AR-Z', 'Rio Gallegos', 'yes', 'SAWG', 'RGL', 'GAL', '', 'https://en.wikipedia.org/wiki/Piloto_Civil_Norberto_Fern%C3%A1ndez_International_Airport', 'Brigadier General D. A. Parodi'),
('0', 'ရန်ကုန် အပြည်ပြည်ဆို', 'aerodrome=international
aeroway=aerodrome
alt_name=Rangoon I', '96° 8'' 9.981"', '16° 54'' 38.531', ' ', '0', 'RGN', 'VYYY', 'Янгон (аэропорт)', '96.1361059', '16.9107031', '26744', 'VYYY', 'large_airport', 'Yangon International Airport', '16.9073009491', '96.1332015991', '109', 'AS', 'MM', 'MM-06', 'Yangon', 'yes', 'VYYY', 'RGN', '', 'http://www.yangonairportonline.com', 'https://en.wikipedia.org/wiki/Yangon_International_Airport', 'Rangoon'),
('0', '청진공항', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=', '129° 38'' 46.07', '41° 25'' 45.834', 'military/public', '0', 'RGO', 'ZKHM', ' ', '129.6461324', '41.4293983', '35242', 'KP-0032', 'medium_airport', 'Orang (Chongjin) Airport', '41.428538', '129.647555', '12', 'AS', 'KP', 'KP-09', 'Hoemun-ri', 'yes', 'ZKHM', 'RGO', '', '', 'https://en.wikipedia.org/wiki/Chongjin_Airport', 'K-33, Hoemun Airfield'),
('0', 'Aeropuerto Internacional Termas de Río Hondo', 'addr:city=Termas de Rio Hondo
addr:housenumber=1101
addr:str', '64° 56'' 7.250"', '27° 29'' 45.942', ' ', '0', 'RHD', 'SANR', ' ', '-64.9353472', '-27.4960949', '318304', 'SANR', 'medium_airport', 'Termas de Río Hondo international Airport', '-27.4966', '-64.93595', '935', 'SA', 'AR', 'AR-G', 'Termas de Río Hondo', 'yes', 'SANR', 'RHD', 'TRH', '', 'https://en.wikipedia.org/wiki/Las_Termas_Airport', ''),
('0', 'Aeroporto de Santa Maria', 'aerodrome=domestic
aerodrome:type=military/public
aeroway=ae', '53° 41'' 28.097', '29° 42'' 46.083', 'military/public', '88', 'RIA', 'SBSM', ' ', '-53.6911381', '-29.7128009', '5972', 'SBSM', 'medium_airport', 'Santa Maria Airport', '-29.711399', '-53.688202', '287', 'SA', 'BR', 'BR-RS', 'Santa Maria', 'yes', 'SBSM', 'RIA', '', '', 'https://en.wikipedia.org/wiki/Santa_Maria_Airport_(Rio_Grande_do_Sul)', ''),
('0', '利尻空港', 'KSJ2:AAC=01519
KSJ2:AAC_label=北海道利尻郡利尻富士', '141° 11'' 27.56', '45° 14'' 35.773', ' ', '0', 'RIS', 'RJER', ' ', '141.190989', '45.2432703', '5560', 'RJER', 'medium_airport', 'Rishiri Airport', '45.2420005798', '141.186004639', '112', 'AS', 'JP', 'JP-01', 'Rishiri', 'yes', 'RJER', 'RIS', '', '', 'https://en.wikipedia.org/wiki/Rishiri_Airport', ''),
('0', 'Rajahmundry Airport', 'aeroway=aerodrome
closest_town=Rajahmundry
iata=RJA
icao=VOR', '81° 49'' 5.560"', '17° 6'' 29.916"', ' ', '0', 'RJA', 'VORY', ' ', '81.8182111', '17.1083099', '26623', 'VORY', 'medium_airport', 'Rajahmundry Airport', '17.1103992462', '81.8181991577', '151', 'AS', 'IN', 'IN-AP', 'Rajahmundry', 'yes', 'VORY', 'RJA', '', 'http://aai.aero/allAirports/rajahmundry_generalinfo.jsp', 'https://en.wikipedia.org/wiki/Rajahmundry_Airport', ''),
('0', '日喀则和平机场', 'aerodrome:type=military/public
aeroway=aerodrome
barrier=fen', '89° 18'' 8.972"', '29° 21'' 1.256"', 'military/public', '3782', 'RKZ', 'ZURK', ' ', '89.3024922', '29.3503489', '44122', 'CN-0046', 'medium_airport', 'Shigatse Air Base', '29.3519', '89.311401', '3782', 'AS', 'CN', 'CN-54', 'Xigazê', 'yes', 'ZURK', 'RKZ', '', '', 'https://en.wikipedia.org/wiki/Shigatse_Peace_Airport', ''),
('0', '巴彦淖尔天吉泰机场', 'aerodrome:type=public
aeroway=aerodrome
arp=yes
iata=RLK
ica', '107° 44'' 44.03', '40° 55'' 32.207', 'public', '0', 'RLK', 'ZBYZ', ' ', '107.7455657', '40.925613', '314499', 'ZBYZ', 'medium_airport', 'Bayannur Tianjitai Airport', '40.926', '107.7428', '3400', 'AS', 'CN', 'CN-15', 'Bavannur', 'yes', 'ZBYZ', 'RLK', '', '', 'https://en.wikipedia.org/wiki/Bayannur_Tianjitai_Airport', ''),
('0', 'Ratmalana Air Force Base', 'aerodrome=international
aerodrome:type=military/public
aerow', '79° 53'' 17.381', '6° 49'' 19.312"', 'military/public', '5', 'RML', 'VCCC', ' ', '79.8881615', '6.8220312', '26466', 'VCCC', 'medium_airport', 'Colombo Ratmalana Airport', '6.82199001312256', '79.8861999511719', '22', 'AS', 'LK', 'LK-1', 'Colombo', 'yes', 'VCCC', 'RML', '', '', 'https://en.wikipedia.org/wiki/Ratmalana_Airport', ''),
('0', 'Reno-Tahoe International Airport', 'addr:state=NV
aeroway=aerodrome
city_served=Reno/Sparks, NV
', '119° 46'' 7.074', '39° 29'' 53.917', ' ', '1538', 'RNO', 'KRNO', ' ', '-119.7686317', '39.4983104', '3853', 'KRNO', 'large_airport', 'Reno Tahoe International Airport', '39.4990997314453', '-119.767997741699', '4415', 'NA', 'US', 'US-NV', 'Reno', 'yes', 'KRNO', 'RNO', 'RNO', '', 'https://en.wikipedia.org/wiki/Reno-Tahoe_International_Airport', ''),
('0', 'Roberts International Airport', 'aerodrome=international
aerodrome:type=civil
aeroway=aerodro', '10° 21'' 47.377', '6° 13'' 56.882"', 'civil', '0', 'ROB', 'GLRB', ' ', '-10.3631604', '6.2324673', '3099', 'GLRB', 'large_airport', 'Roberts International Airport', '6.23379', '-10.3623', '31', 'AF', 'LR', 'LR-MG', 'Monrovia', 'yes', 'GLRB', 'ROB', '', '', 'https://en.wikipedia.org/wiki/Roberts_International_Airport', ''),
('0', 'Rockhampton Airport', 'aeroway=aerodrome
iata=ROK
icao=YBRK
name=Rockhampton Airpor', '150° 28'' 29.51', '23° 22'' 59.661', ' ', '0', 'ROK', 'YBRK', ' ', '150.4748664', '-23.3832391', '26925', 'YBRK', 'medium_airport', 'Rockhampton Airport', '-23.3819007874', '150.475006104', '34', 'OC', 'AU', 'AU-QLD', 'Rockhampton', 'yes', 'YBRK', 'ROK', '', 'http://www.rok.aero/', 'https://en.wikipedia.org/wiki/Rockhampton_Airport', ''),
('0', 'Aeroporto Municipal Maestro Marinho Franco', 'aeroway=aerodrome
alt_name=Aeroporto de Rondonópolis
iata=R', '54° 43'' 29.411', '16° 35'' 3.008"', ' ', '0', 'ROO', 'SWRD', ' ', '-54.7248365', '-16.5841689', '575', 'SWRD', 'medium_airport', 'Maestro Marinho Franco Airport', '-16.586', '-54.7248', '1467', 'SA', 'BR', 'BR-MT', 'Rondonópolis', 'yes', 'SBRD', 'ROO', 'MT0004', 'http://www.rondonopolis.mt.gov.br/orgaos-municipais/aeroporto-municipal-maestro-marinho-franco/', 'https://en.wikipedia.org/wiki/Rondon%C3%B3polis_Airport', 'SWRD, Rondonópolis Airport'),
('0', 'Rota International Airport', 'aeroway=aerodrome
ele=181
gnis:feature_id=1390573
iata=ROP
i', '145° 14'' 28.16', '14° 10'' 23.726', ' ', '181', 'ROP', 'PGRO', ' ', '145.2411558', '14.1732572', '5430', 'PGRO', 'medium_airport', 'Rota International Airport', '14.1743', '145.242996', '607', 'OC', 'MP', 'MP-U-A', 'Rota Island', 'yes', 'PGRO', 'ROP', 'GRO', '', 'https://en.wikipedia.org/wiki/Rota_International_Airport', ''),
('0', 'Roman Tmetuchl International Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Babelthuap', '134° 32'' 36.44', '7° 21'' 58.548"', 'public', '46', 'ROR', 'PTRO', ' ', '134.5434569', '7.3662633', '5501', 'PTRO', 'medium_airport', 'Babelthuap Airport', '7.36731', '134.544236', '176', 'OC', 'PW', 'PW-004', 'Babelthuap Island', 'yes', 'PTRO', 'ROR', 'ROR', '', 'https://en.wikipedia.org/wiki/Palau_International_Airport', ''),
('0', 'Aeropuerto Internacional Rosario', 'addr:city=Rosario
addr:postcode=S2000
addr:street=Jorge Newb', '60° 46'' 36.305', '32° 54'' 12.053', 'public', '56', 'ROS', 'SAAR', 'Международный аэропорт Росар', '-60.7767515', '-32.903348', '5769', 'SAAR', 'medium_airport', 'Islas Malvinas Airport', '-32.9036', '-60.785', '85', 'SA', 'AR', 'AR-S', 'Rosario', 'yes', 'SAAR', 'ROS', 'ROS', '', 'https://en.wikipedia.org/wiki/Rosario_International_Airport', 'Fisherton Airport'),
('0', 'Rotorua Regional Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '176° 18'' 57.43', '38° 6'' 30.702"', 'regional', '285', 'ROT', 'NZRO', ' ', '176.3159534', '-38.1085284', '5052', 'NZRO', 'medium_airport', 'Rotorua Regional Airport', '-38.1091995239258', '176.317001342773', '935', 'OC', 'NZ', 'NZ-BOP', 'Rotorua', 'yes', 'NZRO', 'ROT', '', '', 'https://en.wikipedia.org/wiki/Rotorua_Regional_Airport', ''),
('0', 'Aeropuerto de Santa Rosa', 'aeroway=aerodrome
iata=RSA
icao=SAZR
name=Aeropuerto de Sant', '64° 16'' 30.779', '36° 35'' 22.570', ' ', '0', 'RSA', 'SAZR', ' ', '-64.2752165', '-36.5896027', '5851', 'SAZR', 'medium_airport', 'Santa Rosa Airport', '-36.588299', '-64.275703', '630', 'SA', 'AR', 'AR-L', 'Santa Rosa', 'yes', 'SAZR', 'RSA', 'OSA', '', 'https://en.wikipedia.org/wiki/Santa_Rosa_Airport_(Argentina)', ''),
('0', '여수공항', 'aerodrome:type=public
aeroway=aerodrome
ele=16
iata=RSU
icao', '127° 37'' 1.483', '34° 50'' 32.630', 'public', '16', 'RSU', 'RKJY', ' ', '127.6170786', '34.8423971', '5634', 'RKJY', 'medium_airport', 'Yeosu Airport', '34.8423004150391', '127.616996765137', '53', 'AS', 'KR', 'KR-46', 'Yeosu', 'yes', 'RKJY', 'RSU', '', '', 'https://en.wikipedia.org/wiki/Yeosu_Airport', ''),
('0', 'Southwest Florida International Airport', 'addr:city=Fort Myers
addr:country=US
addr:housenumber=11000
', '81° 45'' 23.069', '26° 31'' 59.196', ' ', '7', 'RSW', 'KRSW', ' ', '-81.756408', '26.5331099', '3858', 'KRSW', 'large_airport', 'Southwest Florida International Airport', '26.5361995697021', '-81.7552032470703', '30', 'NA', 'US', 'US-FL', 'Fort Myers', 'yes', 'KRSW', 'RSW', 'RSW', 'http://www.flylcpa.com/', 'https://en.wikipedia.org/wiki/Southwest_Florida_International_Airport', ''),
('0', 'Juan Manuel Gálvez International Airport', 'aeroway=aerodrome
closest_town=Roatán
ele=5
iata=RTB
icao=M', '86° 31'' 21.499', '16° 19'' 1.203"', ' ', '5', 'RTB', 'MHRO', ' ', '-86.5226387', '16.3170008', '4663', 'MHRO', 'medium_airport', 'Juan Manuel Galvez International Airport', '16.316799', '-86.523003', '39', 'NA', 'HN', 'HN-IB', 'Roatan Island', 'yes', 'MHRO', 'RTB', '', '', 'https://en.wikipedia.org/wiki/Juan_Manuel_G%C3%A1lvez_International_Airport', 'Roatan International'),
('0', 'Rurutu Airport', 'aeroway=aerodrome
iata=RUR
icao=NTAR
name=Rurutu Airport
sou', '151° 21'' 37.23', '22° 26'' 3.864"', ' ', '0', 'RUR', 'NTAR', ' ', '-151.3603442', '-22.4344066', '4980', 'NTAR', 'medium_airport', 'Rurutu Airport', '-22.4340991973877', '-151.360992431641', '18', 'OC', 'PF', 'PF-U-A', '', 'yes', 'NTAR', 'RUR', '', '', 'https://en.wikipedia.org/wiki/Rurutu_Airport', ''),
('0', 'Aeropuerto Santa Cruz', 'aeroway=aerodrome
iata=RZA
icao=SAWU
name=Aeropuerto Santa C', '68° 34'' 32.885', '50° 0'' 47.068"', ' ', '0', 'RZA', 'SAWU', ' ', '-68.5758014', '-50.0130744', '5839', 'SAWU', 'medium_airport', 'Santa Cruz Airport', '-50.0165', '-68.5792', '364', 'SA', 'AR', 'AR-Z', 'Santa Cruz', 'yes', 'SAWU', 'RZA', 'SCZ', '', '', ''),
('0', 'Aeropuerto Internacional El Salvador', 'aerodrome=international
aerodrome:type=military/public
aerow', '89° 3'' 30.441"', '13° 26'' 54.690', 'military/public', '31', 'SAL', 'MSLP', ' ', '-89.0584559', '13.4485251', '4820', 'MSLP', 'large_airport', 'Monseñor Óscar Arnulfo Romero International Airport', '13.4409', '-89.055702', '101', 'NA', 'SV', 'SV-PA', 'San Salvador (San Luis Talpa)', 'yes', 'MSLP', 'SAL', '', '', 'https://en.wikipedia.org/wiki/Monse%C3%B1or_%C3%93scar_Arnulfo_Romero_International_Airport', ''),
('0', 'Aeropuerto Internacional Ramón Villeda Morales', 'addr:city=San Pedro Sula
addr:postcode=504
aerodrome:type=mi', '87° 55'' 25.263', '15° 27'' 12.315', 'military/public', '28', 'SAP', 'MHLM', ' ', '-87.9236843', '15.4534208', '4661', 'MHLM', 'medium_airport', 'Ramón Villeda Morales International Airport', '15.4526', '-87.923599', '91', 'NA', 'HN', 'HN-CR', 'San Pedro Sula', 'yes', 'MHLM', 'SAP', '', '', 'https://en.wikipedia.org/wiki/Ram%C3%B3n_Villeda_Morales_International_Airport', ''),
('0', 'San Luis Obispo Regional Airport', 'ALAND=1109865
AREAID=110510801782
AWATER=0
COUNTYFP=079
FAA:', '120° 38'' 29.71', '35° 14'' 14.898', ' ', '65', 'SBP', 'KSBP', ' ', '-120.6415874', '35.2374718', '3869', 'KSBP', 'medium_airport', 'San Luis County Regional Airport', '35.2368011475', '-120.641998291', '212', 'NA', 'US', 'US-CA', 'San Luis Obispo', 'yes', 'KSBP', 'SBP', 'SBP', 'http://www.sloairport.com/', 'https://en.wikipedia.org/wiki/San_Luis_Obispo_County_Regional_Airport', 'SLO Airport'),
('0', 'Aeroporto de Santiago', 'aerodrome=regional
aeroway=aerodrome
ele=445
icao=SSST
name=', '54° 50'' 27.412', '29° 12'' 54.285', ' ', '0', 'SCQ', 'LEST', ' ', '-54.8409477', '-29.2150792', '4038', 'LEST', 'large_airport', 'Santiago de Compostela Airport', '42.8963012695313', '-8.41514015197754', '1213', 'EU', 'ES', 'ES-GA', 'Santiago de Compostela', 'yes', 'LEST', 'SCQ', '', '', 'https://en.wikipedia.org/wiki/Santiago_de_Compostela_Airport', ''),
('0', 'Aeropuerto Internacional Antonio Maceo', 'aeroway=aerodrome
alt_name=Antonio Maceo Airport
ele=76
iata', '75° 50'' 23.666', '19° 58'' 20.602', ' ', '76', 'SCU', 'MUCU', ' ', '-75.8399072', '19.9723894', '4835', 'MUCU', 'medium_airport', 'Antonio Maceo International Airport', '19.9698009490967', '-75.8354034423828', '249', 'NA', 'CU', 'CU-13', 'Santiago', 'yes', 'MUCU', 'SCU', '', '', 'https://en.wikipedia.org/wiki/Antonio_Maceo_Airport', ''),
('0', 'Aeropuerto de Santiago del Estero', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Santiag', '64° 18'' 31.539', '27° 46'' 1.902"', 'public', '200', 'SDE', 'SANE', ' ', '-64.3087607', '-27.7671949', '5790', 'SANE', 'medium_airport', 'Vicecomodoro Angel D. La Paz Aragonés Airport', '-27.7655563354', '-64.3099975586', '656', 'SA', 'AR', 'AR-G', 'Santiago del Estero', 'yes', 'SANE', 'SDE', 'SDE', '', 'https://en.wikipedia.org/wiki/Santiago_del_Estero_Airport', ''),
('0', '仙台空港', 'KSJ2:AAC=04207
KSJ2:AAC_label=宮城県名取市
KSJ2:AD2=1
', '140° 54'' 51.93', '38° 8'' 14.592"', ' ', '0', 'SDJ', 'RJSS', ' ', '140.9144251', '38.1373868', '5615', 'RJSS', 'large_airport', 'Sendai Airport', '38.139702', '140.917007', '15', 'AS', 'JP', 'JP-04', 'Natori', 'yes', 'RJSS', 'SDJ', '', '', 'https://en.wikipedia.org/wiki/Sendai_Airport', ''),
('0', 'Aeropuerto Internacional Las Americas', 'aerodrome:type=public
aeroway=aerodrome
city_served=Santo Do', '69° 40'' 14.961', '18° 25'' 45.747', 'public', '18', 'SDQ', 'MDSD', ' ', '-69.6708224', '18.4293743', '4638', 'MDSD', 'large_airport', 'Las Américas International Airport', '18.42970085144', '-69.668899536133', '59', 'NA', 'DO', 'DO-01', 'Santo Domingo', 'yes', 'MDSD', 'SDQ', '', 'http://www.aerodom.com/app/do/lasamericas.aspx', 'https://en.wikipedia.org/wiki/Las_Am%C3%A9ricas_International_Airport', 'Aeropuerto Internacional de Las Américas, José Francisco Peña Gómez Intl'),
('0', 'Aeropuerto de Sauce Viejo', 'aeroway=aerodrome
barrier=fence
iata=SFN
icao=SAAV
name=Aero', '60° 48'' 32.511', '31° 42'' 29.619', ' ', '0', 'SFN', 'SAAV', ' ', '-60.8090308', '-31.7082274', '5770', 'SAAV', 'medium_airport', 'Sauce Viejo Airport', '-31.7117', '-60.8117', '55', 'SA', 'AR', 'AR-S', 'Santa Fe', 'yes', 'SAAV', 'SFN', 'SVO', '', '', ''),
('0', 'Sân bay Quốc tế Tân Sơn Nhất', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '106° 39'' 21.22', '10° 49'' 5.623"', 'public', '10', 'SGN', 'VVTS', 'Международный Аэропорт Таншо', '106.6558957', '10.8182287', '26708', 'VVTS', 'large_airport', 'Tan Son Nhat International Airport', '10.8188', '106.652', '33', 'AS', 'VN', 'VN-23', 'Ho Chi Minh City', 'yes', 'VVTS', 'SGN', '', 'http://www.tsnairport.hochiminhcity.gov.vn/', 'https://en.wikipedia.org/wiki/Tan_Son_Nhat_International_Airport', 'Tansonnhat, Sài Gòn, Saigon, Sân bay Quốc tế Tân Sơn Nhất, Tan Son Nhut Air Base, Tan Son Nhut Airfield'),
('0', '中標津空港', 'KSJ2:AAC=01692
KSJ2:AAC_label=北海道標津郡中標津町', '144° 57'' 25.50', '43° 34'' 36.171', ' ', '0', 'SHB', 'RJCN', 'Аэропорт Накасибецу', '144.9570855', '43.5767142', '5549', 'RJCN', 'medium_airport', 'Nakashibetsu Airport', '43.5774993896', '144.960006714', '234', 'AS', 'JP', 'JP-01', 'Nakashibetsu', 'yes', 'RJCN', 'SHB', '', '', 'https://en.wikipedia.org/wiki/Nakashibetsu_Airport', ''),
('0', '南紀白浜空港', 'KSJ2:AAC=30401
KSJ2:AAC_label=和歌山県西牟婁郡白浜', '135° 21'' 53.55', '33° 39'' 44.725', 'public', '91', 'SHM', 'RJBD', ' ', '135.3648751', '33.6624235', '5537', 'RJBD', 'medium_airport', 'Nanki Shirahama Airport', '33.6622009277', '135.363998413', '298', 'AS', 'JP', 'JP-30', 'Shirahama', 'yes', 'RJBD', 'SHM', '', '', 'https://en.wikipedia.org/wiki/Nanki-Shirahama_Airport', ''),
('0', 'Aeropuerto Fernando Luis Ribas Dominicci', 'aeroway=aerodrome
alt_name=Aeropuerto de Isla Grande
ele=3
i', '66° 5'' 53.440"', '18° 27'' 22.338', ' ', '3', 'SIG', 'TJIG', ' ', '-66.0981778', '18.4562051', '6381', 'TJIG', 'medium_airport', 'Fernando Luis Ribas Dominicci Airport', '18.4568004608154', '-66.0980987548828', '10', 'NA', 'PR', 'PR-U-A', 'San Juan', 'yes', 'TJIG', 'SIG', 'SIG', '', 'https://en.wikipedia.org/wiki/Fernando_Luis_Ribas_Dominicci_Airport', 'Isla Grande'),
('0', 'Changi Airport', 'aerodrome=international
aerodrome:type=military/public
aerow', '103° 59'' 15.56', '1° 21'' 18.078"', 'military/public', '7', 'SIN', 'WSSS', ' ', '103.9876561', '1.3550216', '26887', 'WSSS', 'large_airport', 'Singapore Changi Airport', '1.35019', '103.994003', '22', 'AS', 'SG', 'SG-04', 'Singapore', 'yes', 'WSSS', 'SIN', '', 'http://www.changiairport.com/', 'https://en.wikipedia.org/wiki/Singapore_Changi_Airport', 'RAF Changi'),
('0', 'Norman Y. Mineta San José International Airport', 'ALAND=5282101
AREAID=110173687269
AWATER=0
COUNTYFP=085
MTFC', '121° 55'' 48.32', '37° 21'' 50.904', ' ', '17', 'SJC', 'KSJC', ' ', '-121.9300915', '37.3641401', '3883', 'KSJC', 'large_airport', 'Norman Y. Mineta San Jose International Airport', '37.362598', '-121.929001', '62', 'NA', 'US', 'US-CA', 'San Jose', 'yes', 'KSJC', 'SJC', 'SJC', 'http://www.flysanjose.com/', 'https://en.wikipedia.org/wiki/San_Jose_International_Airport', 'QSF, QBA'),
('0', 'Aeropuerto Intl. de Los Cabos', 'aerodrome:type=public
aeroway=aerodrome
closest_town=San Jos', '109° 43'' 7.450', '23° 9'' 4.674"', 'public', '114', 'SJD', 'MMSD', ' ', '-109.718736', '23.1512983', '4750', 'MMSD', 'large_airport', 'Los Cabos International Airport', '23.1518001556396', '-109.721000671387', '374', 'NA', 'MX', 'MX-BCS', 'San José del Cabo', 'yes', 'MMSD', 'SJD', '', 'http://loscabos.aeropuertosgap.com.mx/index.php?lang=eng', 'https://en.wikipedia.org/wiki/Los_Cabos_International_Airport', ''),
('0', 'Jorge Enrique González', 'aeroway=aerodrome
ele=184
iata=SJE
icao=SKSJ
is_in:city=San', '72° 38'' 20.268', '2° 34'' 51.293"', ' ', '184', 'SJE', 'SKSJ', ' ', '-72.6389633', '2.5809147', '6160', 'SKSJ', 'medium_airport', 'Jorge E. Gonzalez Torres Airport', '2.57969', '-72.6394', '605', 'SA', 'CO', 'CO-GUV', 'San José Del Guaviare', 'yes', 'SKSJ', 'SJE', 'SJE', '', 'https://en.wikipedia.org/wiki/Jorge_Enrique_Gonz%C3%A1lez_Torres_Airport', ''),
('0', 'Aeroporto Regional de São Gabriel da Cachoeira', 'aeroway=aerodrome
alt_name=Aeroporto de Uaupés
description=', '66° 59'' 8.850"', '0° 8'' 54.051"', ' ', '0', 'SJL', 'SBUA', 'Сан Габриэль да Кахоэйра Аэро', '-66.9857918', '-0.1483474', '5990', 'SBUA', 'medium_airport', 'São Gabriel da Cachoeira Airport', '-0.14835', '-66.9855', '251', 'SA', 'BR', 'BR-AM', 'São Gabriel Da Cachoeira', 'yes', 'SBUA', 'SJL', '', '', 'https://en.wikipedia.org/wiki/São_Gabriel_da_Cachoeira_Airport', ''),
('0', 'Aeropuerto Internacional Juan Santamaría', 'addr:city=Alajuela
addr:country=CR
addr:postcode=200-4003
ad', '84° 12'' 32.901', '9° 59'' 35.886"', ' ', '0', 'SJO', 'MROC', ' ', '-84.2091391', '9.9933018', '4810', 'MROC', 'medium_airport', 'Juan Santamaría International Airport', '9.99386', '-84.208801', '3021', 'NA', 'CR', 'CR-A', 'San José (Alajuela)', 'yes', 'MROC', 'SJO', '', '', 'https://en.wikipedia.org/wiki/Juan_Santamar%C3%ADa_International_Airport', ''),
('0', 'Aeroporto Estadual Professor Eribelto Manoel Reino de São J', 'aeroway=aerodrome
barrier=fence
ele=543
iata=SJP
icao=SBSR
n', '49° 24'' 29.261', '20° 49'' 1.743"', ' ', '543', 'SJP', 'SBSR', ' ', '-49.408128', '-20.8171507', '5975', 'SBSR', 'medium_airport', 'Prof. Eribelto Manoel Reino State Airport', '-20.8166007996', '-49.40650177', '1784', 'SA', 'BR', 'BR-SP', 'São José Do Rio Preto', 'yes', 'SBSR', 'SJP', '', '', 'https://en.wikipedia.org/wiki/S%C3%A3o_Jos%C3%A9_do_Rio_Preto_Airport', 'São José do Rio Preto Airport'),
('0', 'Aeropuerto Internacional Luis Muñoz Marín', 'aeroway=aerodrome
iata=SJU
icao=TJSJ
is_in=San Juan,Puerto R', '66° 0'' 8.553"', '18° 26'' 34.986', ' ', '0', 'SJU', 'TJSJ', ' ', '-66.0023757', '18.4430518', '6384', 'TJSJ', 'large_airport', 'Luis Munoz Marin International Airport', '18.4393997192', '-66.0018005371', '9', 'NA', 'PR', 'PR-U-A', 'San Juan', 'yes', 'TJSJ', 'SJU', 'SJU', '', 'https://en.wikipedia.org/wiki/Luis_Mu%C3%B1oz_Mar%C3%ADn_International_Airport', 'Isla Verde'),
('0', 'Robert L. Bradshaw International Airport', 'aeroway=aerodrome
closest_town=Basseterre
iata=SKB
icao=TKPK', '62° 43'' 6.971"', '17° 18'' 40.278', ' ', '0', 'SKB', 'TKPK', ' ', '-62.7186031', '17.3111883', '6386', 'TKPK', 'medium_airport', 'Robert L. Bradshaw International Airport', '17.3111991882324', '-62.7187004089356', '170', 'NA', 'KN', 'KN-U-A', 'Basseterre', 'yes', 'TKPK', 'SKB', '', '', 'https://en.wikipedia.org/wiki/Robert_L._Bradshaw_International_Airport', ''),
('0', 'Samarkand International Airport', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Между', '66° 58'' 51.783', '39° 41'' 52.675', 'public', '679', 'SKD', 'UTSS', 'Международный аэропорт Самар', '66.9810507', '39.6979654', '26390', 'UTSS', 'medium_airport', 'Samarkand Airport', '39.7005004882812', '66.9838027954102', '2224', 'AS', 'UZ', 'UZ-SA', 'Samarkand', 'yes', 'UTSS', 'SKD', '', '', 'https://en.wikipedia.org/wiki/Samarkand_Airport', ''),
('0', 'Sukkur Airport', 'aeroway=aerodrome
closest_town=Sukkur
ele=60
iata=SKZ
icao=O', '68° 47'' 34.073', '27° 43'' 17.040', ' ', '60', 'SKZ', 'OPSK', ' ', '68.792798', '27.7214', '5279', 'OPSK', 'medium_airport', 'Sukkur Airport', '27.7220001220703', '68.7917022705078', '196', 'AS', 'PK', 'PK-SD', 'Mirpur Khas', 'yes', 'OPSK', 'SKZ', '', '', 'https://en.wikipedia.org/wiki/Sukkur_Airport', ''),
('0', 'Ponciano Arriaga International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=San Lui', '100° 56'' 12.48', '22° 15'' 37.020', 'public', '1839', 'SLP', 'MMSP', ' ', '-100.9368014', '22.2602833', '4752', 'MMSP', 'medium_airport', 'Ponciano Arriaga International Airport', '22.2542991638', '-100.930999756', '6035', 'NA', 'MX', 'MX-SLP', 'San Luis Potosí', 'yes', 'MMSP', 'SLP', '', '', 'https://en.wikipedia.org/wiki/Ponciano_Arriaga_International_Airport', 'Estafeta'),
('0', 'George F. L. Charles Airport', 'aeroway=aerodrome
alt_name=Vigie
ele=7
iata=SLU
icao=TLPC
is', '60° 59'' 35.143', '14° 1'' 11.554"', ' ', '7', 'SLU', 'TLPC', ' ', '-60.9930953', '14.0198762', '6388', 'TLPC', 'medium_airport', 'George F. L. Charles Airport', '14.0202', '-60.992901', '22', 'NA', 'LC', 'LC-02', 'Castries', 'yes', 'TLPC', 'SLU', '', 'http://www.slaspa.com/contentPages/view/george-f-l-charles-airport', 'https://en.wikipedia.org/wiki/George_F._L._Charles_Airport', 'Vigie Airport'),
('0', 'Aeropuerto Intl. Plan de Guadalupe', 'aerodrome:type=public
aeroway=aerodrome
city_served=Saltillo', '100° 55'' 51.69', '25° 32'' 17.444', 'public', '1456', 'SLW', 'MMIO', ' ', '-100.9310264', '25.5381788', '4716', 'MMIO', 'medium_airport', 'Plan De Guadalupe International Airport', '25.5494995117188', '-100.929000854492', '4778', 'NA', 'MX', 'MX-COA', 'Saltillo', 'yes', 'MMIO', 'SLW', '', '', 'https://en.wikipedia.org/wiki/Plan_de_Guadalupe_International_Airport', ''),
('0', 'Sacramento International Airport', 'addr:state=CA
aeroway=aerodrome
ele=8
gnis:county_name=Sacra', '121° 35'' 35.29', '38° 41'' 34.817', ' ', '8', 'SMF', 'KSMF', ' ', '-121.593136', '38.6930046', '3892', 'KSMF', 'large_airport', 'Sacramento International Airport', '38.6954002380371', '-121.591003417969', '27', 'NA', 'US', 'US-CA', 'Sacramento', 'yes', 'KSMF', 'SMF', 'SMF', '', 'https://en.wikipedia.org/wiki/Sacramento_International_Airport', ''),
('0', 'Stella Maris Airport', 'addr:city=Stella Maris
addr:country=BS
aeroway=aerodrome
ele', '75° 16'' 4.338"', '23° 34'' 58.759', ' ', '3', 'SML', 'MYLS', ' ', '-75.2678717', '23.5829886', '4949', 'MYLS', 'medium_airport', 'Stella Maris Airport', '23.582317', '-75.268621', '10', 'NA', 'BS', 'BS-LI', 'Stella Maris', 'yes', 'MYLS', 'SML', '', '', 'https://en.wikipedia.org/wiki/Stella_Maris_Airport', ''),
('0', 'Simón Bolívar', 'aeroway=aerodrome
iata=SMR
icao=SKSM
name=Simón Bolívar', '74° 13'' 53.741', '11° 7'' 8.958"', ' ', '0', 'SMR', 'SKSM', ' ', '-74.2315946', '11.119155', '6161', 'SKSM', 'medium_airport', 'Simón Bolívar International Airport', '11.1196', '-74.2306', '22', 'SA', 'CO', 'CO-MAG', 'Santa Marta', 'yes', 'SKSM', 'SMR', 'SMR', '', 'https://en.wikipedia.org/wiki/Sim%C3%B3n_Bol%C3%ADvar_International_Airport_(Colombia)', ''),
('0', 'Smara Airport', 'aeroway=aerodrome
ele=106.7
iata=SMW
icao=GMMA
name=Smara Ai', '11° 41'' 8.597"', '26° 43'' 58.665', ' ', '107', 'SMW', 'GMMA', ' ', '-11.6857213', '26.7329625', '3108', 'GMMA', 'medium_airport', 'Smara Airport', '26.731987', '-11.683655', '350', 'AF', 'EH', 'EH-U-A', 'Smara', 'yes', 'GMMA', 'SMW', '', '', 'https://en.wikipedia.org/wiki/Smara_Airport', 'GSMA, Semara'),
('0', 'Santa Maria Public Airport/Capt. G Allan Hancock Field', 'Tiger:MTFCC=K2457
addr:state=CA
aeroway=aerodrome
ele=68
gni', '120° 27'' 42.49', '34° 54'' 6.562"', ' ', '68', 'SMX', 'KSMX', ' ', '-120.4618033', '34.9018228', '3893', 'KSMX', 'medium_airport', 'Santa Maria Pub/Capt G Allan Hancock Field', '34.89889908', '-120.4570007', '261', 'NA', 'US', 'US-CA', 'Santa Maria', 'yes', 'KSMX', 'SMX', 'SMX', '', 'https://en.wikipedia.org/wiki/Santa_Maria_Public_Airport', ''),
('0', 'သံတွဲ လေဆိပ်', 'aeroway=aerodrome
amenity=airport
iata=SNW
icao=VYTD
name=', '94° 18'' 4.708"', '18° 27'' 53.684', ' ', '0', 'SNW', 'VYTD', ' ', '94.3013079', '18.4649121', '26741', 'VYTD', 'medium_airport', 'Thandwe Airport', '18.4606990814209', '94.3001022338867', '20', 'AS', 'MM', 'MM-16', 'Thandwe', 'yes', 'VYTD', 'SNW', '', '', 'https://en.wikipedia.org/wiki/Thandwe_Airport', ''),
('0', 'Santo Pekoa International Airport', 'aerodrome:type=public
aeroway=aerodrome
iata=SON
icao=NVSS
l', '167° 13'' 10.12', '15° 30'' 15.309', 'public', '0', 'SON', 'NVSS', ' ', '167.2194783', '-15.5042524', '5010', 'NVSS', 'medium_airport', 'Santo Pekoa International Airport', '-15.5050001144', '167.220001221', '184', 'OC', 'VU', 'VU-SAM', 'Luganville', 'yes', 'NVSS', 'SON', '', 'http://www.airports.vu/Santo%20Pekoa/Santo%20Pekoa.htm', 'https://en.wikipedia.org/wiki/Santo-Pekoa_International_Airport', ''),
('0', 'Saipan International Airport', 'addr:state=WP
aerodrome:type=public
aeroway=aerodrome
alt_na', '145° 43'' 44.41', '15° 7'' 13.251"', 'public', '61', 'SPN', 'PGSN', ' ', '145.7290038', '15.1203475', '5431', 'PGSN', 'medium_airport', 'Saipan International Airport', '15.119', '145.729004', '215', 'OC', 'MP', 'MP-U-A', 'Saipan Island', 'yes', 'PGSN', 'SPN', 'GSN', '', 'https://en.wikipedia.org/wiki/Saipan_International_Airport', 'Francisco C. Ada'),
('0', 'Menongue Airport', 'aeroway=aerodrome
iata=SPP
icao=FNME
name=Menongue Airport
s', '17° 43'' 14.761', '14° 39'' 28.400', ' ', '0', 'SPP', 'FNME', ' ', '17.720767', '-14.657889', '2945', 'FNME', 'medium_airport', 'Menongue Airport', '-14.657600402832', '17.7198009490967', '4469', 'AF', 'AO', 'AO-CCU', 'Menongue', 'yes', 'FNME', 'SPP', '', '', 'https://en.wikipedia.org/wiki/Menongue_Airport', ''),
('0', 'Sarasota-Bradenton International Airport', 'addr:city=Sarasota
addr:housenumber=6000
addr:postcode=34243', '82° 33'' 19.504', '27° 23'' 41.323', ' ', '9', 'SRQ', 'KSRQ', ' ', '-82.5554178', '27.394812', '3899', 'KSRQ', 'large_airport', 'Sarasota Bradenton International Airport', '27.3953990936279', '-82.5543975830078', '30', 'NA', 'US', 'US-FL', 'Sarasota/Bradenton', 'yes', 'KSRQ', 'SRQ', 'SRQ', '', 'https://en.wikipedia.org/wiki/Sarasota-Bradenton_International_Airport', ''),
('0', 'Aeropuerto El Trompillo', 'aerodrome:type=public
aeroway=aerodrome
barrier=yes
iata=SRZ', '63° 10'' 17.207', '17° 48'' 40.125', 'public', '0', 'SRZ', 'SLET', ' ', '-63.1714465', '-17.8111459', '6183', 'SLET', 'medium_airport', 'El Trompillo Airport', '-17.8115997314', '-63.1715011597', '1371', 'SA', 'BO', 'BO-S', 'Santa Cruz', 'yes', 'SLET', 'SRZ', '', '', 'https://en.wikipedia.org/wiki/El_Trompillo_Airport', ''),
('0', 'Aeropuerto de Malabo', 'aerodrome=public
aeroway=aerodrome
alt_name=Aeropuerto de Sa', '8° 42'' 26.460"', '3° 45'' 23.036"', ' ', '23', 'SSG', 'FGSL', ' ', '8.70735', '3.756399', '2876', 'FGSL', 'medium_airport', 'Malabo Airport', '3.75527000427246', '8.70872020721436', '76', 'AF', 'GQ', 'GQ-BN', 'Malabo', 'yes', 'FGSL', 'SSG', '', '', 'https://en.wikipedia.org/wiki/Malabo_International_Airport', 'Fernando Poo'),
('0', 'Aeropuerto Internacional Buenaventura Vivas', 'aeroway=aerodrome
iata=STD
icao=SVSO
int_name=Santo Domingo', '72° 2'' 7.753"', '7° 33'' 56.277"', ' ', '0', 'STD', 'SVSO', ' ', '-72.0354869', '7.5656325', '6325', 'SVSO', 'medium_airport', 'Mayor Buenaventura Vivas International Airport', '7.56538', '-72.035103', '1083', 'SA', 'VE', 'VE-S', 'Santo Domingo', 'yes', 'SVSO', 'STD', '', '', 'https://en.wikipedia.org/wiki/Mayor_Buenaventura_Vivas_Airport', ''),
('0', 'Aeropuerto Internacional del Cibao', 'aeroway=aerodrome
iata=STI
icao=MDST
name=Aeropuerto Interna', '70° 36'' 17.511', '19° 24'' 18.725', ' ', '0', 'STI', 'MDST', ' ', '-70.6048641', '19.4052013', '4640', 'MDST', 'medium_airport', 'Cibao International Airport', '19.406099319458', '-70.6046981811523', '565', 'NA', 'DO', 'DO-25', 'Santiago', 'yes', 'MDST', 'STI', '', '', 'https://en.wikipedia.org/wiki/Cibao_International_Airport', ''),
('0', 'Sonoma County Airport', 'addr:state=CA
aeroway=aerodrome
closest_town=Santa Rosa, Cal', '122° 48'' 46.52', '38° 30'' 33.001', ' ', '37', 'STS', 'KSTS', ' ', '-122.8129248', '38.509167', '21114', 'KSTS', 'medium_airport', 'Charles M. Schulz Sonoma County Airport', '38.50899887', '-122.8130035', '128', 'NA', 'US', 'US-CA', 'Santa Rosa', 'yes', 'KSTS', 'STS', 'STS', 'http://www.sonomacountyairport.org/', 'https://en.wikipedia.org/wiki/Charles_M._Schulz_-_Sonoma_County_Airport', ''),
('0', 'Cyril E King Airport', 'aeroway=aerodrome
ele=9
gnis:county_name=St. Thomas
gnis:cre', '64° 58'' 17.575', '18° 20'' 5.236"', ' ', '9', 'STT', 'TIST', ' ', '-64.9715485', '18.3347877', '6369', 'TIST', 'medium_airport', 'Cyril E. King Airport', '18.3372993469238', '-64.9733963012695', '23', 'NA', 'VI', 'VI-U-A', 'Charlotte Amalie, Harry S. Truman Airport', 'yes', 'TIST', 'STT', 'STT', 'http://www.viport.com/airports.html', 'https://en.wikipedia.org/wiki/Cyril_E._King_Airport', ''),
('0', 'Surat Airport', 'aeroway=aerodrome
iata=STV
icao=VASU
name=Surat Airport
name', '72° 44'' 43.862', '21° 6'' 55.487"', ' ', '0', 'STV', 'VASU', ' ', '72.7455173', '21.1154131', '26461', 'VASU', 'medium_airport', 'Surat Airport', '21.1140995026', '72.7417984009', '16', 'AS', 'IN', 'IN-GJ', '', 'yes', 'VASU', 'STV', '', '', '', ''),
('0', 'Henry E. Rohlsen Airport', 'FAA=STX
addr:state=VI
aerodrome:type=public
aeroway=aerodrom', '64° 48'' 19.383', '17° 42'' 0.274"', 'public', '6', 'STX', 'TISX', ' ', '-64.8053843', '17.700076', '6370', 'TISX', 'medium_airport', 'Henry E Rohlsen Airport', '17.7019004821777', '-64.7985992431641', '74', 'NA', 'VI', 'VI-U-A', 'Christiansted', 'yes', 'TISX', 'STX', 'STX', '', 'https://en.wikipedia.org/wiki/Henry_E._Rohlsen_Airport', ''),
('0', 'Bandar Udara Internasional Juanda', 'aeroway=aerodrome
iata=SUB
icao=WARR
name=Bandar Udara Inter', '112° 47'' 7.708', '7° 22'' 46.722"', ' ', '0', 'SUB', 'WARR', ' ', '112.7854745', '-7.379645', '26789', 'WARR', 'large_airport', 'Juanda International Airport', '-7.37982988357544', '112.787002563477', '9', 'AS', 'ID', 'ID-JI', 'Surabaya', 'yes', 'WARR', 'SUB', '', 'http://www.juanda-airport.com/', 'https://en.wikipedia.org/wiki/Juanda_International_Airport', 'WRSJ'),
('0', 'Surigao Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=6
iata=SUG
icao=', '125° 28'' 53.34', '9° 45'' 27.721"', 'public', '6', 'SUG', 'RPMS', ' ', '125.4814846', '9.7577002', '5710', 'RPMS', 'medium_airport', 'Surigao Airport', '9.75583832563', '125.480947495', '20', 'AS', 'PH', 'PH-SUN', 'Surigao City', 'yes', 'RPMS', 'SUG', '', '', 'https://en.wikipedia.org/wiki/Surigao_Airport', ''),
('0', 'Nausori International Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=5
iata=SUV
icao=', '178° 33'' 33.75', '18° 2'' 35.377"', 'public', '5', 'SUV', 'NFNA', ' ', '178.5593765', '-18.0431604', '4960', 'NFNA', 'medium_airport', 'Nausori International Airport', '-18.0433006286621', '178.559005737305', '17', 'OC', 'FJ', 'FJ-C', 'Nausori', 'yes', 'NFNA', 'SUV', '', '', 'https://en.wikipedia.org/wiki/Nausori_International_Airport', ''),
('0', 'Aeropuerto Juan Vicente Gomez', 'aeroway=aerodrome
alt_name=SVZ
iata=SVZ
icao=SVSA
int_name=S', '72° 26'' 23.390', '7° 50'' 26.249"', ' ', '0', 'SVZ', 'SVSA', ' ', '-72.4398306', '7.8406247', '6321', 'SVSA', 'medium_airport', 'San Antonio Del Tachira Airport', '7.84082984924316', '-72.439697265625', '1312', 'SA', 'VE', 'VE-S', '', 'yes', 'SVSA', 'SVZ', '', '', '', ''),
('0', '揭阳潮汕国际机场', 'addr:city=揭阳市
addr:district=登岗镇
addr:province=', '116° 30'' 11.77', '23° 33'' 3.151"', 'international', '0', 'SWA', 'ZGOW', ' ', '116.5032717', '23.5508753', '32400', 'ZGOW', 'medium_airport', 'Jieyang Chaoshan International Airport', '23.552', '116.5033', NULL, 'AS', 'CN', 'CN-44', 'Jieyang (Rongcheng)', 'yes', 'ZGOW', 'SWA', '', '', 'https://en.wikipedia.org/wiki/Jieyang_Chaoshan_International_Airport', ''),
('0', 'Kingsford Smith International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '151° 10'' 42.67', '33° 57'' 0.041"', 'public', '6', 'SYD', 'YSSY', ' ', '151.1785197', '-33.9500113', '27145', 'YSSY', 'large_airport', 'Sydney Kingsford Smith International Airport', '-33.9460983276367', '151.177001953125', '21', 'OC', 'AU', 'AU-NSW', 'Sydney', 'yes', 'YSSY', 'SYD', '', 'http://www.sydneyairport.com.au/', 'https://en.wikipedia.org/wiki/Kingsford_Smith_International_Airport', 'RAAF Station Mascot'),
('0', 'Aeropuerto Internacional Tobías Bolaños', 'aerodrome:type=public
aeroway=aerodrome
ele=1002
iata=SYQ
ic', '84° 8'' 20.237"', '9° 57'' 28.248"', 'public', '1002', 'SYQ', 'MRPV', ' ', '-84.1389547', '9.9578466', '4813', 'MRPV', 'medium_airport', 'Tobías Bolaños International Airport', '9.95705', '-84.139801', '3287', 'NA', 'CR', 'CR-SJ', 'San Jose', 'yes', 'MRPV', 'SYQ', '', '', 'https://en.wikipedia.org/wiki/Tob%C3%ADas_Bola%C3%B1os_International_Airport', 'Aeropuerto Internacional Tobías Bolaños Palma'),
('0', 'Sehwan Sharif Airport', 'aeroway=aerodrome
closest_town=Sehwan Sharif
iata=SYW
icao=O', '67° 42'' 56.490', '26° 28'' 25.595', ' ', '0', 'SYW', 'OPSN', ' ', '67.7156916', '26.4737763', '30590', 'OPSN', 'medium_airport', 'Sehwan Sharif Airport', '26.4731006622314', '67.7172012329102', '121', 'AS', 'PK', 'PK-SD', 'Sehwan Sharif', 'yes', 'OPSN', 'SYW', '', '', 'https://en.wikipedia.org/wiki/Sehwan_Sharif_Airport', ''),
('0', '三亚凤凰国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=SYX
icao', '109° 24'' 53.67', '18° 18'' 17.650', 'international', '0', 'SYX', 'ZJSY', 'Аэропорт Феникс', '109.4149088', '18.3049028', '27202', 'ZJSY', 'large_airport', 'Sanya Phoenix International Airport', '18.3029', '109.412003', '92', 'AS', 'CN', 'CN-46', 'Sanya (Tianya)', 'yes', 'ZJSY', 'SYX', '', '', 'https://en.wikipedia.org/wiki/Sanya_Phoenix_International_Airport', ''),
('0', 'Soyo Airport', 'aeroway=aerodrome
iata=SZA
icao=FNSO
name=Soyo Airport
sourc', '12° 22'' 17.777', '6° 8'' 28.246"', ' ', '0', 'SZA', 'FNSO', ' ', '12.3716046', '-6.1411794', '2950', 'FNSO', 'medium_airport', 'Soyo Airport', '-6.14108991622925', '12.3718004226685', '15', 'AF', 'AO', 'AO-ZAI', 'Soyo', 'yes', 'FNSO', 'SZA', '', '', 'https://en.wikipedia.org/wiki/Soyo_Airport', ''),
('0', 'Lapangan Terbang Sultan Abdul Aziz Shah', 'aeroway=aerodrome
alt_name=Subang Airport
ele=27
iata=SZB
ic', '101° 33'' 10.13', '3° 7'' 54.374"', ' ', '27', 'SZB', 'WMSA', ' ', '101.5528156', '3.1317706', '26879', 'WMSA', 'medium_airport', 'Sultan Abdul Aziz Shah International Airport', '3.13057994842529', '101.549003601074', '90', 'AS', 'MY', 'MY-10', 'Subang', 'yes', 'WMSA', 'SZB', '', '', 'https://en.wikipedia.org/wiki/Sultan_Abdul_Aziz_Shah_Airport', ''),
('0', 'A.N.R. Robinson International Airport', 'addr:city=Crown Point
addr:street=Airport Road
aerodrome:typ', '60° 49'' 48.198', '11° 9'' 1.568"', 'public', '12', 'TAB', 'TTCP', ' ', '-60.8300551', '11.1504355', '6409', 'TTCP', 'medium_airport', 'Tobago-Crown Point Airport', '11.1497001647949', '-60.8321990966797', '38', 'NA', 'TT', 'TT-WTO', 'Scarborough', 'yes', 'TTCP', 'TAB', '', '', 'https://en.wikipedia.org/wiki/Crown_Point_Airport', ''),
('0', 'Tanna Airport', 'aeroway=aerodrome
ele=6
iata=TAH
icao=NVVW
length=1230
name=', '169° 13'' 29.66', '19° 27'' 18.135', ' ', '6', 'TAH', 'NVVW', ' ', '169.2249072', '-19.4550374', '5012', 'NVVW', 'medium_airport', 'Tanna Airport', '-19.455099105835', '169.223999023438', '19', 'OC', 'VU', 'VU-TAE', '', 'yes', 'NVVW', 'TAH', '', '', 'https://en.wikipedia.org/wiki/White_Grass_Airport', ''),
('0', '高松空港', 'KSJ2:AAC=37201
KSJ2:AAC_label=香川県高松市
KSJ2:AD2=1
', '134° 0'' 54.481', '34° 12'' 54.995', 'public', '185', 'TAK', 'RJOT', ' ', '134.0151337', '34.2152765', '5601', 'RJOT', 'medium_airport', 'Takamatsu Airport', '34.2141990662', '134.01600647', '607', 'AS', 'JP', 'JP-37', 'Takamatsu', 'yes', 'RJOT', 'TAK', '', '', 'https://en.wikipedia.org/wiki/Takamatsu_Airport', ''),
('0', 'Aeropuerto Internacional General Francisco Javier Mina', 'aeroway=aerodrome
iata=TAM
icao=MMTM
name=Aeropuerto Interna', '97° 51'' 57.517', '22° 17'' 25.994', ' ', '0', 'TAM', 'MMTM', ' ', '-97.8659769', '22.2905538', '4758', 'MMTM', 'medium_airport', 'General Francisco Javier Mina International Airport', '22.2964000702', '-97.8658981323', '80', 'NA', 'MX', 'MX-TAM', 'Tampico', 'yes', 'MMTM', 'TAM', '', '', 'https://en.wikipedia.org/wiki/General_Francisco_Javier_Mina_International_Airport', ''),
('0', '青岛流亭国际机场 Qingdao Liuting International Airpo', 'aerodrome:type=international
aeroway=aerodrome
closest_town=', '120° 22'' 39.33', '36° 15'' 56.577', 'international', '10', 'TAO', 'ZSQD', 'Аэропорт Циндао', '120.3775926', '36.2657158', '27224', 'ZSQD', 'medium_airport', 'Liuting Airport', '36.2661018372', '120.374000549', '33', 'AS', 'CN', 'CN-37', 'Qingdao', 'yes', 'ZSQD', 'TAO', '', '', 'https://en.wikipedia.org/wiki/Qingdao_Liuting_International_Airport', ''),
('0', 'Sân bay Tuy Hòa', 'aeroway=aerodrome
closest_town=Tuy Hoa
ele=6
iata=TBB
icao=V', '109° 20'' 7.952', '13° 2'' 53.866"', ' ', '6', 'TBB', 'VVTH', 'Туихоа', '109.3355423', '13.0482962', '26707', 'VVTH', 'medium_airport', 'Dong Tac Airport', '13.0495996475', '109.333999634', '20', 'AS', 'VN', 'VN-49', 'Tuy Hoa', 'yes', 'VVTH', 'TBB', '', '', 'https://en.wikipedia.org/wiki/%C4%90%C3%B4ng_T%C3%A1c_(Tuy_Hoa)_Airport', ''),
('0', 'Mala‘e Vakapuna Fakavaha‘apule‘anga Fua‘amotu', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Nuku''al', '175° 8'' 57.075', '21° 14'' 31.930', 'public', '38', 'TBU', 'NFTF', ' ', '-175.1491874', '-21.2422029', '4962', 'NFTF', 'medium_airport', 'Fua''amotu International Airport', '-21.2411994934082', '-175.149993896484', '126', 'OC', 'TO', 'TO-04', 'Nuku''alofa', 'yes', 'NFTF', 'TBU', '', '', 'https://en.wikipedia.org/wiki/Fua''amotu_International_Airport', ''),
('0', 'Tennant Creek Airport', 'aeroway=aerodrome
iata=TCA
icao=YTNK
name=Tennant Creek Airp', '134° 10'' 47.85', '19° 38'' 2.392"', ' ', '0', 'TCA', 'YTNK', ' ', '134.1799585', '-19.6339979', '27159', 'YTNK', 'medium_airport', 'Tennant Creek Airport', '-19.6343994140625', '134.182998657227', '1236', 'OC', 'AU', 'AU-NT', 'Tennant Creek', 'yes', 'YTNK', 'TCA', '', 'http://www.tennantcreekairport.com.au/', 'https://en.wikipedia.org/wiki/Tennant_Creek_Airport', ''),
('0', 'Aeroporto de Tefé', 'aeroway=aerodrome
iata=TFF
icao=SBTF
name=Aeroporto de Tefé', '64° 43'' 25.487', '3° 22'' 45.777"', ' ', '0', 'TFF', 'SBTF', ' ', '-64.7237464', '-3.3793825', '5983', 'SBTF', 'medium_airport', 'Tefé Airport', '-3.38294005394', '-64.7240982056', '188', 'SA', 'BR', 'BR-AM', 'Tefé', 'yes', 'SBTF', 'TFF', '', '', 'https://en.wikipedia.org/wiki/Tef%C3%A9_Airport', ''),
('0', '通辽机场', 'aeroway=aerodrome
barrier=fence
ele=730
iata=TGO
icao=ZBTL
n', '122° 11'' 57.95', '43° 33'' 27.806', ' ', '730', 'TGO', 'ZBTL', ' ', '122.1994308', '43.5577239', '32440', 'ZBTL', 'medium_airport', 'Tongliao Airport', '43.556702', '122.199997', '2395', 'AS', 'CN', 'CN-15', 'Tongliao', 'yes', 'ZBTL', 'TGO', '', '', 'https://en.wikipedia.org/wiki/Tongliao_Airport', ''),
('0', 'Aeropuerto Internacional Ángel Albino Corzo', 'OACI=MMTG
aeroway=aerodrome
ele=457
iata=TGZ
icao=MMTG
name=', '93° 1'' 23.491"', '16° 33'' 27.252', ' ', '457', 'TGZ', 'MMTG', ' ', '-93.023192', '16.55757', '43048', 'MMTG', 'medium_airport', 'Angel Albino Corzo International Airport', '16.5636005402', '-93.0224990845', '1499', 'NA', 'MX', 'MX-CHP', 'Tuxtla Gutiérrez', 'yes', 'MMTG', 'TGZ', 'TGZ', 'http://www.chiapas.aero/index.html', 'https://en.wikipedia.org/wiki/Angel_Albino_Corzo_International_Airport', ''),
('0', 'တာချီလိတ် လေဆိပ်', 'aeroway=aerodrome
iata=THL
icao=VYTL
name=တာချီလ', '99° 56'' 5.965"', '20° 29'' 7.187"', ' ', '0', 'THL', 'VYTL', ' ', '99.9349902', '20.4853297', '26742', 'VYTL', 'medium_airport', 'Tachileik Airport', '20.4838008880615', '99.9354019165039', '1280', 'AS', 'MM', 'MM-17', 'Tachileik', 'yes', 'VYTL', 'THL', '', '', 'https://en.wikipedia.org/wiki/Tachilek_Airport', ''),
('0', '天水麦积山机场', 'aeroway=aerodrome
iata=THQ
icao=ZLTS
name=天水麦积山机', '105° 51'' 34.89', '34° 33'' 33.665', ' ', '0', 'THQ', 'ZLTS', ' ', '105.8596918', '34.5593515', '44208', 'CN-0130', 'medium_airport', 'Tianshui Maijishan Airport', '34.559399', '105.860001', '3590', 'AS', 'CN', 'CN-62', 'Tianshui', 'yes', 'ZLTS', 'THQ', '', '', 'https://en.wikipedia.org/wiki/Tianshui_Maijishan_Airport', 'Tianshui Air Base, 天水麦积山机场, ZBEE'),
('0', 'Aéroport de Tikehau', 'aeroway=aerodrome
iata=TIH
icao=NTGC
name=Aéroport de Tikeh', '148° 13'' 51.34', '15° 7'' 10.148"', ' ', '0', 'TIH', 'NTGC', ' ', '-148.230928', '-15.1194855', '4985', 'NTGC', 'medium_airport', 'Tikehau Airport', '-15.1196002960205', '-148.231002807617', '6', 'OC', 'PF', 'PF-U-A', '', 'yes', 'NTGC', 'TIH', '', '', 'https://en.wikipedia.org/wiki/Tikehau_Airport', ''),
('0', 'Timaru Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '171° 13'' 35.15', '44° 17'' 59.760', 'public', '27', 'TIU', 'NZTU', ' ', '171.2264328', '-44.2999333', '5056', 'NZTU', 'medium_airport', 'Timaru Airport', '-44.3027992248535', '171.225006103516', '89', 'OC', 'NZ', 'NZ-CAN', '', 'yes', 'NZTU', 'TIU', '', '', 'https://en.wikipedia.org/wiki/Richard_Pearse_Airport', ''),
('0', 'Aeropuerto Capitán Oriel Lea Plaza', 'aerodrome:type=military/public
aeroway=aerodrome
barrier=fen', '64° 41'' 59.275', '21° 33'' 22.796', 'military/public', '1854', 'TJA', 'SLTJ', ' ', '-64.6997986', '-21.5563321', '6190', 'SLTJ', 'medium_airport', 'Capitan Oriel Lea Plaza Airport', '-21.5557003021', '-64.7013015747', '6079', 'SA', 'BO', 'BO-T', 'Tarija', 'yes', 'SLTJ', 'TJA', '', '', 'https://en.wikipedia.org/wiki/Cap._Oriel_Lea_Plaza_Airport', ''),
('0', 'Takoradi Airport', 'aerodrome:type=military/public
aeroway=aerodrome
ele=6.4
iat', '1° 46'' 28.402"', '4° 53'' 46.109"', 'military/public', '6', 'TKD', 'DGTK', ' ', '-1.7745561', '4.8961413', '2095', 'DGTK', 'medium_airport', 'Takoradi Airport', '4.8960599899292', '-1.77476000785828', '21', 'AF', 'GH', 'GH-WP', 'Sekondi-Takoradi', 'yes', 'DGTK', 'TKD', '', '', 'https://en.wikipedia.org/wiki/Takoradi_Airport', ''),
('0', 'Chuuk International Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Weno
el', '151° 50'' 34.89', '7° 27'' 42.856"', 'public', '2', 'TKK', 'PTKK', ' ', '151.8430257', '7.4619044', '5499', 'PTKK', 'medium_airport', 'Chuuk International Airport', '7.46187019348145', '151.843002319336', '11', 'OC', 'FM', 'FM-TRK', 'Weno Island', 'yes', 'PTKK', 'TKK', 'TKK', '', 'https://en.wikipedia.org/wiki/Chuuk_International_Airport', ''),
('0', '徳島阿波おどり空港', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '134° 36'' 23.03', '34° 7'' 58.530"', 'military/public', '11', 'TKS', 'RJOS', ' ', '134.6063975', '34.1329249', '5600', 'RJOS', 'medium_airport', 'Tokushima Awaodori Airport / JMSDF Tokushima Air Base', '34.132801', '134.606995', '26', 'AS', 'JP', 'JP-36', 'Tokushima', 'yes', 'RJOS', 'TKS', '', '', 'https://en.wikipedia.org/wiki/Tokushima_Airport; https://ja.wikipedia.org/wiki/%E5%BE%B3%E5%B3%B6%E9%A3%9B%E8%A1%8C%E5%A0%B4#.E8', 'Tokushima'),
('0', 'Aeropuerto Internacional Lic. Adolfo López Mateos', 'aeroway=aerodrome
ele=2580
iata=TLC
icao=MMTO
name=Aeropuert', '99° 33'' 50.458', '19° 20'' 16.163', ' ', '2580', 'TLC', 'MMTO', ' ', '-99.5640162', '19.337823', '4760', 'MMTO', 'medium_airport', 'Licenciado Adolfo Lopez Mateos International Airport', '19.3370990753', '-99.5660018921', '8466', 'NA', 'MX', 'MX-MEX', 'Toluca', 'yes', 'MMTO', 'TLC', '', '', 'https://en.wikipedia.org/wiki/Lic._Adolfo_L%C3%B3pez_Mateos_International_Airport', ''),
('0', 'Termez Airport', 'aeroway=aerodrome
iata=TMJ
icao=UTST
is_in=Uzbekistan, Surxo', '67° 18'' 34.996', '37° 17'' 9.053"', ' ', '0', 'TMJ', 'UTST', 'Аэропорт Термез', '67.3097212', '37.285848', '26391', 'UTST', 'medium_airport', 'Termez Airport', '37.287261', '67.311869', '1027', 'AS', 'UZ', 'UZ-SU', 'Termez', 'yes', 'UTST', 'TMJ', '', '', 'https://en.wikipedia.org/wiki/Termez_Airport', ''),
('0', 'Tamale Airport', 'aeroway=aerodrome
ele=169
iata=TML
icao=DGLE
name=Tamale Air', '0° 51'' 46.355"', '9° 33'' 25.495"', ' ', '169', 'TML', 'DGLE', ' ', '-0.8628765', '9.5570819', '2091', 'DGLE', 'medium_airport', 'Tamale Airport', '9.55718994140625', '-0.863214015960693', '553', 'AF', 'GH', 'GH-NP', 'Tamale', 'yes', 'DGLE', 'TML', '', '', 'https://en.wikipedia.org/wiki/Tamale_Airport', ''),
('0', 'Aéroport de Tamanrasset Aguenar - Hadj Bey Akhamok', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '5° 26'' 18.555"', '22° 48'' 47.690', 'public', '1377', 'TMR', 'DAAT', 'Таманрассетский аэропорт', '5.4384874', '22.8132473', '2056', 'DAAT', 'medium_airport', 'Aguenar – Hadj Bey Akhamok Airport', '22.8115005493', '5.45107984543', '4518', 'AF', 'DZ', 'DZ-11', 'Tamanrasset', 'yes', 'DAAT', 'TMR', '', '', 'https://en.wikipedia.org/wiki/Tamanrasset_Airport', ''),
('0', 'São Tomé International Airport', 'aerodrome=public
aerodrome:type=public
aeroway=aerodrome
clo', '6° 42'' 42.926"', '0° 22'' 38.201"', 'public', '10', 'TMS', 'FPST', ' ', '6.7119238', '0.3772781', '2969', 'FPST', 'medium_airport', 'São Tomé International Airport', '0.378174990415573', '6.71215009689331', '33', 'AF', 'ST', 'ST-S', 'São Tomé', 'yes', 'FPST', 'TMS', '', '', 'https://en.wikipedia.org/wiki/S%C3%A3o_Tom%C3%A9_International_Airport', ''),
('0', 'Trombetas Airport', 'aeroway=aerodrome
iata=TMT
icao=SBTB
name=Trombetas Airport
', '56° 23'' 46.705', '1° 29'' 19.478"', ' ', '0', 'TMT', 'SBTB', ' ', '-56.396307', '-1.4887438', '5979', 'SBTB', 'medium_airport', 'Trombetas Airport', '-1.489599943161', '-56.396800994873', '287', 'SA', 'BR', 'BR-PA', 'Oriximiná', 'yes', 'SBTB', 'TMT', '', '', 'https://en.wikipedia.org/wiki/Porto_Trombetas_Airport', 'Porto Trombetas'),
('0', 'Tamworth Regional Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Tamwort', '150° 50'' 43.54', '31° 4'' 45.884"', 'public', '0', 'TMW', 'YSTW', ' ', '150.8454304', '-31.0794121', '27147', 'YSTW', 'medium_airport', 'Tamworth Airport', '-31.0839004517', '150.847000122', '1334', 'OC', 'AU', 'AU-NSW', 'Tamworth', 'yes', 'YSTW', 'TMW', '', 'http://www.tamworth.nsw.gov.au/Council/Council-Enterprises-and-Venues/Tamworth-Regional-Airport/Tamworth-Regional-Airport/defaul', 'https://en.wikipedia.org/wiki/Tamworth_Airport', ''),
('0', '济南遥墙国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=TNA
icao', '117° 12'' 40.37', '36° 51'' 26.254', 'international', '0', 'TNA', 'ZSJN', ' ', '117.2112155', '36.8572928', '27219', 'ZSJN', 'large_airport', 'Yaoqiang Airport', '36.8572006225586', '117.216003417969', '76', 'AS', 'CN', 'CN-37', 'Jinan', 'yes', 'ZSJN', 'TNA', '', '', 'https://en.wikipedia.org/wiki/Jinan_Yaoqiang_Airport', ''),
('0', 'Aeropuerto Alberto Delgado', 'aeroway=aerodrome
ele=38 m
iata=TND
icao=MUTD
name=Aeropuert', '79° 59'' 51.085', '21° 47'' 17.324', ' ', '0', 'TND', 'MUTD', ' ', '-79.9975235', '21.7881455', '4858', 'MUTD', 'medium_airport', 'Alberto Delgado Airport', '21.7882995605469', '-79.997200012207', '125', 'NA', 'CU', 'CU-07', 'Trinidad', 'yes', 'MUTD', 'TND', '', '', '', ''),
('0', '臺南機場', 'addr:city=臺南市
addr:district=南區
addr:full=臺南市', '120° 12'' 27.79', '22° 57'' 2.214"', 'military/public', '19', 'TNN', 'RCNN', ' ', '120.2077211', '22.9506149', '5522', 'RCNN', 'medium_airport', 'Tainan International Airport / Tainan Air Base', '22.950399', '120.206001', '63', 'AS', 'TW', 'TW-TNN', 'Tainan (Rende)', 'yes', 'RCNN', 'TNN', '', 'http://www.tna.gov.tw/', 'https://en.wikipedia.org/wiki/Tainan_Airport', '臺南機場, 臺南航空站'),
('0', 'Tioman Airport', 'aeroway=aerodrome
ele=4
iata=TOD
icao=WMBT
name=Tioman Airpo', '104° 9'' 35.892', '2° 49'' 4.825"', ' ', '4', 'TOD', 'WMBT', ' ', '104.1599701', '2.818007', '26864', 'WMBT', 'medium_airport', 'Pulau Tioman Airport', '2.81818', '104.160004', '15', 'AS', 'MY', 'MY-06', 'Kampung Tekek', 'yes', 'WMBT', 'TOD', '', '', 'https://en.wikipedia.org/wiki/Tioman_Airport', 'Kampung Tekek'),
('0', 'Aéroport de Tombouctu', 'aeroway=aerodrome
barrier=fence
iata=TOM
icao=GATB
name=Aér', '3° 0'' 29.081"', '16° 43'' 51.225', ' ', '0', 'TOM', 'GATB', ' ', '-3.008078', '16.7308958', '3071', 'GATB', 'medium_airport', 'Timbuktu Airport', '16.7304992675781', '-3.00758004188538', '863', 'AF', 'ML', 'ML-7', 'Timbuktu', 'yes', 'GATB', 'TOM', '', '', 'https://en.wikipedia.org/wiki/Timbuktu_Airport', 'Timbuctoo Airport, Tombouctou Airport, Tumbutu Airport, Tombouktou Airport'),
('0', '富山空港', 'KSJ2:AAC=16201
KSJ2:AAC_label=富山県富山市
KSJ2:AD2=5
', '137° 11'' 15.59', '36° 38'' 56.843', ' ', '0', 'TOY', 'RJNT', ' ', '137.1876644', '36.649123', '5587', 'RJNT', 'medium_airport', 'Toyama Airport', '36.6483001708984', '137.188003540039', '95', 'AS', 'JP', 'JP-16', 'Toyama', 'yes', 'RJNT', 'TOY', '', '', 'https://en.wikipedia.org/wiki/Toyama_Airport', ''),
('0', 'Tampa International Airport', 'addr:city=Tampa
addr:country=US
addr:postcode=33607
addr:sta', '82° 31'' 39.955', '27° 58'' 40.754', ' ', '6', 'TPA', 'KTPA', ' ', '-82.5277652', '27.9779872', '3926', 'KTPA', 'large_airport', 'Tampa International Airport', '27.9755001068115', '-82.533203125', '26', 'NA', 'US', 'US-FL', 'Tampa', 'yes', 'KTPA', 'TPA', 'TPA', '', 'https://en.wikipedia.org/wiki/Tampa_International_Airport', ''),
('0', 'Aeropuerto Cadete FAP Guillermo del Castillo Paredes', 'aeroway=aerodrome
closest_town=Tarapoto
ele=265
iata=TPP
ica', '76° 22'' 17.945', '6° 30'' 32.752"', ' ', '265', 'TPP', 'SPST', ' ', '-76.3716515', '-6.5090979', '6237', 'SPST', 'medium_airport', 'Cadete FAP Guillermo Del Castillo Paredes Airport', '-6.50873994827271', '-76.3731994628906', '869', 'SA', 'PE', 'PE-SAM', 'Tarapoto', 'yes', 'SPST', 'TPP', '', '', 'https://en.wikipedia.org/wiki/Cad._FAP_Guillermo_del_Castillo_Paredes_Airport', ''),
('0', 'Aeropuerto Internacional De Torreón Francisco Sarabia', 'aerodrome:type=military/public
aeroway=aerodrome
ele=1124
ia', '103° 24'' 17.59', '25° 33'' 42.730', 'military/public', '1124', 'TRC', 'MMTC', ' ', '-103.404887', '25.5618695', '4755', 'MMTC', 'medium_airport', 'Francisco Sarabia International Airport', '25.5683002472', '-103.411003113', '3688', 'NA', 'MX', 'MX-COA', 'Torreón', 'yes', 'MMTC', 'TRC', '', '', 'https://en.wikipedia.org/wiki/Francisco_Sarabia_International_Airport', ''),
('0', 'Tauranga Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '176° 11'' 56.90', '37° 40'' 21.318', 'public', '4', 'TRG', 'NZTG', ' ', '176.1991393', '-37.6725884', '5055', 'NZTG', 'medium_airport', 'Tauranga Airport', '-37.6719017028809', '176.195999145508', '13', 'OC', 'NZ', 'NZ-BOP', 'Tauranga', 'yes', 'NZTG', 'TRG', '', '', 'https://en.wikipedia.org/wiki/Tauranga_Airport', ''),
('0', 'Taree Airport', 'aerodrome:category=certified
aerodrome:type=public
aeroway=a', '152° 30'' 48.82', '31° 53'' 14.029', 'public', '12', 'TRO', 'YTRE', ' ', '152.5135621', '-31.8872302', '27161', 'YTRE', 'medium_airport', 'Taree Airport', '-31.8885993958', '152.514007568', '38', 'OC', 'AU', 'AU-NSW', 'Taree', 'yes', 'YTRE', 'TRO', '', '', '', ''),
('0', 'Aeropuerto Internacional Capitán FAP Carlos Martínez de Pi', 'addr:street=Ruta al Aeropuerto 104, Huanchaco
aeroway=aerodr', '79° 6'' 32.305"', '8° 4'' 54.097"', ' ', '32', 'TRU', 'SPRU', ' ', '-79.1089736', '-8.0816936', '6235', 'SPRU', 'medium_airport', 'Capitan FAP Carlos Martinez De Pinillos International Airport', '-8.08141040802002', '-79.1088027954102', '106', 'SA', 'PE', 'PE-LAL', 'Trujillo', 'yes', 'SPRU', 'TRU', '', '', 'https://en.wikipedia.org/wiki/Cap._FAP_Carlos_Mart%C3%ADnez_de_Pinillos_International_Airport', ''),
('0', 'Thiruvananthapuram International Aiport', 'aeroway=aerodrome
iata=TRV
icao=VOTV
military=airfield
name=', '76° 55'' 10.524', '8° 28'' 55.089"', ' ', '0', 'TRV', 'VOTV', ' ', '76.91959', '8.4819693', '26629', 'VOTV', 'large_airport', 'Trivandrum International Airport', '8.48211956024', '76.9200973511', '15', 'AS', 'IN', 'IN-KL', 'Thiruvananthapuram', 'yes', 'VOTV', 'TRV', '', '', 'https://en.wikipedia.org/wiki/Trivandrum_International_Airport', 'Thiruvananthapuram International Airport, Trivandrum Air Force Station'),
('0', 'Bonriki Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=3
iata=TRW
icao=', '173° 8'' 50.166', '1° 22'' 53.276"', 'public', '3', 'TRW', 'NGTA', ' ', '173.1472682', '1.3814656', '4972', 'NGTA', 'medium_airport', 'Bonriki International Airport', '1.38164', '173.147003', '9', 'OC', 'KI', 'KI-G', 'Tarawa', 'yes', 'NGTA', 'TRW', '', '', 'https://en.wikipedia.org/wiki/Bonriki_International_Airport', 'Bonriki, Mullinix'),
('0', '台北松山機場', 'addr:city=臺北市
addr:district=松山區
addr:housenumber', '121° 33'' 12.10', '25° 4'' 0.824"', 'military/public', '5', 'TSA', 'RCSS', ' ', '121.5533616', '25.0668955', '5527', 'RCSS', 'medium_airport', 'Taipei Songshan Airport', '25.0694007873535', '121.552001953125', '18', 'AS', 'TW', 'TW-TPE', 'Taipei City', 'yes', 'RCSS', 'TSA', '', 'http://www.tsa.gov.tw/', 'https://en.wikipedia.org/wiki/Taipei_Songshan_Airport', 'Sungshan Airport, 台北松山機場, 臺北松山機場'),
('0', '天津滨海国际机场', 'aerodrome:type=international
aeroway=aerodrome
city_served=T', '117° 21'' 14.74', '39° 7'' 39.415"', 'international', '3', 'TSN', 'ZBTJ', 'Аэропорт Тяньцзинь', '117.3540959', '39.1276153', '27192', 'ZBTJ', 'large_airport', 'Tianjin Binhai International Airport', '39.1244010925', '117.346000671', '10', 'AS', 'CN', 'CN-12', 'Tianjin', 'yes', 'ZBTJ', 'TSN', '', '', 'https://en.wikipedia.org/wiki/Tianjin_Binhai_International_Airport', ''),
('0', 'Townsville Airport', 'aerodrome:type=military/public
aeroway=aerodrome
closest_tow', '146° 45'' 44.30', '19° 14'' 49.837', 'military/public', '5', 'TSV', 'YBTL', ' ', '146.762308', '-19.2471769', '26934', 'YBTL', 'medium_airport', 'Townsville Airport / RAAF Base Townsville', '-19.252904', '146.766512', '18', 'OC', 'AU', 'AU-QLD', 'Townsville', 'yes', 'YBTL', 'TSV', '', 'http://www.townsvilleairport.com.au/', 'https://en.wikipedia.org/wiki/Townsville_Airport', 'RAAF Base Garbutt, RAAF Base Townsville'),
('0', '鳥取空港', 'KSJ2:AAC=31201
KSJ2:AAC_label=鳥取県鳥取市
KSJ2:AD2=5
', '134° 9'' 57.318', '35° 31'' 43.471', 'public', '15', 'TTJ', 'RJOR', ' ', '134.1659216', '35.5287419', '5599', 'RJOR', 'medium_airport', 'Tottori Sand Dunes Conan Airport', '35.530102', '134.167007', '65', 'AS', 'JP', 'JP-31', 'Tottori', 'yes', 'RJOR', 'TTJ', '', '', 'https://en.wikipedia.org/wiki/Tottori_Airport', 'tottori'),
('0', '臺東豐年機場', 'addr:city=臺東市
addr:country=TW
addr:full=95063臺東縣', '121° 6'' 2.225"', '22° 45'' 20.507', ' ', '0', 'TTT', 'RCFN', ' ', '121.1006181', '22.7556963', '5514', 'RCFN', 'medium_airport', 'Taitung Airport', '22.7549991607666', '121.101997375488', '143', 'AS', 'TW', 'TW-TTT', 'Taitung City', 'yes', 'RCFN', 'TTT', '', '', 'https://en.wikipedia.org/wiki/Taitung_Airport', 'Fengnin Airport, 台東機場'),
('0', 'Aeropuerto Teniente Coronel Luis A. Mantilla', 'aeroway=aerodrome
iata=TUA
icao=SETU
name=Aeropuerto Tenient', '77° 42'' 30.158', '0° 48'' 34.632"', ' ', '0', 'TUA', 'SETU', ' ', '-77.7083772', '0.80962', '6086', 'SETU', 'medium_airport', 'Teniente Coronel Luis a Mantilla Airport', '0.809505999088287', '-77.7080993652344', '9649', 'SA', 'EC', 'EC-C', 'Tulcán', 'yes', 'SETU', 'TUA', '', '', 'https://en.wikipedia.org/wiki/Teniente_Coronel_Luis_a_Mantilla_International_Airport', ''),
('0', 'Tubuai – Mataura Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=5
iata=TUB
icao=', '149° 31'' 21.62', '23° 21'' 51.469', 'public', '5', 'TUB', 'NTAT', ' ', '-149.5226735', '-23.3642969', '4981', 'NTAT', 'medium_airport', 'Tubuai Airport', '-23.3654003143311', '-149.524002075195', '7', 'OC', 'PF', 'PF-U-A', '', 'yes', 'NTAT', 'TUB', '', '', 'https://en.wikipedia.org/wiki/Tubuai_-_Mataura_Airport', ''),
('0', 'Aeropuerto Internacional Teniente General Benjamín Matienzo', 'addr:street=Delfin Gallo s/n
aeroway=aerodrome
iata=TUC
icao', '65° 6'' 18.897"', '26° 50'' 20.781', ' ', '0', 'TUC', 'SANT', ' ', '-65.1052493', '-26.8391059', '5794', 'SANT', 'medium_airport', 'Teniente Benjamin Matienzo Airport', '-26.8409', '-65.104897', '1493', 'SA', 'AR', 'AR-T', 'San Miguel de Tucumán', 'yes', 'SANT', 'TUC', 'TUC', '', 'https://en.wikipedia.org/wiki/Benjam%C3%ADn_Matienzo_International_Airport', ''),
('0', 'Tambacounda Airport', 'aeroway=aerodrome
iata=TUD
icao=GOTT
name=Tambacounda Airpor', '13° 39'' 15.707', '13° 44'' 8.285"', ' ', '0', 'TUD', 'GOTT', ' ', '-13.6543631', '13.7356346', '3129', 'GOTT', 'medium_airport', 'Tambacounda Airport', '13.7368001937866', '-13.6531000137329', '161', 'AF', 'SN', 'SN-TC', 'Tambacounda', 'yes', 'GOTT', 'TUD', '', '', 'https://en.wikipedia.org/wiki/Tambacounda_Airport', ''),
('0', 'Turbat Airport', 'aeroway=aerodrome
closest_town=Turbat
ele=122
iata=TUK
icao=', '63° 1'' 47.623"', '25° 59'' 10.768', ' ', '122', 'TUK', 'OPTU', ' ', '63.0298952', '25.9863244', '5283', 'OPTU', 'medium_airport', 'Turbat International Airport', '25.986400604248', '63.030200958252', '498', 'AS', 'PK', 'PK-BA', 'Turbat', 'yes', 'OPTU', 'TUK', '', '', 'https://en.wikipedia.org/wiki/Turbat_Airport', ''),
('0', 'Taupo Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '176° 4'' 55.948', '38° 44'' 24.876', 'public', '407', 'TUO', 'NZAP', ' ', '176.0822079', '-38.7402434', '5024', 'NZAP', 'medium_airport', 'Taupo Airport', '-38.7397003173828', '176.083999633789', '1335', 'OC', 'NZ', 'NZ-WKO', 'Taupo', 'yes', 'NZAP', 'TUO', '', '', 'https://en.wikipedia.org/wiki/Taupo_Airport', ''),
('0', 'Lapangan Terbang Tawau', 'aeroway=aerodrome
ele=17
iata=TWU
icao=WBKW
name=Lapangan Te', '118° 7'' 7.612"', '4° 18'' 42.613"', ' ', '17', 'TWU', 'WBKW', ' ', '118.1187812', '4.3118369', '26817', 'WBKW', 'medium_airport', 'Tawau Airport', '4.32015991210938', '118.127998352051', '57', 'AS', 'MY', 'MY-12', 'Tawau', 'yes', 'WBKW', 'TWU', '', '', 'https://en.wikipedia.org/wiki/Tawau_Airport', ''),
('0', '黄山屯溪国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=TXN
icao', '118° 15'' 23.08', '29° 44'' 0.132"', 'international', '0', 'TXN', 'ZSTX', ' ', '118.2564132', '29.73337', '32489', 'ZSTX', 'medium_airport', 'Tunxi International Airport', '29.7332992553711', '118.255996704102', NULL, 'AS', 'CN', 'CN-34', 'Huangshan', 'yes', 'ZSTX', 'TXN', '', '', 'https://en.wikipedia.org/wiki/Huangshan_Tunxi_International_Airport', ''),
('0', '太原武宿国际机场', 'aerodrome:type=international
aeroway=aerodrome
iata=TYN
icao', '112° 37'' 48.73', '37° 44'' 55.694', 'international', '0', 'TYN', 'ZBYN', ' ', '112.6302054', '37.748804', '27193', 'ZBYN', 'large_airport', 'Taiyuan Wusu Airport', '37.746898651123', '112.627998352051', '2575', 'AS', 'CN', 'CN-14', 'Taiyuan', 'yes', 'ZBYN', 'TYN', '', '', 'https://en.wikipedia.org/wiki/Taiyuan_Wusu_Airport', ''),
('0', 'Aeropuerto Domingo Faustino Sarmiento', 'aeroway=aerodrome
barrier=ditch
iata=UAQ
icao=SANU
name=Aero', '68° 25'' 17.186', '31° 34'' 18.299', ' ', '0', 'UAQ', 'SANU', ' ', '-68.4214405', '-31.5717497', '5795', 'SANU', 'medium_airport', 'Domingo Faustino Sarmiento Airport', '-31.571501', '-68.418198', '1958', 'SA', 'AR', 'AR-J', 'San Juan', 'yes', 'SANU', 'UAQ', 'JUA', '', 'https://en.wikipedia.org/wiki/Domingo_Faustino_Sarmiento_Airport', ''),
('0', 'Aeroporto de Uberaba - Mário de Almeida Franco', 'aeroway=aerodrome
iata=UBA
icao=SVUR
int_name=Uberaba - Már', '47° 57'' 55.443', '19° 45'' 47.658', ' ', '0', 'UBA', 'SVUR', ' ', '-47.9654008', '-19.7632382', '5995', 'SBUR', 'medium_airport', 'Mário de Almeida Franco Airport', '-19.764722824097', '-47.966110229492', '2655', 'SA', 'BR', 'BR-MG', 'Uberaba', 'yes', 'SBUR', 'UBA', '', '', 'https://en.wikipedia.org/wiki/Uberaba_Airport', ''),
('0', '山口宇部空港', 'KSJ2:AAC=35202
KSJ2:AAC_label=山口県宇部市
KSJ2:AD2=5
', '131° 16'' 45.05', '33° 55'' 48.359', 'public', '5', 'UBJ', 'RJDC', ' ', '131.2791807', '33.9300998', '5555', 'RJDC', 'medium_airport', 'Yamaguchi Ube Airport', '33.9300003052', '131.279006958', '23', 'AS', 'JP', 'JP-35', 'Ube', 'yes', 'RJDC', 'UBJ', '', '', 'https://en.wikipedia.org/wiki/Yamaguchi_Ube_Airport', ''),
('0', 'Aeroporto de Uberlândia - Tenente Coronel Aviador César Bo', 'aeroway=aerodrome
iata=UDI
icao=SBUL
name=Aeroporto de Uberl', '48° 13'' 32.763', '18° 52'' 57.392', ' ', '0', 'UDI', 'SBUL', ' ', '-48.2257676', '-18.882609', '5993', 'SBUL', 'medium_airport', 'Ten. Cel. Aviador César Bombonato Airport', '-18.883612', '-48.225277', '3094', 'SA', 'BR', 'BR-MG', 'Uberlândia', 'yes', 'SBUL', 'UDI', '', '', 'https://en.wikipedia.org/wiki/Uberlândia_Airport', ''),
('0', 'Maharana Pratap Airport', 'aeroway=aerodrome
area=yes
barrier=wall
closest_town=Udaipur', '73° 53'' 59.829', '24° 37'' 6.254"', ' ', '513', 'UDR', 'VAUD', ' ', '73.8999525', '24.618404', '26462', 'VAUD', 'medium_airport', 'Maharana Pratap Airport', '24.6177005768', '73.8961029053', '1684', 'AS', 'IN', 'IN-RJ', 'Udaipur', 'yes', 'VAUD', 'UDR', '', 'http://www.airportsindia.org.in/allAirports/udaipur_airpo_gi.jsp', 'https://en.wikipedia.org/wiki/Udaipur_Airport', 'Dabok Airport'),
('0', 'Quetta International Airport', 'aerodrome=international
aeroway=aerodrome
closest_town=Quett', '66° 56'' 14.939', '30° 15'' 9.399"', ' ', '1605', 'UET', 'OPQT', ' ', '66.937483', '30.2526109', '5272', 'OPQT', 'medium_airport', 'Quetta International Airport', '30.2513999938965', '66.9377975463867', '5267', 'AS', 'PK', 'PK-BA', 'Quetta', 'yes', 'OPQT', 'UET', '', '', 'https://en.wikipedia.org/wiki/Quetta_International_Airport', ''),
('0', 'Sân bay Phù Cát', 'aeroway=aerodrome
ele=24 m
iata=UIH
icao=VVPC
int_name=Phu C', '109° 2'' 49.731', '13° 57'' 31.494', ' ', '0', 'UIH', 'VVPC', ' ', '109.0471475', '13.9587484', '26703', 'VVPC', 'medium_airport', 'Phu Cat Airport', '13.955', '109.042', '80', 'AS', 'VN', 'VN-31', 'Quy Nohn', 'yes', 'VVPC', 'UIH', '', 'https://vietnamairport.vn/phucatairport/', 'https://en.wikipedia.org/wiki/Phu_Cat_Airport', 'Phucat'),
('0', 'Aeropuerto Internacional Mariscal Sucre', 'aeroway=aerodrome
iata=UIO
icao=SEQM
name=Aeropuerto Interna', '78° 21'' 30.756', '0° 7'' 26.873"', ' ', '0', 'UIO', 'SEQM', ' ', '-78.3585432', '-0.1241313', '308273', 'SEQM', 'large_airport', 'Mariscal Sucre International Airport', '-0.129166666667', '-78.3575', '7841', 'SA', 'EC', 'EC-P', 'Quito', 'yes', 'SEQM', 'UIO', 'UIO', '', 'https://en.wikipedia.org/wiki/Mariscal_Sucre_International_Airport', 'Nuevo Aeropuerto Internacional Mariscal Sucre'),
('0', 'Aeropuerto Capitan Vazquez', 'aerodrome:type=public
aeroway=aerodrome
ele=58
iata=ULA
icao', '67° 48'' 27.412', '49° 18'' 29.741', 'public', '58', 'ULA', 'SAWJ', ' ', '-67.8076145', '-49.3082613', '5836', 'SAWJ', 'medium_airport', 'Capitan D Daniel Vazquez Airport', '-49.3068', '-67.8026', '203', 'SA', 'AR', 'AR-Z', 'San Julian', 'yes', 'SAWJ', 'ULA', 'SJU', '', '', ''),
('0', 'Aeropuerto Playa Baracoa', 'aerodrome:type=military/public
aeroway=aerodrome
alt_name=Pl', '82° 34'' 49.080', '23° 2'' 0.600"', 'military/public', '31', 'UPB', 'MUPB', ' ', '-82.5803', '23.0335001', '4851', 'MUPB', 'medium_airport', 'Playa Baracoa Airport', '23.0328006744', '-82.5793991089', '102', 'NA', 'CU', 'CU-15', 'Havana', 'yes', 'MUPB', 'UPB', '', '', 'https://en.wikipedia.org/wiki/Playa_Baracoa_Airport', ''),
('0', 'Sultan Hasanuddin International Airport', 'aeroway=aerodrome
iata=UPG
icao=WAAA
name=Sultan Hasanuddin', '119° 33'' 10.24', '5° 4'' 29.801"', ' ', '0', 'UPG', 'WAAA', ' ', '119.5528458', '-5.0749448', '26745', 'WAAA', 'large_airport', 'Hasanuddin International Airport', '-5.06162977218628', '119.554000854492', '47', 'AS', 'ID', 'ID-SN', 'Ujung Pandang-Celebes Island', 'yes', 'WAAA', 'UPG', '', '', 'https://en.wikipedia.org/wiki/Hasanuddin_International_Airport', ''),
('0', 'Aeropuerto Internacional de Uruapan', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Uruapan', '102° 2'' 18.667', '19° 23'' 52.350', 'public', '1603', 'UPN', 'MMPN', ' ', '-102.0385185', '19.397875', '4742', 'MMPN', 'medium_airport', 'Licenciado y General Ignacio Lopez Rayon Airport', '19.3966999053955', '-102.039001464844', '5258', 'NA', 'MX', 'MX-MIC', '', 'yes', 'MMPN', 'UPN', '', '', 'https://en.wikipedia.org/wiki/Uruapan_International_Airport', ''),
('0', 'Aeroporto Internacional Rubem Berta', 'aerodrome=international
aeroway=aerodrome
alt_name=Aeroporto', '57° 2'' 14.182"', '29° 46'' 58.462', ' ', '78', 'URG', 'SBUG', ' ', '-57.0372727', '-29.7829061', '5992', 'SBUG', 'medium_airport', 'Rubem Berta Airport', '-29.7821998596', '-57.0382003784', '256', 'SA', 'BR', 'BR-RS', 'Uruguaiana', 'yes', 'SBUG', 'URG', '', '', 'https://en.wikipedia.org/wiki/Ruben_Berta_International_Airport', 'Ruben Berta'),
('0', 'Aeropuerto Internacional de Ushuaia Malvinas Argentinas', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Ushuaia Int', '68° 17'' 44.896', '54° 50'' 30.005', 'public', '22', 'USH', 'SAWH', ' ', '-68.2958045', '-54.841668', '5835', 'SAWH', 'medium_airport', 'Malvinas Argentinas Airport', '-54.8433', '-68.2958', '102', 'SA', 'AR', 'AR-V', 'Ushuahia', 'yes', 'SAWH', 'USH', 'USU', '', 'https://en.wikipedia.org/wiki/Ushuaia_International_Airport', ''),
('0', '울산공항', 'aeroway=aerodrome
iata=USN
icao=RKPU
name=울산공항
name:', '129° 21'' 9.152', '35° 35'' 36.786', ' ', '0', 'USN', 'RKPU', ' ', '129.3525422', '35.5935516', '5644', 'RKPU', 'medium_airport', 'Ulsan Airport', '35.59349823', '129.352005005', '45', 'AS', 'KR', 'KR-31', 'Ulsan', 'yes', 'RKPU', 'USN', '', '', 'https://en.wikipedia.org/wiki/Ulsan_Airport', ''),
('0', 'Francisco B. Reyes Airport', 'addr:city=Coron
addr:postcode=5316
aeroway=aerodrome
alt_nam', '120° 5'' 56.948', '12° 7'' 15.641"', ' ', '0', 'USU', 'RPVV', ' ', '120.0991523', '12.1210115', '5749', 'RPVV', 'medium_airport', 'Francisco B. Reyes Airport', '12.1215000153', '120.099998474', '148', 'AS', 'PH', 'PH-PLW', 'Coron', 'yes', 'RPVV', 'USU', '', '', 'https://en.wikipedia.org/wiki/Busuanga_Airport', 'Busuanga Airport'),
('0', 'Upington Airport', 'aeroway=aerodrome
barrier=fence
ele=850.697
fixme=gates, whe', '21° 15'' 54.146', '28° 24'' 7.876"', ' ', '851', 'UTN', 'FAUP', ' ', '21.2650406', '-28.4021878', '2847', 'FAUP', 'medium_airport', 'Pierre Van Ryneveld Airport', '-28.39909935', '21.2602005005', '2782', 'AF', 'ZA', 'ZA-NC', 'Upington', 'yes', 'FAUP', 'UTN', '', '', 'https://en.wikipedia.org/wiki/Upington_Airport', ''),
('0', 'Mthatha Airport', 'aeroway=aerodrome
barrier=wall
ele=740.4
iata=UTT
icao=FAUT
', '28° 40'' 31.272', '31° 32'' 51.212', ' ', '740', 'UTT', 'FAUT', ' ', '28.6753533', '-31.5475589', '2848', 'FAUT', 'medium_airport', 'K. D. Matanzima Airport', '-31.5463631849', '28.6733551025', '2400', 'AF', 'ZA', 'ZA-EC', 'Mthatha', 'yes', 'FAUT', 'UTT', '', '', 'https://en.wikipedia.org/wiki/K.D._Matanzima_Airport', 'Umtata'),
('0', 'Аэропорт Южно-Сахалинск', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '142° 43'' 14.04', '46° 53'' 9.688"', 'public', '18', 'UUS', 'UHSS', 'Аэропорт Южно-Сахалинск', '142.7205687', '46.8860245', '6461', 'UHSS', 'medium_airport', 'Yuzhno-Sakhalinsk Airport', '46.8886985778809', '142.718002319336', '59', 'AS', 'RU', 'RU-SAK', 'Yuzhno-Sakhalinsk', 'yes', 'UHSS', 'UUS', '', 'http://www.airportus.ru/', 'https://en.wikipedia.org/wiki/Yuzhno-Sakhalinsk_Airport', 'Аэропорт Южно-Сахалинск, Khomutovo'),
('0', 'Villa International Airport Maamigili', 'aeroway=aerodrome
iata=VAM
icao=VRMV
name=Villa Internationa', '72° 50'' 7.621"', '3° 28'' 18.771"', ' ', '0', 'VAM', 'VRMV', ' ', '72.8354503', '3.4718809', '301316', 'VRMV', 'medium_airport', 'Villa Airport', '3.47055555556', '72.8358333333', '6', 'AS', 'MV', 'MV-13', 'Maamigili', 'yes', 'VRMV', 'VAM', '', '', 'https://en.wikipedia.org/wiki/Maamigili_Airport', ''),
('0', 'Mala''e Vakapuna Lupepau''u', 'aerodrome:type=public
aeroway=aerodrome
ele=72
iata=VAV
icao', '173° 57'' 46.48', '18° 35'' 7.862"', 'public', '72', 'VAV', 'NFTV', ' ', '-173.9629121', '-18.5855173', '4964', 'NFTV', 'medium_airport', 'Vava''u International Airport', '-18.5853004455566', '-173.962005615234', '236', 'OC', 'TO', 'TO-05', 'Vava''u Island', 'yes', 'NFTV', 'VAV', '', '', 'https://en.wikipedia.org/wiki/Vava''u_International_Airport', 'Vavau, Lupepau''u'),
('0', 'Trà Nóc Airport', 'aeroway=aerodrome
closest_town=Can Tho
ele=3
iata=VCA
icao=V', '105° 42'' 52.29', '10° 5'' 1.071"', ' ', '3', 'VCA', 'VVCT', ' ', '105.7145268', '10.0836308', '26695', 'VVCT', 'medium_airport', 'Can Tho International Airport', '10.085100174', '105.711997986', '9', 'AS', 'VN', 'VN-02', 'Can Tho', 'yes', 'VVCT', 'VCA', '', '', 'https://en.wikipedia.org/wiki/Can_Tho_International_Airport', 'Binh Thuy Air Base, Trà Nóc Airport'),
('0', 'Aeroporto Internacional de Viracopos', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '47° 8'' 18.640"', '23° 0'' 23.557"', 'public', '661', 'VCP', 'SBKP', ' ', '-47.138511', '-23.0065435', '5927', 'SBKP', 'medium_airport', 'Viracopos International Airport', '-23.0074005127', '-47.1344985962', '2170', 'SA', 'BR', 'BR-SP', 'Campinas', 'yes', 'SBKP', 'VCP', '', '', 'https://en.wikipedia.org/wiki/Viracopos-Campinas_International_Airport', ''),
('0', 'Sân bay Cỏ Ống', 'aeroway=aerodrome
iata=VCS
icao=VVCS
name=Sân bay Cỏ Ốn', '106° 37'' 56.71', '8° 43'' 55.060"', ' ', '0', 'VCS', 'VVCS', 'Aэропорт Кондао', '106.6324208', '8.731961', '26694', 'VVCS', 'medium_airport', 'Con Dao Airport', '8.73183', '106.633003', '20', 'AS', 'VN', 'VN-43', 'Con Dao', 'yes', 'VVCS', 'VCS', '', '', 'https://en.wikipedia.org/wiki/Con_Dao_Airport', 'Conson Airport'),
('0', 'Aeroporto de Vitória da Conquista - Pedro Otacílio Figuere', 'aeroway=aerodrome
iata=VDC
icao=SBQV
name=Aeroporto de Vitó', '40° 51'' 46.793', '14° 51'' 51.103', ' ', '0', 'VDC', 'SBQV', ' ', '-40.8629981', '-14.8641954', '333222', 'BR-0569', 'medium_airport', 'Glauber de Andrade Rocha Airport', '-14.907885', '-40.914804', '2940', 'SA', 'BR', 'BR-BA', 'Vitória da Conquista', 'yes', 'SBVC', 'VDC', 'BA0005', '', 'https://en.wikipedia.org/wiki/Glauber_Rocha_Airport', ''),
('0', 'Aeropuerto de El Hierro', 'aeroway=aerodrome
iata=VDE
icao=GCHI
name=Aeropuerto de El H', '17° 53'' 14.003', '27° 48'' 51.444', ' ', '0', 'VDE', 'GCHI', ' ', '-17.8872231', '27.8142901', '3079', 'GCHI', 'medium_airport', 'Hierro Airport', '27.8148002624512', '-17.8871002197266', '103', 'EU', 'ES', 'ES-CN', 'El Hierro Island', 'yes', 'GCHI', 'VDE', '', '', 'https://en.wikipedia.org/wiki/El_Hierro_Airport', ''),
('0', 'Aeropuerto Gobernador Castello', 'aeroway=aerodrome
iata=VDM
icao=SAVV
internet_access=yes
nam', '62° 59'' 43.060', '40° 51'' 49.442', ' ', '0', 'VDM', 'SAVV', ' ', '-62.9952944', '-40.8637339', '5828', 'SAVV', 'medium_airport', 'Gobernador Castello Airport', '-40.8692', '-63.0004', '20', 'SA', 'AR', 'AR-R', 'Viedma / Carmen de Patagones', 'yes', 'SAVV', 'VDM', 'VIE', '', '', ''),
('0', 'Victoria Falls Airport', 'aeroway=aerodrome
iata=VFA
icao=FVFA
name=Victoria Falls Air', '25° 50'' 5.599"', '18° 5'' 48.254"', ' ', '0', 'VFA', 'FVFA', ' ', '25.8348886', '-18.0967373', '3004', 'FVFA', 'medium_airport', 'Victoria Falls International Airport', '-18.0958995819092', '25.8390007019043', '3490', 'AF', 'ZW', 'ZW-MN', 'Victoria Falls', 'yes', 'FVFA', 'VFA', '', '', 'https://en.wikipedia.org/wiki/Victoria_Falls_Airport', ''),
('0', 'Vijayawada Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Vijayaw', '80° 48'' 0.413"', '16° 31'' 47.448', 'public', '25', 'VGA', 'VOBZ', ' ', '80.8001148', '16.5298466', '26606', 'VOBZ', 'medium_airport', 'Vijayawada Airport', '16.530399', '80.796799', '82', 'AS', 'IN', 'IN-AP', 'Gannavaram', 'yes', 'VOBZ', 'VGA', '', 'https://www.aai.aero/en/airports/vijayawada', 'https://en.wikipedia.org/wiki/Vijayawada_Airport', ''),
('0', 'مطار الداخلة الدولي', 'aerodrome:type=military/public
aeroway=aerodrome
barrier=wal', '15° 55'' 45.534', '23° 43'' 29.276', 'military/public', '11', 'VIL', 'GMMH/G', ' ', '-15.929315', '23.7247988', '3110', 'GMMH', 'medium_airport', 'Dakhla Airport', '23.7183', '-15.932', '36', 'AF', 'EH', 'EH-U-A', 'Dakhla', 'yes', 'GMMH', 'VIL', '', '', 'https://en.wikipedia.org/wiki/Dakhla_Airport', 'GSVO, Villa Cisneros, Dajla, Oued Ed-Dahab'),
('0', 'Aeroporto de Vitória', 'aeroway=aerodrome
alt_name=Aeroporto Eurico de Aguiar Salles', '40° 17'' 0.954"', '20° 15'' 21.267', ' ', '0', 'VIX', 'SBVT', ' ', '-40.2835983', '-20.2559075', '5998', 'SBVT', 'medium_airport', 'Eurico de Aguiar Salles Airport', '-20.258057', '-40.286388', '11', 'SA', 'BR', 'BR-ES', 'Vitória', 'yes', 'SBVT', 'VIX', '', '', 'https://en.wikipedia.org/wiki/Eurico_de_Aguiar_Salles_Airport', ''),
('0', 'Bauerfield Airport', 'aerodrome:type=public
aeroway=aerodrome
ele=21
iata=VLI
icao', '168° 19'' 5.343', '17° 41'' 56.491', 'public', '21', 'VLI', 'NVVV', ' ', '168.3181508', '-17.6990252', '5011', 'NVVV', 'large_airport', 'Bauerfield International Airport', '-17.699301', '168.320007', '70', 'OC', 'VU', 'VU-SEE', 'Port Vila', 'yes', 'NVVV', 'VLI', '', '', 'https://en.wikipedia.org/wiki/Bauerfield_International_Airport', 'Efate Field, Vila Field, McDonald Field, Bauer Field'),
('0', 'Lal Bahadur Shastri Airport (Varanasi Airport)', 'aeroway=aerodrome
ele=81
iata=VNS
icao=VIBN
is_in:country=In', '82° 51'' 45.693', '25° 27'' 3.896"', ' ', '81', 'VNS', 'VIBN', ' ', '82.8626924', '25.4510822', '26545', 'VIBN', 'medium_airport', 'Lal Bahadur Shastri Airport', '25.452129', '82.861805', '266', 'AS', 'IN', 'IN-UP', 'Varanasi', 'yes', 'VEBN', 'VNS', '', 'https://www.aai.aero/en/airports/varanasi', 'https://en.wikipedia.org/wiki/Lal_Bahadur_Shastri_Airport', 'VIBN, Babatpur Airport, Varanasi Airport'),
('0', 'Aeropuerto Internacional Juan Gualberto Gómez', 'aeroway=aerodrome
alt_name=Aeropuerto de Varadero
ele=64 m
i', '81° 26'' 11.714', '23° 2'' 2.032"', ' ', '0', 'VRA', 'MUVR', ' ', '-81.4365873', '23.0338977', '4859', 'MUVR', 'large_airport', 'Juan Gualberto Gomez International Airport', '23.0344009399414', '-81.435302734375', '210', 'NA', 'CU', 'CU-04', 'Varadero', 'yes', 'MUVR', 'VRA', '', '', 'https://en.wikipedia.org/wiki/Juan_Gualberto_G%C3%B3mez_Airport', ''),
('0', 'Aeropuerto Internacional Carlos Rovirosa Pérez', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Villahe', '92° 49'' 9.225"', '17° 59'' 45.357', 'public', '14', 'VSA', 'MMVA', ' ', '-92.8192292', '17.9959326', '4763', 'MMVA', 'medium_airport', 'Carlos Rovirosa Pérez International Airport', '17.9969997406006', '-92.8173980712891', '46', 'NA', 'MX', 'MX-TAB', 'Villahermosa', 'yes', 'MMVA', 'VSA', '', '', 'https://en.wikipedia.org/wiki/Carlos_Rovirosa_P%C3%A9rez_International_Airport', 'Villahermosa International Airport'),
('0', 'Aeropuerto Hermanos Ameijeiras', 'aeroway=aerodrome
ele=100
iata=VTU
icao=MUVT
name=Aeropuerto', '76° 56'' 8.617"', '20° 59'' 13.960', ' ', '100', 'VTU', 'MUVT', ' ', '-76.9357269', '20.987211', '4860', 'MUVT', 'medium_airport', 'Hermanos Ameijeiras Airport', '20.9876003265381', '-76.9357986450195', '328', 'NA', 'CU', 'CU-10', 'Las Tunas', 'yes', 'MUVT', 'VTU', '', '', 'https://en.wikipedia.org/wiki/Hermanos_Ameijeiras_Airport', ''),
('0', 'Alfonso López Pumarejo', 'aeroway=aerodrome
closest_town=Valledupar
ele=138
iata=VUP
i', '73° 14'' 56.394', '10° 26'' 6.846"', ' ', '138', 'VUP', 'SKVP', ' ', '-73.2489982', '10.435235', '6176', 'SKVP', 'medium_airport', 'Alfonso López Pumarejo Airport', '10.435', '-73.2495', '483', 'SA', 'CO', 'CO-CES', 'Valledupar', 'yes', 'SKVP', 'VUP', 'VUP', '', 'https://en.wikipedia.org/wiki/Valledupar_Airport', ''),
('0', 'Vanguardia', 'aeroway=aerodrome
closest_town=Villavicencio
ele=425
iata=VV', '73° 36'' 47.596', '4° 10'' 8.593"', ' ', '425', 'VVC', 'KSVV', ' ', '-73.6132212', '4.1690536', '6177', 'SKVV', 'medium_airport', 'Vanguardia Airport', '4.16787', '-73.6138', '1394', 'SA', 'CO', 'CO-MET', 'Villavicencio', 'yes', 'SKVV', 'VVC', 'VVC', '', 'https://en.wikipedia.org/wiki/La_Vanguardia_Airport', ''),
('0', 'Aeropuerto Internacional Viru Viru', 'aerodrome:type=public
aeroway=aerodrome
ele=373
iata=VVI
ica', '63° 8'' 13.867"', '17° 39'' 34.077', 'public', '373', 'VVI', 'SLVR', ' ', '-63.1371853', '-17.6594659', '6193', 'SLVR', 'large_airport', 'Viru Viru International Airport', '-17.6448', '-63.135399', '1224', 'SA', 'BO', 'BO-S', 'Santa Cruz', 'yes', 'SLVR', 'VVI', '', '', 'https://en.wikipedia.org/wiki/Viru_Viru_International_Airport', ''),
('0', 'Международный аэропорт Владивос', 'aerodrome:type=public
aeroway=aerodrome
alt_name=Кневи', '132° 9'' 2.174"', '43° 23'' 28.990', 'public', '14', 'VVO', 'UHWW', 'Международный аэропорт Влади', '132.1506038', '43.3913862', '6462', 'UHWW', 'large_airport', 'Vladivostok International Airport', '43.396256', '132.148155', '59', 'EU', 'RU', 'RU-PRI', 'Artyom', 'yes', 'UHWW', 'VVO', '', 'http://www.vladivostokavia.ru/en/airport/', 'https://en.wikipedia.org/wiki/Vladivostok_International_Airport', 'Vladivostok-Knevichi Airport'),
('0', 'Whanganui Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '175° 1'' 26.760', '39° 57'' 48.157', 'public', '8', 'WAG', 'NZWU', ' ', '175.0241001', '-39.9633769', '5068', 'NZWU', 'medium_airport', 'Wanganui Airport', '-39.9622001647949', '175.024993896484', '27', 'OC', 'NZ', 'NZ-MWT', 'Wanganui', 'yes', 'NZWU', 'WAG', '', '', 'https://en.wikipedia.org/wiki/Wanganui_Airport', ''),
('0', 'Wapenamanda Airport', 'aeroway=aerodrome
ele=1795
iata=WBM
icao=AYWD
is_in=Wapenama', '143° 53'' 36.62', '5° 38'' 5.345"', ' ', '1795', 'WBM', 'AYWD', ' ', '143.8935057', '-5.6348181', '70', 'AYWD', 'medium_airport', 'Wapenamanda Airport', '-5.6433', '143.895004', '5889', 'OC', 'PG', 'PG-EPW', 'Wapenamanda', 'yes', 'AYWD', 'WBM', '', '', 'https://en.wikipedia.org/wiki/Wapenamanda_Airport', ''),
('0', 'Hosea Kutako International Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '17° 28'' 8.853"', '22° 28'' 55.682', 'public', '0', 'WDH', 'FYWH', ' ', '17.4691257', '-22.4821339', '3039', 'FYWH', 'large_airport', 'Hosea Kutako International Airport', '-22.4799', '17.4709', '5640', 'AF', 'NA', 'NA-KH', 'Windhoek', 'yes', 'FYWH', 'WDH', '', 'http://airports.com.na/', 'https://en.wikipedia.org/wiki/Windhoek_Hosea_Kutako_International_Airport', 'J.G. Strijdom Airport'),
('0', '潍坊南苑机场', 'aeroway=aerodrome
iata=WEF
icao=ZSWF
name=潍坊南苑机场', '119° 6'' 58.287', '36° 38'' 49.775', ' ', '0', 'WEF', 'ZSWF', ' ', '119.1161908', '36.6471598', '32646', 'ZSWF', 'medium_airport', 'Weifang Airport', '36.646702', '119.119003', NULL, 'AS', 'CN', 'CN-37', 'Weifang', 'yes', 'ZSWF', 'WEF', '', '', 'https://en.wikipedia.org/wiki/Weifang_Airport', ''),
('0', '威海大水泊国际机场', 'aeroway=aerodrome
iata=WEH
icao=ZSWH
int_name=Weihai Dashuib', '122° 13'' 42.75', '37° 11'' 9.906"', ' ', '0', 'WEH', 'ZSWH', ' ', '122.228543', '37.1860849', '27226', 'ZSWH', 'medium_airport', 'Weihai Airport', '37.1870994567871', '122.228996276855', '145', 'AS', 'CN', 'CN-37', 'Weihai', 'yes', 'ZSWH', 'WEH', '', '', 'https://en.wikipedia.org/wiki/Weihai_Airport', 'Dashuipo, Yancheng Air Base'),
('0', 'Wagga Wagga Regional Airport', 'aeroway=aerodrome
ele=221 m
iata=WGA
icao=YSWG
internet_acce', '147° 28'' 11.21', '35° 9'' 57.040"', ' ', '0', 'WGA', 'YSWG', ' ', '147.4697825', '-35.1658444', '27148', 'YSWG', 'medium_airport', 'Wagga Wagga City Airport', '-35.1652984619', '147.466003418', '724', 'OC', 'AU', 'AU-NSW', 'Wagga Wagga', 'yes', 'YSWG', 'WGA', '', 'http://www.wagga.nsw.gov.au/www/html/271-airport.asp', 'https://en.wikipedia.org/wiki/Wagga_Wagga_Airport', 'RAAF Base Wagga'),
('0', 'Whakatāne Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '176° 55'' 0.897', '37° 55'' 20.028', ' ', '0', 'WHK', 'NZWK', ' ', '176.9169159', '-37.92223', '5062', 'NZWK', 'medium_airport', 'Whakatane Airport', '-37.9206008911133', '176.914001464844', '20', 'OC', 'NZ', 'NZ-BOP', '', 'yes', 'NZWK', 'WHK', '', '', 'https://en.wikipedia.org/wiki/Whakatane_Airport', ''),
('0', 'Winton Aerodrome', 'aerodrome:type=public
aeroway=aerodrome
ele=194
iata=WIN
ica', '143° 4'' 59.272', '22° 21'' 48.375', 'public', '194', 'WIN', 'YWTN', ' ', '143.083131', '-22.3634376', '27180', 'YWTN', 'medium_airport', 'Winton Airport', '-22.3635997772217', '143.085998535156', '638', 'OC', 'AU', 'AU-QLD', '', 'yes', 'YWTN', 'WIN', '', '', 'https://en.wikipedia.org/wiki/Winton_Airport', ''),
('0', '원주공항', 'aeroway=aerodrome
iata=WJU
icao=RKNW
landuse=military
name=', '127° 57'' 49.88', '37° 26'' 27.573', ' ', '0', 'WJU', 'RKNW', ' ', '127.9638564', '37.4409925', '5637', 'RKNW', 'medium_airport', 'Wonju/Hoengseong Air Base (K-38/K-46)', '37.441201', '127.963858', '329', 'AS', 'KR', 'KR-42', 'Wonju', 'yes', 'RKNW', 'WJU', '', 'http://wonju.airport.co.kr/', 'https://en.wikipedia.org/wiki/Wonju_Airport; https://en.wikipedia.org/wiki/List_of_airports_in_South_Korea', 'Wonju, Hoengseong Air Base, K-38, K-46, WJU, RKNW, RKNH'),
('0', 'Wanaka Aerodrome', 'aeroway=aerodrome
iata=WKA
icao=NZWF
name=Wanaka Aerodrome', '169° 14'' 48.64', '44° 43'' 20.772', ' ', '0', 'WKA', 'NZWF', ' ', '169.2468452', '-44.7224367', '5060', 'NZWF', 'medium_airport', 'Wanaka Airport', '-44.722198486328', '169.24600219727', '1142', 'OC', 'NZ', 'NZ-OTA', '', 'yes', 'NZWF', 'WKA', '', '', 'https://en.wikipedia.org/wiki/Wanaka_Airport', 'Luggate Airport'),
('0', '稚内空港', 'KSJ2:AAC=01214
KSJ2:AAC_label=北海道稚内市
KSJ2:AD2=1
', '141° 47'' 57.73', '45° 24'' 13.567', 'public', '8', 'WKJ', 'RJCW', 'Аэропорт Вакканай', '141.799372', '45.4037687', '5552', 'RJCW', 'medium_airport', 'Wakkanai Airport', '45.4042015076', '141.800994873', '30', 'AS', 'JP', 'JP-01', 'Wakkanai', 'yes', 'RJCW', 'WKJ', '', '', 'https://en.wikipedia.org/wiki/Wakkanai_Airport', ''),
('0', 'Wellington International Airport', 'LINZ:dataset=mainland
LINZ:layer=airport_poly
LINZ:source_ve', '174° 48'' 27.73', '41° 19'' 36.701', 'international', '12', 'WLG', 'NZWN', ' ', '174.8077031', '-41.3268614', '5063', 'NZWN', 'large_airport', 'Wellington International Airport', '-41.3272018433', '174.804992676', '41', 'OC', 'NZ', 'NZ-WGN', 'Wellington', 'yes', 'NZWN', 'WLG', '', 'http://www.wellingtonairport.co.nz/', 'https://en.wikipedia.org/wiki/Wellington_International_Airport', ''),
('0', 'Aéroport de Wallis-Hihifo', 'aerodrome:type=public
aeroway=aerodrome
ele=24
iata=WLS
icao', '176° 11'' 59.42', '13° 14'' 22.465', 'public', '24', 'WLS', 'NLWW', ' ', '-176.1998408', '-13.2395736', '4975', 'NLWW', 'medium_airport', 'Hihifo Airport', '-13.2383003235', '-176.199005127', '79', 'OC', 'WF', 'WF-U-A', 'Wallis Island', 'yes', 'NLWW', 'WLS', '', '', 'https://en.wikipedia.org/wiki/Hihifo_Airport', ''),
('0', 'Ilawarra Regional Airport', 'aeroway=aerodrome
iata=WOL
icao=YWOL
name=Ilawarra Regional', '150° 47'' 20.33', '34° 33'' 34.076', ' ', '0', 'WOL', 'YWOL', ' ', '150.7889816', '-34.5594655', '27177', 'YWOL', 'medium_airport', 'Shellharbour Airport', '-34.5611', '150.789001', '31', 'OC', 'AU', 'AU-NSW', '', 'yes', 'YSHL', 'WOL', '', '', 'https://en.wikipedia.org/wiki/Wollongong_Airport', 'RAAF Albion Park'),
('0', 'Whangarei Airport', 'LINZ:source_version=2012-06-06
aeroway=aerodrome
attribution', '174° 21'' 49.04', '35° 46'' 11.193', ' ', '0', 'WRE', 'NZWR', ' ', '174.3636244', '-35.7697758', '5066', 'NZWR', 'medium_airport', 'Whangarei Airport', '-35.7682991027832', '174.365005493164', '133', 'OC', 'NZ', 'NZ-NTL', '', 'yes', 'NZWR', 'WRE', '', '', 'https://en.wikipedia.org/wiki/Whangarei_Airport', ''),
('0', 'Brisbane West Wellcamp Airport', 'aerodrome:type=public
aeroway=aerodrome
contact:website=http', '151° 47'' 41.52', '27° 33'' 29.287', 'public', '460', 'WTB', 'YBWW', ' ', '151.7948685', '-27.5581354', '315119', 'YBWW', 'medium_airport', 'Brisbane West Wellcamp Airport', '-27.558332', '151.793335', '1509', 'OC', 'AU', 'AU-QLD', 'Toowoomba', 'yes', 'YBWW', 'WTB', '', 'http://www.wellcamp.com.au', 'https://en.wikipedia.org/wiki/Toowoomba_Wellcamp_Airport', 'Brisbane West Wellcamp Airport'),
('0', '武汉天河国际机场', 'aerodrome:type=international
aeroway=aerodrome
alt_name=Wuha', '114° 13'' 1.237', '30° 46'' 26.420', 'international', '34', 'WUH', 'ZHHH', ' ', '114.2170104', '30.7740056', '27200', 'ZHHH', 'large_airport', 'Wuhan Tianhe International Airport', '30.7838', '114.208', '113', 'AS', 'CN', 'CN-42', 'Wuhan', 'yes', 'ZHHH', 'WUH', '', 'http://www.whairport.com/', 'https://en.wikipedia.org/wiki/Wuhan_Tianhe_International_Airport', ''),
('0', 'Walvis Bay Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '14° 38'' 49.391', '22° 58'' 41.676', 'public', '91', 'WVB', 'FYWB', ' ', '14.6470531', '-22.9782433', '3037', 'FYWB', 'medium_airport', 'Walvis Bay Airport', '-22.9799', '14.6453', '299', 'AF', 'NA', 'NA-ER', 'Walvis Bay', 'yes', 'FYWB', 'WVB', '', '', 'https://en.wikipedia.org/wiki/Walvis_Bay_Airport', ''),
('0', 'Whyalla Airport', 'aeroway=aerodrome
ele=12 m
iata=WYA
icao=YWHA
name=Whyalla A', '137° 30'' 50.90', '33° 3'' 29.851"', ' ', '0', 'WYA', 'YWHA', ' ', '137.5141413', '-33.0582919', '27170', 'YWHA', 'medium_airport', 'Whyalla Airport', '-33.0588989257812', '137.514007568359', '41', 'OC', 'AU', 'AU-SA', 'Whyalla', 'yes', 'YWHA', 'WYA', '', '', 'https://en.wikipedia.org/wiki/Whyalla_Airport', ''),
('0', 'Aeroporto Municipal de Chapecó', 'addr:city=Chapecó
addr:hamlet=Palmital dos Fundos
addr:hous', '52° 39'' 24.532', '27° 8'' 3.312"', ' ', '654', 'XAP', 'SBCH', ' ', '-52.6568144', '-27.1342533', '5884', 'SBCH', 'medium_airport', 'Serafin Enoss Bertaso Airport', '-27.134199142456', '-52.656600952148', '2146', 'SA', 'BR', 'BR-SC', 'Chapecó', 'yes', 'SBCH', 'XAP', '', '', 'https://en.wikipedia.org/wiki/Chapec%C3%B3_Airport', 'Chapecó Airport'),
('0', 'Christmas Island Airport', 'aerodrome=international
aerodrome:type=public
aeroway=aerodr', '105° 41'' 24.61', '10° 26'' 59.079', 'public', '279', 'XCH', 'YPXM', ' ', '105.6901715', '-10.4497442', '27122', 'YPXM', 'medium_airport', 'Christmas Island Airport', '-10.4506', '105.690002', '916', 'AS', 'CX', 'CX-U-A', 'Flying Fish Cove', 'yes', 'YPXM', 'XCH', '', 'http://www.christmasislandairport.com/', 'https://en.wikipedia.org/wiki/Christmas_Island_Airport', ''),
('0', 'Aéroport de Saint-Louis', 'aerodrome:type=public
aeroway=aerodrome
area=yes
barrier=fen', '16° 27'' 37.722', '16° 3'' 1.390"', 'public', '3', 'XLS', 'GOSS', ' ', '-16.4604783', '16.0503861', '3126', 'GOSS', 'medium_airport', 'Saint Louis Airport', '16.049814', '-16.461039', '9', 'AF', 'SN', 'SN-SL', 'Saint Louis', 'yes', 'GOSS', 'XLS', '', '', 'https://en.wikipedia.org/wiki/Saint_Louis_Airport', ''),
('0', '西宁曹家堡机场', 'aeroway=aerodrome
iata=XNN
icao=ZLXN
name=西宁曹家堡机', '102° 2'' 20.491', '36° 31'' 46.851', ' ', '0', 'XNN', 'ZLXN', ' ', '102.0390253', '36.5296808', '32709', 'ZLXN', 'medium_airport', 'Xining Caojiabu Airport', '36.5275', '102.042999', '7119', 'AS', 'CN', 'CN-63', 'Xining', 'yes', 'ZLXN', 'XNN', '', '', 'https://en.wikipedia.org/wiki/Xining_Caojiabu_Airport', ''),
('0', 'Aeropuerto de Quepos', 'aeroway=aerodrome
closest_town=Quepos
ele=26
iata=XQP
icao=M', '84° 7'' 58.654"', '9° 26'' 23.335"', ' ', '26', 'XQP', 'MRQP', ' ', '-84.1329595', '9.4398154', '4814', 'MRQP', 'medium_airport', 'Quepos Managua Airport', '9.44316005706787', '-84.1297988891602', '85', 'NA', 'CR', 'CR-P', 'Quepos', 'yes', 'MRQP', 'XQP', '', '', 'https://en.wikipedia.org/wiki/Quepos_Managua_Airport', ''),
('0', 'Seletar Airport', 'aeroway=aerodrome
description=General aviation airport, main', '103° 51'' 41.81', '1° 24'' 31.106"', ' ', '0', 'XSP', 'WSSL', ' ', '103.8616138', '1.4086406', '26886', 'WSSL', 'medium_airport', 'Seletar Airport', '1.4169499874115', '103.86799621582', '36', 'AS', 'SG', 'SG-02', 'Seletar', 'yes', 'WSSL', 'XSP', '', '', 'https://en.wikipedia.org/wiki/Seletar_Airport', ''),
('0', 'Thargomindah Airport', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Thargom', '143° 48'' 42.25', '27° 59'' 15.013', 'public', '132', 'XTG', 'YTGM', ' ', '143.8117374', '-27.9875037', '27154', 'YTGM', 'medium_airport', 'Thargomindah Airport', '-27.986400604248', '143.811004638672', '433', 'OC', 'AU', 'AU-QLD', '', 'yes', 'YTGM', 'XTG', '', '', 'https://en.wikipedia.org/wiki/Thargomindah_Airport', ''),
('0', 'Base Aérienne 101 de Yaoundé', 'aerodrome:type=military
aeroway=aerodrome
closest_town=Yaoun', '11° 31'' 23.291', '3° 50'' 11.845"', 'military', '751', 'YAO', 'FKKY', ' ', '11.5231363', '3.8366235', '2889', 'FKKY', 'medium_airport', 'Yaoundé Airport', '3.83604001998901', '11.5235004425049', '2464', 'AF', 'CM', 'CM-CE', 'Yaoundé', 'yes', 'FKKY', 'YAO', '', '', 'https://en.wikipedia.org/wiki/Yaound%C3%A9_Airport', 'Yaoundé Ville Airport'),
('0', '美保飛行場', 'KSJ2:AAC=31204
KSJ2:AAC_label=鳥取県境港市
KSJ2:AD2=2
', '133° 14'' 19.91', '35° 29'' 36.042', ' ', '0', 'YGJ', 'RJOH', 'Йонаго', '133.2388647', '35.493345', '5594', 'RJOH', 'medium_airport', 'Yonago Kitaro Airport / JASDF Miho Air Base', '35.492199', '133.235992', '20', 'AS', 'JP', 'JP-31', 'Yonago', 'yes', 'RJOH', 'YGJ', '', '', 'https://en.wikipedia.org/wiki/Miho-Yonago_Airport', 'miho, yonago'),
('0', '义乌机场', 'addr:full=中華人民共和國浙江省金华市义乌市', '120° 1'' 54.014', '29° 20'' 40.282', ' ', '0', 'YIW', 'ZSYW', ' ', '120.0316706', '29.3445228', '32726', 'ZSYW', 'medium_airport', 'Yiwu Airport', '29.3446998596', '120.031997681', '262', 'AS', 'CN', 'CN-33', 'Yiwu', 'yes', 'ZSYW', 'YIW', '', '', 'https://en.wikipedia.org/wiki/Yiwu_Airport', ''),
('0', 'Moisés Benzaquen Rengifo Airport', 'aeroway=aerodrome
ele=179
iata=YMS
icao=SPMS
name=Moisés Be', '76° 7'' 6.900"', '5° 53'' 38.705"', ' ', '179', 'YMS', 'SPMS', ' ', '-76.1185832', '-5.8940848', '6228', 'SPMS', 'medium_airport', 'Moises Benzaquen Rengifo Airport', '-5.89377021789551', '-76.1182022094727', '587', 'SA', 'PE', 'PE-LOR', 'Yurimaguas', 'yes', 'SPMS', 'YMS', '', '', 'https://en.wikipedia.org/wiki/Mois%C3%A9s_Benzaquen_Rengifo_Airport', ''),
('0', '延吉朝阳川机场', 'aerodrome:type=international
aeroway=aerodrome
iata=YNJ
icao', '129° 27'' 5.593', '42° 52'' 47.849', 'international', '0', 'YNJ', 'ZYYJ', ' ', '129.4515535', '42.879958', '27244', 'ZYYJ', 'medium_airport', 'Yanji Chaoyangchuan Airport', '42.8828010559', '129.451004028', '624', 'AS', 'CN', 'CN-22', 'Yanji', 'yes', 'ZYYJ', 'YNJ', '', '', 'https://en.wikipedia.org/wiki/Yanji_Chaoyangchuan_Airport', ''),
('0', '양양국제공항 (Yangyang International Airport)', 'aeroway=aerodrome
alt_name=양양공항
closest_town=Gangneu', '128° 40'' 13.92', '38° 3'' 35.526"', ' ', '73', 'YNY', 'RKNY', 'Аэропорт Янг-Янг', '128.6705336', '38.0598682', '5638', 'RKNY', 'medium_airport', 'Yangyang International Airport', '38.061298', '128.669006', '241', 'AS', 'KR', 'KR-42', 'Gonghang-ro', 'yes', 'RKNY', 'YNY', '', '', 'https://en.wikipedia.org/wiki/Yangyang_International_Airport', 'RKNY, YNY, Yangyang, Gonghang'),
('0', 'Yola Airport', 'aeroway=aerodrome
ele=183
iata=YOL
icao=DNYO
name=Yola Airpo', '12° 25'' 54.260', '9° 15'' 32.182"', ' ', '183', 'YOL', 'DNYO', ' ', '12.4317389', '9.2589394', '2122', 'DNYO', 'medium_airport', 'Yola Airport', '9.25755023956299', '12.4303998947144', '599', 'AF', 'NG', 'NG-AD', 'Yola', 'yes', 'DNYO', 'YOL', '', 'http://www.faannigeria.org/nigeria-airport.php?airport=16', 'https://en.wikipedia.org/wiki/Yola_Airport', ''),
('0', 'Lethbridge Airport', 'aeroway=aerodrome
name=Lethbridge Airport
source=Bing', '144° 6'' 11.059', '37° 55'' 18.581', ' ', '0', 'YQL', 'CYQL', ' ', '144.103072', '-37.9218281', '1871', 'CYQL', 'medium_airport', 'Lethbridge County Airport', '49.6302986145', '-112.800003052', '3048', 'NA', 'CA', 'CA-AB', 'Lethbridge', 'yes', 'CYQL', 'YQL', '', '', 'https://en.wikipedia.org/wiki/Lethbridge_County_Airport', 'Kenyon Field'),
('0', '扬州泰州机场', 'aerodrome:type=international
aeroway=aerodrome
ele=2.1
iata=', '119° 43'' 3.613', '32° 33'' 38.560', 'international', '2', 'YTY', 'ZSYA', ' ', '119.7176702', '32.5607112', '317774', 'ZSYA', 'medium_airport', 'Yangzhou Taizhou Airport', '32.5634', '119.7198', '7', 'AS', 'CN', 'CN-32', 'Yangzhou and Taizhou', 'yes', 'ZSYA', 'YTY', '', 'http://www.yztzairport.net/', 'https://en.wikipedia.org/wiki/Yangzhou_Taizhou_Airport', 'Yangtai Airport'),
('0', 'Zamboanga International Airport', 'addr:city=Zamboanga City
addr:postcode=7000
addr:street=Airp', '122° 3'' 35.039', '6° 55'' 19.345"', 'military/public', '6', 'ZAM', 'RPMZ', ' ', '122.0597331', '6.9220402', '5712', 'RPMZ', 'medium_airport', 'Zamboanga International Airport', '6.92242002487183', '122.059997558594', '33', 'AS', 'PH', 'PH-ZAS', 'Zamboanga City', 'yes', 'RPMZ', 'ZAM', '', 'http://www.zamboanga.net/airportinsert.htm', 'https://en.wikipedia.org/wiki/Zamboanga_International_Airport', ''),
('0', 'Aeropuerto Intl. General Leobardo C. Ruiz', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Zacatec', '102° 41'' 8.597', '22° 53'' 44.859', 'public', '2177', 'ZCL', 'MMZC', ' ', '-102.6857214', '22.8957942', '4765', 'MMZC', 'medium_airport', 'General Leobardo C. Ruiz International Airport', '22.8971004486', '-102.68699646', '7141', 'NA', 'MX', 'MX-ZAC', 'Zacatecas', 'yes', 'MMZC', 'ZCL', '', 'http://www.oma.aero/es/aeropuertos/zacatecas/', 'https://en.wikipedia.org/wiki/General_Leobardo_C._Ruiz_International_Airport', 'Zacatecas International Airport'),
('0', 'Aeropuerto La Araucanía', 'aerodrome:type=public
aeroway=aerodrome
ele=98
iata=ZCO
icao', '72° 39'' 1.771"', '38° 55'' 30.579', 'public', '98', 'ZCO', 'SCQP', ' ', '-72.6504919', '-38.9251608', '314783', 'SCQP', 'medium_airport', 'La Araucanía Airport', '-38.9259', '-72.6515', '333', 'SA', 'CL', 'CL-AR', 'Temuco', 'yes', 'SCQP', 'ZCO', '', '', 'https://en.wikipedia.org/wiki/La_Araucan%C3%ADa_International_Airport', ''),
('0', '湛江机场', 'aeroway=aerodrome
iata=ZHA
icao=ZGZJ
name=湛江机场
name:', '110° 21'' 31.59', '21° 12'' 53.495', ' ', '0', 'ZHA', 'ZGZJ', ' ', '110.3587768', '21.2148597', '32743', 'ZGZJ', 'medium_airport', 'Zhanjiang Xintang Airport', '21.214399', '110.358002', '125', 'AS', 'CN', 'CN-44', 'Zhanjiang (Xiashan)', 'yes', 'ZGZJ', 'ZHA', '', '', 'https://en.wikipedia.org/wiki/Zhanjiang_Airport', ''),
('0', 'Aéroport de Ziguinchor', 'aerodrome:type=public
aeroway=aerodrome
area=yes
ele=23
iata', '16° 16'' 59.639', '12° 33'' 23.460', 'public', '23', 'ZIG', 'GOGG', ' ', '-16.2832331', '12.5565166', '3122', 'GOGG', 'medium_airport', 'Ziguinchor Airport', '12.5556', '-16.281799', '75', 'AF', 'SN', 'SN-ZG', 'Ziguinchor', 'yes', 'GOGG', 'ZIG', '', '', 'https://en.wikipedia.org/wiki/Ziguinchor_Airport', 'Basse Casamance'),
('0', 'Aeropuerto Intl. de Ixtapa - Zihuatanejo', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Zihuata', '101° 27'' 48.83', '17° 36'' 9.706"', 'public', '8', 'ZIH', 'MMZH', ' ', '-101.4635642', '17.6026961', '4766', 'MMZH', 'medium_airport', 'Ixtapa Zihuatanejo International Airport', '17.601600647', '-101.460998535', '26', 'NA', 'MX', 'MX-GRO', 'Ixtapa', 'yes', 'MMZH', 'ZIH', '', '', 'https://en.wikipedia.org/wiki/Ixtapa-Zihuatanejo_International_Airport', ''),
('0', 'Aeropuerto Internacional Playa de Oro', 'aerodrome:type=public
aeroway=aerodrome
city_served=Manzanil', '104° 33'' 23.12', '19° 8'' 41.127"', 'public', '9', 'ZLO', 'MMZO', ' ', '-104.5564236', '19.1447575', '4768', 'MMZO', 'medium_airport', 'Playa De Oro International Airport', '19.1448001862', '-104.558998108', '30', 'NA', 'MX', 'MX-COL', 'Manzanillo', 'yes', 'MMZO', 'ZLO', '', 'http://manzanillo.aeropuertosgap.com.mx/index.php?lang=eng', 'https://en.wikipedia.org/wiki/Playa_de_Oro_International_Airport', ''),
('0', 'Aeródromo Cañal Bajo - Carlos Hott Siebert', 'aerodrome:type=public
aeroway=aerodrome
closest_town=Osorno,', '73° 3'' 36.840"', '40° 36'' 41.211', 'public', '57', 'ZOS', 'SCJO', ' ', '-73.0602332', '-40.6114476', '6027', 'SCJO', 'medium_airport', 'Cañal Bajo Carlos - Hott Siebert Airport', '-40.611198', '-73.060997', '187', 'SA', 'CL', 'CL-LL', 'Osorno', 'yes', 'SCJO', 'ZOS', '', '', 'https://en.wikipedia.org/wiki/Canal_Bajo_Carlos_Hott_Siebert_Airport', ''),
('0', 'Queenstown Airport', 'aeroway=aerodrome
ele=357
iata=ZQN
icao=NZQN
name=Queenstown', '168° 44'' 47.81', '45° 1'' 14.190"', ' ', '357', 'ZQN', 'NZQN', ' ', '168.7466139', '-45.0206084', '5051', 'NZQN', 'medium_airport', 'Queenstown International Airport', '-45.021099', '168.738998', '1171', 'OC', 'NZ', 'NZ-OTA', 'Queenstown', 'yes', 'NZQN', 'ZQN', '', '', 'https://en.wikipedia.org/wiki/Queenstown_International_Airport', '');
